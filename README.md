# Predicate Abstraction For RDMA Programs #

**Building the project without Eclipse:**

First build the project using ANT and the build file build.xml

Create the JAR by running ANT on createJar.xml

**Command line arguments:**

-h : show help

-ir: relative path of input program source file

-pr: relative path of predicate file

-l: treat rdma statements as synchronous statements

-r: treat rdma statements as asynchronous statements (Default)

-acc: Accumulate all the sets of the same read variable to one set per process (results in less predicates but lower precision, only for remote case). Default: false

-ucs: The maximum cube size used in the search of predicates that can't hold at the same time. Default: 3

-cs: The maximum cube size used in the search of the cubes for the Weakest Pre-Condition and Strongest Post-Condition. Default: 3

-ct: If set, fender checks if a found error trace in a boolean program is spurious (Default method: WP)

-WP: If ct is set, use WP for the error trace check method (Default)

-Interp: If ct is set, use the interpolation method for the error trace check.

-NoFL: If set, there will be no static analysis for remote statements in loops and they will all be set inactive non-deterministically after their execution

-NoStAn: If set, the dataflow to determine what remote statements are active at a point in the boolean program is not conducted. Every remote statement might be considered active at any point in the program if the flag is not set.

-NoExtrap: Run without boolean program extrapolation.


**Examples:**

local: -ir ./test/ticket/ticket.ir -pr ./test/ticket/ticket.preds -l

remote -ir ./test/ticket/ticket.ir -pr ./test/ticket/ticket.preds -r