package flowgraph;

import org.antlr.v4.runtime.tree.ParseTree;

public class FlushStatementNode extends FlowGraphNode{
	
	int srcProcNr;
	
	int dstProcNr;
	
	public FlushStatementNode(FlowGraph flowGraph, ParseTree context, int srcProcNr, int dstProcNr){
		super(flowGraph, context);
		this.srcProcNr = srcProcNr;
		this.dstProcNr = dstProcNr;
	}
	
	public int getSrcProcNr(){
		return srcProcNr;
	}
	
	public int getDstProcNr(){
		return dstProcNr;
	}
}
