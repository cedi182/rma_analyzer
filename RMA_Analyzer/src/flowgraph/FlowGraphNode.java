package flowgraph;

import java.util.HashSet;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import blp.RdmaPredicate;

public class FlowGraphNode {
	
	Set<RdmaPredicate> inSet;
	
	Set<FlowGraphNode> successors;
	
	FlowGraph flowGraph;
	
	ParseTree context;
	
	public FlowGraphNode(FlowGraph flowGraph, ParseTree context){
		inSet = new HashSet<RdmaPredicate>();
		successors = new HashSet<FlowGraphNode>();
		this.flowGraph = flowGraph;
		this.context = context;
	}
	
	public void setSuccessor(FlowGraphNode node){
		successors.add(node);
	}
	
	public Set<RdmaPredicate> getInSet(){
		return inSet;
	}
	
	public Set<FlowGraphNode> getSuccessors(){
		return successors;
	}
	
	public ParseTree getContext(){
		return context;
	}

}
