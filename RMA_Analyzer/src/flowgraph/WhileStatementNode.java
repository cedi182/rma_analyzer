package flowgraph;

import org.antlr.v4.runtime.tree.ParseTree;

public class WhileStatementNode extends FlowGraphNode{

	BlockNode loopBlock;
	
	public WhileStatementNode(FlowGraph flowGraph, ParseTree context){
		super(flowGraph, context);
	}
	
	public void setBlock(BlockNode block){
		loopBlock = block;
	}
	
	public BlockNode getBlock(){
		return loopBlock;
	}
	
	public void setSuccessor(FlowGraphNode node){
		successors.add(node);
		loopBlock.setSuccessor(node);
	}
	
	public BlockNode getLoopBlock(){
		return loopBlock;
	}
}
