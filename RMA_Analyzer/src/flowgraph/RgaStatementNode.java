package flowgraph;

import org.antlr.v4.runtime.tree.ParseTree;

import blp.RgaPredicate;

public class RgaStatementNode extends FlowGraphNode{
	RgaPredicate pred;
	
	public RgaStatementNode(FlowGraph flowGraph, ParseTree context, RgaPredicate pred){
		super(flowGraph, context);
		this.pred = pred;
	}
	
	public RgaPredicate getPred(){
		return pred;
	}
}
