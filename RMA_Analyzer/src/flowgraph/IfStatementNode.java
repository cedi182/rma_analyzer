package flowgraph;

import org.antlr.v4.runtime.tree.ParseTree;

public class IfStatementNode extends FlowGraphNode{
	BlockNode thenBlock;
	
	BlockNode elseBlock;
	
	public IfStatementNode(FlowGraph flowGraph, ParseTree context){
		super(flowGraph, context);
	}
	
	public void setThenBlock(BlockNode block){
		thenBlock = block;
	}
	
	public void setElseBlock(BlockNode block){
		elseBlock = block;
	}
	
	public void setSuccessor(FlowGraphNode node){
		successors.add(node);
		thenBlock.setSuccessor(node);
		if(elseBlock != null){
			elseBlock.setSuccessor(node);
		}
	}
	
	public BlockNode getThenBlock(){
		return thenBlock;
	}
	
	public BlockNode getelseBlock(){
		return elseBlock;
	}
}
