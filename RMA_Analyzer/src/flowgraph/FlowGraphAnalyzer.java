package flowgraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLParser.CasContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.RgaContext;
import blp.CasPredicate;
import blp.Flush;
import blp.GetPredicate;
import blp.PutPredicate;
import blp.RdmaPredicate;
import blp.RgaPredicate;

/** class to perform the data flow analysis on created flowgraph*/
public class FlowGraphAnalyzer {
	
	List<FlowGraphNode> visitList;
	
	Map<Integer,FlowGraph> graphSet;
	
	ParseTree program;
	
	Map<PutContext, PutPredicate> putPreds;
	
	Map<GetContext, GetPredicate> getPreds;
	
	Map<CasContext, CasPredicate> casPreds;
	
	Map<RgaContext, RgaPredicate> rgaPreds;
	
	Set<Flush> flushes;
	
	FlowGraphBuilder graphBuilder;
	
	boolean noStaticAnalysis;
	
	public FlowGraphAnalyzer(ParseTree program, Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds, Map<CasContext, CasPredicate> casPreds, Map<RgaContext, RgaPredicate> rgaPreds, Set<Flush> flushes, boolean noStaticAnalysis){
		this.program = program;
		this.putPreds = putPreds;
		this.getPreds = getPreds;
		this.casPreds = casPreds;
		this.rgaPreds = rgaPreds;
		this.visitList = new ArrayList<FlowGraphNode>();
		this.graphBuilder = new FlowGraphBuilder(putPreds, getPreds, casPreds, rgaPreds, flushes);
		// build and extract the flow graph for each process
		this.graphBuilder.visit(program);
		this.graphSet = this.graphBuilder.getFlowGraphSet();
		this.flushes = flushes;
		this.noStaticAnalysis = noStaticAnalysis;
	}
	
	public Map<Integer, FlowGraph> getGraphs(){
		return graphSet;
	}
	
	public void analyzeGraphs(){
		graphLoop:for(FlowGraph graph: graphSet.values()){
			// analyze each graph
			boolean changed = true;
			// iterate through the flow graph until we have a fix point
			while(changed){
				changed = false;
				Set<FlowGraphNode> visitedSet = new HashSet<FlowGraphNode>();
				if(graph.getEntryPoint() == null){
					continue graphLoop;
				}
				// add the entry point to the visitList and start flowing through the graph
				visitList.add(graph.getEntryPoint());
				while(visitList.size() != 0){
					FlowGraphNode node = visitList.remove(0);
					Set<RdmaPredicate> workSet = new HashSet<RdmaPredicate>(node.getInSet());
					if(node instanceof BlockNode){
						// just propagate the set if the block is empty, do nothing else
						BlockNode blockNode = (BlockNode) node;
						if(blockNode.getStatements().size() == 0){
							for(FlowGraphNode successor: blockNode.getSuccessors()){
								changed = changed | successor.getInSet().addAll(workSet);
								if(!visitedSet.contains(successor)){
									// we visit the successor if we haven't visited it yet
									visitedSet.add(successor);
									visitList.add(successor);									
								}
							}
						} else {
							FlowGraphNode successor = blockNode.getStatements().get(0);
							changed = changed | successor.getInSet().addAll(workSet);
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						}
					} else if(node instanceof CasStatementNode){
						CasStatementNode casNode = (CasStatementNode) node;
						workSet.add(casNode.getPred());
						for(FlowGraphNode successor: casNode.getSuccessors()){
							changed = changed | successor.getInSet().addAll(workSet);
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						}
					} else if(node instanceof FlushStatementNode){
						FlushStatementNode flushNode = (FlushStatementNode) node;
						Iterator<RdmaPredicate> it = workSet.iterator();
						while(it.hasNext()){
							RdmaPredicate pred = it.next();
							if(pred instanceof PutPredicate){
								PutPredicate putPred = (PutPredicate) pred;
								if(flushNode.getSrcProcNr() == putPred.getSrcProcNr() && flushNode.getDstProcNr() == putPred.getDstProcNr()){
									// we remove the statement because it's flushed
									it.remove();
								}
							} else if(pred instanceof GetPredicate){
								GetPredicate getPred = (GetPredicate) pred;
								if(flushNode.getSrcProcNr() == getPred.getSrcProcNr() && flushNode.getDstProcNr() == getPred.getDstProcNr()){
									// we remove the statement because it's flushed
									it.remove();
								}
							} else if(pred instanceof CasPredicate){
								CasPredicate casPred = (CasPredicate) pred;
								if(flushNode.getSrcProcNr() == casPred.getSrcProcNr() && flushNode.getDstProcNr() == casPred.getDstProcNr()){
									// we remove the statement because it's flushed
									it.remove();
								}
							} else if(pred instanceof RgaPredicate){
								RgaPredicate rgaPred = (RgaPredicate) pred;
								if(flushNode.getSrcProcNr() == rgaPred.getSrcProcNr() && flushNode.getDstProcNr() == rgaPred.getDstProcNr()){
									// we remove the statement because it's flushed
									it.remove();
								}
							}
						}
						for(FlowGraphNode successor: flushNode.getSuccessors()){
							changed = changed | successor.getInSet().addAll(workSet);
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						}
					} else if(node instanceof GetStatementNode){
						GetStatementNode getNode = (GetStatementNode) node;
						workSet.add(getNode.getPred());
						for(FlowGraphNode successor: getNode.getSuccessors()){
							changed = changed | successor.getInSet().addAll(workSet);
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						}
					} else if(node instanceof IfStatementNode){
						// propagate the state to the then and else block if it exists
						IfStatementNode ifNode = (IfStatementNode) node;
						// then block
						FlowGraphNode successor = ifNode.getThenBlock();
						changed = changed | successor.getInSet().addAll(workSet);
						if(!visitedSet.contains(successor)){
							// we visit the successor if we haven't visited it yet
							visitedSet.add(successor);
							visitList.add(successor);									
						}
						// else block
						successor = ifNode.getelseBlock();
						if(successor != null){
							changed = changed | successor.getInSet().addAll(workSet);
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						} else {
							// propagate directly to the successors because we might not take the then branch
							for(FlowGraphNode successorNode: ifNode.getSuccessors()){
								changed = changed | successorNode.getInSet().addAll(workSet);
								if(!visitedSet.contains(successorNode)){
									// we visit the successor if we haven't visited it yet
									visitedSet.add(successorNode);
									visitList.add(successorNode);									
								}
							}
						}
					} else if(node instanceof PutStatementNode){
						PutStatementNode putNode = (PutStatementNode) node;
						workSet.add(putNode.getPred());
						for(FlowGraphNode successor: putNode.getSuccessors()){
							changed = changed | successor.getInSet().addAll(workSet);
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						}
					} else if(node instanceof RgaStatementNode){
						RgaStatementNode rgaNode = (RgaStatementNode) node;
						workSet.add(rgaNode.getPred());
						for(FlowGraphNode successor: rgaNode.getSuccessors()){
							changed = changed | successor.getInSet().addAll(workSet);
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						}
					} else if(node instanceof WhileStatementNode){
						// propagate the state to the loop block
						WhileStatementNode whileNode = (WhileStatementNode) node;
						FlowGraphNode successor = whileNode.getLoopBlock();
						changed = changed | successor.getInSet().addAll(workSet);
						if(!visitedSet.contains(successor)){
							// we visit the successor if we haven't visited it yet
							visitedSet.add(successor);
							visitList.add(successor);									
						}
						// propagate directly to the successors because we might not take the loop block
						for(FlowGraphNode successorNode: whileNode.getSuccessors()){
							changed = changed | successorNode.getInSet().addAll(workSet);
							if(!visitedSet.contains(successorNode)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successorNode);
								visitList.add(successorNode);									
							}
						}
					} else{
						// we just have a normal flowGraphNode
						// just propagate the state
						for(FlowGraphNode successor: node.getSuccessors()){
							changed = changed | successor.getInSet().addAll(workSet);
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						}
					}
				}
			}
			// now we go over the graph again and collect the sets to a mapping
			visitList.add(graph.getEntryPoint());
			Set<FlowGraphNode> visitedSet = new HashSet<FlowGraphNode>();
			visitedSet.add(graph.getEntryPoint());
			Map<ParseTree, Set<RdmaPredicate>> statementMap = new HashMap<ParseTree, Set<RdmaPredicate>>();
			while(visitList.size() != 0){
				FlowGraphNode node = visitList.remove(0);
				
				Set<RdmaPredicate> workSet = new HashSet<RdmaPredicate>(node.getInSet());
				statementMap.put(node.getContext(), workSet);
				graph.joinActivePreds(workSet);
				if(node instanceof BlockNode){
					BlockNode blockNode = (BlockNode) node;
					if(blockNode.getStatements().size() == 0){
						for(FlowGraphNode successor: blockNode.getSuccessors()){
							if(!visitedSet.contains(successor)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successor);
								visitList.add(successor);									
							}
						}
					} else {
						FlowGraphNode successor = blockNode.getStatements().get(0);
						if(!visitedSet.contains(successor)){
							// we visit the successor if we haven't visited it yet
							visitedSet.add(successor);
							visitList.add(successor);									
						}
					}
				} else if(node instanceof IfStatementNode){
					IfStatementNode ifNode = (IfStatementNode) node;
					// then block
					FlowGraphNode successor = ifNode.getThenBlock();
					if(!visitedSet.contains(successor)){
						// we visit the successor if we haven't visited it yet
						visitedSet.add(successor);
						visitList.add(successor);									
					}
					// else block
					successor = ifNode.getelseBlock();
					if(successor != null){
						if(!visitedSet.contains(successor)){
							// we visit the successor if we haven't visited it yet
							visitedSet.add(successor);
							visitList.add(successor);									
						}
					} else {
						for(FlowGraphNode successorNode: ifNode.getSuccessors()){
							if(!visitedSet.contains(successorNode)){
								// we visit the successor if we haven't visited it yet
								visitedSet.add(successorNode);
								visitList.add(successorNode);									
							}
						}
					}
				} else if(node instanceof WhileStatementNode){
					WhileStatementNode whileNode = (WhileStatementNode) node;
					FlowGraphNode successor = whileNode.getLoopBlock();
					if(!visitedSet.contains(successor)){
						// we visit the successor if we haven't visited it yet
						visitedSet.add(successor);
						visitList.add(successor);									
					}
					for(FlowGraphNode successorNode: whileNode.getSuccessors()){
						if(!visitedSet.contains(successorNode)){
							// we visit the successor if we haven't visited it yet
							visitedSet.add(successorNode);
							visitList.add(successorNode);									
						}
					}
				} else{
					// we just have ordinary flow
					for(FlowGraphNode successor: node.getSuccessors()){
						if(!visitedSet.contains(successor)){
							// we visit the successor if we haven't visited it yet
							visitedSet.add(successor);
							visitList.add(successor);									
						}
					}
				}
			}
			graph.setStatementMap(statementMap);
		}
		// now add all the active predicates of the other processes to the map
		for(FlowGraph graph: graphSet.values()){
			for(Set<RdmaPredicate> set: graph.getStatementMap().values()){
				for(FlowGraph addGraph: graphSet.values()){
					if(graph != addGraph){
						set.addAll(addGraph.getActivePreds());
					}
				}
			}
		}
		// add all the statements so that all might be active
		if(noStaticAnalysis){
			for(FlowGraph graph: graphSet.values()){
				for(Set<RdmaPredicate> set: graph.getStatementMap().values()){
					set.addAll(putPreds.values());
					set.addAll(getPreds.values());
					set.addAll(rgaPreds.values());
					set.addAll(casPreds.values());
				}
			}
		}
	}
}
