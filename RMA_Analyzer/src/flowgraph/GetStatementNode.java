package flowgraph;

import org.antlr.v4.runtime.tree.ParseTree;

import blp.GetPredicate;

public class GetStatementNode extends FlowGraphNode{
	GetPredicate pred;
	
	public GetStatementNode(FlowGraph flowGraph, ParseTree context, GetPredicate pred){
		super(flowGraph, context);
		this.pred = pred;
	}
	
	public GetPredicate getPred(){
		return pred;
	}
}
