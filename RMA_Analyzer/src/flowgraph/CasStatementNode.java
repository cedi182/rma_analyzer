package flowgraph;

import org.antlr.v4.runtime.tree.ParseTree;

import blp.CasPredicate;

public class CasStatementNode extends FlowGraphNode{
	CasPredicate pred;
	
	public CasStatementNode(FlowGraph flowGraph, ParseTree context, CasPredicate pred){
		super(flowGraph, context);
		this.pred = pred;
	}
	
	public CasPredicate getPred(){
		return pred;
	}
}
