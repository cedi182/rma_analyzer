package flowgraph;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import antlr.TLBaseVisitor;
import antlr.TLParser.AssertAlwaysContext;
import antlr.TLParser.AssertFinalContext;
import antlr.TLParser.AssignmentContext;
import antlr.TLParser.AssumptionContext;
import antlr.TLParser.AtomicSectionContext;
import antlr.TLParser.BlockContext;
import antlr.TLParser.CasContext;
import antlr.TLParser.FlushContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.IfStatementContext;
import antlr.TLParser.LoadContext;
import antlr.TLParser.ProcessContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.RgaContext;
import antlr.TLParser.StatementContext;
import antlr.TLParser.StoreContext;
import antlr.TLParser.WhileStatementContext;
import blp.CasPredicate;
import blp.Flush;
import blp.GetPredicate;
import blp.PutPredicate;
import blp.RgaPredicate;

/** class that builds flow graph of a given program*/
public class FlowGraphBuilder extends TLBaseVisitor<FlowGraphNode>{
	
	Map<Integer, FlowGraph> flowGraphSet;
	
	FlowGraph currentFlowGraph;
	
	Map<PutContext, PutPredicate> putPreds;
	
	Map<GetContext, GetPredicate> getPreds;
	
	Map<CasContext, CasPredicate> casPreds;
	
	Map<RgaContext, RgaPredicate> rgaPreds;
	
	Set<Flush> flushes;
	
	public FlowGraphBuilder(Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds, Map<CasContext, CasPredicate> casPreds, Map<RgaContext, RgaPredicate> rgaPreds, Set<Flush> flushes){
		flowGraphSet = new HashMap<Integer,FlowGraph>();
		this.putPreds = putPreds;
		this.getPreds = getPreds;
		this.casPreds = casPreds;
		this.rgaPreds = rgaPreds;
		this.flushes = flushes;
	}
	
	public Map<Integer,FlowGraph> getFlowGraphSet(){
		return flowGraphSet;
	}
	
	@Override
	public FlowGraphNode visitProcess(ProcessContext ctx){
		boolean first = true;
		FlowGraphNode lastNode = null;
		FlowGraphNode currentNode = null;
		FlowGraph flowGraph = new FlowGraph(Integer.parseInt(ctx.Number().getText()));
		flowGraphSet.put(Integer.parseInt(ctx.Number().getText()), flowGraph);
		currentFlowGraph = flowGraph;
		for(StatementContext statement: ctx.statement()){
			lastNode = currentNode;
			currentNode = visit(statement);
			if(first){
				first = false;
				flowGraph.setEntryPoint(currentNode);
			} else {
				lastNode.setSuccessor(currentNode);
			}
		}
		return null;
	}
	
	@Override
	public FlowGraphNode visitIfStatement(IfStatementContext ctx){
		IfStatementNode ifStatement = new IfStatementNode(currentFlowGraph, ctx);
		
		ifStatement.setThenBlock((BlockNode)visit(ctx.getChild(3)));
		
		if(ctx.children.size() == 7){
			// there is a else block
			ifStatement.setElseBlock((BlockNode)visit(ctx.getChild(5)));
		}
		return ifStatement;
	}
	
	@Override
	public FlowGraphNode visitWhileStatement(WhileStatementContext ctx){
		WhileStatementNode whileStatement = new WhileStatementNode(currentFlowGraph, ctx);
		whileStatement.setBlock((BlockNode) visit(ctx.getChild(3)));
		// set the statement itself to its successor to simulate loop behavior
		whileStatement.getBlock().setSuccessor(whileStatement);
		return whileStatement;
	}
	
	@Override
	public FlowGraphNode visitBlock(BlockContext ctx){
		BlockNode blockNode = new BlockNode(currentFlowGraph, ctx);
		boolean first = true;
		FlowGraphNode lastNode = null;
		FlowGraphNode currentNode = null;
		for(StatementContext statement: ctx.statement()){
			lastNode = currentNode;
			currentNode = visit(statement);
			blockNode.addStatement(currentNode);
			if(first){
				first = false;
			} else {
				lastNode.setSuccessor(currentNode);
			}
		}
		return blockNode;
	}
	
	public FlowGraphNode visitStatement(StatementContext ctx){
		return visit(ctx.getChild(0));
	}
	
	@Override
	public FlowGraphNode visitAtomicSection(AtomicSectionContext ctx){
		BlockNode blockNode = new BlockNode(currentFlowGraph, ctx);
		boolean first = true;
		FlowGraphNode lastNode = null;
		FlowGraphNode currentNode = null;
		for(StatementContext statement: ctx.statement()){
			lastNode = currentNode;
			currentNode = visit(statement);
			blockNode.addStatement(currentNode);
			if(first){
				first = false;
			} else {
				lastNode.setSuccessor(currentNode);
			}
		}
		return blockNode;
	}
	
	@Override
	public FlowGraphNode visitLoad(LoadContext ctx){
		FlowGraphNode node = new FlowGraphNode(currentFlowGraph, ctx);
		return node;
	}
	
	@Override
	public FlowGraphNode visitStore(StoreContext ctx){
		FlowGraphNode node = new FlowGraphNode(currentFlowGraph, ctx);
		return node;
	}
	
	@Override
	public FlowGraphNode visitAssignment(AssignmentContext ctx){
		FlowGraphNode node = new FlowGraphNode(currentFlowGraph, ctx);
		return node;
	}
	
	@Override
	public FlowGraphNode visitAssumption(AssumptionContext ctx){
		FlowGraphNode node = new FlowGraphNode(currentFlowGraph, ctx);
		return node;
	}
	
	@Override
	public FlowGraphNode visitAssertAlways(AssertAlwaysContext ctx){
		FlowGraphNode node = new FlowGraphNode(currentFlowGraph, ctx);
		return node;
	}
	
	@Override
	public FlowGraphNode visitAssertFinal(AssertFinalContext ctx){
		FlowGraphNode node = new FlowGraphNode(currentFlowGraph, ctx);
		return node;
	}
	
	@Override
	public FlowGraphNode visitFlush(FlushContext ctx){
		for(Flush f: flushes){
			if(f.getContext().equals(ctx)){
				// we remove the flush here because we tried to verify without it
				FlowGraphNode node = new FlowGraphNode(currentFlowGraph, ctx);
				return node;
			}
		}
		// the flush should really be executed
		int srcProcNr = currentFlowGraph.getProcNr();
		int dstProcNr = Integer.parseInt(ctx.Number().getText());
		FlushStatementNode node = new FlushStatementNode(currentFlowGraph, ctx, srcProcNr, dstProcNr);
		return node;
	}
	
	@Override
	public FlowGraphNode visitPut(PutContext ctx){
		PutStatementNode node = new PutStatementNode(currentFlowGraph, ctx, putPreds.get(ctx));
		return node;
	}
	
	@Override
	public FlowGraphNode visitGet(GetContext ctx){
		GetStatementNode node = new GetStatementNode(currentFlowGraph, ctx, getPreds.get(ctx));
		return node;
	}
	
	@Override
	public FlowGraphNode visitCas(CasContext ctx){
		CasStatementNode node = new CasStatementNode(currentFlowGraph, ctx, casPreds.get(ctx));
		return node;
	}
	
	@Override
	public FlowGraphNode visitRga(RgaContext ctx){
		RgaStatementNode node = new RgaStatementNode(currentFlowGraph, ctx, rgaPreds.get(ctx));
		return node;
	}
}
