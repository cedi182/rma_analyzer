package flowgraph;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;

public class BlockNode extends FlowGraphNode{
	List<FlowGraphNode> statements;
	
	public BlockNode(FlowGraph flowGraph, ParseTree context){
		super(flowGraph, context);
		statements = new ArrayList<FlowGraphNode>(); 
	}
	
	public void addStatement(FlowGraphNode statement){
		statements.add(statement);
	}
	
	@Override
	public void setSuccessor(FlowGraphNode node){
		successors.add(node);
		if(statements.size() != 0){
			statements.get(statements.size() - 1).setSuccessor(node);
		}
	}
	
	public List<FlowGraphNode> getStatements(){
		return statements;
	}
}
