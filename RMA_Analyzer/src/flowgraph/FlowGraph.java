package flowgraph;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import blp.RdmaPredicate;

public class FlowGraph {

	/** the process number of this flowgraph*/
	int procNr;
	
	Map<ParseTree, Set<RdmaPredicate>> statementMap;
	
	/** the entry point of the flowgraph*/
	FlowGraphNode entryPoint;
	
	/** the union of all the active rdms predicates in this flowgraph */
	Set<RdmaPredicate> activePreds;
	
	public FlowGraph(int procNr){
		this.procNr = procNr;
		this.activePreds = new HashSet<RdmaPredicate>();
	}
	
	public void setEntryPoint(FlowGraphNode entry){
		entryPoint = entry;
	}
	
	public void joinActivePreds(Set<RdmaPredicate> pred){
		activePreds.addAll(pred);
	}
	
	public int getProcNr(){
		return procNr;
	}
	
	public FlowGraphNode getEntryPoint(){
		return entryPoint;
	}
	
	public void setStatementMap(Map<ParseTree, Set<RdmaPredicate>> statementMap){
		this.statementMap = statementMap;
	}
	
	public Map<ParseTree, Set<RdmaPredicate>> getStatementMap(){
		return statementMap;
	}
	
	public Set<RdmaPredicate> getActivePreds(){
		return activePreds;
	}
}
