package flowgraph;

import org.antlr.v4.runtime.tree.ParseTree;

import blp.PutPredicate;

public class PutStatementNode extends FlowGraphNode{
	PutPredicate pred;
	
	public PutStatementNode(FlowGraph flowGraph, ParseTree context, PutPredicate pred){
		super(flowGraph, context);
		this.pred = pred;
	}
	
	public PutPredicate getPred(){
		return pred;
	}
}
