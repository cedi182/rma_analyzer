package blp;

import java.util.Set;

/** A class used to cache found cubes for a certain predicate for later reuse */
public class CubeCache {
	
	String predString;
	
	Set<Predicate> addPreds;
	
	Set<Predicate> addNotPreds;
	
	public CubeCache(String predString, Set<Predicate> addPreds, Set<Predicate> addNotPreds){
		this.addPreds = addPreds;
		this.addNotPreds = addNotPreds;
		this.predString = predString;
	}
	
	public String getPredString(){
		return predString;
	}
	
	public Set<Predicate> getAddPreds(){
		return addPreds;
	}
	
	public Set<Predicate> getAddNotPreds(){
		return addNotPreds;
	}
	
	public boolean equals(Object other){
		if(! (other instanceof CubeCache)){
			return false;
		}
		CubeCache otherCubeCache = (CubeCache) other;
		
		return (predString.equals(otherCubeCache.getPredString()) && addPreds.equals(otherCubeCache.getAddPreds()) && addNotPreds.equals(otherCubeCache.getAddNotPreds()));
	}
	
	public int hashCode(){
		return predString.hashCode() + addPreds.hashCode() + addNotPreds.hashCode();
	}
}
