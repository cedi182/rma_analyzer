package blp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLBaseVisitor;
import antlr.TLParser.AssertAlwaysContext;
import antlr.TLParser.AssertFinalContext;
import antlr.TLParser.CasContext;
import antlr.TLParser.FlushContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.ProcessContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.RgaContext;
import antlr.TLParser.WhileStatementContext;

/** a visitor to extract all the rdma statements before building the boolean program */
public class RdmaStatementExtractVisitor extends TLBaseVisitor<Object>{

	Map<PutContext, PutPredicate> putPreds;
	
	Map<GetContext, GetPredicate> getPreds;
	
	Map<CasContext, CasPredicate> casPreds;
	
	Map<RgaContext, RgaPredicate> rgaPreds;
	
	Set<Predicate> preds;
	
	Set<Predicate> notPreds;
	
	Set<Flush> flushes;
	
	boolean accumulate;
	
	int curProcess = 0;
	
	int maxProcess = -1;
	
	int curId;
	
	boolean inLoop = false;
	
	List<String> assertionVars;
	
	public RdmaStatementExtractVisitor(int startId, Set<Predicate> preds, Set<Predicate> notPreds, boolean accumulate){
		this.preds = preds;
		this.notPreds = notPreds;
		this.accumulate = accumulate;
		putPreds = new HashMap<PutContext,PutPredicate>();
		getPreds = new HashMap<GetContext, GetPredicate>();
		casPreds = new HashMap<CasContext, CasPredicate>();
		rgaPreds = new HashMap<RgaContext, RgaPredicate>();
		flushes = new HashSet<Flush>();
		assertionVars = new ArrayList<String>();
		curId = startId;
	}
	
	public Map<PutContext, PutPredicate> getPutPreds(){
		return putPreds;
	}
	
	public Map<GetContext, GetPredicate> getGetPreds(){
		return getPreds;
	}
	
	public Map<CasContext, CasPredicate> getCasPreds(){
		return casPreds;
	}
	
	public Map<RgaContext, RgaPredicate> getRgaPreds(){
		return rgaPreds;
	}
	
	public Set<Flush> getFlushes(){
		return flushes;
	}
	
	public int getCurId(){
		return curId;
	}
	
	public int getMaxProcess(){
		return maxProcess;
	}
	
	public List<String> getAssertionVars(){
		return assertionVars;
	}
	
	@Override
	public Object visitWhileStatement(WhileStatementContext ctx) {
		// state that we are in a loop for the Rdma statements in the loop
		boolean setInLoop = false;
		if(!inLoop){
			inLoop = true;
			setInLoop = true;
		}
		
		super.visitWhileStatement(ctx);
		
		if(setInLoop){
			inLoop = false;
		}
		return null;
	}
	
	@Override
	public Object visitAssertFinal(AssertFinalContext ctx) {
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(3));
		assertionVars.addAll(varVis.getVars());
		return super.visitAssertFinal(ctx);
	}

	@Override
	public Object visitAssertAlways(AssertAlwaysContext ctx) {
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(3));
		assertionVars.addAll(varVis.getVars());
		return super.visitAssertAlways(ctx);
	}

	@Override
	public Object visitFlush(FlushContext ctx){
		int start = ctx.start.getStartIndex();
		int stop = ctx.stop.getStopIndex();
		int line = ctx.start.getLine();
		Flush flush = new Flush(start, stop, line, ctx);
		flushes.add(flush);
		return null;
	}
	
	
	@Override
	public Object visitPut(PutContext ctx) {
		
		int dstProcNr = Integer.parseInt(ctx.Number().getText());
        ParseTree writeVar = ctx.Identifier(0);
        ParseTree readVar = ctx.Identifier(1);
        if(accumulate){
	        Map<Predicate, Predicate> predMap = null;
	    	Set<Predicate> addPreds = null;
	    	Set<Predicate> addNotPreds = null;
	    	String setName = "";
	        
	        for(PutPredicate putPred: putPreds.values()){
	        	if(putPred.getReadVar().equals(readVar.getText()) && curProcess == putPred.getSrcProcNr()){
	        		// we already have the set
	        		predMap = putPred.getPredMap();
	        		addPreds = putPred.getAddPreds();
	        		addNotPreds = putPred.getAddNotPreds();
	        		setName = putPred.getSetName();
	        	}
	        }
	        if(predMap == null){
	        	for(GetPredicate getPred: getPreds.values()){
	        		if(getPred.getReadVar().equals(readVar.getText()) && curProcess == getPred.getSrcProcNr()){
		        		// we already have the set
		        		predMap = getPred.getPredMap();
		        		addPreds = getPred.getAddPreds();
		        		addNotPreds = getPred.getAddNotPreds();
		        		setName = getPred.getSetName();
		        	}
	        	}
	        }
	        PutPredicate pred;
	        if(predMap == null){
	        	pred = new PutPredicate(ctx, curId++, curProcess, dstProcNr, writeVar.getText(), readVar.getText(), preds, notPreds, true, inLoop);
        	} else {
        		pred = new PutPredicate(ctx, curId++, curProcess, dstProcNr, writeVar.getText(), readVar.getText(), preds, notPreds, predMap, addPreds, addNotPreds, setName, inLoop);
        	}
            curId = pred.getHighestId();
            putPreds.put(ctx,pred);
        } else {
        	PutPredicate pred = new PutPredicate(ctx, curId++, curProcess, dstProcNr, writeVar.getText(), readVar.getText(), preds, notPreds, false, inLoop);
            curId = pred.getHighestId();
            putPreds.put(ctx,pred);
        }
		return null;
	}

	@Override
	public Object visitProcess(ProcessContext ctx) {
		curProcess = Integer.parseInt(ctx.Number().getText());
		maxProcess = Math.max(maxProcess, curProcess);
		return super.visitProcess(ctx);
	}

	@Override
	public Object visitGet(GetContext ctx) {
		
		int dstProcNr = Integer.parseInt(ctx.Number().getText());
        ParseTree writeVar = ctx.Identifier(0);
        ParseTree readVar = ctx.Identifier(1);
        
        if(accumulate){
	        Map<Predicate, Predicate> predMap = null;
	    	Set<Predicate> addPreds = null;
	    	Set<Predicate> addNotPreds = null;
	    	String setName = "";
	        
	        for(PutPredicate putPred: putPreds.values()){
	        	if(putPred.getReadVar().equals(readVar.getText()) && curProcess == putPred.getSrcProcNr()){
	        		// we already have the set
	        		predMap = putPred.getPredMap();
	        		addPreds = putPred.getAddPreds();
	        		addNotPreds = putPred.getAddNotPreds();
	        		setName = putPred.getSetName();
	        	}
	        }
	        if(predMap == null){
	        	for(GetPredicate getPred: getPreds.values()){
	        		if(getPred.getReadVar().equals(readVar.getText()) && curProcess == getPred.getSrcProcNr()){
		        		// we already have the set
		        		predMap = getPred.getPredMap();
		        		addPreds = getPred.getAddPreds();
		        		addNotPreds = getPred.getAddNotPreds();
		        		setName = getPred.getSetName();
		        	}
	        	}
	        }
	        GetPredicate pred;
	        if(predMap == null){
	        	pred = new GetPredicate(ctx, curId++, curProcess, dstProcNr, writeVar.getText(), readVar.getText(), preds, notPreds, true, inLoop);
        	} else {
        		pred = new GetPredicate(ctx, curId++, curProcess, dstProcNr, writeVar.getText(), readVar.getText(), preds, notPreds, predMap, addPreds, addNotPreds, setName, inLoop);
        	}
            curId = pred.getHighestId();
            getPreds.put(ctx,pred);
        } else {
        	GetPredicate pred = new GetPredicate(ctx, curId++, curProcess, dstProcNr, writeVar.getText(), readVar.getText(), preds, notPreds, false, inLoop);
            curId = pred.getHighestId();
            getPreds.put(ctx, pred);
        }
        
		return null;
	}

	@Override
	public Object visitRga(RgaContext ctx) {
		//Identifier '=' Rga '(' atomicity ',' atomicity ',' atomicity ')' '(' Identifier ',' Number ',' Identifier ')'
		int dstProcNr = Integer.parseInt(ctx.Number().getText());
		ParseTree finalWriteVar = ctx.Identifier(0);
		ParseTree writeVar = ctx.Identifier(1);
		ParseTree accumulateVar  = ctx.Identifier(2);
		RgaPredicate pred = new RgaPredicate(finalWriteVar.getText(), accumulateVar.getText(), writeVar.getText(), ctx, curProcess, dstProcNr, curId, accumulate, preds, notPreds, inLoop);
		curId = pred.getHighestId();
		rgaPreds.put(ctx, pred);
		return null;
	}

	@Override
	public Object visitCas(CasContext ctx) {
		int dstProcNr = Integer.parseInt(ctx.Number().getText());
		ParseTree finalWriteVar = ctx.Identifier(0);
		ParseTree writeVar = ctx.Identifier(1);
		ParseTree compareVar = ctx.Identifier(2);
		ParseTree replaceVar = ctx.Identifier(3);
		CasPredicate pred = new CasPredicate(finalWriteVar.getText(), writeVar.getText(), compareVar.getText(), replaceVar.getText(), ctx, curProcess, dstProcNr, curId, preds, notPreds, inLoop);
		curId = pred.getHighestId();
		casPreds.put(ctx, pred);
		return null;
	}
}
