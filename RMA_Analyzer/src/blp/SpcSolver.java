package blp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import antlr.TLLexer;
import antlr.TLParser;


/** a class that is used to find cubes that are implied by the expression of an if statement or loop condition */
public class SpcSolver {
	
	Set<Predicate> predicates;
	
	Set<Predicate> notPredicates;
	
	TreeToZ3Visitor z3Visitor;
	
	Set<Predicate> unsatSet;
	
	Set<Predicate> validSet;
	
	Set<Set<Predicate>> unsatCubes;
	
	Map<CubeCache, List<Set<Set<Predicate>>>> cubeCacheMap;
	
	int maxCubeSize;
	
	public SpcSolver(Set<Predicate> preds, Set<Predicate> notPreds, Set<Predicate> unsatSet, Set<Predicate> validSet, Set<Set<Predicate>> unsatCubes, int cubeSize){
		predicates = preds;
		notPredicates = notPreds;
		z3Visitor = new TreeToZ3Visitor();
		this.unsatSet = unsatSet;
		this.validSet = validSet;
		this.unsatCubes = unsatCubes;
		this.maxCubeSize = cubeSize;
		this.cubeCacheMap = new HashMap<CubeCache, List<Set<Set<Predicate>>>>();
	}
	
	
	List<Set<Set<Predicate>>> computeSpc(String condition){
		TLLexer lexer = new TLLexer(new ANTLRInputStream(condition));
		TLParser parser = new TLParser(new CommonTokenStream(lexer));
		parser.setBuildParseTree(true);
		ParseTree tree = parser.parse();
		return computeSpc(tree);
	}
	
	
	List<Set<Set<Predicate>>> computeSpc(ParseTree condition){
		List<Set<Set<Predicate>>> result = new ArrayList<Set<Set<Predicate>>>();
		Set<Set<Predicate>> spcSet = new HashSet<Set<Predicate>>();
		Set<Set<Predicate>> notSpcSet = new HashSet<Set<Predicate>>();
		z3Visitor.reset();
		Context context = z3Visitor.getContext();
		// first we want to compute the condition as a z3 formula
		BoolExpr cond = (BoolExpr) z3Visitor.visit(condition);
		BoolExpr notCond = context.mkNot(cond);
		
		// check for cache
		CubeCache cubeCache;
		cubeCache = new CubeCache(cond.toString(), new HashSet<Predicate>(), new HashSet<Predicate>());
		if(cubeCacheMap.keySet().contains(cubeCache)){
			return cubeCacheMap.get(cubeCache);
		}
		Solver solver = context.mkSolver();
		
		Set<String> spcVars = z3Visitor.getContainedVars();
		//first we want to check whether the condition is even satisfiable
		solver.push();
		solver.add(notCond);	
		
		if(solver.check() == Status.UNSATISFIABLE){
			// the condition is always true -> valid
			spcSet.add(validSet);
			notSpcSet.add(unsatSet);
			result.add(spcSet);
			result.add(notSpcSet);
			return result;
		}
		solver.pop();
		solver.add(cond);	

		if(solver.check() == Status.UNSATISFIABLE){
			// the condition is always false -> not reachable
			spcSet.add(unsatSet);
			notSpcSet.add(validSet);
			result.add(spcSet);
			result.add(notSpcSet);
			return result;
		}
				
		List<Predicate> predList = new ArrayList<Predicate>();
		List<Predicate> notPredList = new ArrayList<Predicate>();
		predList.addAll(predicates);
		predList.addAll(notPredicates);
		Set<Predicate> predChoice = computeAffectedPreds(spcVars, predList, maxCubeSize);
		predList = new ArrayList<Predicate>(predChoice);
		notPredList = new ArrayList<Predicate>(predChoice);
		
		Set<Integer> containedSet = new HashSet<Integer>();
		Set<Integer> chosenSet = new HashSet<Integer>();
		boolean searchHigherDimension = true;
		
		Iterator<Predicate> it = predList.iterator();
		
		while(it.hasNext()){
			Predicate pred = it.next();
			// first check if the predicate contains a variable that is also in the wpc
			boolean contained = false;
			for(String var: pred.getContainedVars()){
				if(spcVars.contains(var)){
					contained = true;
					containedSet.add(pred.getId());
					break;
				}
			}
			if(! contained){
				continue;
			}
			if(pred.getMakeNot()){
				// we have a negative predicate
				solver.push();
				solver.add(context.mkNot((BoolExpr)z3Visitor.visit(pred.getTree())));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					spcSet.add(predSet);
					chosenSet.add(pred.getId());
					// also remove it from the not set because both can't be valid
					notPredList.remove(pred);
					it.remove();
				}
				solver.pop();
			} else {
				// we have a normal predicate
				solver.push();
				solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					spcSet.add(predSet);
					chosenSet.add(pred.getId());
					// also remove it from the not set because both can't be valid
					notPredList.remove(pred);
					it.remove();
				}
				solver.pop();
			}
		}
		if(chosenSet.equals(containedSet)){
			searchHigherDimension = false;
		}
		
		containedSet = new HashSet<Integer>();
		chosenSet = new HashSet<Integer>();
		boolean notSearchHigherDimension = true;
		
		Solver notSolver = context.mkSolver();
		notSolver.push();		
		notSolver.add(notCond);
		// now compute it for the notWpc
		it = notPredList.iterator();
		while(it.hasNext()){
			Predicate pred = it.next();
			// first check if the predicate contains a variable that is also in the wpc
			boolean contained = false;
			for(String var: pred.getContainedVars()){
				if(spcVars.contains(var)){
					contained = true;
					containedSet.add(pred.getId());
					break;
				}
			}
			if(! contained){
				continue;
			}
			if(pred.getMakeNot()){
				// we have a negative predicate
				notSolver.push();
				notSolver.add(context.mkNot((BoolExpr)z3Visitor.visit(pred.getTree())));
				if(notSolver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					notSpcSet.add(predSet);
					chosenSet.add(pred.getId());
					// also remove it from the set because both can't be valid
					predList.remove(pred);
					it.remove();
				}
				notSolver.pop();
			} else {
				// we have a normal predicate
				notSolver.push();
				notSolver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
				if(notSolver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					notSpcSet.add(predSet);
					chosenSet.add(pred.getId());
					// also remove it from the set because both can't be valid
					predList.remove(pred);
					it.remove();
				}
				notSolver.pop();
			}
		}
		if(chosenSet.equals(containedSet)){
			notSearchHigherDimension = false;
		}
		if(!searchHigherDimension && !notSearchHigherDimension || maxCubeSize == 1){
			result = new ArrayList<Set<Set<Predicate>>>();
			result.add(spcSet);
			result.add(notSpcSet);
			return result;
		}
		
		int spcMaxSize = getHighestCubeSize(predList);
		int notSpcMaxSize = getHighestCubeSize(notPredList);
		
		
		for(int i = 2; i <= maxCubeSize; i++){
			result = solveWpcHigherOrder(i, spcSet, predList, solver, notSpcSet, notPredList, notSolver, spcMaxSize, notSpcMaxSize, searchHigherDimension, notSearchHigherDimension);
		}
		
		cubeCacheMap.put(cubeCache, result);
		
		return result;
	}
	
	public int getHighestCubeSize(List<Predicate> predList){
		Set<Integer> idSet = new HashSet<Integer>();
		for(Predicate pred: predList){
			idSet.add(pred.getId());
		}
		return idSet.size();
	}
	
	
	public List<Set<Set<Predicate>>> solveWpcHigherOrder(int cubeOrder, Set<Set<Predicate>> partSol, List<Predicate> preds, Solver solver, Set<Set<Predicate>> notPartSol, List<Predicate> notPreds, Solver notSolver, int spcMaxSize, int notSpcMaxSize, boolean search, boolean notSearch){
		Context context = z3Visitor.getContext();
		Set<Set<Predicate>> checkedSet = new HashSet<Set<Predicate>>();
		Set<Set<Predicate>> notCheckedSet = new HashSet<Set<Predicate>>();
		int[] counters = new int[cubeOrder];
		for(int i = 0; i < cubeOrder; i++){
			counters[i] = Math.min(i, preds.size() - 1);
		}
		if(search && spcMaxSize >= cubeOrder){
			while(counters[0] < preds.size()){
				boolean checkSat = true;
				//add the predicates to the set and check if a part of the formula is already present in partSol
				Set<Predicate> predSet = new HashSet<Predicate>();
				Set<Integer> idSet = new HashSet<Integer>();
				for(int i=0; i < cubeOrder; i++){
					// remove the cases where a predicate and its negation is in the cube
					if(idSet.contains(preds.get(counters[i]).getId())){
						checkSat = false;
						break;
					}
					predSet.add(preds.get(counters[i]));
					idSet.add(preds.get(counters[i]).getId());
				}
				if(checkSat && predSet.size() < cubeOrder){
					// remove the cases where a predicate is contained 2 times in the cube
					checkSat = false;
				}
				if(checkSat && checkedSet.contains(predSet)){
					checkSat = false;
				}
				if(checkSat){
					for(Set<Predicate> unsatSet: unsatCubes){
						if(predSet.containsAll(unsatSet)){
							// our predSet contains a cube that is not satisfiable -> no need to check it 
							checkSat = false;
							break;
						}
					}
				}
				
				if(checkSat){
					for(Set<Predicate> partSolSet: partSol){
						// remove the case where the cube already contains a cube that is implied by the expression
						if(predSet.containsAll(partSolSet)){
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					// add the predicates to the solver
					solver.push();
					for(Predicate pred: predSet){
						if(pred.getMakeNot()){
							solver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
						} else {
							solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
						}
					}
					if(solver.check() == Status.UNSATISFIABLE){
						// predSet implies the weakest precondition -> add it to partSol
						partSol.add(predSet);
						// add it to the not checked set, so we won't check if we are looking for the not wpc
						notCheckedSet.add(predSet);
					}
					solver.pop();
				}
				//increment the counters
				counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
				if(counters[cubeOrder - 1] == preds.size()){
					int i = cubeOrder - 1;
					while(i >= 0){
						if(counters[i] == preds.size() && i != 0){
							counters[i] = 0;
							counters[i-1] = counters[i-1] + 1;
							i--;
						} else {
							break;
						}
					}
				}
				while(!checkCounters(counters)){
					// keep increasing the counters
					counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
					if(counters[cubeOrder - 1] == preds.size()){
						int i = cubeOrder - 1;
						while(i >= 0){
							if(counters[i] == preds.size() && i != 0){
								counters[i] = 0;
								counters[i-1] = counters[i-1] + 1;
								i--;
							} else {
								break;
							}
						}
					}
				}
				
			}
		}
		if(notSearch && notSpcMaxSize >= cubeOrder){
			counters = new int[cubeOrder];
			for(int i = 0; i < cubeOrder; i++){
				counters[i] = Math.min(i, notPreds.size() - 1);
			}
			while(counters[0] < notPreds.size()){
				boolean checkSat = true;
				//add the predicates to the set and check if a part of the formula is already present in partSol
				Set<Predicate> predSet = new HashSet<Predicate>();
				Set<Integer> idSet = new HashSet<Integer>();
				for(int i=0; i < cubeOrder; i++){
					// remove the cases where a predicate and its negation is in the cube
					if(idSet.contains(notPreds.get(counters[i]).getId())){
						checkSat = false;
						break;
					}
					predSet.add(notPreds.get(counters[i]));
					idSet.add(notPreds.get(counters[i]).getId());
				}
				if(checkSat && predSet.size() < cubeOrder){
					// remove the cases where a predicate is contained 2 times in the cube
					checkSat = false;
				}
				
				if(checkSat && notCheckedSet.contains(predSet)){
					checkSat = false;
				}
				if(checkSat){
					for(Set<Predicate> unsatSet: unsatCubes){
						if(predSet.containsAll(unsatSet)){
							// our predSet contains a cube that is not satisfiable -> no need to check it 
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					for(Set<Predicate> partSolSet: notPartSol){
						// remove the case where the cube already contains a cube that is implied by the expression
						if(predSet.containsAll(partSolSet)){
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					// add the predicates to the solver
					notSolver.push();
					for(Predicate pred: predSet){
						if(pred.getMakeNot()){
							notSolver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
						} else {
							notSolver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
						}
					}
					if(notSolver.check() == Status.UNSATISFIABLE){
						// predSet implies the weakest precondition -> add it to partSol
						notPartSol.add(predSet);
						// add it to the not checked set, so we won't check if we are looking for the not spc
						checkedSet.add(predSet);
					}
					notSolver.pop();
				}
				//increment the counters
				counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
				if(counters[cubeOrder - 1] == notPreds.size()){
					int i = cubeOrder - 1;
					while(i >= 0){
						if(counters[i] == notPreds.size() && i != 0){
							counters[i] = 0;
							counters[i-1] = counters[i-1] + 1;
							i--;
						} else {
							break;
						}
					}
				}
				while(!checkCounters(counters)){
					// keep increasing the counters
					counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
					if(counters[cubeOrder - 1] == notPreds.size()){
						int i = cubeOrder - 1;
						while(i >= 0){
							if(counters[i] == notPreds.size() && i != 0){
								counters[i] = 0;
								counters[i-1] = counters[i-1] + 1;
								i--;
							} else {
								break;
							}
						}
					}
				}
			}
		}
		List<Set<Set<Predicate>>> result = new ArrayList<Set<Set<Predicate>>>();
		result.add(partSol);
		result.add(notPartSol);
		return result;
	}
	
	
	Set<Set<Predicate>> computeSpc(String condition, boolean compNotSpc){
		TLLexer lexer = new TLLexer(new ANTLRInputStream(condition));
		TLParser parser = new TLParser(new CommonTokenStream(lexer));
		parser.setBuildParseTree(true);
		ParseTree tree = parser.parse();
		return computeSpc(tree, compNotSpc);
	}
	
	
	Set<Set<Predicate>> computeSpc(ParseTree condition, boolean compNotSpc){
		Set<Set<Predicate>> spcSet = new HashSet<Set<Predicate>>();
		Context context = z3Visitor.getContext();
		z3Visitor.reset();
		Solver solver = context.mkSolver();
		
		// first we want to compute the condition as a z3 formula
		BoolExpr cond = (BoolExpr) z3Visitor.visit(condition);
		BoolExpr notCond = context.mkNot(cond);
		Set<String> spcVars = z3Visitor.getContainedVars();
		
		//first we want to check whether the condition is even satisfiable
		solver.push();
		if(compNotSpc){
			solver.add(cond);
		} else {
			solver.add(notCond);	
		}
		
		if(solver.check() == Status.UNSATISFIABLE){
			// the condition is always false -> not reachable
			spcSet.add(validSet);
			return spcSet;
		}
		solver.pop();
		if(compNotSpc){
			solver.add(notCond);
		} else {
			solver.add(cond);	
		}

		if(solver.check() == Status.UNSATISFIABLE){
			// the condition is always false -> not reachable
			spcSet.add(unsatSet);
			return spcSet;
		}
				
		List<Predicate> predList = new ArrayList<Predicate>();
		predList.addAll(predicates);
		predList.addAll(notPredicates);
		Set<Predicate> predChoice = computeAffectedPreds(spcVars, predList,  maxCubeSize);
		predList = new ArrayList<Predicate>(predChoice);
		
		Set<Integer> containedSet = new HashSet<Integer>();
		Set<Integer> chosenSet = new HashSet<Integer>();
		
		Iterator<Predicate> it = predList.iterator();
		
		while(it.hasNext()){
			Predicate pred = it.next();
			// first check if the predicate contains a variable that is also in the spc
			boolean contained = false;
			for(String var: pred.getContainedVars()){
				if(spcVars.contains(var)){
					contained = true;
					containedSet.add(pred.getId());
					break;
				}
			}
			if(! contained){
				continue;
			}
			if(pred.getMakeNot()){
				// we have a negative predicate
				solver.push();
				solver.add(context.mkNot((BoolExpr)z3Visitor.visit(pred.getTree())));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					spcSet.add(predSet);
					chosenSet.add(pred.getId());
					it.remove();
				}
				solver.pop();
			} else {
				// we have a normal predicate
				solver.push();
				solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					spcSet.add(predSet);
					chosenSet.add(pred.getId());
					it.remove();
				}
				solver.pop();
			}
		}
		
		if(chosenSet.equals(containedSet)){
			return spcSet;
		}
		int spcMaxSize = getHighestCubeSize(predList);
		for(int i = 2; i <= maxCubeSize; i++){
			spcSet = solveWpcHigherOrder2(i, spcSet, predList, solver, spcMaxSize);
		}
		return spcSet;
	}
	
	
	public Set<Set<Predicate>> solveWpcHigherOrder2(int cubeOrder, Set<Set<Predicate>> partSol, List<Predicate> preds, Solver solver, int spcMaxSize){
		Context context = z3Visitor.getContext();
		int[] counters = new int[cubeOrder];
		Set<Set<Predicate>> checkedSet = new HashSet<Set<Predicate>>();
		if(spcMaxSize >= cubeOrder){
			while(counters[0] < preds.size()){
				boolean checkSat = true;
				//add the predicates to the set and check if a part of the formula is already present in partSol
				Set<Predicate> predSet = new HashSet<Predicate>();
				Set<Integer> idSet = new HashSet<Integer>();
				for(int i=0; i < cubeOrder; i++){
					// remove the cases where a predicate and its negation is in the cube
					if(idSet.contains(preds.get(counters[i]).getId())){
						checkSat = false;
						break;
					}
					predSet.add(preds.get(counters[i]));
					idSet.add(preds.get(counters[i]).getId());
				}
				if(checkSat && predSet.size() < cubeOrder){
					// remove the cases where a predicate is contained 2 times in the cube
					checkSat = false;
				}
				
				if(checkSat && checkedSet.contains(predSet)){
					// remove the case where we already checked the satisfiability -> case [1][2] results in same cube as [2][1]
					checkSat = false;
				}
				
				if(checkSat){
					for(Set<Predicate> unsatSet: unsatCubes){
						if(predSet.containsAll(unsatSet)){
							// our predSet contains a cube that is not satisfiable -> no need to check it 
							checkSat = false;
							break;
						}
					}
				}
								
				if(checkSat){
					for(Set<Predicate> partSolSet: partSol){
						// remove the case where the cube already contains a cube that is implied by the expression
						if(predSet.containsAll(partSolSet)){
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					// add the predicates to the solver
					solver.push();
					for(Predicate pred: predSet){
						if(pred.getMakeNot()){
							solver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
						} else {
							solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
						}
					}
					if(solver.check() == Status.UNSATISFIABLE){
						// predSet implies the weakest precondition -> add it to partSol
						partSol.add(predSet);
					}
					solver.pop();
					checkedSet.add(predSet);
				}
				
				
				//increment the counters
				counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
				if(counters[cubeOrder - 1] == preds.size()){
					int i = cubeOrder - 1;
					while(i >= 0){
						if(counters[i] == preds.size() && i != 0){
							counters[i] = 0;
							counters[i-1] = counters[i-1] + 1;
							i--;
						} else {
							break;
						}
					}
				}
				while(!checkCounters(counters)){
					// keep increasing the counters
					counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
					if(counters[cubeOrder - 1] == preds.size()){
						int i = cubeOrder - 1;
						while(i >= 0){
							if(counters[i] == preds.size() && i != 0){
								counters[i] = 0;
								counters[i-1] = counters[i-1] + 1;
								i--;
							} else {
								break;
							}
						}
					}
				}
				
			}
		}
		return partSol;
	}
	
	
	public Set<Predicate> computeAffectedPreds(Set<String> vars, List<Predicate> predicates, int cubeSize){
		Set<Predicate> result = new HashSet<Predicate>();
		
		if(cubeSize == 1){
			// only inlcude the predicates that directly contain the variable
			for(Predicate pred: predicates){
				for(String var: vars){
					if(pred.getContainedVars().contains(var)){
						result.add(pred);
						break;
					}
				}
			}
		} else {
			for(int i = 1; i < cubeSize; i++){
				result.addAll(computeAffectedPreds(vars, predicates, i));
			}
			Set<String> identifiers = new HashSet<String>();
			// first add all identifers that we have in predicates so far
			// no need to add the vars argument here as we already added those predicates in cubesize 1
			for(Predicate pred: result){
				identifiers.addAll(pred.getContainedVars());
			}
			// now add all the predicates that contain the identifiers
			for(Predicate pred: predicates){
				for(String var: identifiers){
					if(pred.getContainedVars().contains(var)){
						result.add(pred);
						break;
					}
				}
			}
		}
		return result;
	}
	
	/** returns true if no value in counters is the same, else false */
	public boolean checkCounters(int[] counters){
		Set<Integer> checkSet = new HashSet<Integer>();
		for(int i = 0; i < counters.length; i++){
			if(!checkSet.add(counters[i])){
				return false;
			}
		}
		return true;
	}
	
}
