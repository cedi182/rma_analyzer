package blp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLLexer;
import antlr.TLParser;
import antlr.TLParser.RgaContext;

public class RgaPredicate extends RdmaPredicate{
	
	PutPredicate putPred;
	
	GetPredicate getPred;
	
	String finalWriteVar;
	
	String accumulateVar;
	
	RgaContext rgaContext;
	
	String local1Name;
	Map<Predicate, Predicate> predMap1;
	Set<Predicate> addPreds1;
	Set<Predicate> addNotPreds1;
	
	String local2Name;
	Map<Predicate, Predicate> predMap2;
	Set<Predicate> addPreds2;
	Set<Predicate> addNotPreds2;
	
	public RgaPredicate(String finalWriteVar, String accumulateVar, String writeVar, RgaContext ctx, int srcProc, int dstProc, int id, boolean accumulate, Set<Predicate> preds, Set<Predicate> notPreds, boolean inLoop){
		this.finalWriteVar = finalWriteVar;
		this.writeVar = writeVar;
		this.readVar = "";
		this.accumulateVar = accumulateVar;
		this.rgaContext = ctx;
		this.srcProcNr = srcProc;
		this.dstProcNr = dstProc;
		this.id = id;
		this.inLoop = inLoop;
		this.ctx = ctx;
		highestId = id + 1;
		predMap = new HashMap<Predicate, Predicate>();
		
		
		// create predicates for t1
		predMap1 = new HashMap<Predicate, Predicate>();
		addPreds1 = new HashSet<Predicate>();
		addNotPreds1 = new HashSet<Predicate>();
		local1Name = "t1RGA" + id;
		VarVisitor  varVis = new VarVisitor();
		// transform each predicate
		for(Predicate pred: preds){
			// we only copy the predicates that contain the read var
			if(! pred.getContainedVars().contains(accumulateVar)){
				continue;
			}
			// rewrite the predicate for each var
			String predString = pred.getPredString();
			// rewrite the predicate for each var
			PredRewriteVisitor predRewriteVis = new PredRewriteVisitor(accumulateVar);
			TLLexer lexer = new TLLexer(new ANTLRInputStream(predString));
			TLParser parser = new TLParser(new CommonTokenStream(lexer));
			parser.setBuildParseTree(true);
			ParseTree tree = parser.parse();
			predRewriteVis.visit(tree);
			for(Integer pos: predRewriteVis.getSolution()){
				if(! pos.equals(0)){
					predString = predString.substring(0, pos) + predString.substring(pos, predString.length()).replaceFirst(accumulateVar, local1Name);
				} else {
					predString = predString.replaceFirst(accumulateVar, local1Name);
				}
			}
			lexer = new TLLexer(new ANTLRInputStream(predString));
			parser = new TLParser(new CommonTokenStream(lexer));
			parser.setBuildParseTree(true);
			tree = parser.parse();
			varVis.visit(tree);
			Predicate copyPred = new Predicate(predString, tree, varVis.getVars(), false, highestId);
			addPreds1.add(copyPred);
			highestId++;
			predMap1.put(pred, copyPred);
		}
		Set<Predicate> iterSet = new HashSet<Predicate>(predMap1.keySet());
		for(Predicate pred: iterSet){
			Predicate copyPred = predMap1.get(pred);
			Predicate notPred = new Predicate(copyPred.getPredString(), copyPred.getTree(), copyPred.getContainedVars(), true, copyPred.getId());
			addNotPreds1.add(notPred);
			// find the according not pred to store the not copy pred in the pred map
			Predicate notNormalPred = null;
			for(Predicate p: notPreds){
				if(pred.getId() == p.getId()){
					notNormalPred = p;
					break;
				}
			}
			predMap1.put(notNormalPred, notPred);
			addNotPreds1.add(notPred);
		}
		
		
		// create predicates for t2
		predMap2 = new HashMap<Predicate, Predicate>();
		local2Name = "t2RGA" + id;
		addPreds2 = new HashSet<Predicate>();
		addNotPreds2 = new HashSet<Predicate>();
		varVis = new VarVisitor();
		// transform each predicate
		for(Predicate pred: preds){
			// we only copy the predicates that contain the read var
			if(! pred.getContainedVars().contains(writeVar)){
				continue;
			}
			// rewrite the predicate for each var
			String predString = pred.getPredString();
			// rewrite the predicate for each var
			PredRewriteVisitor predRewriteVis = new PredRewriteVisitor(writeVar);
			TLLexer lexer = new TLLexer(new ANTLRInputStream(predString));
			TLParser parser = new TLParser(new CommonTokenStream(lexer));
			parser.setBuildParseTree(true);
			ParseTree tree = parser.parse();
			predRewriteVis.visit(tree);
			for(Integer pos: predRewriteVis.getSolution()){
				if(! pos.equals(0)){
					predString = predString.substring(0, pos) + predString.substring(pos, predString.length()).replaceFirst(writeVar, local2Name);
				} else {
					predString = predString.replaceFirst(writeVar, local2Name);
				}
			}
			lexer = new TLLexer(new ANTLRInputStream(predString));
			parser = new TLParser(new CommonTokenStream(lexer));
			parser.setBuildParseTree(true);
			tree = parser.parse();
			varVis.visit(tree);
			Predicate copyPred = new Predicate(predString, tree, varVis.getVars(), false, highestId);
			addPreds2.add(copyPred);
			highestId++;
			predMap2.put(pred, copyPred);
		}
		iterSet = new HashSet<Predicate>(predMap2.keySet());
		for(Predicate pred: iterSet){
			Predicate copyPred = predMap2.get(pred);
			Predicate notPred = new Predicate(copyPred.getPredString(), copyPred.getTree(), copyPred.getContainedVars(), true, copyPred.getId());
			addNotPreds2.add(notPred);
			// find the according not pred to store the not copy pred in the pred map
			Predicate notNormalPred = null;
			for(Predicate p: notPreds){
				if(pred.getId() == p.getId()){
					notNormalPred = p;
					break;
				}
			}
			predMap2.put(notNormalPred, notPred);
			addNotPreds2.add(notPred);
		}
		
		Set<Predicate> getPreds = new HashSet<Predicate>(preds);
		getPreds.addAll(addPreds1);
		Set<Predicate> getNotPreds = new HashSet<Predicate>(notPreds);
		getNotPreds.addAll(addNotPreds1);
		// create the put and get statement
		this.getPred = new GetPredicate(null, highestId, -1, dstProcNr, local1Name, this.accumulateVar, getPreds, getNotPreds, false, false);
		highestId = getPred.getHighestId();
		Set<Predicate> putPreds = new HashSet<Predicate>(preds);
		putPreds.addAll(addPreds2);
		Set<Predicate> putNotPreds = new HashSet<Predicate>(notPreds);
		putNotPreds.addAll(addNotPreds2);
		this.putPred = new PutPredicate(null, highestId, -1, dstProcNr, this.finalWriteVar, local2Name, putPreds, putNotPreds, false, false);
		highestId = putPred.getHighestId();
	}
	
	public String getFinalWriteVar(){
		return finalWriteVar;
	}
	
	public String getAccumulateVar(){
		return accumulateVar;
	}
	
	public Map<Predicate, Predicate> getPredMap1(){
		return predMap1;
	}
	
	public Map<Predicate, Predicate> getPredMap2(){
		return predMap2;
	}
	
	public Set<Predicate> getAddPreds1(){
		return addPreds1;
	}
	
	public Set<Predicate> getAddNotPreds1(){
		return addNotPreds1;
	}
	
	public Set<Predicate> getAddPreds2(){
		return addPreds2;
	}
	
	public Set<Predicate> getAddNotPreds2(){
		return addNotPreds2;
	}
	
	public String getLocal1Name(){
		return local1Name;
	}
	
	public String getLocal2Name(){
		return local2Name;
	}
	
	public GetPredicate getGetPred(){
		return getPred;
	}
	
	public PutPredicate getPutPred(){
		return putPred;
	}
}
