package blp;

import java.util.HashSet;
import java.util.Set;

import antlr.TLBaseVisitor;
import antlr.TLParser.IdentifierExpressionContext;

/** a simple visitor to extract the contained variable names in a predicate */
public class VarVisitor extends TLBaseVisitor<Object>{
	
	Set<String> varSet;
	
	public VarVisitor(){
		varSet = new HashSet<String>();
	}
	
	public void reset(){
		varSet = new HashSet<String>();
	}
	
	public Set<String> getVars(){
		return varSet;
	}

	@Override
	public Object visitIdentifierExpression(IdentifierExpressionContext ctx) {
		// we add the identifier to the variable set
		varSet.add(ctx.getText());
		return super.visitIdentifierExpression(ctx);
	}

}
