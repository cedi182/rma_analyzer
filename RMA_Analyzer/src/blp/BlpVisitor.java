package blp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLBaseVisitor;
import antlr.TLLexer;
import antlr.TLParser;
import antlr.TLParser.AssertAlwaysContext;
import antlr.TLParser.AssertFinalContext;
import antlr.TLParser.AssignmentContext;
import antlr.TLParser.AssumptionContext;
import antlr.TLParser.AtomicSectionContext;
import antlr.TLParser.CasContext;
import antlr.TLParser.FlushContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.IfStatementContext;
import antlr.TLParser.LoadContext;
import antlr.TLParser.ProcessContext;
import antlr.TLParser.ProgramfileContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.RgaContext;
import antlr.TLParser.StatementContext;
import antlr.TLParser.StoreContext;
import antlr.TLParser.VarDeclContext;
import antlr.TLParser.WhileStatementContext;


/** A visitor that visits the parsed tree and generates the boolean program that captures tha in place rma behavior*/
public class BlpVisitor extends TLBaseVisitor<Object>{
	boolean inAtomic;
	StringBuilder result;
	Set<Predicate> predicates;
	Set<Predicate> notPredicates;
	WpcSolver wpcSolver;
	SpcSolver spcSolver;
	int label = 0;
	int curProc = 0;
	Predicate validPred;
	Predicate unsatPred;
	Set<Predicate> validSet;
	Set<Predicate> unsatSet;
	Set<Set<Predicate>> unsatCubes;
	String unsatString;
	/** used for the junit test to compare the produced cubes by reference solver and solver */
	List<Set<Set<Predicate>>> wpcTestList;
	List<Set<Set<Predicate>>> spcTestList;
	Map<PutContext, PutPredicate> putPreds;
	Map<GetContext, GetPredicate> getPreds;
	Map<CasContext, CasPredicate> casPreds;
	Map<RgaContext, RgaPredicate> rgaPreds;
	int cubeSize;
	int unsatCubeSize;
	String fenderPredString;
	UnsatSolver unsatSolver;
	int highestId;
	
	public BlpVisitor(StringBuilder result, Set<Predicate> predicates, Set<Predicate> notPredicates, Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds, Map<CasContext, CasPredicate> casPreds, Map<RgaContext, RgaPredicate>rgaPreds, int cubeSize, int unsatCubeSize){
		// create a representation for valid and unsat conditions
		validPred = new Predicate("true", null, null, false, -1);
		unsatPred = new Predicate("false", null, null, false, -1);
		validSet = new HashSet<Predicate>();
		unsatSet = new HashSet<Predicate>();
		validSet.add(validPred);
		unsatSet.add(unsatPred);
		wpcTestList = new ArrayList<Set<Set<Predicate>>>();
		spcTestList = new ArrayList<Set<Set<Predicate>>>();
		this.cubeSize = cubeSize;
		this.unsatCubeSize = unsatCubeSize;
		this.result = result;
		this.predicates = predicates;
		this.notPredicates = notPredicates;
		this.putPreds = putPreds;
		this.getPreds = getPreds;
		this.casPreds = casPreds;
		this.rgaPreds = rgaPreds;
		this.unsatSolver = new UnsatSolver(predicates, notPredicates, unsatCubeSize);
		this.unsatCubes = unsatSolver.getUnsatCubes();
		this.unsatString = "assume(" + formPredString(unsatCubes, true)  + ");";
		
		this.wpcSolver = new WpcSolver(predicates, notPredicates, validSet, unsatSet, unsatCubes, cubeSize);
		this.spcSolver = new SpcSolver(predicates, notPredicates, unsatSet, validSet, unsatCubes, cubeSize);
		this.inAtomic = false;
		
		addAndInitVars();
		initializeFenderPredString();
		
	}

	/** constructor used for the junit tests (not used anymore) */
	public BlpVisitor(StringBuilder result, Set<Predicate> predicates, Set<Predicate> notPredicates, Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds, Map<CasContext, CasPredicate> casPreds, Map<RgaContext, RgaPredicate>rgaPreds, Predicate validPred, Predicate unsatPred){
		// create a representation for valid and unsat conditions
		this.cubeSize = 3;
		this.unsatCubeSize = 3;
		this.validPred = validPred;
		this.unsatPred = unsatPred;
		validSet = new HashSet<Predicate>();
		unsatSet = new HashSet<Predicate>();
		validSet.add(validPred);
		unsatSet.add(unsatPred);
		wpcTestList = new ArrayList<Set<Set<Predicate>>>();
		spcTestList = new ArrayList<Set<Set<Predicate>>>();
		this.result = result;
		this.predicates = predicates;
		this.notPredicates = notPredicates;
		UnsatSolver unsatSolver = new UnsatSolver(predicates, notPredicates, unsatCubeSize);
		this.unsatCubes = unsatSolver.getUnsatCubes();
		this.unsatString = "assume(" + formPredString(unsatCubes, true)  + ");";
		this.wpcSolver = new WpcSolver(predicates, notPredicates, validSet, unsatSet, unsatCubes, cubeSize);
		this.spcSolver = new SpcSolver(predicates, notPredicates, unsatSet, validSet, unsatCubes, cubeSize);
		this.putPreds = putPreds;
		this.getPreds = getPreds;
		this.casPreds = casPreds;
		this.rgaPreds = rgaPreds;
		
		addAndInitVars();
		initializeFenderPredString();
	}
	
	public Set<Predicate> getValidSet(){
		return validSet;
	}
	
	public Set<Predicate> getUnsatSet(){
		return unsatSet;
	}
	
	public SpcSolver getSpcSolver(){
		return spcSolver;
	}
	
	public WpcSolver getWpcSolver(){
		return wpcSolver;
	}
	
	public UnsatSolver getUnsatSolver(){
		return unsatSolver;
	}
	
	public void initializeFenderPredString(){
		fenderPredString = "";
		for(int i = 0; i <= highestId; i++){
			for(Predicate pred: predicates){
				if(pred.getId() == i){
					fenderPredString = fenderPredString + pred.getPredString().replaceAll("==", "=") + "\r\n";
				}
			}
		}
		if(! fenderPredString.equals("")){
			fenderPredString = fenderPredString.substring(0, fenderPredString.length() - 2);
		}
	}
	
	/** returns the string of predicates used for interpolation in fender*/
	public String getFenderPredString(){
		return fenderPredString;
	}
	
	
	@Override
	public Object visitAtomicSection(AtomicSectionContext ctx) {
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
			inAtomic = true;
			
			visitChildren(ctx);
			
			commitCodeLn(unsatString);
			commitCodeLn("end_atomic;");
			inAtomic = false;
		} else {
			// we are already in an atomic section
			visitChildren(ctx);
		}
		return null;
	}
	
	@Override
	public Object visitIfStatement(IfStatementContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		// compute the spc for the if and else block
		List<Set<Set<Predicate>>> spcList = spcSolver.computeSpc(ctx.getChild(1));
		Set<Set<Predicate>> spc = spcList.get(0);
		String spcString = formPredString(spc, false);
		Set<Set<Predicate>> elseSpc = spcList.get(1);
		String elseSpcString = formPredString(elseSpc, false);
		
		int elseLabel = getNewLabel();
		int afterLabel = getNewLabel();
		commitCodeLn("if (*) goto " + elseLabel + ";");
		commitCodeLn(unsatString);
		commitNop();
		commitComment("Statement assume " + ctx.expression().getText().replaceAll("==", "="));
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		commitNop();
		
		// visit the then block
		visit(ctx.getChild(3));
		commitCodeLn("if(true) goto " + afterLabel + ";");
	
		commitLn(elseLabel + ": nop;");
		commitCodeLn(unsatString);
		commitNop();
		commitComment("Statement assume (!" + ctx.expression().getText().replaceAll("==", "=") + ")");
		if(!elseSpcString.equals("")){
			commitCodeLn("assume(" + elseSpcString + ");");
		}
		commitNop();
		if(ctx.children.size() == 7){
			// there is a else block
			visit(ctx.getChild(5));
		}
		commitLn(afterLabel + ": nop;");
		commitCodeLn(unsatString);
		spcTestList.add(spc);
		spcTestList.add(elseSpc);
		return null;
	}

	@Override
	public Object visitWhileStatement(WhileStatementContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		// compute the spc for the if and else block
		List<Set<Set<Predicate>>> spcList = spcSolver.computeSpc(ctx.getChild(1));
		Set<Set<Predicate>> spc = spcList.get(0);
		String spcString = formPredString(spc, false);
		Set<Set<Predicate>> elseSpc = spcList.get(1);
		String elseSpcString = formPredString(elseSpc, false);
		
		int startLabel = getNewLabel();
		int condCheckLabel = getNewLabel();
		commitCodeLn("if(true) goto " + condCheckLabel + ";");
		commitLn(startLabel + ": nop;");
		commitCodeLn(unsatString);
		commitNop();
		commitComment("Statement assume " + ctx.expression().getText().replaceAll("==", "="));
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		
		// now we visit the while block
		visit(ctx.getChild(3));
		
		commitLn(condCheckLabel + ": if (*) goto " + startLabel + ";");
		
		commitCodeLn(unsatString);
		commitNop();
		commitComment("Statement assume (!" + ctx.expression().getText().replaceAll("==", "=") + ")");
		if(!elseSpcString.equals("")){
			commitCodeLn("assume(" + elseSpcString + ");");
		}
		
		spcTestList.add(spc);
		spcTestList.add(elseSpc);
		
		return null;
	}

	@Override
	public Object visitAssumption(AssumptionContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		commitCodeLn(unsatString);
		Set<Set<Predicate>> spc = spcSolver.computeSpc(ctx.getChild(2), false);
		String spcString = formPredString(spc, false);
		commitNop();
		commitComment("Statement assume (!" + ctx.expression().getText().replaceAll("==", "=") + ")");
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		
		spcTestList.add(spc);
		return null;
	}

	@Override
	public Object visitVarDecl(VarDeclContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		if(ctx.children.size() == 3){
			// the variable declaration involves an assignment -> code copied from assignment abstract transformer computation
			if(! inAtomic){
				commitCodeLn("begin_atomic;");
			}
			commitNop();
			commitComment("Statement " + ctx.getChild(0).getText() + " = " + ctx.getChild(2).getText());
			Set<Predicate> preds = wpcSolver.computeAffectedPreds(ctx.getChild(0).getText(), predicates);
			List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
			List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
			List<Predicate> predList = new ArrayList<Predicate>();
			for(Predicate pred: preds){
				List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(ctx, pred);
				Set<Set<Predicate>> wpc = wpcs.get(0);
				Set<Set<Predicate>> notWpc = wpcs.get(1);
				wpcList.add(wpc);
				notWpcList.add(notWpc);
				predList.add(pred);
			}
			
			Set<Integer> loadSet = getContainedPredIds(wpcList);
			loadSet.addAll(getContainedPredIds(notWpcList));
			loadPredsToLocal(loadSet);
			
			for(int i = 0; i < wpcList.size(); i++){
				commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
			}
			
			resetLocals(loadSet);
			commitCodeLn(unsatString);
			if(! inAtomic){
				commitCodeLn("end_atomic;");
			}
			
			for(Set<Set<Predicate>> wpc: wpcList){
				wpcTestList.add(wpc);
			}
			for(Set<Set<Predicate>> notWpc: notWpcList){
				wpcTestList.add(notWpc);
			}
		}
		return null;
	}

	@Override
	public Object visitAssertAlways(AssertAlwaysContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		commitCodeLn(unsatString);
		Set<Set<Predicate>> wpc = wpcSolver.computeWpc2(ctx.getChild(3));
		String wpcString = formPredString(wpc, false);
		if(!wpcString.equals("")){
			if(wpcString.startsWith("!")){
				// remove the leading ! because this is placed for the weakest postcondition strings
				wpcString = wpcString.substring(1, wpcString.length());
			}
			commitLn("assert always(" + wpcString + ");");
		} else {
			commitLn("assert always(false);");
		}
		
		spcTestList.add(wpc);
		return null;
	}

	@Override
	public Object visitAssertFinal(AssertFinalContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		commitCodeLn(unsatString);
		Set<Set<Predicate>> wpc = wpcSolver.computeWpc2(ctx.getChild(3));
		String wpcString = formPredString(wpc, false);
		if(!wpcString.equals("")){
			if(wpcString.startsWith("!")){
				// remove the leading ! because this is placed for the weakest postcondition strings
				wpcString = wpcString.substring(1, wpcString.length());
			}
			commitLn("assert final(" + wpcString + ");");
		} else {
			commitLn("assert final(false);");
		}
		
		spcTestList.add(wpc);
		return null;
	}

	@Override
	public Object visitLoad(LoadContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
		}
		commitNop();
		commitComment("Statement " + ctx.Identifier(0).getText() + " = " + ctx.Identifier(1).getText());
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(ctx.getChild(4).getText(), predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		for(Predicate pred: preds){
			List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(ctx, pred);
			Set<Set<Predicate>> wpc = wpcs.get(0);
			Set<Set<Predicate>> notWpc = wpcs.get(1);
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
		}
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		resetLocals(loadSet);
		commitCodeLn(unsatString);
		if(! inAtomic){
			commitCodeLn("end_atomic;");
		}
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
		return null;
	}

	@Override
	public Object visitStore(StoreContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
		}
		commitNop();
		commitComment("Statement " + ctx.Identifier().getText() + " = " + ctx.getChild(6).getText());
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(ctx.getChild(4).getText(), predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		for(Predicate pred: preds){
			List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(ctx, pred);
			Set<Set<Predicate>> wpc = wpcs.get(0);
			Set<Set<Predicate>> notWpc = wpcs.get(1);
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
		}
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		resetLocals(loadSet);
		commitCodeLn(unsatString);
		if(! inAtomic){
			commitCodeLn("end_atomic;");
		}
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
		return null;
	}

	@Override
	public Object visitAssignment(AssignmentContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		computeAssignmentTransformer(ctx, ctx.getChild(0).getText(), "Statement " + ctx.Identifier().getText() + " = " + ctx.getChild(2).getText());
		return null;
	}
	
	@Override
	public Object visitProcess(ProcessContext ctx) {
		int procNumber = Integer.parseInt(ctx.Number().getText());
		curProc = procNumber;
		// commit the process label to the result
		commitLn("process " + procNumber);
		for(StatementContext statement: ctx.statement()){
			visit(statement);
		}
		return null;
	}
	
	@Override
	public Object visitProgramfile(ProgramfileContext ctx) {
		for(ProcessContext proc: ctx.process()){
			visit(proc.decl());
		}
		return super.visitProgramfile(ctx);
	}

	public Set<Integer> getContainedPredIds(List<Set<Set<Predicate>>> setList){
		Set<Integer> preds = new HashSet<Integer>();
		for(Set<Set<Predicate>> predSets: setList){
			if(predSets != null){
				for(Set<Predicate> predSet: predSets){
					for(Predicate pred: predSet){
						preds.add(new Integer(pred.getId()));
					}
				}
			}
		}
		return preds;
	}
	
	public void loadPredsToLocal(Set<Integer> preds){
		for(Integer id: preds){
			if(id != -1){
				// id == -1 represents true or false
				commitCodeLn("load t" + id.toString() + " = B" + id.toString() + ";");
			}
		}
	}
	
	public void resetLocals(Set<Integer> preds){
		for(Integer id: preds){
			if(id != -1){
				commitCodeLn("t" + id.toString() + " = 0;");
			}
		}
	}
	/** used for the conditions of if, assert and while statements */
	public String formPredString(Set<Set<Predicate>> predSets, boolean unsatCube){
		String result = "";
		if(predSets.size()==1 && predSets.contains(unsatSet)){
			// the condition is not satisfiable -> return false to mark unreachable code
			result = "false";
		} else if(predSets.size()==1 && predSets.contains(validSet)){
			// the condition is valid
			result = "true";
		}
		else if(predSets.size() > 0){
			// we have to put a ! in front because we calculated the cubes for the inverted case
			result = "!(";
			for(Set<Predicate> set: predSets){
				result = result + "(";
				for(Predicate pred: set){
					if(pred.getMakeNot()){
						result = result + "(B" + pred.getId() + " == 0)&&";
					} else {
						result = result + "(B" + pred.getId() + " != 0)&&";
					}
				}
				// remove the trailing &&
				result = result.substring(0, result.length() - 2);
				result = result + ")||";
			}
			// remove the last "||"
			result = result.substring(0, result.length() - 2);
			result = result + ")";
		} else {
			if(unsatCube){
				result = "true";
			} else {
				// the condition implies no predicate -> no assume statement will be needed
				result = "";
			}
		}
		return result;
	}
	
	public List<String> getCommitString(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred){
		commitComment("Transform B" + storePred.getId() + " that corresponds to predicate: " + storePred.getPredString());
		String trueString = "";
		String falseString = "";
		if(trueSet.size() == 1 && trueSet.contains(validSet)){
			trueString = "true";
			falseString = "false";
		} else if(trueSet.size() == 1 && trueSet.contains(unsatSet)) {
			trueString = "false";
			falseString = "true";
		} else {
			if(trueSet.size() > 0){
				for(Set<Predicate> set: trueSet){
					trueString = trueString + "(";
					for(Predicate pred: set){
						if(pred.getMakeNot()){
							trueString = trueString + "(t" + pred.getId() + " == 0)&&";
						} else {
							trueString = trueString + "(t" + pred.getId() + " != 0)&&";
						}
					}
					trueString = trueString.substring(0, trueString.length() - 2);
					trueString = trueString + ")||";
				}
				// remove the last "||"
				trueString = trueString.substring(0, trueString.length() - 2);
			} else {
				trueString = "false";
			}
		}
		
		if(falseString.equals("")){
			if(falseSet.size() > 0){
				for(Set<Predicate> set: falseSet){
					falseString = falseString + "(";
					for(Predicate pred: set){
						if(pred.getMakeNot()){
							falseString = falseString + "(t" + pred.getId() + " == 0)&&";
						} else {
							falseString = falseString + "(t" + pred.getId() + " != 0)&&";
						}
					}
					falseString = falseString.substring(0, falseString.length() - 2);
					falseString = falseString + ")||";
				}
				// remove the last "||"
				falseString = falseString.substring(0, falseString.length() - 2);
			} else {
				falseString = "false";
			}
		}
		List<String> result = new ArrayList<String>();
		result.add(trueString);
		result.add(falseString);
		return result;
		}
	
	public void commitChoose(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred){
		List<String> strings = getCommitString(trueSet, falseSet, storePred);
		String trueString = strings.get(0);
		String falseString = strings.get(1);
		
		commitCodeLn("store B" + storePred.getId() + "= choose(" + trueString + ", " + falseString + ");");
	}
	
	public void commitPutGetChoose(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred){
		List<String> strings = getCommitString(trueSet, falseSet, storePred);
		String trueString = strings.get(0);
		String falseString = strings.get(1);
		
		commitCodeLn("store B" + storePred.getId() + "= choose(B" + storePred.getId() + "&&"  + trueString + ", !B"+ storePred.getId() + "&&" + falseString + ");");
	}

	/** returns a unique number at each call used for the program labels */
	int getNewLabel(){
		return label++;
	}
	
	void commitCodeLn(String text){
		commitLn(getNewLabel() + ": " + text);
	}
	
	/** adds text to the string builder and adds a new line character at the end */
	void commitLn(String text){
		result.append(text + "\r\n");
	}
	
	void commitNop(){
		commitLn(getNewLabel() + ": nop;");
	}
	
	void commitComment(String text){
		commitLn("/* " + text + " */");
	}
	
	@Override
	public Object visitFlush(FlushContext ctx) {
		return null;
	}

	@Override
	public Object visitRga(RgaContext ctx) {
		boolean setAtomic = false;
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		
		RgaPredicate  rgaPred = rgaPreds.get(ctx);
		// t1 = Y
		computeRdmaAssignmentTransformer(rgaPred.getLocal1Name(), rgaPred.getAccumulateVar(), false, rgaPred.getAddPreds1(), rgaPred.getAddNotPreds1());
		if(! inAtomic){
			inAtomic = true;
			setAtomic = true;
			commitCodeLn("begin_atomic;");
		}
		// t2 = Z
		computeRdmaAssignmentTransformer(rgaPred.getLocal2Name(), rgaPred.getWriteVar(), true, rgaPred.getAddPreds2(), rgaPred.getAddNotPreds2());
		// Z = Z + t1
		Set<Predicate> newAddPreds = new HashSet<Predicate>(rgaPred.getAddPreds1());
		newAddPreds.addAll(rgaPred.getAddPreds2());
		Set<Predicate> newAddNotPreds = new HashSet<Predicate>(rgaPred.getAddPreds2());
		newAddNotPreds.addAll(rgaPred.getAddNotPreds2());
		computeRdmaAssignmentTransformerExpr(rgaPred.getWriteVar(), rgaPred.getWriteVar() + " + " + rgaPred.local1Name, true, newAddPreds, newAddNotPreds);
		if(setAtomic){
			inAtomic = false;
			commitCodeLn("end_atomic;");
		}
		// X = t2
		computeRdmaAssignmentTransformer(rgaPred.getFinalWriteVar(), rgaPred.getLocal2Name(), false, rgaPred.getAddPreds2(), rgaPred.getAddNotPreds2());
		return null;
	}

	@Override
	public Object visitCas(CasContext ctx) {
		boolean setAtomic = false;
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		
		CasPredicate casPred = casPreds.get(ctx);
		int elseLabel = getNewLabel();
		int endLabel = getNewLabel();
		commitCodeLn("if (*) goto " + elseLabel + ";");
		// t1 = Y
		computeRdmaAssignmentTransformer(casPred.getLocal1Name(), casPred.getCompareVar(), false, casPred.getAddPreds1(), casPred.getAddNotPreds1());
		// t2 = W
		computeRdmaAssignmentTransformer(casPred.getLocal2Name(), casPred.getReplaceVar(), false, casPred.getAddPreds2(), casPred.getAddNotPreds2());
		
		commitCodeLn("if (true) goto " + endLabel + ";");
		commitLn(elseLabel + ": nop;");
		
		// t2 = W
		computeRdmaAssignmentTransformer(casPred.getLocal2Name(), casPred.getReplaceVar(), false, casPred.getAddPreds2(), casPred.getAddNotPreds2());
		// t1 = Y
		computeRdmaAssignmentTransformer(casPred.getLocal1Name(), casPred.getCompareVar(), false, casPred.getAddPreds1(), casPred.getAddNotPreds1());
				
		commitLn(endLabel + ": nop;");
		
		if(! inAtomic){
			inAtomic = true;
			setAtomic = true;
			commitCodeLn("begin_atomic;");
		}
		
		// t3 = Z
		computeRdmaAssignmentTransformer(casPred.getLocal3Name(), casPred.getWriteVar(), true, casPred.getAddPreds3(), casPred.getAddNotPreds3());
		
		//if(t1 == t3)
		List<Set<Set<Predicate>>> ifCondition = spcSolver.computeSpc("(" + casPred.getLocal1Name() + " == " + casPred.getLocal3Name() + ")");
		elseLabel = getNewLabel();
		endLabel = getNewLabel();
		commitCodeLn("if (*) goto " + elseLabel + ";");
		
		String spcString = formPredString(ifCondition.get(0), false);
		if(! spcString.equals("")){
			commitNop();
			commitComment("Statement assume (" + casPred.getLocal1Name() + " = " + casPred.getLocal3Name() + ")");
			commitCodeLn("assume(" + spcString + ");");
		}
		
		// Z = t2
		computeRdmaAssignmentTransformer(casPred.getWriteVar(), casPred.getLocal2Name(), true, casPred.getAddPreds2(), casPred.getAddNotPreds2());
		commitCodeLn("if (true) goto " + endLabel + ";");
		// else branch
		commitLn(elseLabel + ": nop;");
		String notSpcString = formPredString(ifCondition.get(1), false);
		if(! notSpcString.equals("")){
			commitNop();
			commitComment("Statement assume (!(" + casPred.getLocal1Name() + " = " + casPred.getLocal3Name() + "))");
			commitCodeLn("assume(" + notSpcString + ");");
		}
		commitLn(endLabel + ": nop;");
		
		if(setAtomic){
			inAtomic = false;
			commitCodeLn("end_atomic;");
		}
		// X = t3
		computeRdmaAssignmentTransformer(casPred.getFinalWriteVar(), casPred.getLocal3Name(), false, casPred.getAddPreds3(), casPred.getAddNotPreds3());
		return null;
	}
	
	public void computeRdmaAssignmentTransformer(String identifier, String identifier2, boolean inAtomicStatement, Set<Predicate> addPreds, Set<Predicate> addNotPreds){
		if(! inAtomic && ! inAtomicStatement){
			commitCodeLn("begin_atomic;");
		}
		commitComment("Statement " + identifier + " = " + identifier2);
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predicates);
		preds.addAll(addPreds);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		for(Predicate pred: preds){
			List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(identifier, identifier2, pred, addPreds, addNotPreds);
			Set<Set<Predicate>> wpc = wpcs.get(0);
			Set<Set<Predicate>> notWpc = wpcs.get(1);
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
			
		}
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		
		resetLocals(loadSet);
		commitCodeLn(unsatString);
		if(! inAtomic && ! inAtomicStatement){
			commitCodeLn("end_atomic;");
		}
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
	}
	
	public void computeRdmaAssignmentTransformerExpr(String identifier, String rhsExpr, boolean inAtomicStatement, Set<Predicate> addPreds, Set<Predicate> addNotPreds){
		if(!inAtomic && ! inAtomicStatement){
			commitCodeLn("begin_atomic;");
		}
		commitComment("Statement " + identifier + " = " + rhsExpr);
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predicates);
		preds.addAll(addPreds);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		
		TLLexer lexer = new TLLexer(new ANTLRInputStream(rhsExpr));
		TLParser parser = new TLParser(new CommonTokenStream(lexer));
		parser.setBuildParseTree(true);
		ParseTree tree = parser.parse();
		
		for(Predicate pred: preds){
			List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(identifier, tree, pred, addPreds, addNotPreds);
			Set<Set<Predicate>> wpc = wpcs.get(0);
			Set<Set<Predicate>> notWpc = wpcs.get(1);
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
		}
		
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		
		resetLocals(loadSet);
		commitCodeLn(unsatString);
		if(! inAtomic && ! inAtomicStatement){
			commitCodeLn("end_atomic;");
		}
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
	}
	
	public void computeAssignmentTransformer(ParseTree ctx, String identifier, String statementComment){
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
		}
		// add the statement comment
		commitNop();
		commitComment(statementComment);
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		for(Predicate pred: preds){
			List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(ctx, pred);
			Set<Set<Predicate>> wpc = wpcs.get(0);
			Set<Set<Predicate>> notWpc = wpcs.get(1);
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
		}
		
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		
		resetLocals(loadSet);
		commitCodeLn(unsatString);
		if(! inAtomic){
			commitCodeLn("end_atomic;");
		}
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
	}

	public List<Set<Set<Predicate>>> getWpcTestList(){
		return wpcTestList;
	}
	
	public List<Set<Set<Predicate>>> getSpcTestList(){
		return spcTestList;
	}
	
	public void addAndInitVars(){
		// first we add a global variable for each predicate
		String vars = "shared ";
		highestId = -1;
		Set<Integer> idSet = new HashSet<Integer>();
		for(Predicate pred: predicates){
			idSet.add(pred.getId());
			highestId = Math.max(highestId, pred.getId());
		}
		for(int i = 0; i <= highestId; i++){
			if(idSet.contains(i)){
				vars = vars + "B" + i + ", ";
			}
		}
		if(vars.length() > 7){
			vars = vars.substring(0, vars.length() - 2) + ";";
			commitLn(vars);
		}
		// now we add a local variable for each normal predicate
		vars = "local ";
		for(int i = 0; i <= highestId; i++){
			if(idSet.contains(i)){
				vars = vars + "t" + i + ", ";
			}
		}
		if(vars.length() > 6){
			vars = vars.substring(0, vars.length() - 2) + ";";
			commitLn(vars);
		}
		commitLn("init");
		for(Predicate pred: predicates){
			commitComment("Predicate: " + pred.getPredString() + " corresponds to B" + pred.getId());
		}
		//initialize all Predicates to *
		for(int i = 0; i <= highestId; i++){
			if(idSet.contains(i)){
				commitCodeLn("store B" + i + " = *;");
			}
		}
	}

	@Override
	public Object visitPut(PutContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		computeAssignmentTransformer(ctx, ctx.getChild(7).getText(), "Statement " + ctx.getChild(7).getText() + " = " + ctx.getChild(11).getText());
		return null;
	}

	@Override
	public Object visitGet(GetContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		computeAssignmentTransformer(ctx, ctx.getChild(0).getText(), "Statement " + ctx.getChild(0).getText() + " = " + ctx.getChild(9).getText());
		return null;
	}
}
