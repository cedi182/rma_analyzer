package blp;

import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

/** a class used to represent remote statements as an object for the boolean program visitors*/
public abstract class RdmaPredicate {
	ParseTree ctx;
	String readVar;
	String writeVar;
	int srcProcNr;
	int dstProcNr;
	int id;
	int highestId;
	Set<Predicate> preds;
	Set<Predicate> notPreds;
	String setName;
	/** used to declare if the statement is in a loop*/
	boolean inLoop;
	/** used to declare that the statement is followed by a flush and therefore will only execute once per loop iteration*/
	boolean flushFollowed;
	
	Map<Predicate, Predicate> predMap;
	Set<Predicate> addPreds;
	Set<Predicate> addNotPreds;
	
	public void setFlushFollowed(boolean set){
		flushFollowed = set;
	}
	
	public boolean getFlushFollowed(){
		return flushFollowed;
	}
	
	public boolean getInLoop(){
		return inLoop;
	}
	
	public Set<Predicate> getAddPreds(){
		return addPreds;
	}
	
	public Set<Predicate> getAddNotPreds(){
		return addNotPreds;
	}
	
	public Map<Predicate, Predicate> getPredMap(){
		return predMap;
	}
	
	
	public int getHighestId(){
		return highestId;
	}
	
	public int getId(){
		return id;
	}
	
	public String getText(){
		return ctx.getText();
	}
	
	public String getWriteVar(){
		return writeVar;
	}
	
	public String getReadVar(){
		return readVar;
	}
	
	public int getSrcProcNr(){
		return srcProcNr;
	}
	
	public int getDstProcNr(){
		return dstProcNr;
	}
	
	public String getSetName(){
		return setName;
	}
}
