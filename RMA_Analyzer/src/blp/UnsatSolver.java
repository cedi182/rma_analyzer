package blp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

public class UnsatSolver {
	Set<Predicate> predicates;
	
	Set<Predicate> notPredicates;
	
	List<Predicate> preds;
	
	TreeToZ3Visitor z3Visitor;
	
	int maxCubeSize;
	
	Set<Map<Predicate, Predicate>> predMaps;
	
	Set<Set<Predicate>> unsatCubes;
	
	public UnsatSolver(Set<Predicate> predicates, Set<Predicate> notPredicates, int unsatCubeSize){
		this.predicates = predicates;
		this.notPredicates = notPredicates;
		this.z3Visitor = new TreeToZ3Visitor();
		this.preds = new ArrayList<Predicate>();
		this.preds.addAll(predicates);
		this.preds.addAll(notPredicates);
		this.maxCubeSize = unsatCubeSize;
		this.predMaps = new HashSet<Map<Predicate, Predicate>>();
	}
	
	public UnsatSolver(Set<Predicate> predicates, Set<Predicate> notPredicates, Set<Map<Predicate, Predicate>> predMaps, int unsatCubeSize){
		this.predicates = predicates;
		this.notPredicates = notPredicates;
		this.z3Visitor = new TreeToZ3Visitor();
		this.preds = new ArrayList<Predicate>();
		this.preds.addAll(predicates);
		this.preds.addAll(notPredicates);
		this.maxCubeSize = unsatCubeSize;
		this.predMaps = predMaps;
	}
	
	public Set<Set<Predicate>> getUnsatCubes(){
		Set<Set<Predicate>> result = new HashSet<Set<Predicate>>();
		
		for(int i = 1; i <= maxCubeSize; i++){
			result = getCubes(i, result);
		}
		
		// now go through all the found cubes again and map them to the set predicates
		Set<Set<Predicate>> additionalCubes = new HashSet<Set<Predicate>>();
		for(Map<Predicate, Predicate> predMap: predMaps){
			for(Set<Predicate> cube: result){
				boolean mapped = false;
				Set<Predicate> mappedCube = new HashSet<Predicate>();
				for(Predicate pred: cube){
					if(predMap.keySet().contains(pred)){
						// the predicate is in the predMap, map it to the set predicate and add it to the cube
						mapped = true;
						mappedCube.add(predMap.get(pred));
					} else {
						// add the same predicate to the cube
						mappedCube.add(pred);
					}
				}
				if(mapped){
					additionalCubes.add(mappedCube);
				}
			}
		}
		
		result.addAll(additionalCubes);
		unsatCubes = result;
		return result;
	}
	
	public Set<Set<Predicate>> getUnsatCubes(Set<Map<Predicate, Predicate>> predMaps){
		this.predMaps = predMaps;
		Set<Set<Predicate>> result = new HashSet<Set<Predicate>>();
		
		if(unsatCubes != null){
			// reuse the cubes from the SC case
			result = unsatCubes;
		} else {
			for(int i = 1; i <= maxCubeSize; i++){
				result = getCubes(i, result);
			}
		}
		
		// now go through all the found cubes again and map them to the set predicates
		Set<Set<Predicate>> additionalCubes = new HashSet<Set<Predicate>>();
		for(Map<Predicate, Predicate> predMap: predMaps){
			for(Set<Predicate> cube: result){
				boolean mapped = false;
				Set<Predicate> mappedCube = new HashSet<Predicate>();
				for(Predicate pred: cube){
					if(predMap.keySet().contains(pred)){
						// the predicate is in the predMap, map it to the set predicate and add it to the cube
						mapped = true;
						mappedCube.add(predMap.get(pred));
					} else {
						// add the same predicate to the cube
						mappedCube.add(pred);
					}
				}
				if(mapped){
					additionalCubes.add(mappedCube);
				}
			}
		}
		
		result.addAll(additionalCubes);
		return result;
	}
	
	public Set<Set<Predicate>> getCubes(int cubeOrder, Set<Set<Predicate>> partSol){
		Context context = z3Visitor.getContext();
		Solver solver = context.mkSolver();
		Set<Set<Predicate>> checkedSet = new HashSet<Set<Predicate>>();
		int[] counters = new int[cubeOrder];
		for(int i = 0; i < cubeOrder; i++){
			counters[i] = Math.min(i, preds.size() - 1);
		}
		while(counters[0] < preds.size()){
			boolean checkSat = true;
			//add the predicates to the set and check if a part of the formula is already present in partSol
			Set<Predicate> predSet = new HashSet<Predicate>();
			Set<Integer> idSet = new HashSet<Integer>();
			for(int i=0; i < cubeOrder; i++){
				// remove the cases where a predicate and its negation is in the cube
				if(idSet.contains(preds.get(counters[i]).getId())){
					checkSat = false;
					break;
				}
				predSet.add(preds.get(counters[i]));
				idSet.add(preds.get(counters[i]).getId());
			}
			if(checkSat && predSet.size() < cubeOrder){
				// remove the cases where a predicate is contained 2 times in the cube
				checkSat = false;
			}
			if(checkSat){
				if(checkedSet.contains(predSet)){
					checkSat = false;
				}
			}
			
			if(checkSat){
				for(Set<Predicate> partSolSet: partSol){
					// remove the case where the cube already contains a cube that is unsat
					if(predSet.containsAll(partSolSet)){
						checkSat = false;
						break;
					}
				}
			}

			if(checkSat){
				// add the predicates to the solver
				solver.push();
				for(Predicate pred: predSet){
					if(pred.getMakeNot()){
						solver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
					} else {
						solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
					}
				}
				if(solver.check() == Status.UNSATISFIABLE){
					// predSet is unsat -> add it to partSol
					partSol.add(predSet);
				}
				solver.pop();
				checkedSet.add(predSet);
			}
			
			//increment the counters
			counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
			if(counters[cubeOrder - 1] == preds.size()){
				int i = cubeOrder - 1;
				while(i >= 0){
					if(counters[i] == preds.size() && i != 0){
						counters[i] = 0;
						counters[i-1] = counters[i-1] + 1;
						i--;
					} else {
						break;
					}
				}
			}
			while(!checkCounters(counters)){
				// keep increasing the counters
				counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
				if(counters[cubeOrder - 1] == preds.size()){
					int i = cubeOrder - 1;
					while(i >= 0){
						if(counters[i] == preds.size() && i != 0){
							counters[i] = 0;
							counters[i-1] = counters[i-1] + 1;
							i--;
						} else {
							break;
						}
					}
				}
			}
			
		}
		return partSol;
	}
	
	/** returns true if no value in counters is the same, else false */
	public boolean checkCounters(int[] counters){
		Set<Integer> checkSet = new HashSet<Integer>();
		for(int i = 0; i < counters.length; i++){
			if(!checkSet.add(counters[i])){
				return false;
			}
		}
		return true;
	}
}
