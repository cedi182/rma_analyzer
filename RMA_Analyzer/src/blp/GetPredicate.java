package blp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLLexer;
import antlr.TLParser;
import antlr.TLParser.GetContext;

public class GetPredicate extends RdmaPredicate{


	public GetPredicate(GetContext ctx, int id, int srcProcNr, int dstProcNr, String writeVar, String readVar, Set<Predicate> preds, Set<Predicate> notPreds, boolean accumulate, boolean inLoop){
		this.flushFollowed = false;
		this.inLoop = inLoop;
		this.ctx = ctx;
		this.srcProcNr = srcProcNr;
		this.dstProcNr = dstProcNr;
		this.writeVar = writeVar;
		this.readVar = readVar;
		this.id = id;
		highestId = id + 1;
		this.addPreds = new HashSet<Predicate>();
		this.addNotPreds = new HashSet<Predicate>();
		this.preds = preds;
		this.notPreds = notPreds;
		this.predMap = new HashMap<Predicate, Predicate>();
		
		if(accumulate){
			setName = readVar + "_SetProc" + srcProcNr;
		} else {
			setName = "Get" + id;
		}
		VarVisitor  varVis = new VarVisitor();
		// transform each predicate
		for(Predicate pred: preds){
			// we only copy the predicates that contain the read var
			if(! pred.getContainedVars().contains(readVar)){
				continue;
			}
			// rewrite the predicate for each var
			String predString = pred.getPredString();
			// rewrite the predicate for each var
			PredRewriteVisitor predRewriteVis = new PredRewriteVisitor(readVar);
			TLLexer lexer = new TLLexer(new ANTLRInputStream(predString));
			TLParser parser = new TLParser(new CommonTokenStream(lexer));
			parser.setBuildParseTree(true);
			ParseTree tree = parser.parse();
			predRewriteVis.visit(tree);
			for(Integer pos: predRewriteVis.getSolution()){
				if(accumulate){
					if(! pos.equals(0)){
						predString = predString.substring(0, pos) + predString.substring(pos, predString.length()).replaceFirst(readVar, readVar + "_SetProc" + srcProcNr);
					} else {
						predString = predString.replaceFirst(readVar, readVar + "_SetProc" + srcProcNr);
					}
				} else {
					if(! pos.equals(0)){
						predString = predString.substring(0, pos) + predString.substring(pos, predString.length()).replaceFirst(readVar, "Get" + id);
					} else {
						predString = predString.replaceFirst(readVar, "Get" + id);
					}
				}
			}
			lexer = new TLLexer(new ANTLRInputStream(predString));
			parser = new TLParser(new CommonTokenStream(lexer));
			parser.setBuildParseTree(true);
			tree = parser.parse();
			varVis.visit(tree);
			Predicate copyPred = new Predicate(predString, tree, varVis.getVars(), false, highestId);
			addPreds.add(copyPred);
			highestId++;
			predMap.put(pred, copyPred);
		}
		Set<Predicate> iterSet = new HashSet<Predicate>(predMap.keySet());
		for(Predicate pred: iterSet){
			Predicate copyPred = predMap.get(pred);
			Predicate notPred = new Predicate(copyPred.getPredString(), copyPred.getTree(), copyPred.getContainedVars(), true, copyPred.getId());
			addNotPreds.add(notPred);
			// find the according not pred to store the not copy pred in the pred map
			Predicate notNormalPred = null;
			for(Predicate p: notPreds){
				if(pred.getId() == p.getId()){
					notNormalPred = p;
					break;
				}
			}
			predMap.put(notNormalPred, notPred);
		}
	}
	
	public GetPredicate(GetContext ctx, int id, int srcProcNr, int dstProcNr, String writeVar, String readVar, Set<Predicate> preds, Set<Predicate> notPreds, Map<Predicate, Predicate> predMap, Set<Predicate> addPreds, Set<Predicate> addNotPreds, String setName, boolean inLoop){
		this.flushFollowed = false;
		this.inLoop = inLoop;
		this.ctx = ctx;
		this.srcProcNr = srcProcNr;
		this.dstProcNr = dstProcNr;
		this.writeVar = writeVar;
		this.readVar = readVar;
		this.id = id;
		highestId = id + 1;
		this.addPreds = addPreds;
		this.addNotPreds = addNotPreds;
		this.preds = preds;
		this.notPreds = notPreds;
		this.predMap = predMap;
		this.setName = setName;
	}
	
}
