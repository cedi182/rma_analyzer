package blp;

import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

/** a class that represents a predicate used in the generation of the boolean program */
public class Predicate {
	
	int id;
	
	/** used to represent the negation of a predicate */
	boolean makeNot;
	
	String predString;
	
	ParseTree predTree;
	
	/** a set of variables that is contained in this predicate */
	Set<String> containedVars;
	
	public Predicate(String predicate, ParseTree tree, Set<String> vars, boolean makeNot, int id){
		predString = predicate;
		predTree = tree;
		containedVars = vars;
		this.makeNot = makeNot;
		this.id = id;
		
	}
	
	public ParseTree getTree(){
		return predTree;
	}
	
	public Set<String> getContainedVars(){
		return containedVars;
	}
	
	public int getId(){
		return id;
	}
	
	public String getPredString(){
		return predString;
	}
	
	public boolean getMakeNot(){
		return makeNot;
	}

}
