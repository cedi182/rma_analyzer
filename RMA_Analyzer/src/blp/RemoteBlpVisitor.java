package blp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLBaseVisitor;
import antlr.TLLexer;
import antlr.TLParser;
import antlr.TLParser.AssertAlwaysContext;
import antlr.TLParser.AssertFinalContext;
import antlr.TLParser.AssignmentContext;
import antlr.TLParser.AssumptionContext;
import antlr.TLParser.AtomicSectionContext;
import antlr.TLParser.CasContext;
import antlr.TLParser.FlushContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.IfStatementContext;
import antlr.TLParser.LoadContext;
import antlr.TLParser.ProcessContext;
import antlr.TLParser.ProgramfileContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.RgaContext;
import antlr.TLParser.StatementContext;
import antlr.TLParser.StoreContext;
import antlr.TLParser.VarDeclContext;
import antlr.TLParser.WhileStatementContext;
import flowgraph.FlowGraph;

/** a visitor that visits the parsed program and generates a boolean program that captures the remote rma behavior */
public class RemoteBlpVisitor extends TLBaseVisitor<Object>{
	boolean inAtomic;
	StringBuilder result;
	StringBuilder addProcesses;
	Set<Predicate> predicates;
	Set<Predicate> notPredicates;
	WpcSolver wpcSolver;
	SpcSolver spcSolver;
	int label = 0;
	int curProc = 0;
	Predicate validPred;
	Predicate unsatPred;
	Set<Predicate> validSet;
	Set<Predicate> unsatSet;
	Set<Set<Predicate>> unsatCubes;
	String unsatString;
	/** used for the junit test to compare the produced cubes by reference solver and solver */
	List<Set<Set<Predicate>>> wpcTestList;
	List<Set<Set<Predicate>>> spcTestList;
	Map<PutContext, PutPredicate> putPreds;
	Map<GetContext, GetPredicate> getPreds;
	Map<CasContext, CasPredicate> casPreds;
	Map<RgaContext, RgaPredicate> rgaPreds;
	Map<PutPredicate, Map<Predicate, List<Set<Set<Predicate>>>>> putAssignmentTransformers;
	Map<GetPredicate, Map<Predicate, List<Set<Set<Predicate>>>>> getAssignmentTransformers;
	Map<String, Map<Predicate, List<Set<Set<Predicate>>>>> PutGetAddToSetTransformers;
	Map<GetPredicate, Map<Predicate, List<Set<Set<Predicate>>>>> getSetTransformers;
	Map<PutPredicate, Map<Predicate, List<Set<Set<Predicate>>>>> putSetTransformers;
	Map<String, Set<PutPredicate>> varPuts;
	Map<String, Set<GetPredicate>> varGets;
	Map<Integer, FlowGraph> graphSet;
	Set<Flush> flushes;
	List<String> assertionVars;
	boolean accumulate;
	boolean noExtrapolation;
	int cubeSize;
	int unsatCubeSize;
	String fenderPredString;
	int highestId;
	int maxProcess;
	int currAddProcessId;
	Set<RdmaPredicate> rgaRdmaPreds;
	Set<RdmaPredicate> casRdmaPreds;
	boolean noFlushLookahead;
	
	public RemoteBlpVisitor(StringBuilder result, Set<Predicate> predicates, Set<Predicate> notPredicates, Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds, Map<CasContext, CasPredicate> casPreds, Map<RgaContext, RgaPredicate>rgaPreds, Map<Integer, FlowGraph> graphSet, boolean accumulate, int cubeSize, int unsatCubeSize, List<String> assertionVars, WpcSolver wpcSolver, SpcSolver spcSolver, Set<Predicate> validSet, Set<Predicate> unsatSet, UnsatSolver unsatSolver, boolean noExtrapolation, int maxProcess, boolean noFlushLookahead){
		// create a representation for valid and unsat conditions
		this.validSet = validSet;
		this.unsatSet = unsatSet;
		wpcTestList = new ArrayList<Set<Set<Predicate>>>();
		spcTestList = new ArrayList<Set<Set<Predicate>>>();
		this.cubeSize = cubeSize;
		this.unsatCubeSize = unsatCubeSize;
		this.result = result;
		this.addProcesses = new StringBuilder();
		this.predicates = predicates;
		this.notPredicates = notPredicates;
		this.putPreds = putPreds;
		this.getPreds = getPreds;
		this.casPreds = casPreds;
		this.rgaPreds = rgaPreds;
		this.accumulate = accumulate;
		this.assertionVars = assertionVars;
		this.maxProcess = maxProcess;
		this.currAddProcessId = maxProcess + 1;
		this.noFlushLookahead = noFlushLookahead;
		Set<Map<Predicate, Predicate>> predMaps = new HashSet<Map<Predicate, Predicate>>();
		for(PutPredicate putPred: putPreds.values()){
			predMaps.add(putPred.getPredMap());
		}
		for(GetPredicate getPred: getPreds.values()){
			predMaps.add(getPred.getPredMap());
		}
		rgaRdmaPreds = new HashSet<RdmaPredicate>();
		for(RgaPredicate rgaPred: rgaPreds.values()){
			rgaRdmaPreds.add(rgaPred.getGetPred());
			rgaRdmaPreds.add(rgaPred.getPutPred());
			predMaps.add(rgaPred.getGetPred().getPredMap());
			predMaps.add(rgaPred.getPutPred().getPredMap());
		}
		casRdmaPreds = new HashSet<RdmaPredicate>();
		for(CasPredicate casPred: casPreds.values()){
			casRdmaPreds.add(casPred.getGetPred1());
			casRdmaPreds.add(casPred.getGetPred2());
			casRdmaPreds.add(casPred.getPutPred());
			predMaps.add(casPred.getGetPred1().getPredMap());
			predMaps.add(casPred.getGetPred2().getPredMap());
			predMaps.add(casPred.getPutPred().getPredMap());
		}
		if(noExtrapolation){
			for(PutPredicate putPred: putPreds.values()){
				this.predicates.addAll(putPred.getAddPreds());
				this.notPredicates.addAll(putPred.getAddNotPreds());
			}
			for(GetPredicate getPred: getPreds.values()){
				this.predicates.addAll(getPred.getAddPreds());
				this.notPredicates.addAll(getPred.getAddNotPreds());
			}
			for(RgaPredicate rgaPred: rgaPreds.values()){
				this.predicates.addAll(rgaPred.getGetPred().getAddPreds());
				this.predicates.addAll(rgaPred.getGetPred().getAddNotPreds());
				this.predicates.addAll(rgaPred.getPutPred().getAddPreds());
				this.predicates.addAll(rgaPred.getPutPred().getAddNotPreds());
			}
			for(CasPredicate casPred: casPreds.values()){
				this.predicates.addAll(casPred.getGetPred1().getAddPreds());
				this.predicates.addAll(casPred.getGetPred1().getAddNotPreds());
				this.predicates.addAll(casPred.getGetPred2().getAddPreds());
				this.predicates.addAll(casPred.getGetPred2().getAddNotPreds());
				this.predicates.addAll(casPred.getPutPred().getAddPreds());
				this.predicates.addAll(casPred.getPutPred().getAddNotPreds());
			}
			UnsatSolver unsatSolver2 = new UnsatSolver(this.predicates, this.notPredicates, this.unsatCubeSize);
			this.unsatCubes = unsatSolver2.getUnsatCubes();
			this.wpcSolver = new WpcSolver(predicates, notPredicates, validSet, unsatSet, unsatCubes, cubeSize);
			this.spcSolver = new SpcSolver(predicates, notPredicates, unsatSet, validSet, unsatCubes, cubeSize);
		} else {
			this.unsatCubes = unsatSolver.getUnsatCubes(predMaps);
			this.wpcSolver = wpcSolver;
			this.spcSolver = spcSolver;
		}
		this.unsatString = "assume(" + formPredString(unsatCubes, true)  + ");";
		this.inAtomic = false;
		this.putAssignmentTransformers = new HashMap<PutPredicate, Map<Predicate, List<Set<Set<Predicate>>>>>();
		this.getAssignmentTransformers = new HashMap<GetPredicate, Map<Predicate, List<Set<Set<Predicate>>>>>();
		this.graphSet = graphSet;
		this.getSetTransformers = new HashMap<GetPredicate, Map<Predicate, List<Set<Set<Predicate>>>>>();
		this.putSetTransformers = new HashMap<PutPredicate, Map<Predicate, List<Set<Set<Predicate>>>>>();
		this.PutGetAddToSetTransformers = new HashMap<String, Map<Predicate, List<Set<Set<Predicate>>>>>();
		// no flushes here because this is from the initial blp verification with all flushes
		this.flushes = new HashSet<Flush>();
		this.noExtrapolation = noExtrapolation;
		addAndInitVars();
		initializeFenderPredString();
		initPutGetAssignments();
		initPutGetFlushFollowed();
		
	}

	/** constructor used for the junit tests */
	public RemoteBlpVisitor(StringBuilder result, Set<Predicate> predicates, Set<Predicate> notPredicates, Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds, Map<CasContext, CasPredicate> casPreds, Map<RgaContext, RgaPredicate>rgaPreds, Predicate validPred, Predicate unsatPred, Map<Integer, FlowGraph> graphSet, List<String> assertionVars){
		// create a representation for valid and unsat conditions
		this.cubeSize = 3;
		this.unsatCubeSize = 3;
		this.validPred = validPred;
		this.unsatPred = unsatPred;
		validSet = new HashSet<Predicate>();
		unsatSet = new HashSet<Predicate>();
		validSet.add(validPred);
		unsatSet.add(unsatPred);
		wpcTestList = new ArrayList<Set<Set<Predicate>>>();
		spcTestList = new ArrayList<Set<Set<Predicate>>>();
		this.result = result;
		this.predicates = predicates;
		this.notPredicates = notPredicates;
		this.assertionVars = assertionVars;
		Set<Map<Predicate, Predicate>> predMaps = new HashSet<Map<Predicate, Predicate>>();
		for(PutPredicate putPred: putPreds.values()){
			predMaps.add(putPred.getPredMap());
		}
		for(GetPredicate getPred: getPreds.values()){
			predMaps.add(getPred.getPredMap());
		}
		UnsatSolver unsatSolver = new UnsatSolver(predicates, notPredicates, predMaps, unsatCubeSize);
		this.unsatCubes = unsatSolver.getUnsatCubes();
		this.unsatString = "assume(" + formPredString(unsatCubes, true)  + ");";
		this.wpcSolver = new WpcSolver(predicates, notPredicates, validSet, unsatSet, unsatCubes, cubeSize);
		this.spcSolver = new SpcSolver(predicates, notPredicates, unsatSet, validSet, unsatCubes, cubeSize);
		this.putPreds = putPreds;
		this.getPreds = getPreds;
		this.casPreds = casPreds;
		this.rgaPreds = rgaPreds;
		this.putAssignmentTransformers = new HashMap<PutPredicate, Map<Predicate, List<Set<Set<Predicate>>>>>();
		this.getAssignmentTransformers = new HashMap<GetPredicate, Map<Predicate, List<Set<Set<Predicate>>>>>();
		this.graphSet = graphSet;
		this.getSetTransformers = new HashMap<GetPredicate, Map<Predicate, List<Set<Set<Predicate>>>>>();
		this.PutGetAddToSetTransformers = new HashMap<String, Map<Predicate, List<Set<Set<Predicate>>>>>();
		// no flushes here because this is from the initial blp verification with all flushes
		this.flushes = new HashSet<Flush>();
		this.accumulate = false;
		addAndInitVars();
		initializeFenderPredString();
		initPutGetAssignments();
		initPutGetFlushFollowed();
	}
	
	public StringBuilder getAddProcesses(){
		return addProcesses;
	}
	
	public void initPutGetFlushFollowed(){
		if(noFlushLookahead){
			// the optimization is turned off, skip the method
			return;
		}
		for(PutContext ctx: putPreds.keySet()){
			PutPredicate pred = putPreds.get(ctx);
			if(pred.getInLoop()){
				ParseTree tree = ctx.getParent();
				// go up in the tree until we get to the loop
				while(! (tree instanceof WhileStatementContext)){
					tree = tree.getParent();
				}
				// now we should have a loop
				if(!(tree instanceof WhileStatementContext)){
					System.out.println("error with the flush followed loop search: no loop");
				} else {
					WhileStatementContext whileCtx = (WhileStatementContext) tree;
					FlushLookAheadVisitor flushVis = new FlushLookAheadVisitor(ctx, pred, this.flushes);
					flushVis.visit(whileCtx.getChild(3));
				}
				if(! pred.getFlushFollowed()){
					// the statement itself might be in an if block -> additionally check in the block that the statement is contained in for flushes
					tree = ctx;
					while(!(tree.getParent() instanceof WhileStatementContext || tree.getParent() instanceof IfStatementContext)){
						tree = tree.getParent();
					}
					FlushLookAheadVisitor flushVis = new FlushLookAheadVisitor(ctx, pred, this.flushes);
					flushVis.visit(tree);
				}
			}
		}
		
		for(GetContext ctx: getPreds.keySet()){
			GetPredicate pred = getPreds.get(ctx);
			if(pred.getInLoop()){
				ParseTree tree = ctx.getParent();
				// go up in the tree until we get to the loop
				while(! (tree instanceof WhileStatementContext)){
					tree = tree.getParent();
				}
				// now we should have a loop
				if(!(tree instanceof WhileStatementContext)){
					System.out.println("error with the flush followed loop search: no loop");
				} else {
					WhileStatementContext whileCtx = (WhileStatementContext) tree;
					FlushLookAheadVisitor flushVis = new FlushLookAheadVisitor(ctx, pred, this.flushes);
					flushVis.visit(whileCtx.getChild(3));
				}
				if(! pred.getFlushFollowed()){
					// the statement itself might be in an if block -> additionally check in the block that the statement is contained in for flushes
					tree = ctx;
					while(!(tree.getParent() instanceof WhileStatementContext || tree.getParent() instanceof IfStatementContext)){
						tree = tree.getParent();
					}
					FlushLookAheadVisitor flushVis = new FlushLookAheadVisitor(ctx, pred, this.flushes);
					flushVis.visit(tree);
				}
			}
		}
		
		for(RgaContext ctx: rgaPreds.keySet()){
			RgaPredicate pred = rgaPreds.get(ctx);
			if(pred.getInLoop()){
				ParseTree tree = ctx.getParent();
				// go up in the tree until we get to the loop
				while(! (tree instanceof WhileStatementContext)){
					tree = tree.getParent();
				}
				// now we should have a loop
				if(!(tree instanceof WhileStatementContext)){
					System.out.println("error with the flush followed loop search: no loop");
				} else {
					WhileStatementContext whileCtx = (WhileStatementContext) tree;
					FlushLookAheadVisitor flushVis = new FlushLookAheadVisitor(ctx, pred, this.flushes);
					flushVis.visit(whileCtx.getChild(3));
				}
				if(! pred.getFlushFollowed()){
					// the statement itself might be in an if block -> additionally check in the block that the statement is contained in for flushes
					tree = ctx;
					while(!(tree.getParent() instanceof WhileStatementContext || tree.getParent() instanceof IfStatementContext)){
						tree = tree.getParent();
					}
					FlushLookAheadVisitor flushVis = new FlushLookAheadVisitor(ctx, pred, this.flushes);
					flushVis.visit(tree);
				}
			}
		}
		
		for(CasContext ctx: casPreds.keySet()){
			CasPredicate pred = casPreds.get(ctx);
			if(pred.getInLoop()){
				ParseTree tree = ctx.getParent();
				// go up in the tree until we get to the loop
				while(! (tree instanceof WhileStatementContext)){
					tree = tree.getParent();
				}
				// now we should have a loop
				if(!(tree instanceof WhileStatementContext)){
					System.out.println("error with the flush followed loop search: no loop");
				} else {
					WhileStatementContext whileCtx = (WhileStatementContext) tree;
					FlushLookAheadVisitor flushVis = new FlushLookAheadVisitor(ctx, pred, this.flushes);
					flushVis.visit(whileCtx.getChild(3));
				}
				if(! pred.getFlushFollowed()){
					// the statement itself might be in an if block -> additionally check in the block that the statement is contained in for flushes
					tree = ctx;
					while(!(tree.getParent() instanceof WhileStatementContext || tree.getParent() instanceof IfStatementContext)){
						tree = tree.getParent();
					}
					FlushLookAheadVisitor flushVis = new FlushLookAheadVisitor(ctx, pred, this.flushes);
					flushVis.visit(tree);
				}
			}
		}
	}
	
	public void reset(StringBuilder result, Map<Integer, FlowGraph> graphSet, Set<Flush> flushes){
		this.addProcesses = new StringBuilder();
		this.result = result;
		this.graphSet = graphSet;
		this.flushes = flushes;
		this.label = 0;
		addAndInitVars();
		initPutGetFlushFollowed();
	}
	
	public void initPutGetAssignments(){
		varPuts = new HashMap<String, Set<PutPredicate>>();
		varGets = new HashMap<String, Set<GetPredicate>>();
		for(PutPredicate pred: putPreds.values()){
			if(varPuts.get(pred.getWriteVar()) == null){
				Set<PutPredicate> putSet = new HashSet<PutPredicate>();
				varPuts.put(pred.getWriteVar(), putSet);
			}
			Set<PutPredicate> putSet = varPuts.get(pred.getWriteVar());
			putSet.add(pred);
			
			if(noExtrapolation){
				putAssignmentTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getWriteVar(), pred.getSetName(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
			} else {
				// get the direct assignment cubes from SC
				Map<Predicate, List<Set<Set<Predicate>>>> cubes = computeRdmaAssignmentTransformerExpr(pred.getWriteVar(), pred.getReadVar(), false, false);
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				// change the readVar preds to set preds using the pred map
				for(Predicate mapPred: cubes.keySet()){
					List<Set<Set<Predicate>>> list = cubes.get(mapPred);
					List<Set<Set<Predicate>>> newList = new ArrayList<Set<Set<Predicate>>>();
					for(int i = 0; i < list.size(); i++){
						Set<Set<Predicate>> cubeSet = list.get(i);
						Set<Set<Predicate>> newCubeSet = new HashSet<Set<Predicate>>();
						for(Set<Predicate> singleCube: cubeSet){
							Set<Predicate> newSingleCube = new HashSet<Predicate>();
							for(Predicate cubePred: singleCube){
								if(pred.getPredMap().keySet().contains(cubePred)){
									// add the set predicate instead of the normal predicate
									newSingleCube.add(pred.getPredMap().get(cubePred));
								} else {
									// add the normal predicate
									newSingleCube.add(cubePred);
								}
							}
							newCubeSet.add(newSingleCube);
						}
						newList.add(newCubeSet);
					}
					newCubes.put(mapPred, newList);
				}
				putAssignmentTransformers.put(pred, newCubes);
			}
			
		}
		for(GetPredicate pred: getPreds.values()){
			if(varGets.get(pred.getWriteVar()) == null){
				Set<GetPredicate> getSet = new HashSet<GetPredicate>();
				varGets.put(pred.getWriteVar(), getSet);
			}
			Set<GetPredicate> getSet = varGets.get(pred.getWriteVar());
			getSet.add(pred);
			
			if(noExtrapolation){
				getAssignmentTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getWriteVar(), pred.getSetName(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
			} else {
				// get the direct assignment cubes from SC
				Map<Predicate, List<Set<Set<Predicate>>>> cubes = computeRdmaAssignmentTransformerExpr(pred.getWriteVar(), pred.getReadVar(), false, false);
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				// change the readVar preds to set preds using the pred map
				for(Predicate mapPred: cubes.keySet()){
					List<Set<Set<Predicate>>> list = cubes.get(mapPred);
					List<Set<Set<Predicate>>> newList = new ArrayList<Set<Set<Predicate>>>();
					for(int i = 0; i < list.size(); i++){
						Set<Set<Predicate>> cubeSet = list.get(i);
						Set<Set<Predicate>> newCubeSet = new HashSet<Set<Predicate>>();
						for(Set<Predicate> singleCube: cubeSet){
							Set<Predicate> newSingleCube = new HashSet<Predicate>();
							for(Predicate cubePred: singleCube){
								if(pred.getPredMap().keySet().contains(cubePred)){
									// add the set predicate instead of the normal predicate
									newSingleCube.add(pred.getPredMap().get(cubePred));
								} else {
									// add the normal predicate
									newSingleCube.add(cubePred);
								}
							}
							newCubeSet.add(newSingleCube);
						}
						newList.add(newCubeSet);
					}
					newCubes.put(mapPred, newList);
				}
				getAssignmentTransformers.put(pred, newCubes);	
			}
		}
		for(RgaPredicate pred: rgaPreds.values()){
			// first handle the get
			GetPredicate getPred = pred.getGetPred();
			if(varGets.get(getPred.getWriteVar()) == null){
				Set<GetPredicate> getSet = new HashSet<GetPredicate>();
				varGets.put(getPred.getWriteVar(), getSet);
			}
			Set<GetPredicate> getSet = varGets.get(getPred.getWriteVar());
			getSet.add(getPred);
			if(noExtrapolation){
				Set<Predicate> addPreds = new HashSet<Predicate>(getPred.getAddPreds());
				Set<Predicate> addNotPreds = new HashSet<Predicate>(getPred.getAddNotPreds());
				getAssignmentTransformers.put(getPred, computeRdmaAssignmentTransformer(getPred.getWriteVar(), getPred.getSetName(), false, addPreds, addNotPreds, false, true));
			} else {
				// just copy the values as readVar and writeVar have the same predicates
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				for(Predicate transPred: pred.getPredMap1().keySet()){
					if(!transPred.getMakeNot()){
						// only care about the regular predicates because we would get them 2 times for the negation
						List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
						Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
						Set<Predicate> cubeSet = new HashSet<Predicate>();
						cubeSet.add(getPred.getPredMap().get(transPred));
						trueSet.add(cubeSet);
						transList.add(trueSet);
						boolean error = true;
						// now search for the negated predicate
						for(Predicate negPred: pred.getPredMap1().keySet()){
							if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
								// add the negative predicate to the falseSet
								Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
								cubeSet = new HashSet<Predicate>();
								cubeSet.add(getPred.getPredMap().get(negPred));
								falseSet.add(cubeSet);
								transList.add(falseSet);
								error = false;
								break;
							}
						}
						if(error){
							// we should not get here
							System.out.println("error: " + pred.getText());
						}
						newCubes.put(pred.getPredMap1().get(transPred), transList);
					}
				}
				getAssignmentTransformers.put(getPred, newCubes);
			}
			// handle the put
			PutPredicate putPred = pred.getPutPred();
			if(varPuts.get(putPred.getWriteVar()) == null){
				Set<PutPredicate> putSet = new HashSet<PutPredicate>();
				varPuts.put(putPred.getWriteVar(), putSet);
			}
			Set<PutPredicate> putSet = varPuts.get(putPred.getWriteVar());
			putSet.add(putPred);
			if(noExtrapolation){
				Set<Predicate> addPreds = new HashSet<Predicate>(putPred.getAddPreds());
				Set<Predicate> addNotPreds = new HashSet<Predicate>(putPred.getAddNotPreds());
				putAssignmentTransformers.put(putPred, computeRdmaAssignmentTransformer(putPred.getWriteVar(), putPred.getSetName(), false, addPreds, addNotPreds, false, true));
			} else {
				// get the direct assignment cubes from SC
				Map<Predicate, List<Set<Set<Predicate>>>> cubes = computeRdmaAssignmentTransformerExpr(putPred.getWriteVar(), putPred.getReadVar(), false, false);
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				// change the readVar preds to set preds using the pred map
				for(Predicate mapPred: cubes.keySet()){
					List<Set<Set<Predicate>>> list = cubes.get(mapPred);
					List<Set<Set<Predicate>>> newList = new ArrayList<Set<Set<Predicate>>>();
					for(int i = 0; i < list.size(); i++){
						Set<Set<Predicate>> cubeSet = list.get(i);
						Set<Set<Predicate>> newCubeSet = new HashSet<Set<Predicate>>();
						for(Set<Predicate> singleCube: cubeSet){
							Set<Predicate> newSingleCube = new HashSet<Predicate>();
							for(Predicate cubePred: singleCube){
								if(putPred.getPredMap().keySet().contains(cubePred)){
									// add the set predicate instead of the normal predicate
									newSingleCube.add(putPred.getPredMap().get(cubePred));
								} else {
									// add the normal predicate
									newSingleCube.add(cubePred);
								}
							}
							newCubeSet.add(newSingleCube);
						}
						newList.add(newCubeSet);
					}
					newCubes.put(mapPred, newList);
				}
				putAssignmentTransformers.put(putPred, newCubes);
			}
		}
		
		for(CasPredicate pred: casPreds.values()){
			// first handle the first get
			GetPredicate getPred = pred.getGetPred1();
			if(varGets.get(getPred.getWriteVar()) == null){
				Set<GetPredicate> getSet = new HashSet<GetPredicate>();
				varGets.put(getPred.getWriteVar(), getSet);
			}
			Set<GetPredicate> getSet = varGets.get(getPred.getWriteVar());
			getSet.add(getPred);
			if(noExtrapolation){
				Set<Predicate> addPreds = new HashSet<Predicate>(getPred.getAddPreds());
				Set<Predicate> addNotPreds = new HashSet<Predicate>(getPred.getAddNotPreds());
				getAssignmentTransformers.put(getPred, computeRdmaAssignmentTransformer(getPred.getWriteVar(), getPred.getSetName(), false, addPreds, addNotPreds, false, true));
			} else {
				// just copy the values as readVar and writeVar have the same predicates
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				for(Predicate transPred: pred.getPredMap1().keySet()){
					if(!transPred.getMakeNot()){
						// only care about the regular predicates because we would get them 2 times for the negation
						List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
						Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
						Set<Predicate> cubeSet = new HashSet<Predicate>();
						cubeSet.add(getPred.getPredMap().get(transPred));
						trueSet.add(cubeSet);
						transList.add(trueSet);
						boolean error = true;
						// now search for the negated predicate
						for(Predicate negPred: pred.getPredMap1().keySet()){
							if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
								// add the negative predicate to the falseSet
								Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
								cubeSet = new HashSet<Predicate>();
								cubeSet.add(getPred.getPredMap().get(negPred));
								falseSet.add(cubeSet);
								transList.add(falseSet);
								error = false;
								break;
							}
						}
						if(error){
							// we should not get here
							System.out.println("error: " + pred.getText());
						}
						newCubes.put(pred.getPredMap1().get(transPred), transList);
					}
				}
				getAssignmentTransformers.put(getPred, newCubes);
			}
			// now handle the second get
			getPred = pred.getGetPred2();
			if(varGets.get(getPred.getWriteVar()) == null){
				getSet = new HashSet<GetPredicate>();
				varGets.put(getPred.getWriteVar(), getSet);
			}
			getSet = varGets.get(getPred.getWriteVar());
			getSet.add(getPred);
			if(noExtrapolation){
				Set<Predicate> addPreds = new HashSet<Predicate>(getPred.getAddPreds());
				Set<Predicate> addNotPreds = new HashSet<Predicate>(getPred.getAddNotPreds());
				getAssignmentTransformers.put(getPred, computeRdmaAssignmentTransformer(getPred.getWriteVar(), getPred.getSetName(), false, addPreds, addNotPreds, false, true));
			} else {
				// just copy the values as readVar and writeVar have the same predicates
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				for(Predicate transPred: pred.getPredMap2().keySet()){
					if(!transPred.getMakeNot()){
						// only care about the regular predicates because we would get them 2 times for the negation
						List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
						Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
						Set<Predicate> cubeSet = new HashSet<Predicate>();
						cubeSet.add(getPred.getPredMap().get(transPred));
						trueSet.add(cubeSet);
						transList.add(trueSet);
						boolean error = true;
						// now search for the negated predicate
						for(Predicate negPred: pred.getPredMap2().keySet()){
							if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
								// add the negative predicate to the falseSet
								Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
								cubeSet = new HashSet<Predicate>();
								cubeSet.add(getPred.getPredMap().get(negPred));
								falseSet.add(cubeSet);
								transList.add(falseSet);
								error = false;
								break;
							}
						}
						if(error){
							// we should not get here
							System.out.println("error: " + pred.getText());
						}
						newCubes.put(pred.getPredMap2().get(transPred), transList);
					}
				}
				getAssignmentTransformers.put(getPred, newCubes);
			}
			// handle the put
			PutPredicate putPred = pred.getPutPred();
			if(varPuts.get(putPred.getWriteVar()) == null){
				Set<PutPredicate> putSet = new HashSet<PutPredicate>();
				varPuts.put(putPred.getWriteVar(), putSet);
			}
			Set<PutPredicate> putSet = varPuts.get(putPred.getWriteVar());
			putSet.add(putPred);
			if(noExtrapolation){
				Set<Predicate> addPreds = new HashSet<Predicate>(putPred.getAddPreds());
				Set<Predicate> addNotPreds = new HashSet<Predicate>(putPred.getAddNotPreds());
				putAssignmentTransformers.put(putPred, computeRdmaAssignmentTransformer(putPred.getWriteVar(), putPred.getSetName(), false, addPreds, addNotPreds, false, true));
			} else {
				// get the direct assignment cubes from SC
				Map<Predicate, List<Set<Set<Predicate>>>> cubes = computeRdmaAssignmentTransformerExpr(putPred.getWriteVar(), putPred.getReadVar(), false, false);
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				// change the readVar preds to set preds using the pred map
				for(Predicate mapPred: cubes.keySet()){
					List<Set<Set<Predicate>>> list = cubes.get(mapPred);
					List<Set<Set<Predicate>>> newList = new ArrayList<Set<Set<Predicate>>>();
					for(int i = 0; i < list.size(); i++){
						Set<Set<Predicate>> cubeSet = list.get(i);
						Set<Set<Predicate>> newCubeSet = new HashSet<Set<Predicate>>();
						for(Set<Predicate> singleCube: cubeSet){
							Set<Predicate> newSingleCube = new HashSet<Predicate>();
							for(Predicate cubePred: singleCube){
								if(putPred.getPredMap().keySet().contains(cubePred)){
									// add the set predicate instead of the normal predicate
									newSingleCube.add(putPred.getPredMap().get(cubePred));
								} else {
									// add the normal predicate
									newSingleCube.add(cubePred);
								}
							}
							newCubeSet.add(newSingleCube);
						}
						newList.add(newCubeSet);
					}
					newCubes.put(mapPred, newList);
				}
				putAssignmentTransformers.put(putPred, newCubes);
			}
		}
	}
	
	@Override
	public Object visitAtomicSection(AtomicSectionContext ctx) {
		if(! inAtomic){
			AtomicSectionVisitor atmVis = new AtomicSectionVisitor(putPreds, getPreds);
			atmVis.visit(ctx);
			commitCodeLn("begin_atomic;");
			inAtomic = true;
			// do remote ops for all read/write vars inside the section
			doRemoteOps(ctx, atmVis.getReadVars(), atmVis.getWriteVars(), false);
			
			visitChildren(ctx);
			
			commitCodeLn(unsatString);
			commitCodeLn("end_atomic;");
			inAtomic = false;
		} else {
			// we are already in an atomic section
			visitChildren(ctx);
		}
		
		
		return null;
	}
	
	@Override
	public Object visitIfStatement(IfStatementContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		List<String> readVars = new ArrayList<String>();
		List<String> writeVars = new ArrayList<String>();
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(1));
		readVars.addAll(varVis.getVars());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		commitNop();
		commitComment("Statement if " + ctx.expression().getText().replaceAll("==", "="));
		// compute the spc for the if and else block
		List<Set<Set<Predicate>>> spcList = spcSolver.computeSpc(ctx.getChild(1));
		Set<Set<Predicate>> spc = spcList.get(0);
		String spcString = formPredString(spc, false);
		Set<Set<Predicate>> elseSpc = spcList.get(1);
		String elseSpcString = formPredString(elseSpc, false);
		
		int elseLabel = getNewLabel();
		int afterLabel = getNewLabel();
		commitCodeLn("if (*) goto " + elseLabel + ";");
		commitCodeLn(unsatString);
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
		}
		commitNop();
		commitComment("Statement assume " + ctx.expression().getText().replaceAll("==", "="));
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		if(! inAtomic){
			commitCodeLn("end_atomic;");
		}
		commitNop();
		
		// visit the then block
		visit(ctx.getChild(3));
		commitCodeLn("if(true) goto " + afterLabel + ";");
		
		commitLn(elseLabel + ": nop;");
		commitCodeLn(unsatString);
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
		}
		commitNop();
		commitComment("Statement assume (!" + ctx.expression().getText().replaceAll("==", "=") + ")");
		if(!elseSpcString.equals("")){
			commitCodeLn("assume(" + elseSpcString + ");");
		}
		if(! inAtomic){
			commitCodeLn("end_atomic;");
		}
		commitNop();
		if(ctx.children.size() == 7){
			// there is a else block
			visit(ctx.getChild(5));
		}
		commitLn(afterLabel + ": nop;");
		commitCodeLn(unsatString);
		spcTestList.add(spc);
		spcTestList.add(elseSpc);
		
		return null;
	}

	@Override
	public Object visitWhileStatement(WhileStatementContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		List<String> readVars = new ArrayList<String>();
		List<String> writeVars = new ArrayList<String>();
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(1));
		readVars.addAll(varVis.getVars());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		//commitComment("Statement while " + ctx.expression().getText());
		// compute the spc for the if and else block
		List<Set<Set<Predicate>>> spcList = spcSolver.computeSpc(ctx.getChild(1));
		Set<Set<Predicate>> spc = spcList.get(0);
		String spcString = formPredString(spc, false);
		Set<Set<Predicate>> elseSpc = spcList.get(1);
		String elseSpcString = formPredString(elseSpc, false);
		
		int startLabel = getNewLabel();
		int condCheckLabel = getNewLabel();
		commitCodeLn("if(true) goto " + condCheckLabel + ";");
		commitLn(startLabel + ": nop;");
		commitCodeLn(unsatString);
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
		}
		commitNop();
		commitComment("Statement assume " + ctx.expression().getText().replaceAll("==", "="));
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		if(! inAtomic){
			commitCodeLn("end_atomic;");
		}
		
		// now we visit the while block
		visit(ctx.getChild(3));
		
		// do remote operations again because we check the condition again which results in reads
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		
		commitLn(condCheckLabel + ": if (*) goto " + startLabel + ";");
		
		commitCodeLn(unsatString);
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
		}
		commitNop();
		commitComment("Statement assume (!" + ctx.expression().getText().replaceAll("==", "=") + ")");
		if(!elseSpcString.equals("")){
			commitCodeLn("assume(" + elseSpcString + ");");
		}
		if(! inAtomic){
			commitCodeLn("end_atomic;");
		}
		spcTestList.add(spc);
		spcTestList.add(elseSpc);
		
		return null;
	}

	@Override
	public Object visitAssumption(AssumptionContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		List<String> readVars = new ArrayList<String>();
		List<String> writeVars = new ArrayList<String>();
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(2));
		readVars.addAll(varVis.getVars());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
		}
		commitNop();
		commitComment("Statement assume " + ctx.expression().getText());
		commitCodeLn(unsatString);
		Set<Set<Predicate>> spc = spcSolver.computeSpc(ctx.getChild(2), false);
		String spcString = formPredString(spc, false);
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		if(! inAtomic){
			commitCodeLn("end_atomic;");
		}
		spcTestList.add(spc);
		
		return null;
	}

	@Override
	public Object visitVarDecl(VarDeclContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		if(ctx.children.size() == 3){
			commitNop();
			commitComment("Statement " + ctx.getChild(0).getText() + " = " + ctx.getChild(2).getText());
			// the variable declaration involves an assignment -> code copied from assignment abstract transformer computation
			Set<Predicate> preds = wpcSolver.computeAffectedPreds(ctx.getChild(0).getText(), predicates);
			List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
			List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
			List<Predicate> predList = new ArrayList<Predicate>();
			for(Predicate pred: preds){
				List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(ctx, pred);
				Set<Set<Predicate>> wpc = wpcs.get(0);
				Set<Set<Predicate>> notWpc = wpcs.get(1);
				wpcList.add(wpc);
				notWpcList.add(notWpc);
				predList.add(pred);
				
			}
			Set<Integer> loadSet = getContainedPredIds(wpcList);
			loadSet.addAll(getContainedPredIds(notWpcList));
			loadPredsToLocal(loadSet);
			
			for(int i = 0; i < wpcList.size(); i++){
				commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i), false);
			}
			resetLocals(loadSet);
			
			for(Set<Set<Predicate>> wpc: wpcList){
				wpcTestList.add(wpc);
			}
			for(Set<Set<Predicate>> notWpc: notWpcList){
				wpcTestList.add(notWpc);
			}
		}
		return null;
	}

	@Override
	public Object visitAssertAlways(AssertAlwaysContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		List<String> readVars = new ArrayList<String>();
		List<String> writeVars = new ArrayList<String>();
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(3));
		readVars.addAll(varVis.getVars());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		commitCodeLn(unsatString);
		Set<Set<Predicate>> wpc = wpcSolver.computeWpc2(ctx.getChild(3));
		String wpcString = formPredString(wpc, false);
		if(!wpcString.equals("")){
			if(wpcString.startsWith("!")){
				// remove the leading ! because this is placed for the weakest postcondition strings
				wpcString = wpcString.substring(1, wpcString.length());
			}
			commitLn("assert always(" + wpcString + ");");
		} else {
			commitLn("assert always(false);");
		}
		
		spcTestList.add(wpc);
		
		return null;
	}

	@Override
	public Object visitAssertFinal(AssertFinalContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		List<String> readVars = new ArrayList<String>();
		List<String> writeVars = new ArrayList<String>();
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(3));
		readVars.addAll(varVis.getVars());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		commitCodeLn(unsatString);
		Set<Set<Predicate>> wpc = wpcSolver.computeWpc2(ctx.getChild(3));
		String wpcString = formPredString(wpc, false);
		if(!wpcString.equals("")){
			if(wpcString.startsWith("!")){
				// remove the leading ! because this is placed for the weakest postcondition strings
				wpcString = wpcString.substring(1, wpcString.length());
			}
			commitLn("assert final(" + wpcString + ");");
		} else {
			commitLn("assert final(false);");
		}
		
		spcTestList.add(wpc);
		
		return null;
	}

	@Override
	public Object visitLoad(LoadContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		List<String> readVars = new ArrayList<String>();
		List<String> writeVars = new ArrayList<String>();
		readVars.add(ctx.Identifier(1).getText());
		writeVars.add(ctx.Identifier(0).getText());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		computeAssignmentTransformer(ctx, ctx.getChild(4).getText(), "Statement " + ctx.Identifier(0).getText() + " = " + ctx.Identifier(1).getText());
				
		return null;
	}

	@Override
	public Object visitStore(StoreContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		List<String> readVars = new ArrayList<String>();
		List<String> writeVars = new ArrayList<String>();
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(6));
		readVars.addAll(varVis.getVars());
		writeVars.add(ctx.Identifier().getText());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		boolean setAtomic = false;
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
			inAtomic = true;
			setAtomic = true;
		}
		Map<Predicate, List<Set<Set<Predicate>>>> cubes = computeAssignmentTransformer(ctx, ctx.getChild(4).getText(), "Statement " + ctx.Identifier().getText() + " = " + ctx.getChild(6).getText());
		
		commitComment("Compute affected variables transformers");
		for(String var: varPuts.keySet()){
			Set<PutPredicate> putSet = varPuts.get(var);
			List<PutPredicate> putList = new ArrayList<PutPredicate>(putSet);
			Iterator<PutPredicate> it = putList.iterator();
			while(it.hasNext()){
				PutPredicate pred = it.next();
				// we remove the put predicates that might not be active at this point
				Set<RdmaPredicate> activePreds = new HashSet<RdmaPredicate>(graphSet.get(curProc).getStatementMap().get(ctx));
				activePreds.addAll(rgaRdmaPreds);
				activePreds.addAll(casRdmaPreds);
				if(!activePreds.contains(pred)){
					it.remove();
					continue;
				}
				// we only want the put predicates that read the variable that we just changed
				if(!(pred.getReadVar().equals(ctx.getChild(4).getText()))){
					it.remove();
				}
			}
			if(putList.size() == 0){
				continue;
			}
			
			
			for(PutPredicate pred: putList){
				commitNop();
				commitComment("Statement addtoset " + pred.getSetName() + " , " + ctx.getChild(6).getText());
				
				// we compute the assignment to the special variable
				Map<Predicate, Predicate> predMap = pred.getPredMap();
				Set<Integer> loadSet = getContainedPredIdsMap(cubes, predMap);
				// add the predicates for the join
				for(Predicate assignPred: cubes.keySet()){
					Predicate assignPred2 = predMap.get(assignPred);
					if(assignPred2 == null){
						continue;
					}
					loadSet.add(assignPred2.getId());
				}
				loadPredsToLocal(loadSet);
				for(Predicate assignPred: cubes.keySet()){
					Predicate assignPred2 = predMap.get(assignPred);
					if(assignPred2 == null){
						continue;
					}
					commitPutGetChoose(cubes.get(assignPred).get(0), cubes.get(assignPred).get(1), assignPred2, true, predMap);
				}
				resetLocals(loadSet);
			}
		}
		
		for(String var: varGets.keySet()){
			Set<GetPredicate> getSet = varGets.get(var);
			List<GetPredicate> getList = new ArrayList<GetPredicate>(getSet);
			Iterator<GetPredicate> it = getList.iterator();
			while(it.hasNext()){
				GetPredicate pred = it.next();
				// we remove the put predicates that might not be active at this point
				Set<RdmaPredicate> activePreds = graphSet.get(curProc).getStatementMap().get(ctx);
				activePreds.addAll(rgaRdmaPreds);
				activePreds.addAll(casRdmaPreds);
				if(!activePreds.contains(pred)){
					it.remove();
					continue;
				}
				// we only want the get predicates that read the variable that we just changed
				if(!(pred.getReadVar().equals(ctx.getChild(4).getText()))){
					it.remove();
				}
			}
			if(getList.size() == 0){
				continue;
			}
			
			for(GetPredicate pred: getList){
				commitNop();
				commitComment("Statement addtoset " + pred.getSetName() + " , " + ctx.getChild(6).getText());
				
				// we compute the assignment to the special variable

				Map<Predicate, Predicate> predMap = pred.getPredMap();
				Set<Integer> loadSet = getContainedPredIdsMap(cubes, predMap);
				// add the predicates for the join
				for(Predicate assignPred: cubes.keySet()){
					Predicate assignPred2 = predMap.get(assignPred);
					if(assignPred2 == null){
						continue;
					}
					loadSet.add(assignPred2.getId());
				}
				loadPredsToLocal(loadSet);
				for(Predicate assignPred: cubes.keySet()){
					Predicate assignPred2 = predMap.get(assignPred);
					if(assignPred2 == null){
						continue;
					}
					commitPutGetChoose(cubes.get(assignPred).get(0), cubes.get(assignPred).get(1), assignPred2, true, predMap);
					
				}
				resetLocals(loadSet);
			}
		}
		commitCodeLn(unsatString);
		
		if(setAtomic){
			commitCodeLn("end_atomic;");
			inAtomic = false;
		}
		return null;
	}

	@Override
	public Object visitAssignment(AssignmentContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		List<String> readVars = new ArrayList<String>();
		List<String> writeVars = new ArrayList<String>();
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(2));
		readVars.addAll(varVis.getVars());
		writeVars.add(ctx.Identifier().getText());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		computeAssignmentTransformer(ctx, ctx.getChild(0).getText(), "Statement " + ctx.Identifier().getText() + " = " + ctx.getChild(2).getText());
		
		return null;
	}
	
	@Override
	public Object visitProcess(ProcessContext ctx) {
		int procNumber = Integer.parseInt(ctx.Number().getText());
		curProc = procNumber;
		// commit the process label to the result
		commitLn("process " + procNumber);
		for(int i = 0; i < ctx.statement().size(); i++){
			StatementContext statement =  ctx.statement().get(i);
			if((i == ctx.statement().size() - 1) && statement.getText().toLowerCase().startsWith("assert")){
				// do the last flush before the assertion
				doRemoteOps(null, new ArrayList<String>(), new ArrayList<String>(), true);
				// append the additional processes
				result.append(addProcesses);
			}
			visit(statement);
			if((i == ctx.statement().size() - 1) && !(statement.getText().toLowerCase().startsWith("assert"))){
				// do the last flush after the last statement
				doRemoteOps(null, new ArrayList<String>(), new ArrayList<String>(), true);
			}
		}
		
		return null;
	}
	
	@Override
	public Object visitProgramfile(ProgramfileContext ctx) {
		for(ProcessContext proc: ctx.process()){
			visit(proc.decl());
		}
		// put one unsat cube after the init section
		commitCodeLn(unsatString);
		return super.visitProgramfile(ctx);
	}

	public Set<Integer> getContainedPredIds(List<Set<Set<Predicate>>> setList){
		Set<Integer> preds = new HashSet<Integer>();
		for(Set<Set<Predicate>> predSets: setList){
			if(predSets != null){
				for(Set<Predicate> predSet: predSets){
					for(Predicate pred: predSet){
						preds.add(new Integer(pred.getId()));
					}
				}
			}
		}
		
		return preds;
	}
	
	public Set<Integer> getContainedPredIds(Map<Predicate, List<Set<Set<Predicate>>>> setMap){
		Set<Integer> preds = new HashSet<Integer>();
		for(Predicate keyPred: setMap.keySet()){
			List<Set<Set<Predicate>>> list = setMap.get(keyPred);
			for(Set<Set<Predicate>> predSets: list){
				if(predSets != null){
					for(Set<Predicate> predSet: predSets){
						for(Predicate pred: predSet){
							preds.add(new Integer(pred.getId()));
						}
					}
				}
			}
		}
		return preds;
	}
	
	
	
	public Set<Integer> getContainedPredIdsMap(Map<Predicate, List<Set<Set<Predicate>>>> setMap, Map<Predicate, Predicate> predMap){
		Set<Integer> preds = new HashSet<Integer>();
		for(Predicate keyPred: setMap.keySet()){
			List<Set<Set<Predicate>>> list = setMap.get(keyPred);
			for(Set<Set<Predicate>> predSets: list){
				if(predSets != null){
					for(Set<Predicate> predSet: predSets){
						for(Predicate pred: predSet){
							if(predMap.containsKey(pred)){
								preds.add(new Integer(predMap.get(pred).getId()));
							} else {
								preds.add(new Integer(pred.getId()));
							}
						}
					}
				}
			}
		}
		return preds;
	}
	
	public Set<Integer> getContainedPredIdsMap(Set<Set<Predicate>> preds, Map<Predicate, Predicate> predMap){
		Set<Integer> result = new HashSet<Integer>();
		for(Set<Predicate> predSet: preds){
			for(Predicate pred: predSet){
				if(predMap.containsKey(pred)){
					result.add(predMap.get(pred).getId());
				} else {
					result.add(pred.getId());
				}
			}
		}
		return result;
	}
	
	
	
	public void loadPredsToLocal(Set<Integer> preds){
		for(Integer id: preds){
			if(id != -1){
				// id == -1 represents true or false
				commitCodeLn("load t" + id.toString() + " = B" + id.toString() + ";");
			}
		}
	}
	
	public void loadPredsToLocalAdd(Set<Integer> preds){
		for(Integer id: preds){
			if(id != -1){
				// id == -1 represents true or false
				commitCodeLnAdd("load t" + id.toString() + " = B" + id.toString() + ";");
			}
		}
	}
	
	public void resetLocals(Set<Integer> preds){
		for(Integer id: preds){
			if(id != -1){
				commitCodeLn("t" + id.toString() + " = 0;");
			}
		}
	}
	
	public void resetLocalsAdd(Set<Integer> preds){
		for(Integer id: preds){
			if(id != -1){
				commitCodeLnAdd("t" + id.toString() + " = 0;");
			}
		}
	}
	
	/** used for the conditions of if, assert and while statements */
	public String formPredString(Set<Set<Predicate>> predSets, boolean unsatCube){
		String result = "";
		if(predSets.size()==1 && predSets.contains(unsatSet)){
			// the condition is not satisfiable -> return false to mark unreachable code
			result = "false";
		} else if(predSets.size()==1 && predSets.contains(validSet)){
			// the condition is valid
			result = "true";
		}
		else if(predSets.size() > 0){
			// we have to put a ! in front because we calculated the cubes for the inverted case
			result = "!(";
			for(Set<Predicate> set: predSets){
				result = result + "(";
				for(Predicate pred: set){
					if(pred.getMakeNot()){
						result = result + "(B" + pred.getId() + " == 0)&&";
					} else {
						result = result + "(B" + pred.getId() + " != 0)&&";
					}
				}
				// remove the trailing &&
				result = result.substring(0, result.length() - 2);
				result = result + ")||";
			}
			// remove the last "||"
			result = result.substring(0, result.length() - 2);
			result = result + ")";
		} else {
			if(unsatCube){
				result = "true";
			} else {
				// the condition implies no predicate -> no assume statement will be needed
				result = "";
			}
		}
		return result;
	}
	
	public List<String> getCommitString(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred, boolean addProcess){
		if(addProcess){
			commitCommentAdd("Transform B" + storePred.getId() + " that corresponds to predicate: " + storePred.getPredString());
		} else {
			commitComment("Transform B" + storePred.getId() + " that corresponds to predicate: " + storePred.getPredString());
		}
		String trueString = "";
		String falseString = "";
		if(trueSet.size() == 1 && trueSet.contains(validSet)){
			trueString = "true";
			falseString = "false";
		} else if(trueSet.size() == 1 && trueSet.contains(unsatSet)) {
			trueString = "false";
			falseString = "true";
		} else {
			if(trueSet.size() > 0){
				for(Set<Predicate> set: trueSet){
					trueString = trueString + "(";
					for(Predicate pred: set){
						if(pred.getMakeNot()){
							trueString = trueString + "(t" + pred.getId() + " == 0)&&";
						} else {
							trueString = trueString + "(t" + pred.getId() + " != 0)&&";
						}
					}
					trueString = trueString.substring(0, trueString.length() - 2);
					trueString = trueString + ")||";
				}
				// remove the last "||"
				trueString = trueString.substring(0, trueString.length() - 2);
			} else {
				trueString = "false";
			}
		}
		
		if(falseString.equals("")){
			if(falseSet.size() > 0){
				for(Set<Predicate> set: falseSet){
					falseString = falseString + "(";
					for(Predicate pred: set){
						if(pred.getMakeNot()){
							falseString = falseString + "(t" + pred.getId() + " == 0)&&";
						} else {
							falseString = falseString + "(t" + pred.getId() + " != 0)&&";
						}
					}
					falseString = falseString.substring(0, falseString.length() - 2);
					falseString = falseString + ")||";
				}
				// remove the last "||"
				falseString = falseString.substring(0, falseString.length() - 2);
			} else {
				falseString = "false";
			}
		}
		List<String> result = new ArrayList<String>();
		result.add(trueString);
		result.add(falseString);
		return result;
		}
	
	public List<String> getCommitString(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred, Map<Predicate, Predicate> predMap, boolean addProcess){
		if(addProcess){
			commitCommentAdd("Transform B" + storePred.getId() + " that corresponds to predicate: " + storePred.getPredString());
		} else {
			commitComment("Transform B" + storePred.getId() + " that corresponds to predicate: " + storePred.getPredString());
		}
		String trueString = "";
		String falseString = "";
		if(trueSet.size() == 1 && trueSet.contains(validSet)){
			trueString = "true";
			falseString = "false";
		} else if(trueSet.size() == 1 && trueSet.contains(unsatSet)) {
			trueString = "false";
			falseString = "true";
		} else {
			if(trueSet.size() > 0){
				for(Set<Predicate> set: trueSet){
					trueString = trueString + "(";
					for(Predicate pred: set){
						if(predMap.containsKey(pred)){
							pred = predMap.get(pred);
						}
						if(pred.getMakeNot()){
							trueString = trueString + "(t" + pred.getId() + " == 0)&&";
						} else {
							trueString = trueString + "(t" + pred.getId() + " != 0)&&";
						}
					}
					trueString = trueString.substring(0, trueString.length() - 2);
					trueString = trueString + ")||";
				}
				// remove the last "||"
				trueString = trueString.substring(0, trueString.length() - 2);
			} else {
				trueString = "false";
			}
		}
		
		if(falseString.equals("")){
			if(falseSet.size() > 0){
				for(Set<Predicate> set: falseSet){
					falseString = falseString + "(";
					for(Predicate pred: set){
						if(predMap.containsKey(pred)){
							pred = predMap.get(pred);
						}
						if(pred.getMakeNot()){
							falseString = falseString + "(t" + pred.getId() + " == 0)&&";
						} else {
							falseString = falseString + "(t" + pred.getId() + " != 0)&&";
						}
					}
					falseString = falseString.substring(0, falseString.length() - 2);
					falseString = falseString + ")||";
				}
				// remove the last "||"
				falseString = falseString.substring(0, falseString.length() - 2);
			} else {
				falseString = "false";
			}
		}
		List<String> result = new ArrayList<String>();
		result.add(trueString);
		result.add(falseString);
		return result;
		}
	
	
	public void commitChoose(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred, boolean inRemoteOps){
		List<String> strings = getCommitString(trueSet, falseSet, storePred, false);
		String trueString = strings.get(0);
		String falseString = strings.get(1);
		if(inRemoteOps && ((trueString.equals("true") && falseString.equals("false")) || (trueString.equals("false") && falseString.equals("true")))){
			// we just assign *
			trueString = "false";
			falseString = "false";
		}
		
		commitCodeLn("store B" + storePred.getId() + "= choose(" + trueString + ", " + falseString + ");");
	}
	
	public void commitChooseAdd(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred, boolean inRemoteOps){
		List<String> strings = getCommitString(trueSet, falseSet, storePred, true);
		String trueString = strings.get(0);
		String falseString = strings.get(1);
		if(inRemoteOps && ((trueString.equals("true") && falseString.equals("false")) || (trueString.equals("false") && falseString.equals("true")))){
			// we just assign *
			trueString = "false";
			falseString = "false";
		}
		
		commitCodeLnAdd("store B" + storePred.getId() + "= choose(" + trueString + ", " + falseString + ");");
	}
	
	public void commitChoose(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred, boolean join, Set<Integer> loadSet){
		List<String> strings = getCommitString(trueSet, falseSet, storePred, false);
		String trueString = strings.get(0);
		String falseString = strings.get(1);
		
		if(join){
			if(! loadSet.contains(storePred.getId())){
				commitCodeLn("load t" + storePred.getId() + " = B" + storePred.getId() + ";" );
			}
			commitCodeLn("store B" + storePred.getId() + "= choose((t" + storePred.getId() + " != 0) &&" + trueString + ", (t"+ storePred.getId() + "== 0) &&" + falseString + ");");
			if(! loadSet.contains(storePred.getId())){
				commitCodeLn("store t" + storePred.getId() + " = 0;");
			}
		} else {
			commitCodeLn("store B" + storePred.getId() + "= choose(" + trueString + ", " + falseString + ");");
		}
	}
	
	public void commitChooseAdd(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred, boolean join, Set<Integer> loadSet){
		List<String> strings = getCommitString(trueSet, falseSet, storePred, true);
		String trueString = strings.get(0);
		String falseString = strings.get(1);
		
		if(join){
			if(! loadSet.contains(storePred.getId())){
				commitCodeLnAdd("load t" + storePred.getId() + " = B" + storePred.getId() + ";" );
			}
			commitCodeLnAdd("store B" + storePred.getId() + "= choose((t" + storePred.getId() + " != 0) &&" + trueString + ", (t"+ storePred.getId() + "== 0) &&" + falseString + ");");
			if(! loadSet.contains(storePred.getId())){
				commitCodeLnAdd("store t" + storePred.getId() + " = 0;");
			}
		} else {
			commitCodeLnAdd("store B" + storePred.getId() + "= choose(" + trueString + ", " + falseString + ");");
		}
	}
	
	public void commitPutGetChoose(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred, boolean join, Map<Predicate, Predicate> predMap){
		List<String> strings = getCommitString(trueSet, falseSet, storePred, predMap, false);
		String trueString = strings.get(0);
		String falseString = strings.get(1);	
		
		if(join){
			commitCodeLn("store B" + storePred.getId() + "= choose((t" + storePred.getId() + " != 0) &&"  + trueString + ", (t"+ storePred.getId() + "== 0) &&" + falseString + ");");
		} else {
			commitCodeLn("store B" + storePred.getId() + "= choose(" + trueString + ", " + falseString + ");");
		}
	}
	
	public void commitPutGetChooseAdd(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred, boolean join, Map<Predicate, Predicate> predMap){
		List<String> strings = getCommitString(trueSet, falseSet, storePred, predMap, true);
		String trueString = strings.get(0);
		String falseString = strings.get(1);	
		
		if(join){
			commitCodeLnAdd("store B" + storePred.getId() + "= choose((t" + storePred.getId() + " != 0) &&"  + trueString + ", (t"+ storePred.getId() + "== 0) &&" + falseString + ");");
		} else {
			commitCodeLnAdd("store B" + storePred.getId() + "= choose(" + trueString + ", " + falseString + ");");
		}
	}

	/** returns a unique number at each call used for the program labels */
	int getNewLabel(){
		return label++;
	}
	
	void commitCodeLn(String text){
		commitLn(getNewLabel() + ": " + text);
	}
	
	/** adds text to the string builder and adds a new line character at the end */
	void commitLn(String text){
		result.append(text + "\r\n");
	}
	
	void commitNop(){
		commitLn(getNewLabel() + ": nop;");
	}
	
	void commitComment(String text){
		commitLn("/* " + text + " */");
	}
	
	void commitCodeLnAdd(String text){
		commitLnAdd(getNewLabel() + ": " + text);
	}
	
	void commitLnAdd(String text){
		addProcesses.append(text + "\r\n");
	}
	
	void commitNopAdd(){
		commitLnAdd(getNewLabel() + ": nop;");
	}
	
	void commitCommentAdd(String text){
		commitLnAdd("/* " + text + " */");
	}
	
	@Override
	public Object visitFlush(FlushContext ctx) {
		for(Flush f: flushes){
			if(f.getContext().equals(ctx)){
				// we don't include the flush because we want to verify without it
				return null;
			}
		}
		boolean setAtomic = false;
		int srcProc = curProc;
		int dstProc = Integer.parseInt(ctx.Number().getText());
		if(!inAtomic){
			inAtomic = true;
			setAtomic = true;
		}
		
		// get the list of active rdma statements
		Set<RdmaPredicate> activeStmts = new HashSet<RdmaPredicate>(graphSet.get(curProc).getStatementMap().get(ctx));
		activeStmts.addAll(rgaRdmaPreds);
		activeStmts.addAll(casRdmaPreds);
		Set<RdmaPredicate> activeStmtsFiltered = new HashSet<RdmaPredicate>();
		Set<RdmaPredicate> activeStmtsRemaining = new HashSet<RdmaPredicate>();
		
		// only add the predicates that match the src and dst process number
		for(RdmaPredicate pred: activeStmts){
			if(pred.getSrcProcNr() == srcProc && pred.getDstProcNr() == dstProc){
				activeStmtsFiltered.add(pred);
			} else {
				activeStmtsRemaining.add(pred);
			}
		}
		
		if(setAtomic){
			// only do this if we are not in an atomic section
			/* now add all predicates that read a variable that one in activeStmtsFiltered writes to 
			 * or write to the same variable as one in activeStmtsFiltered */
			boolean changed = true;
			while(changed){
				changed = false;
				boolean found = false;
				Iterator<RdmaPredicate> it = activeStmtsRemaining.iterator();
				while(it.hasNext()){
					RdmaPredicate remainingPred = it.next();
					for(RdmaPredicate filteredPred: activeStmtsFiltered){
						if(remainingPred.getWriteVar().equals(filteredPred.getReadVar()) || remainingPred.getWriteVar().equals(filteredPred.getWriteVar())){
							changed = true;
							found = true;
							break;
						}
					}
					if(found){
						found = false;
						activeStmtsFiltered.add(remainingPred);
						it.remove();
					}
				}
			}
		}
		
		// only proceed if there is something to execute
		if(activeStmtsFiltered.isEmpty()){
			return null;
		}
		
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		if(setAtomic){
			commitCodeLn("begin_atomic;");
		}		
		
		// now activeStmtsFiltered contains all rdma statements that should have the chance to be executed
		int loopStartLabel = getNewLabel();
		int checkLabel = getNewLabel();
		// now enter a loop that contains the transformer for all those statements
		
		// loop start
		commitCodeLn("if (true) goto " + checkLabel + ";");
		commitLn(loopStartLabel + ": nop;");
		
		for(RdmaPredicate pred: activeStmtsFiltered){
			if(pred instanceof RgaPredicate || pred instanceof CasPredicate){
				continue;
			}
			int afterLabel2 = getNewLabel();
			commitCodeLn("if (*) goto " + afterLabel2 + ";");
			commitNop();
			commitComment("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
			commitCodeLn("assume (B" + pred.getId() + " != 0);");
			commitNop();
			commitComment("Statement setassign " + pred.getWriteVar() + " = " + pred.getSetName());
			Map<Predicate, List<Set<Set<Predicate>>>> transformer = null;
			if(pred instanceof PutPredicate){
				transformer = putAssignmentTransformers.get(pred);
			} else if (pred instanceof GetPredicate){
				transformer = getAssignmentTransformers.get(pred);
			}
			Set<Integer> loadSet = getContainedPredIds(transformer);
			// load the predicates for the affected set predicates to the locals
			for(Predicate changePred: transformer.keySet()){
				for(RdmaPredicate addPred: activeStmts){
					if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
						loadSet.addAll(getContainedPredIdsMap(transformer.get(changePred).get(0), addPred.getPredMap()));
						loadSet.addAll(getContainedPredIdsMap(transformer.get(changePred).get(1), addPred.getPredMap()));
						// load the changed predicate for the join
						loadSet.add(addPred.getPredMap().get(changePred).getId());
					}
				}
			}
			loadPredsToLocal(loadSet);					
			for(Predicate transPred: transformer.keySet()){
				List<Set<Set<Predicate>>> transList = transformer.get(transPred);
				commitChoose(transList.get(0), transList.get(1), transPred, true);
			}
			// change the affected set predicates
			for(RdmaPredicate addPred: activeStmts){
				boolean found = false;
				// first check if this rdma statement is affected
				for(Predicate changePred: transformer.keySet()){
					if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
						found = true;
						break;
					}
				}
				if(found){
					// the statement is affected
					for(Predicate changePred: transformer.keySet()){
						if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
							commitPutGetChoose(transformer.get(changePred).get(0), transformer.get(changePred).get(1), addPred.getPredMap().get(changePred), true, addPred.getPredMap());
						}
					}
				}
			}
			resetLocals(loadSet);
			commitCodeLn(unsatString);
			
			// now go over all active predicates again and add to their set if we just wrote to a variable that they read from
			for(RdmaPredicate addToSetPred: activeStmtsFiltered){
				if(addToSetPred instanceof RgaPredicate || addToSetPred instanceof CasPredicate){
					continue;
				}
				if(pred != addToSetPred && addToSetPred.getReadVar().equals(pred.getWriteVar())){
					commitNop();
					commitComment("Statement addtoset " + addToSetPred.getSetName() + " , " + pred.getSetName());
					
					Map<Predicate, List<Set<Set<Predicate>>>> addTransformer = null;
					if(addToSetPred instanceof GetPredicate){
						GetPredicate getAddToSetPred = (GetPredicate) addToSetPred;
						if(!getSetTransformers.containsKey(getAddToSetPred)){
							// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
							Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
							for(Predicate transPred: getAddToSetPred.getPredMap().keySet()){
								if(!transPred.getMakeNot()){
									// only care about the regular predicates because we would get them 2 times for the negation
									List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
									Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
									Set<Predicate> cubeSet = new HashSet<Predicate>();
									cubeSet.add(transPred);
									trueSet.add(cubeSet);
									transList.add(trueSet);
									boolean error = true;
									// now search for the negated predicate
									for(Predicate negPred: getAddToSetPred.getPredMap().keySet()){
										if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
											// add the negative predicate to the falseSet
											Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
											cubeSet = new HashSet<Predicate>();
											cubeSet.add(negPred);
											falseSet.add(cubeSet);
											transList.add(falseSet);
											error = false;
											break;
										}
									}
									if(error){
										// we should not get here
										System.out.println("error: " + addToSetPred.getText());
									}
									newCubes.put(getAddToSetPred.getPredMap().get(transPred), transList);
								}
							}
							getSetTransformers.put(getAddToSetPred, newCubes);
						}
						addTransformer = getSetTransformers.get(getAddToSetPred);
					} else {
						PutPredicate putAddToSetPred = (PutPredicate) addToSetPred;
						if(!putSetTransformers.containsKey(putAddToSetPred)){							
							// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
							Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
							for(Predicate transPred: putAddToSetPred.getPredMap().keySet()){
								if(!transPred.getMakeNot()){
									// only care about the regular predicates because we would get them 2 times for the negation
									List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
									Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
									Set<Predicate> cubeSet = new HashSet<Predicate>();
									cubeSet.add(transPred);
									trueSet.add(cubeSet);
									transList.add(trueSet);
									boolean error = true;
									// now search for the negated predicate
									for(Predicate negPred: putAddToSetPred.getPredMap().keySet()){
										if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
											// add the negative predicate to the falseSet
											Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
											cubeSet = new HashSet<Predicate>();
											cubeSet.add(negPred);
											falseSet.add(cubeSet);
											transList.add(falseSet);
											error = false;
											break;
										}
									}
									if(error){
										// we should not get here
										System.out.println("error: " + addToSetPred.getText());
									}
									newCubes.put(putAddToSetPred.getPredMap().get(transPred), transList);
								}
							}
							putSetTransformers.put(putAddToSetPred, newCubes);
						}
						addTransformer = putSetTransformers.get(putAddToSetPred);
					}
					
					Set<Integer> addLoadSet = getContainedPredIds(addTransformer);
					
					loadPredsToLocal(addLoadSet);					
					for(Predicate transPred: addTransformer.keySet()){
						List<Set<Set<Predicate>>> transList = addTransformer.get(transPred);
						commitChoose(transList.get(0), transList.get(1), transPred, true, addLoadSet);
					}					
					resetLocals(addLoadSet);
					commitCodeLn(unsatString);					
				}
			}
			if(!pred.getInLoop() || (pred.getInLoop() && pred.getFlushFollowed())){
				// the rdma statement was not in a loop (or followed by flush) -> set it to inactive
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 0");
				commitCodeLn("store B" + pred.getId() + " = 0;");
			} else if(pred.getInLoop() && srcProc == pred.getSrcProcNr() && dstProc == pred.getDstProcNr()){
				// it was in a loop and should be flushed -> non-deterministically set it to inactive
				int afterAssignment = getNewLabel();
				commitCodeLn("if (*) goto " + afterAssignment + ";");
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 0");
				commitCodeLn("store B" + pred.getId() + " = 0;");
				commitLn(afterAssignment + ": nop;");
			} else {
				// statement is in a loop but should not be flushed -> do nothing
			}
			commitLn(afterLabel2 + ": nop;");
		}
		// end loop
		commitLn(checkLabel + ": nop;");
		commitCodeLn("if (*) goto " + loopStartLabel + ";");
		commitCodeLn(unsatString);
		
		//now assume that all the operations with matching dst and src process are flushed
		for(RdmaPredicate pred: graphSet.get(curProc).getStatementMap().get(ctx)){
			if(srcProc == pred.getSrcProcNr() && dstProc == pred.getDstProcNr()){
				commitNop();
				commitComment("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
				commitCodeLn("assume (B" + pred.getId() + " == 0);");
				
				if(pred instanceof RgaPredicate){
					// also assume that the contained get and put is not active
					RgaPredicate rgaPred = (RgaPredicate) pred;
					commitNop();
					commitComment("Statement assume (RdmaOperation" + rgaPred.getGetPred().getId() + " = 0)");
					commitCodeLn("assume (B" + rgaPred.getGetPred().getId() + " == 0);");
					commitNop();
					commitComment("Statement assume (RdmaOperation" + rgaPred.getPutPred().getId() + " = 0)");
					commitCodeLn("assume (B" + rgaPred.getPutPred().getId() + " == 0);");
				}
				if(pred instanceof CasPredicate){
					// also assume that the contained gets and put is not active
					CasPredicate casPred = (CasPredicate) pred;
					commitNop();
					commitComment("Statement assume (RdmaOperation" + casPred.getGetPred1().getId() + " = 0)");
					commitCodeLn("assume (B" + casPred.getGetPred1().getId() + " == 0);");
					commitNop();
					commitComment("Statement assume (RdmaOperation" + casPred.getGetPred2().getId() + " = 0)");
					commitCodeLn("assume (B" + casPred.getGetPred2().getId() + " == 0);");
					commitNop();
					commitComment("Statement assume (RdmaOperation" + casPred.getPutPred().getId() + " = 0)");
					commitCodeLn("assume (B" + casPred.getPutPred().getId() + " == 0);");
				}
			}
		}
		
		if(setAtomic){
			commitCodeLn("end_atomic;");
			inAtomic = false;
		}
		
		return null;
	}
	
	public Map<Predicate, List<Set<Set<Predicate>>>> computeRdmaAssignmentTransformer(String identifier, String identifier2, boolean inAtomicStatement, Set<Predicate> addPreds, Set<Predicate> addNotPreds, boolean commit, boolean putGetSetAssignment){
		boolean setAtomic = false;
		if(! inAtomic && ! inAtomicStatement && commit){
			commitCodeLn("begin_atomic;");
			inAtomic = true;
			setAtomic = true;
		}
		Set<Predicate> predSet = new HashSet<Predicate>(predicates);
		predSet.addAll(addPreds);
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predSet);
		if(putGetSetAssignment){
			// remove all predicates that don't contain the set
			Iterator<Predicate> it = preds.iterator();
			while(it.hasNext()){
				Predicate affectedPred = it.next();
				if(!affectedPred.getContainedVars().contains(identifier)){
					//it.remove();
				}
			}
		}
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		Map<Predicate, List<Set<Set<Predicate>>>> result = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
		for(Predicate pred: preds){
			List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(identifier, identifier2, pred, addPreds, addNotPreds);
			result.put(pred, wpcs);
			Set<Set<Predicate>> wpc = wpcs.get(0);
			Set<Set<Predicate>> notWpc = wpcs.get(1);
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
		}
		
		if(commit){
			Set<Integer> loadSet = getContainedPredIds(wpcList);
			loadSet.addAll(getContainedPredIds(notWpcList));
			// load the predicates for the affected set predicates to the locals
			for(int i = 0; i < predList.size(); i++){
				Predicate changePred = predList.get(i);
				for(PutPredicate pred: putPreds.values()){
					if(pred.getPredMap().containsKey(changePred) && pred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						loadSet.addAll(getContainedPredIdsMap(wpcList.get(i), pred.getPredMap()));
						loadSet.addAll(getContainedPredIdsMap(notWpcList.get(i), pred.getPredMap()));
						// load the changed predicate for the join
						loadSet.add(pred.getPredMap().get(changePred).getId());
					}
				}
				for(GetPredicate pred: getPreds.values()){
					if(pred.getPredMap().containsKey(changePred) && pred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						loadSet.addAll(getContainedPredIdsMap(wpcList.get(i), pred.getPredMap()));
						loadSet.addAll(getContainedPredIdsMap(notWpcList.get(i), pred.getPredMap()));
						// load the changed predicate for the join					
						loadSet.add(pred.getPredMap().get(changePred).getId());
					}
				}
				for(RdmaPredicate pred: rgaRdmaPreds){
					if(pred.getPredMap().containsKey(changePred) && pred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						loadSet.addAll(getContainedPredIdsMap(wpcList.get(i), pred.getPredMap()));
						loadSet.addAll(getContainedPredIdsMap(notWpcList.get(i), pred.getPredMap()));
						// load the changed predicate for the join					
						loadSet.add(pred.getPredMap().get(changePred).getId());
					}
				}
				for(RdmaPredicate pred: casRdmaPreds){
					if(pred.getPredMap().containsKey(changePred) && pred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						loadSet.addAll(getContainedPredIdsMap(wpcList.get(i), pred.getPredMap()));
						loadSet.addAll(getContainedPredIdsMap(notWpcList.get(i), pred.getPredMap()));
						// load the changed predicate for the join					
						loadSet.add(pred.getPredMap().get(changePred).getId());
					}
				}
			}
			loadPredsToLocal(loadSet);
			
			for(int i = 0; i < wpcList.size(); i++){
				commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i), false);
			}
			// change the affected set predicates
			for(int i = 0; i < wpcList.size(); i++){
				Predicate changePred = predList.get(i);
				for(PutPredicate pred: putPreds.values()){
					if(pred.getPredMap().containsKey(changePred)&& pred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						int elseLabel = getNewLabel();
						int afterLabel = getNewLabel();
						commitCodeLn("if (*) goto " + elseLabel + " ;");
						commitNop();
						commitComment("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
						commitCodeLn("assume (B" + pred.getId() + " != 0);");
						commitPutGetChoose(wpcList.get(i), notWpcList.get(i), pred.getPredMap().get(changePred), true, pred.getPredMap());
						commitCodeLn("if (true) goto " + afterLabel + " ;");
						commitLn(elseLabel + ": nop;");
						commitNop();
						commitComment("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
						commitCodeLn("assume (B" + pred.getId() + " == 0);");
						commitLn(afterLabel + ": nop;");
					}
				}
				for(GetPredicate pred: getPreds.values()){
					if(pred.getPredMap().containsKey(changePred)&& pred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						int elseLabel = getNewLabel();
						int afterLabel = getNewLabel();
						commitCodeLn("if (*) goto " + elseLabel + " ;");
						commitNop();
						commitComment("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
						commitCodeLn("assume (B" + pred.getId() + " != 0);");
						commitPutGetChoose(wpcList.get(i), notWpcList.get(i), pred.getPredMap().get(changePred), true, pred.getPredMap());
						commitCodeLn("if (true) goto " + afterLabel + " ;");
						commitLn(elseLabel + ": nop;");
						commitNop();
						commitComment("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
						commitCodeLn("assume (B" + pred.getId() + " == 0);");
						commitLn(afterLabel + ": nop;");
					}
				}
				for(RdmaPredicate pred: rgaRdmaPreds){
					if(pred.getPredMap().containsKey(changePred)&& pred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						int elseLabel = getNewLabel();
						int afterLabel = getNewLabel();
						commitCodeLn("if (*) goto " + elseLabel + " ;");
						commitNop();
						commitComment("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
						commitCodeLn("assume (B" + pred.getId() + " != 0);");
						commitPutGetChoose(wpcList.get(i), notWpcList.get(i), pred.getPredMap().get(changePred), true, pred.getPredMap());
						commitCodeLn("if (true) goto " + afterLabel + " ;");
						commitLn(elseLabel + ": nop;");
						commitNop();
						commitComment("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
						commitCodeLn("assume (B" + pred.getId() + " == 0);");
						commitLn(afterLabel + ": nop;");
					}
				}
				for(RdmaPredicate pred: casRdmaPreds){
					if(pred.getPredMap().containsKey(changePred)&& pred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						int elseLabel = getNewLabel();
						int afterLabel = getNewLabel();
						commitCodeLn("if (*) goto " + elseLabel + " ;");
						commitNop();
						commitComment("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
						commitCodeLn("assume (B" + pred.getId() + " != 0);");
						commitPutGetChoose(wpcList.get(i), notWpcList.get(i), pred.getPredMap().get(changePred), true, pred.getPredMap());
						commitCodeLn("if (true) goto " + afterLabel + " ;");
						commitLn(elseLabel + ": nop;");
						commitNop();
						commitComment("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
						commitCodeLn("assume (B" + pred.getId() + " == 0);");
						commitLn(afterLabel + ": nop;");
					}
				}
			}
			
			resetLocals(loadSet);
			commitCodeLn(unsatString);
			if(setAtomic){
				commitCodeLn("end_atomic;");
				inAtomic = false;
			}
		}
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
		return result; 
	}
	
	public void doRemoteOps(ParseTree ctx, List<String> readVars, List<String> writeVars, boolean finalStatement){
		
		// treat the variables in the assertion as a read
		readVars.addAll(assertionVars);
		
		Set<RdmaPredicate> activeStmts;
		Set<RdmaPredicate> activeStmtsFiltered;
		Set<RdmaPredicate> activeStmtsRemaining;
		
		if(finalStatement){
			activeStmtsFiltered = new HashSet<RdmaPredicate>();
			activeStmtsFiltered.addAll(putPreds.values());
			activeStmtsFiltered.addAll(getPreds.values());
			for(RgaPredicate pred: rgaPreds.values()){
				activeStmtsFiltered.add(pred.getGetPred());
				activeStmtsFiltered.add(pred.getPutPred());
			}
			for(CasPredicate pred: casPreds.values()){
				activeStmtsFiltered.add(pred.getGetPred1());
				activeStmtsFiltered.add(pred.getGetPred2());
				activeStmtsFiltered.add(pred.getPutPred());
			}
			activeStmts = new HashSet<RdmaPredicate>(activeStmtsFiltered);
		} else {
			// get the list of active rdma statements
			activeStmts = new HashSet<RdmaPredicate>(graphSet.get(curProc).getStatementMap().get(ctx));
			for(RgaPredicate pred: rgaPreds.values()){
				activeStmts.add(pred.getGetPred());
				activeStmts.add(pred.getPutPred());
				// remove the RGA predicate itself because it's not handled in here
				activeStmts.remove(pred);
			}
			for(CasPredicate pred: casPreds.values()){
				activeStmts.add(pred.getGetPred1());
				activeStmts.add(pred.getGetPred2());
				activeStmts.add(pred.getPutPred());
				// remove the CAS predicate itself because it's not handled in here
				activeStmts.remove(pred);
			}
			activeStmtsFiltered = new HashSet<RdmaPredicate>();
			activeStmtsRemaining = new HashSet<RdmaPredicate>();
			
			// only add the predicates that write to a variable in readVars or writeVars
			for(RdmaPredicate pred: activeStmts){
				if(readVars.contains(pred.getWriteVar()) || writeVars.contains(pred.getWriteVar())){
					activeStmtsFiltered.add(pred);
				} else {
					activeStmtsRemaining.add(pred);
				}
			}
			
			/* now add all predicates that read a variable that one in activeStmtsFiltered writes to 
			 * or write to the same variable as one in activeStmtsFiltered */
			boolean changed = true;
			while(changed){
				changed = false;
				boolean found = false;
				Iterator<RdmaPredicate> it = activeStmtsRemaining.iterator();
				while(it.hasNext()){
					RdmaPredicate remainingPred = it.next();
					for(RdmaPredicate filteredPred: activeStmtsFiltered){
						if(remainingPred.getWriteVar().equals(filteredPred.getReadVar()) || remainingPred.getWriteVar().equals(filteredPred.getWriteVar())){
							changed = true;
							found = true;
							break;
						}
					}
					if(found){
						found = false;
						activeStmtsFiltered.add(remainingPred);
						it.remove();
					}
				}
			}
		}
		// only proceed if we have something to execute
		if(activeStmtsFiltered.isEmpty()){
			return;
		}
		boolean setAtomic = false;
		if(!inAtomic){
			commitCodeLn("begin_atomic;");
			inAtomic = true;
			setAtomic = true;
		}
		commitLn("");
		commitLn("");
		commitComment("doRemoteOps");
		
		inAtomic = true;
		
		// now activeStmtsFiltered contains all rdma statements that should have the chance to be executed
		
		int loopStartLabel = getNewLabel();
		int checkLabel = getNewLabel();
		
		// now enter a loop that contains the transformer for all those statements
		
		// loop start
		commitCodeLn("if (true) goto " + checkLabel + ";");
		commitLn(loopStartLabel + ": nop;");
		
		for(RdmaPredicate pred: activeStmtsFiltered){
			int afterLabel2 = getNewLabel();
			commitCodeLn("if (*) goto " + afterLabel2 + ";");
			commitNop();
			commitComment("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
			commitCodeLn("assume (B" + pred.getId() + " != 0);");
			commitNop();
			commitComment("Statement setassign " + pred.getWriteVar() + " = " + pred.getSetName());
			Map<Predicate, List<Set<Set<Predicate>>>> transformer = null;
			if(pred instanceof PutPredicate){
				transformer = putAssignmentTransformers.get(pred);
			} else if (pred instanceof GetPredicate){
				transformer = getAssignmentTransformers.get(pred);
			}
			
			Set<Integer> loadSet = getContainedPredIds(transformer);
			
			// load the predicates for the affected set predicates to the locals
			for(Predicate changePred: transformer.keySet()){
				for(RdmaPredicate addPred: activeStmts){
					if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
						loadSet.addAll(getContainedPredIdsMap(transformer.get(changePred).get(0), addPred.getPredMap()));
						loadSet.addAll(getContainedPredIdsMap(transformer.get(changePred).get(1), addPred.getPredMap()));
						// load the changed predicate for the join
						loadSet.add(addPred.getPredMap().get(changePred).getId());
					}
				}
			}
			
			loadPredsToLocal(loadSet);					
			for(Predicate transPred: transformer.keySet()){
				List<Set<Set<Predicate>>> transList = transformer.get(transPred);
				commitChoose(transList.get(0), transList.get(1), transPred, true);
			}
			
			// change the affected set predicates
			for(RdmaPredicate addPred: activeStmts){
				boolean found = false;
				// first check if this rdma statement is affected
				for(Predicate changePred: transformer.keySet()){
					if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
						found = true;
						break;
					}
				}
				if(found){
					// the statement is affected
					for(Predicate changePred: transformer.keySet()){
						if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
							//commitPutGetChoose(transformer.get(changePred).get(0), transformer.get(changePred).get(1), addPred.getPredMap().get(changePred), true, addPred.getPredMap());
							// assign * here in order to be sound with the predicates that we have
							commitCodeLn("store B" + addPred.getPredMap().get(changePred).getId() + "= choose(false, false);");
							
						}
					}
				}
			}
			
			resetLocals(loadSet);
			commitCodeLn(unsatString);
			
			// now go over all active predicates again and add to their set if we just wrote to a variable that they read from
			for(RdmaPredicate addToSetPred: activeStmtsFiltered){
				if(pred != addToSetPred && addToSetPred.getReadVar().equals(pred.getWriteVar())){
					commitNop();
					commitComment("Statement addtoset " + addToSetPred.getSetName() + " , " + pred.getSetName());
					Map<Predicate, List<Set<Set<Predicate>>>> addTransformer = null;
					if(addToSetPred instanceof GetPredicate){
						GetPredicate getAddToSetPred = (GetPredicate) addToSetPred;
						if(!getSetTransformers.containsKey(getAddToSetPred)){
							//getSetTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getReadVar() + "_SetProc" + pred.getSrcProcNr(), pred.getReadVar(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
							// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
							Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
							for(Predicate transPred: getAddToSetPred.getPredMap().keySet()){
								if(!transPred.getMakeNot()){
									// only care about the regular predicates because we would get them 2 times for the negation
									List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
									Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
									Set<Predicate> cubeSet = new HashSet<Predicate>();
									cubeSet.add(transPred);
									trueSet.add(cubeSet);
									transList.add(trueSet);
									boolean error = true;
									// now search for the negated predicate
									for(Predicate negPred: getAddToSetPred.getPredMap().keySet()){
										if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
											// add the negative predicate to the falseSet
											Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
											cubeSet = new HashSet<Predicate>();
											cubeSet.add(negPred);
											falseSet.add(cubeSet);
											transList.add(falseSet);
											error = false;
											break;
										}
									}
									if(error){
										// we should not get here
										System.out.println("error: " + addToSetPred.getText());
									}
									newCubes.put(getAddToSetPred.getPredMap().get(transPred), transList);
								}
							}
							getSetTransformers.put(getAddToSetPred, newCubes);
						}
						addTransformer = getSetTransformers.get(getAddToSetPred);
					} else {
						PutPredicate putAddToSetPred = (PutPredicate) addToSetPred;
						if(!putSetTransformers.containsKey(putAddToSetPred)){
							//getSetTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getReadVar() + "_SetProc" + pred.getSrcProcNr(), pred.getReadVar(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
							// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
							Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
							for(Predicate transPred: putAddToSetPred.getPredMap().keySet()){
								if(!transPred.getMakeNot()){
									// only care about the regular predicates because we would get them 2 times for the negation
									List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
									Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
									Set<Predicate> cubeSet = new HashSet<Predicate>();
									cubeSet.add(transPred);
									trueSet.add(cubeSet);
									transList.add(trueSet);
									boolean error= true;
									// now search for the negated predicate
									for(Predicate negPred: putAddToSetPred.getPredMap().keySet()){
										if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
											// add the negative predicate to the falseSet
											Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
											cubeSet = new HashSet<Predicate>();
											cubeSet.add(negPred);
											falseSet.add(cubeSet);
											transList.add(falseSet);
											error = false;
											break;
										}
									}
									if(error){
										// we should not get here
										System.out.println("error: " + addToSetPred.getText());
									}
									newCubes.put(putAddToSetPred.getPredMap().get(transPred), transList);
								}
							}
							putSetTransformers.put(putAddToSetPred, newCubes);
						}
						addTransformer = putSetTransformers.get(putAddToSetPred);
					}
					Set<Integer> addLoadSet = getContainedPredIds(addTransformer);
					loadPredsToLocal(addLoadSet);					
					for(Predicate transPred: addTransformer.keySet()){
						List<Set<Set<Predicate>>> transList = addTransformer.get(transPred);
						commitChoose(transList.get(0), transList.get(1), transPred, true, addLoadSet);
					}
					resetLocals(addLoadSet);
					commitCodeLn(unsatString);
				}
			}
			commitCodeLn(unsatString);
			if(!pred.getInLoop() || (pred.getInLoop() && pred.getFlushFollowed())){
				// the executed rdma statement was not in a loop (or followed by flush), so we have to set it to inactive after one execution
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 0");
				commitCodeLn("store B" + pred.getId() + " = 0;");
			}
			commitLn(afterLabel2 + ": nop;");
		}
		// end loop
		commitLn(checkLabel + ": nop;");
		commitCodeLn("if (*) goto " + loopStartLabel + ";");
		commitCodeLn(unsatString);
		if(setAtomic){
			commitCodeLn("end_atomic;");
			inAtomic = false;
		}
	}
	
	public void doRemoteOpsAdd(List<String> readVars, List<String> writeVars, boolean inAtomic){
		// treat the variables in the assertion as a read
		readVars.addAll(assertionVars);	
		Set<RdmaPredicate> activeStmts;
		Set<RdmaPredicate> activeStmtsFiltered;
		Set<RdmaPredicate> activeStmtsRemaining;
		// get the list of active rdma statements
		activeStmts = new HashSet<RdmaPredicate>(putPreds.values());
		activeStmts.addAll(getPreds.values());
		for(RgaPredicate pred: rgaPreds.values()){
			activeStmts.add(pred.getGetPred());
			activeStmts.add(pred.getPutPred());
			// remove the RGA predicate itself because it's not handled in here
			activeStmts.remove(pred);
		}
		for(CasPredicate pred: casPreds.values()){
			activeStmts.add(pred.getGetPred1());
			activeStmts.add(pred.getGetPred2());
			activeStmts.add(pred.getPutPred());
			// remove the CAS predicate itself because it's not handled in here
			activeStmts.remove(pred);
		}
		activeStmtsFiltered = new HashSet<RdmaPredicate>();
		activeStmtsRemaining = new HashSet<RdmaPredicate>();
		
		// only add the predicates that write to a variable in readVars or writeVars
		for(RdmaPredicate pred: activeStmts){
			if(readVars.contains(pred.getWriteVar()) || writeVars.contains(pred.getWriteVar())){
				activeStmtsFiltered.add(pred);
			} else {
				activeStmtsRemaining.add(pred);
			}
		}
		/* now add all predicates that read a variable that one in activeStmtsFiltered writes to 
		 * or write to the same variable as one in activeStmtsFiltered */
		boolean changed = true;
		while(changed){
			changed = false;
			boolean found = false;
			Iterator<RdmaPredicate> it = activeStmtsRemaining.iterator();
			while(it.hasNext()){
				RdmaPredicate remainingPred = it.next();
				for(RdmaPredicate filteredPred: activeStmtsFiltered){
					if(remainingPred.getWriteVar().equals(filteredPred.getReadVar()) || remainingPred.getWriteVar().equals(filteredPred.getWriteVar())){
						changed = true;
						found = true;
						break;
					}
				}
				if(found){
					found = false;
					activeStmtsFiltered.add(remainingPred);
					it.remove();
				}
			}
		}
		// only proceed if we have something to execute
		if(activeStmtsFiltered.isEmpty()){
			return;
		}
		if(! inAtomic){
			commitCodeLnAdd("begin_atomic;");
		}
		commitLnAdd("");
		commitLnAdd("");
		commitCommentAdd("doRemoteOps");
		
		inAtomic = true;
		
		// now activeStmtsFiltered contains all rdma statements that should have the chance to be executed
		int loopStartLabel = getNewLabel();
		int checkLabel = getNewLabel();
		
		// now enter a loop that contains the transformer for all those statements
		// loop start
		commitCodeLnAdd("if (true) goto " + checkLabel + ";");
		commitLnAdd(loopStartLabel + ": nop;");
		
		for(RdmaPredicate pred: activeStmtsFiltered){
			int afterLabel2 = getNewLabel();
			commitCodeLnAdd("if (*) goto " + afterLabel2 + ";");
			commitNopAdd();
			commitCommentAdd("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
			commitCodeLnAdd("assume (B" + pred.getId() + " != 0);");
			commitNopAdd();
			commitCommentAdd("Statement setassign " + pred.getWriteVar() + " = " + pred.getSetName());
			Map<Predicate, List<Set<Set<Predicate>>>> transformer = null;
			if(pred instanceof PutPredicate){
				transformer = putAssignmentTransformers.get(pred);
			} else if (pred instanceof GetPredicate){
				transformer = getAssignmentTransformers.get(pred);
			}
			
			Set<Integer> loadSet = getContainedPredIds(transformer);
			// load the predicates for the affected set predicates to the locals
			for(Predicate changePred: transformer.keySet()){
				for(RdmaPredicate addPred: activeStmts){
					if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
						loadSet.addAll(getContainedPredIdsMap(transformer.get(changePred).get(0), addPred.getPredMap()));
						loadSet.addAll(getContainedPredIdsMap(transformer.get(changePred).get(1), addPred.getPredMap()));
						// load the changed predicate for the join
						loadSet.add(addPred.getPredMap().get(changePred).getId());
					}
				}
			}
			
			loadPredsToLocalAdd(loadSet);					
			for(Predicate transPred: transformer.keySet()){
				List<Set<Set<Predicate>>> transList = transformer.get(transPred);
				commitChooseAdd(transList.get(0), transList.get(1), transPred, true);
			}
			// change the affected set predicates
			for(RdmaPredicate addPred: activeStmts){
				boolean found = false;
				// first check if this rdma statement is affected
				for(Predicate changePred: transformer.keySet()){
					if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
						found = true;
						break;
					}
				}
				if(found){
					// the statement is affected
					for(Predicate changePred: transformer.keySet()){
						if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(pred.getWriteVar())){
							//commitPutGetChooseAdd(transformer.get(changePred).get(0), transformer.get(changePred).get(1), addPred.getPredMap().get(changePred), true, addPred.getPredMap());
							// assign * here in order to be sound with the predicates that we have
							commitCodeLnAdd("store B" + addPred.getPredMap().get(changePred).getId() + "= choose(false, false);");
						}
					}
				}
			}
			resetLocalsAdd(loadSet);
			commitCodeLnAdd(unsatString);
			
			// now go over all active predicates again and add to their set if we just wrote to a variable that they read from
			for(RdmaPredicate addToSetPred: activeStmtsFiltered){
				if(pred != addToSetPred && addToSetPred.getReadVar().equals(pred.getWriteVar())){
					commitNopAdd();
					commitCommentAdd("Statement addtoset " + addToSetPred.getSetName() + " , " + pred.getSetName());
					
					Map<Predicate, List<Set<Set<Predicate>>>> addTransformer = null;
					if(addToSetPred instanceof GetPredicate){
						GetPredicate getAddToSetPred = (GetPredicate) addToSetPred;
						if(!getSetTransformers.containsKey(getAddToSetPred)){
							//getSetTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getReadVar() + "_SetProc" + pred.getSrcProcNr(), pred.getReadVar(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
							// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
							Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
							for(Predicate transPred: getAddToSetPred.getPredMap().keySet()){
								if(!transPred.getMakeNot()){
									// only care about the regular predicates because we would get them 2 times for the negation
									List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
									Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
									Set<Predicate> cubeSet = new HashSet<Predicate>();
									cubeSet.add(transPred);
									trueSet.add(cubeSet);
									transList.add(trueSet);
									boolean error = true;
									// now search for the negated predicate
									for(Predicate negPred: getAddToSetPred.getPredMap().keySet()){
										if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
											// add the negative predicate to the falseSet
											Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
											cubeSet = new HashSet<Predicate>();
											cubeSet.add(negPred);
											falseSet.add(cubeSet);
											transList.add(falseSet);
											error = false;
											break;
										}
									}
									if(error){
										// we should not get here
										System.out.println("error: " + addToSetPred.getText());
									}
									newCubes.put(getAddToSetPred.getPredMap().get(transPred), transList);
								}
							}
							getSetTransformers.put(getAddToSetPred, newCubes);
						}
						addTransformer = getSetTransformers.get(getAddToSetPred);
					} else {
						PutPredicate putAddToSetPred = (PutPredicate) addToSetPred;
						if(!putSetTransformers.containsKey(putAddToSetPred)){
							//getSetTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getReadVar() + "_SetProc" + pred.getSrcProcNr(), pred.getReadVar(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
							// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates							
							Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
							for(Predicate transPred: putAddToSetPred.getPredMap().keySet()){
								if(!transPred.getMakeNot()){
									// only care about the regular predicates because we would get them 2 times for the negation
									List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
									Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
									Set<Predicate> cubeSet = new HashSet<Predicate>();
									cubeSet.add(transPred);
									trueSet.add(cubeSet);
									transList.add(trueSet);
									boolean error= true;
									// now search for the negated predicate
									for(Predicate negPred: putAddToSetPred.getPredMap().keySet()){
										if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
											// add the negative predicate to the falseSet
											Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
											cubeSet = new HashSet<Predicate>();
											cubeSet.add(negPred);
											falseSet.add(cubeSet);
											transList.add(falseSet);
											error = false;
											break;
										}
									}
									if(error){
										// we should not get here
										System.out.println("error: " + addToSetPred.getText());
									}
									newCubes.put(putAddToSetPred.getPredMap().get(transPred), transList);
								}
							}
							putSetTransformers.put(putAddToSetPred, newCubes);
						}
						addTransformer = putSetTransformers.get(putAddToSetPred);
					}
					Set<Integer> addLoadSet = getContainedPredIds(addTransformer);
					
					loadPredsToLocalAdd(addLoadSet);					
					for(Predicate transPred: addTransformer.keySet()){
						List<Set<Set<Predicate>>> transList = addTransformer.get(transPred);
						commitChooseAdd(transList.get(0), transList.get(1), transPred, true, addLoadSet);
					}
					resetLocalsAdd(addLoadSet);
					commitCodeLnAdd(unsatString);
				}
			}
			commitCodeLnAdd(unsatString);
			if(!pred.getInLoop() || (pred.getInLoop() && pred.getFlushFollowed())){
				// the executed rdma statement was not in a loop (or followed by flush), so we have to set it to inactive after one execution
				commitNopAdd();
				commitCommentAdd("Statement RdmaOperation" + pred.getId() + " = 0");
				commitCodeLnAdd("store B" + pred.getId() + " = 0;");
			}
			commitLnAdd(afterLabel2 + ": nop;");
		}
		// end loop
		commitLnAdd(checkLabel + ": nop;");
		commitCodeLnAdd("if (*) goto " + loopStartLabel + ";");
		commitCodeLnAdd(unsatString);
		if(! inAtomic){
			commitCodeLnAdd("end_atomic;");
		}
	}
	
	public Map<Predicate, List<Set<Set<Predicate>>>> computeRdmaAssignmentTransformerExpr(String identifier, String rhsExpr, boolean inAtomicStatement, boolean commit){

		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		
		TLLexer lexer = new TLLexer(new ANTLRInputStream(rhsExpr));
		TLParser parser = new TLParser(new CommonTokenStream(lexer));
		parser.setBuildParseTree(true);
		ParseTree tree = parser.parse();
		Map<Predicate, List<Set<Set<Predicate>>>> result = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
		for(Predicate pred: preds){
			List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(identifier, tree, pred);
			result.put(pred, wpcs);
			Set<Set<Predicate>> wpc = wpcs.get(0);
			Set<Set<Predicate>> notWpc = wpcs.get(1);
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
		}
		
		if(commit){
			Set<RdmaPredicate> activeStmts = new HashSet<RdmaPredicate>();
			activeStmts.addAll(putPreds.values());
			activeStmts.addAll(getPreds.values());
			for(RgaPredicate rgaPred: rgaPreds.values()){
				activeStmts.add(rgaPred.getGetPred());
				activeStmts.add(rgaPred.getPutPred());
			}
			for(CasPredicate casPred: casPreds.values()){
				activeStmts.add(casPred.getGetPred1());
				activeStmts.add(casPred.getGetPred2());
				activeStmts.add(casPred.getPutPred());
			}
			Set<Integer> loadSet = getContainedPredIds(result);
			
			// load the predicates for the affected set predicates to the locals
			for(Predicate changePred: result.keySet()){
				for(RdmaPredicate addPred: activeStmts){
					if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(identifier)){
						loadSet.addAll(getContainedPredIdsMap(result.get(changePred).get(0), addPred.getPredMap()));
						loadSet.addAll(getContainedPredIdsMap(result.get(changePred).get(1), addPred.getPredMap()));
						// load the changed predicate for the join
						loadSet.add(addPred.getPredMap().get(changePred).getId());
					}
				}
			}
			loadPredsToLocalAdd(loadSet);					
			for(Predicate transPred: result.keySet()){
				List<Set<Set<Predicate>>> transList = result.get(transPred);
				commitChooseAdd(transList.get(0), transList.get(1), transPred, false);
			}
			
			// change the affected set predicates
			for(RdmaPredicate addPred: activeStmts){
				boolean found = false;
				// first check if this rdma statement is affected
				for(Predicate changePred: result.keySet()){
					if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(identifier)){
						found = true;
						break;
					}
				}
				if(found){
					// the statement is affected
					for(Predicate changePred: result.keySet()){
						if(addPred.getPredMap().containsKey(changePred)&& addPred.getPredMap().get(changePred).containedVars.contains(identifier)){
							commitPutGetChooseAdd(result.get(changePred).get(0), result.get(changePred).get(1), addPred.getPredMap().get(changePred), true, addPred.getPredMap());
						}
					}
				}
			}
			resetLocalsAdd(loadSet);
			commitCodeLnAdd(unsatString);
		}
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
		return result; 
	}
	
	public Map<Predicate, List<Set<Set<Predicate>>>> computeAssignmentTransformer(ParseTree ctx, String identifier, String statementComment){
		boolean setAtomic = false;
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
			inAtomic = true;
			setAtomic = true;
		}
		commitNop();
		commitComment(statementComment);
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		Map<Predicate, List<Set<Set<Predicate>>>> result = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
		for(Predicate pred: preds){
			List<Set<Set<Predicate>>> wpcs = wpcSolver.computeWpc(ctx, pred);
			result.put(pred, wpcs);
			Set<Set<Predicate>> wpc = wpcs.get(0);
			Set<Set<Predicate>> notWpc = wpcs.get(1);
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
		}
		
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		
		// load the predicates for the affected set predicates to the locals
		for(int i = 0; i < predList.size(); i++){
			Predicate changePred = predList.get(i);
			for(PutPredicate pred: putPreds.values()){
				if(pred.getPredMap().containsKey(changePred) && pred.getPredMap().get(changePred).containedVars.contains(identifier)){
					loadSet.addAll(getContainedPredIdsMap(wpcList.get(i), pred.getPredMap()));
					loadSet.addAll(getContainedPredIdsMap(notWpcList.get(i), pred.getPredMap()));
					// load the changed predicate for the join
					loadSet.add(pred.getPredMap().get(changePred).getId());
				}
			}
			for(GetPredicate pred: getPreds.values()){
				if(pred.getPredMap().containsKey(changePred)&& pred.getPredMap().get(changePred).containedVars.contains(identifier)){
					loadSet.addAll(getContainedPredIdsMap(wpcList.get(i), pred.getPredMap()));
					loadSet.addAll(getContainedPredIdsMap(notWpcList.get(i), pred.getPredMap()));
					// load the changed predicate for the join					
					loadSet.add(pred.getPredMap().get(changePred).getId());
				}
			}
		}
		
		loadPredsToLocal(loadSet);
			
		// change the regular predicates
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i), false);
		}
		
		// change the affected set predicates
		for(PutPredicate addPred: putPreds.values()){
			boolean found = false;
			for(int i = 0; i < wpcList.size(); i++){
				Predicate changePred = predList.get(i);
				if(addPred.getPredMap().containsKey(changePred) && addPred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
					found = true;
					break;
				}
			}
			if(found){
				for(int i = 0; i < wpcList.size(); i++){
					Predicate changePred = predList.get(i);
					if(addPred.getPredMap().containsKey(changePred) && addPred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						commitPutGetChoose(wpcList.get(i), notWpcList.get(i), addPred.getPredMap().get(changePred), true, addPred.getPredMap());
					}
				}
			}
		}
		
		for(RdmaPredicate addPred: rgaRdmaPreds){
			boolean found = false;
			for(int i = 0; i < wpcList.size(); i++){
				Predicate changePred = predList.get(i);
				if(addPred.getPredMap().containsKey(changePred) && addPred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
					found = true;
					break;
				}
			}
			if(found){
				for(int i = 0; i < wpcList.size(); i++){
					Predicate changePred = predList.get(i);
					if(addPred.getPredMap().containsKey(changePred) && addPred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						commitPutGetChoose(wpcList.get(i), notWpcList.get(i), addPred.getPredMap().get(changePred), true, addPred.getPredMap());
					}
				}
			}
		}
		
		for(RdmaPredicate addPred: casRdmaPreds){
			boolean found = false;
			for(int i = 0; i < wpcList.size(); i++){
				Predicate changePred = predList.get(i);
				if(addPred.getPredMap().containsKey(changePred) && addPred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
					found = true;
					break;
				}
			}
			if(found){
				for(int i = 0; i < wpcList.size(); i++){
					Predicate changePred = predList.get(i);
					if(addPred.getPredMap().containsKey(changePred) && addPred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						commitPutGetChoose(wpcList.get(i), notWpcList.get(i), addPred.getPredMap().get(changePred), true, addPred.getPredMap());
					}
				}
			}
		}
		
		for(GetPredicate addPred: getPreds.values()){
			boolean found = false;
			for(int i = 0; i < wpcList.size(); i++){
				Predicate changePred = predList.get(i);
				if(addPred.getPredMap().containsKey(changePred) && addPred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
					found = true;
					break;
				}
			}
			if(found){
				for(int i = 0; i < wpcList.size(); i++){
					Predicate changePred = predList.get(i);
					if(addPred.getPredMap().containsKey(changePred) && addPred.getPredMap().get(changePred).getContainedVars().contains(identifier)){
						commitPutGetChoose(wpcList.get(i), notWpcList.get(i), addPred.getPredMap().get(changePred), true, addPred.getPredMap());
					}
				}
			}
		}
		resetLocals(loadSet);
		commitCodeLn(unsatString);
		if(setAtomic){
			commitCodeLn("end_atomic;");
			inAtomic = false;
		}
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
		return result; 
	}

	public List<Set<Set<Predicate>>> getWpcTestList(){
		return wpcTestList;
	}
	
	public List<Set<Set<Predicate>>> getSpcTestList(){
		return spcTestList;
	}
	
	public String getFenderPredString(){
		return fenderPredString;
	}
	
	public void initializeFenderPredString(){
		fenderPredString = "";
		for(int i = 0; i <= highestId; i++){
			for(Predicate pred: predicates){
				if(pred.getId() == i){
					fenderPredString = fenderPredString + pred.getPredString().replaceAll("==", "=") + "\r\n";
				}
			}
		}
		Set<RdmaPredicate> rdmaPreds = new HashSet<RdmaPredicate>();
		Set<Predicate> addPreds = new HashSet<Predicate>();
		rdmaPreds.addAll(putPreds.values());
		rdmaPreds.addAll(getPreds.values());
		for(RgaPredicate rgaPred: rgaPreds.values()){
			rdmaPreds.add(rgaPred);
			rdmaPreds.add(rgaPred.getGetPred());
			rdmaPreds.add(rgaPred.getPutPred());
		}
		for(CasPredicate casPred: casPreds.values()){
			rdmaPreds.add(casPred);
			rdmaPreds.add(casPred.getGetPred1());
			rdmaPreds.add(casPred.getGetPred2());
			rdmaPreds.add(casPred.getPutPred());
		}
		for(RdmaPredicate pred: rdmaPreds){
			if(! (pred instanceof RgaPredicate || pred instanceof CasPredicate)){
				addPreds.addAll(pred.getPredMap().values());
			}
		}
		idLoop:for(int i = 0; i <= highestId; i++){
			for(RdmaPredicate pred: rdmaPreds){
				if(i == pred.getId()){
					fenderPredString = fenderPredString + "(RdmaOperation" + i + " = 1)\r\n";
					continue idLoop;
				}
			}
			for(Predicate pred: addPreds){
				if(i == pred.getId()){
					fenderPredString = fenderPredString + pred.getPredString().replaceAll("==", "=") + "\r\n";
				}
			}
			
		}
		if(!fenderPredString.equals("")){
			fenderPredString = fenderPredString.substring(0, fenderPredString.length() - 2);
		}
		
	}
	
	public void addAndInitVars(){
		// first we add a global variable for each predicate and the Put & Get Predicates*/
		String vars = "shared ";
		highestId = -1;
		
		for(Predicate pred: predicates){
			highestId = Math.max(highestId, pred.getId());
		}
		
		Set<Predicate> addPreds = new HashSet<Predicate>();
		for(PutPredicate pred: putPreds.values()){
			highestId = Math.max(highestId, pred.getId());
			for(Predicate mapPred: pred.getPredMap().keySet()){
					Predicate p = pred.getPredMap().get(mapPred);
					addPreds.add(p);
			}
		}
		for(GetPredicate pred: getPreds.values()){
			highestId = Math.max(highestId, pred.getId());
			for(Predicate mapPred: pred.getPredMap().keySet()){
					Predicate p = pred.getPredMap().get(mapPred);
					addPreds.add(p);
			}
		}
		for(RgaPredicate pred: rgaPreds.values()){
			highestId = Math.max(highestId, pred.getId());
			highestId = Math.max(highestId, pred.getPutPred().getId());
			highestId = Math.max(highestId, pred.getGetPred().getId());
			addPreds.addAll(pred.getGetPred().getAddPreds());
			addPreds.addAll(pred.getPutPred().getAddPreds());
			
		}
		for(CasPredicate pred: casPreds.values()){
			highestId = Math.max(highestId, pred.getId());
			highestId = Math.max(highestId, pred.getPutPred().getId());
			highestId = Math.max(highestId, pred.getGetPred1().getId());
			highestId = Math.max(highestId, pred.getGetPred2().getId());
			addPreds.addAll(pred.getPutPred().getAddPreds());
			addPreds.addAll(pred.getGetPred1().getAddPreds());
			addPreds.addAll(pred.getGetPred2().getAddPreds());
		}
		for(Predicate pred: addPreds){
			highestId = Math.max(highestId, pred.getId());
		}
		for(int i = 0; i <= highestId; i++){
			vars = vars + "B" + i + ", ";
		}
		vars = vars.substring(0, vars.length() - 2) + ";";
		commitLn(vars);
		// now we add a local variable for each normal predicate*/
		vars = "local ";
		for(int i = 0; i <= highestId; i++){
			vars = vars + "t" + i + ", ";
		}
		vars = vars.substring(0, vars.length() - 2) + ";";
		
		commitLn(vars);
		
		commitLn("init");
		for(Predicate pred: predicates){
			commitComment("Predicate: " + pred.getPredString() + " corresponds to B" + pred.getId());
		}
		//initialize all Predicates to *
		for(Predicate pred: predicates){
			commitCodeLn("store B" + pred.getId() + " = *;");
		}
		
		//initialize all Put & Get Predicates to false
		for(PutPredicate pred: putPreds.values()){
			commitCodeLn("store B" + pred.getId() + " = 0;");
		}
		for(GetPredicate pred: getPreds.values()){
			commitCodeLn("store B" + pred.getId() + " = 0;");
		}
		for(RgaPredicate pred: rgaPreds.values()){
			commitCodeLn("store B" + pred.getId() + " = 0;");
			commitCodeLn("store B" + pred.getGetPred().getId() + " = 0;");
			commitCodeLn("store B" + pred.getPutPred().getId() + " = 0;");
		}
		for(CasPredicate pred: casPreds.values()){
			commitCodeLn("store B" + pred.getId() + " = 0;");
			commitCodeLn("store B" + pred.getGetPred1().getId() + " = 0;");
			commitCodeLn("store B" + pred.getGetPred2().getId() + " = 0;");
			commitCodeLn("store B" + pred.getPutPred().getId() + " = 0;");
		}
		// initialize all additional predicates to *
		for(Predicate pred: addPreds){
			commitCodeLn("store B" + pred.getId() + " = *;");
		}
		
		for(PutPredicate pred: putPreds.values()){
			commitComment("Predicate: " + pred.getText() + " corresponds to B" + pred.getId());
		}
		for(GetPredicate pred: getPreds.values()){
			commitComment("Predicate: " + pred.getText() + " corresponds to B" + pred.getId());
		}
		for(RgaPredicate pred: rgaPreds.values()){
			commitComment("Predicate: " + pred.getText() + " corresponds to B" + pred.getId());
		}
		for(CasPredicate pred: casPreds.values()){
			commitComment("Predicate: " + pred.getText() + " corresponds to B" + pred.getId());
		}
		for(Predicate pred: addPreds){
			commitComment("Predicate: " + pred.getPredString() + " corresponds to B" + pred.getId());
		}
	}

	@Override
	public Object visitCas(CasContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		CasPredicate pred = casPreds.get(ctx);
		
		// first create a new process
		commitLnAdd("process " + currAddProcessId);
		currAddProcessId++;
		// set the cas to active in the initial process
		boolean setAtomic = false;
		if(!inAtomic){
			setAtomic = true;
			commitCodeLn("begin_atomic;");
			inAtomic = true;
		}
		commitNop();
		commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
		commitCodeLn("store B" + pred.getId() + " = 1;");
		if(setAtomic){
			inAtomic = false;
			commitCodeLn("end_atomic;");
		}
		
		// add a start label to keep the process in an endless loop
		int processStartLabel = getNewLabel();
		commitLnAdd(processStartLabel + ": nop;");
		
		commitCodeLnAdd("begin_atomic;");
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + pred.getId() + " = 1)");
		commitCodeLnAdd("assume (B" + pred.getId() + " == 1);");
		commitCodeLnAdd("end_atomic;");
		
		commitCodeLnAdd("begin_atomic;");
		// initialize the first get
		GetPredicate getPred = pred.getGetPred1();
		// don't do the join
		commitNopAdd();
		commitCommentAdd("Statement setinit " + getPred.getSetName() + " = " + getPred.getReadVar());
		if(!getSetTransformers.containsKey(getPred)){
			if(noExtrapolation){
				getSetTransformers.put(getPred, computeRdmaAssignmentTransformer(getPred.getSetName(), getPred.getReadVar(), false, getPred.getAddPreds(), getPred.getAddNotPreds(), false, true));
			} else {
				// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				for(Predicate transPred: getPred.getPredMap().keySet()){
					if(!transPred.getMakeNot()){
						// only care about the regular predicates because we would get them 2 times for the negation
						List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
						Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
						Set<Predicate> cubeSet = new HashSet<Predicate>();
						cubeSet.add(transPred);
						trueSet.add(cubeSet);
						transList.add(trueSet);
						boolean error = true;
						// now search for the negated predicate
						for(Predicate negPred: getPred.getPredMap().keySet()){
							if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
								// add the negative predicate to the falseSet
								Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
								cubeSet = new HashSet<Predicate>();
								cubeSet.add(negPred);
								falseSet.add(cubeSet);
								transList.add(falseSet);
								error = false;
								break;
							}
						}
						if(error){
							// we should not get here
							System.out.println("error: " + pred.getText());
						}
						newCubes.put(getPred.getPredMap().get(transPred), transList);
					}
				}
				getSetTransformers.put(getPred, newCubes);
			}
		}
		Map<Predicate, List<Set<Set<Predicate>>>> getTransformer = getSetTransformers.get(getPred);
		Set<Integer> loadSet = getContainedPredIds(getTransformer);
		loadPredsToLocalAdd(loadSet);					
		for(Predicate transPred: getTransformer.keySet()){
			List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
			commitChooseAdd(transList.get(0), transList.get(1), transPred, false, loadSet);
		}
		resetLocalsAdd(loadSet);
		commitNopAdd();
		commitCommentAdd("Statement RdmaOperation" + getPred.getId() + " = 1");
		commitCodeLnAdd("store B" + getPred.getId() + " = 1;");
		
		commitCodeLnAdd("end_atomic;");
		
		// now give the get a chance to execute
		List<String> readVarList = new ArrayList<String>();
		readVarList.add(getPred.getWriteVar());
		doRemoteOpsAdd(readVarList, new ArrayList<String>(), false);
		
		commitCodeLnAdd("begin_atomic;");
		// initialize the second get
		getPred = pred.getGetPred2();
		// don't do the join
		commitNopAdd();
		commitCommentAdd("Statement setinit " + getPred.getSetName() + " = " + getPred.getReadVar());
		if(!getSetTransformers.containsKey(getPred)){
			if(noExtrapolation){
				getSetTransformers.put(getPred, computeRdmaAssignmentTransformer(getPred.getSetName(), getPred.getReadVar(), false, getPred.getAddPreds(), getPred.getAddNotPreds(), false, true));
			} else {
				// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				for(Predicate transPred: getPred.getPredMap().keySet()){
					if(!transPred.getMakeNot()){
						// only care about the regular predicates because we would get them 2 times for the negation
						List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
						Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
						Set<Predicate> cubeSet = new HashSet<Predicate>();
						cubeSet.add(transPred);
						trueSet.add(cubeSet);
						transList.add(trueSet);
						boolean error = true;
						// now search for the negated predicate
						for(Predicate negPred: getPred.getPredMap().keySet()){
							if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
								// add the negative predicate to the falseSet
								Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
								cubeSet = new HashSet<Predicate>();
								cubeSet.add(negPred);
								falseSet.add(cubeSet);
								transList.add(falseSet);
								error = false;
								break;
							}
						}
						if(error){
							// we should not get here
							System.out.println("error: " + pred.getText());
						}
						newCubes.put(getPred.getPredMap().get(transPred), transList);
					}
				}
				getSetTransformers.put(getPred, newCubes);
			}
		}
		getTransformer = getSetTransformers.get(getPred);
		loadSet = getContainedPredIds(getTransformer);
		loadPredsToLocalAdd(loadSet);					
		for(Predicate transPred: getTransformer.keySet()){
			List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
			commitChooseAdd(transList.get(0), transList.get(1), transPred, false, loadSet);
		}
		resetLocalsAdd(loadSet);
		commitNopAdd();
		commitCommentAdd("Statement RdmaOperation" + getPred.getId() + " = 1");
		commitCodeLnAdd("store B" + getPred.getId() + " = 1;");
		
		commitCodeLnAdd("end_atomic;");
		
		// now give the get a chance to execute
		readVarList = new ArrayList<String>();
		readVarList.add(getPred.getWriteVar());
		// also add the write var from before, so the case where the second get executes before the first one is covered
		readVarList.add(pred.getGetPred1().getWriteVar());
		doRemoteOpsAdd(readVarList, new ArrayList<String>(), false);
		
		commitCodeLnAdd("begin_atomic;");
		// now assume that both get completed
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + pred.getGetPred1().getId() + " = 0)");
		commitCodeLnAdd("assume (B" + pred.getGetPred1().getId() + " == 0);");
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + pred.getGetPred2().getId() + " = 0)");
		commitCodeLnAdd("assume (B" + pred.getGetPred2().getId() + " == 0);");
		commitCodeLnAdd("end_atomic;");
		
		commitCodeLnAdd("begin_atomic;");
		readVarList = new ArrayList<String>();
		readVarList.add(pred.getWriteVar());
		// execute remote ops before reading Z
		doRemoteOpsAdd(readVarList, new ArrayList<String>(), true);
		// t3 = Z
		commitCommentAdd("Statement " + pred.getLocal3Name() + " = " + pred.getWriteVar());
		// simply predicate copy as t3 has the same predicates as Z
		Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
		for(Predicate transPred: pred.getPredMap3().keySet()){
			if(!transPred.getMakeNot()){
				// only care about the regular predicates because we would get them 2 times for the negation
				List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
				Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
				Set<Predicate> cubeSet = new HashSet<Predicate>();
				cubeSet.add(transPred);
				trueSet.add(cubeSet);
				transList.add(trueSet);
				boolean error = true;
				// now search for the negated predicate
				for(Predicate negPred: pred.getPredMap3().keySet()){
					if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
						// add the negative predicate to the falseSet
						Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
						cubeSet = new HashSet<Predicate>();
						cubeSet.add(negPred);
						falseSet.add(cubeSet);
						transList.add(falseSet);
						error = false;
						break;
					}
				}
				if(error){
					// we should not get here
					System.out.println("error: " + pred.getText());
				}
				newCubes.put(pred.getPredMap3().get(transPred), transList);
			}
		}
		loadSet = getContainedPredIds(newCubes);
		loadPredsToLocalAdd(loadSet);					
		for(Predicate transPred: newCubes.keySet()){
			List<Set<Set<Predicate>>> transList = newCubes.get(transPred);
			commitChooseAdd(transList.get(0), transList.get(1), transPred, false);
		}
		resetLocalsAdd(loadSet);
		commitCodeLnAdd(unsatString);
		
		//if(t3 == t1)
		List<Set<Set<Predicate>>> ifCondition = spcSolver.computeSpc("(" + pred.getLocal1Name() + " == " + pred.getLocal3Name() + ")");
		int elseLabel = getNewLabel();
		int endLabel = getNewLabel();
		commitCodeLnAdd("if (*) goto " + elseLabel + ";");
		
		String spcString = formPredString(ifCondition.get(0), false);
		if(! spcString.equals("")){
			commitNopAdd();
			commitCommentAdd("Statement assume (" + pred.getLocal1Name() + " = " + pred.getLocal3Name() + ")");
			commitCodeLnAdd("assume(" + spcString + ");");
		}
		
		// Z = t2
		commitNopAdd();
		commitCommentAdd("Statement " + pred.getWriteVar() + " = " + pred.getLocal2Name());
		computeRdmaAssignmentTransformerExpr(pred.getWriteVar(), pred.getLocal2Name(), true, true);
		
		commitCodeLnAdd("if (true) goto " + endLabel + ";");
		commitLnAdd(elseLabel + ": nop;");
		String notSpcString = formPredString(ifCondition.get(1), false);
		if(! notSpcString.equals("")){
			commitNopAdd();
			commitCommentAdd("Statement assume (!(" + pred.getLocal1Name() + " = " + pred.getLocal3Name() + "))");
			commitCodeLnAdd("assume(" + notSpcString + ");");
		}
		commitLnAdd(endLabel + ": nop;");
		commitCodeLnAdd("end_atomic;");
		
		//initialize the put
		commitCodeLnAdd("begin_atomic;");
		PutPredicate putPred = pred.getPutPred();
		// don't do the join
		commitNopAdd();
		commitCommentAdd("Statement setinit " + putPred.getSetName() + " = " + putPred.getReadVar());
		if(!putSetTransformers.containsKey(putPred)){
			if(noExtrapolation){
				putSetTransformers.put(putPred, computeRdmaAssignmentTransformer(putPred.getSetName(), putPred.getReadVar(), false, putPred.getAddPreds(), putPred.getAddNotPreds(), false, true));
			} else {
				// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
				newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				for(Predicate transPred: putPred.getPredMap().keySet()){
					if(!transPred.getMakeNot()){
						// only care about the regular predicates because we would get them 2 times for the negation
						List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
						Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
						Set<Predicate> cubeSet = new HashSet<Predicate>();
						cubeSet.add(transPred);
						trueSet.add(cubeSet);
						transList.add(trueSet);
						boolean error = true;
						// now search for the negated predicate
						for(Predicate negPred: putPred.getPredMap().keySet()){
							if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
								// add the negative predicate to the falseSet
								Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
								cubeSet = new HashSet<Predicate>();
								cubeSet.add(negPred);
								falseSet.add(cubeSet);
								transList.add(falseSet);
								error = false;
								break;
							}
						}
						if(error){
							// we should not get here
							System.out.println("error: " + pred.getText());
						}
						newCubes.put(putPred.getPredMap().get(transPred), transList);
					}
				}
				putSetTransformers.put(putPred, newCubes);
			}
		}
		Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = putSetTransformers.get(putPred);
		loadSet = getContainedPredIds(putTransformer);
		loadPredsToLocalAdd(loadSet);					
		for(Predicate transPred: putTransformer.keySet()){
			List<Set<Set<Predicate>>> transList = putTransformer.get(transPred);
			commitChooseAdd(transList.get(0), transList.get(1), transPred, false, loadSet);
		}
		resetLocalsAdd(loadSet);
		commitNopAdd();
		commitCommentAdd("Statement RdmaOperation" + putPred.getId() + " = 1");
		commitCodeLnAdd("store B" + putPred.getId() + " = 1;");
		commitCodeLnAdd("end_atomic;");
		
		// now give the put a chance to execute
		readVarList = new ArrayList<String>();
		readVarList.add(putPred.getWriteVar());
		doRemoteOpsAdd(readVarList, new ArrayList<String>(), false);
		
		commitCodeLnAdd("begin_atomic;");
		// now assume that the put completed
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + putPred.getId() + " = 0)");
		commitCodeLnAdd("assume (B" + putPred.getId() + " == 0);");
		commitCodeLnAdd("end_atomic;");
		
		if(pred.getInLoop() && ! pred.getFlushFollowed()){
			// non-deterministically set it to inactive
			int ifAfterLabel = getNewLabel();
			commitCodeLnAdd("if (*) goto " + ifAfterLabel + ";");
			commitNopAdd();
			commitCommentAdd("Statement RdmaOperation" + pred.getId() + " = 0");
			commitCodeLnAdd("store B" + pred.getId() + " = 0;");
			commitLnAdd(ifAfterLabel + ": nop;");
		} else {
			// set it to inactive
			commitNopAdd();
			commitCommentAdd("Statement RdmaOperation" + pred.getId() + " = 0");
			commitCodeLnAdd("store B" + pred.getId() + " = 0;");
		}
		int startLabel = getNewLabel();
		int afterLabel = getNewLabel();
		commitCodeLnAdd("if (true) goto " + afterLabel + ";");
		commitLnAdd(startLabel + ": nop;");
		commitCommentAdd("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
		commitCodeLnAdd("assume (B" + pred.getId() + " == 0);");
		commitLnAdd(afterLabel + ": nop;");
		commitCodeLnAdd("if (*) goto " + startLabel + ";");
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + pred.getId() + " = 1)");
		commitCodeLnAdd("assume (B" + pred.getId() + " == 1);");
		
		// keep the process in an endless loop
		commitCodeLnAdd("if (true) goto " + processStartLabel + ";");
		
		return null;
	}

	
	@Override
	public Object visitRga(RgaContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		RgaPredicate pred = rgaPreds.get(ctx);
		// first create a new process
		commitLnAdd("process " + currAddProcessId);
		currAddProcessId++;
		// set the rga to active in the initial process
		boolean setAtomic = false;
		if(!inAtomic){
			setAtomic = true;
			commitCodeLn("begin_atomic;");
			inAtomic = true;
		}
		commitNop();
		commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
		commitCodeLn("store B" + pred.getId() + " = 1;");
		if(setAtomic){
			inAtomic = false;
			commitCodeLn("end_atomic;");
		}
		
		// add a start label to keep the process in an endless loop
		int processStartLabel = getNewLabel();
		commitLnAdd(processStartLabel + ": nop;");
		
		commitCodeLnAdd("begin_atomic;");
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + pred.getId() + " = 1)");
		commitCodeLnAdd("assume (B" + pred.getId() + " == 1);");
		commitCodeLnAdd("end_atomic;");
		
		commitCodeLnAdd("begin_atomic;");
		// initialize the first get
		GetPredicate getPred = pred.getGetPred();
		// don't do the join
		commitNopAdd();
		commitCommentAdd("Statement setinit " + getPred.getSetName() + " = " + getPred.getReadVar());
		if(!getSetTransformers.containsKey(getPred)){
			if(noExtrapolation){
				getSetTransformers.put(getPred, computeRdmaAssignmentTransformer(getPred.getSetName(), getPred.getReadVar(), false, getPred.getAddPreds(), getPred.getAddNotPreds(), false, true));
			} else {
				// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
				Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				for(Predicate transPred: getPred.getPredMap().keySet()){
					if(!transPred.getMakeNot()){
						// only care about the regular predicates because we would get them 2 times for the negation
						List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
						Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
						Set<Predicate> cubeSet = new HashSet<Predicate>();
						cubeSet.add(transPred);
						trueSet.add(cubeSet);
						transList.add(trueSet);
						boolean error = true;
						// now search for the negated predicate
						for(Predicate negPred: getPred.getPredMap().keySet()){
							if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
								// add the negative predicate to the falseSet
								Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
								cubeSet = new HashSet<Predicate>();
								cubeSet.add(negPred);
								falseSet.add(cubeSet);
								transList.add(falseSet);
								error = false;
								break;
							}
						}
						if(error){
							// we should not get here
							System.out.println("error: " + pred.getText());
						}
						newCubes.put(getPred.getPredMap().get(transPred), transList);
					}
				}
				getSetTransformers.put(getPred, newCubes);
			}
		}
		Map<Predicate, List<Set<Set<Predicate>>>> getTransformer = getSetTransformers.get(getPred);
		Set<Integer> loadSet = getContainedPredIds(getTransformer);
		loadPredsToLocalAdd(loadSet);					
		for(Predicate transPred: getTransformer.keySet()){
			List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
			commitChooseAdd(transList.get(0), transList.get(1), transPred, false, loadSet);
		}
		resetLocalsAdd(loadSet);
		commitNopAdd();
		commitCommentAdd("Statement RdmaOperation" + getPred.getId() + " = 1");
		commitCodeLnAdd("store B" + getPred.getId() + " = 1;");
		
		commitCodeLnAdd("end_atomic;");
		
		// now give the get a chance to execute
		List<String> readVarList = new ArrayList<String>();
		readVarList.add(getPred.getWriteVar());
		doRemoteOpsAdd(readVarList, new ArrayList<String>(), false);
		
		commitCodeLnAdd("begin_atomic;");
		// now assume that the get completed
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + getPred.getId() + " = 0)");
		commitCodeLnAdd("assume (B" + getPred.getId() + " == 0);");
		commitCodeLnAdd("end_atomic;");
		
		commitCodeLnAdd("begin_atomic;");
		readVarList = new ArrayList<String>();
		readVarList.add(pred.getWriteVar());
		// execute remote ops before reading Z
		doRemoteOpsAdd(readVarList, new ArrayList<String>(), true);
		// t2 = Z
		commitCommentAdd("Statement " + pred.getLocal2Name() + " = " + pred.getWriteVar());
		// simply predicate copy as t2 has the same predicates as Z
		Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
		for(Predicate transPred: pred.getPredMap2().keySet()){
			if(!transPred.getMakeNot()){
				// only care about the regular predicates because we would get them 2 times for the negation
				List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
				Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
				Set<Predicate> cubeSet = new HashSet<Predicate>();
				cubeSet.add(transPred);
				trueSet.add(cubeSet);
				transList.add(trueSet);
				boolean error = true;
				// now search for the negated predicate
				for(Predicate negPred: pred.getPredMap2().keySet()){
					if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
						// add the negative predicate to the falseSet
						Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
						cubeSet = new HashSet<Predicate>();
						cubeSet.add(negPred);
						falseSet.add(cubeSet);
						transList.add(falseSet);
						error = false;
						break;
					}
				}
				if(error){
					// we should not get here
					System.out.println("error: " + pred.getText());
				}
				newCubes.put(pred.getPredMap2().get(transPred), transList);
			}
		}
		loadSet = getContainedPredIds(newCubes);
		loadPredsToLocalAdd(loadSet);					
		for(Predicate transPred: newCubes.keySet()){
			List<Set<Set<Predicate>>> transList = newCubes.get(transPred);
			commitChooseAdd(transList.get(0), transList.get(1), transPred, false);
		}
		resetLocalsAdd(loadSet);
		commitCodeLnAdd(unsatString);
		
		// Z = Z + t1
		commitNopAdd();
		commitCommentAdd("Statement " + pred.getWriteVar() + " = " + pred.getWriteVar() + " + " + pred.getLocal1Name());
		computeRdmaAssignmentTransformerExpr(pred.getWriteVar(), pred.getWriteVar() + " + " + pred.getLocal1Name(), true, true);
		
		commitCodeLnAdd("end_atomic;");
		
		// initialize the put
		commitCodeLnAdd("begin_atomic;");
		PutPredicate putPred = pred.getPutPred();
		// don't do the join
		commitNopAdd();
		commitCommentAdd("Statement setinit " + putPred.getSetName() + " = " + putPred.getReadVar());
		if(!putSetTransformers.containsKey(putPred)){
			if(noExtrapolation){
				putSetTransformers.put(putPred, computeRdmaAssignmentTransformer(putPred.getSetName(), putPred.getReadVar(), false, putPred.getAddPreds(), putPred.getAddNotPreds(), false, true));
			} else {
				// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
				newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
				for(Predicate transPred: putPred.getPredMap().keySet()){
					if(!transPred.getMakeNot()){
						// only care about the regular predicates because we would get them 2 times for the negation
						List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
						Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
						Set<Predicate> cubeSet = new HashSet<Predicate>();
						cubeSet.add(transPred);
						trueSet.add(cubeSet);
						transList.add(trueSet);
						boolean error = true;
						// now search for the negated predicate
						for(Predicate negPred: putPred.getPredMap().keySet()){
							if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
								// add the negative predicate to the falseSet
								Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
								cubeSet = new HashSet<Predicate>();
								cubeSet.add(negPred);
								falseSet.add(cubeSet);
								transList.add(falseSet);
								error = false;
								break;
							}
						}
						if(error){
							// we should not get here
							System.out.println("error: " + pred.getText());
						}
						newCubes.put(putPred.getPredMap().get(transPred), transList);
					}
				}
				putSetTransformers.put(putPred, newCubes);
			}
		}
		Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = putSetTransformers.get(putPred);
		loadSet = getContainedPredIds(putTransformer);
		loadPredsToLocalAdd(loadSet);					
		for(Predicate transPred: putTransformer.keySet()){
			List<Set<Set<Predicate>>> transList = putTransformer.get(transPred);
			commitChooseAdd(transList.get(0), transList.get(1), transPred, false, loadSet);
		}
		resetLocalsAdd(loadSet);
		commitNopAdd();
		commitCommentAdd("Statement RdmaOperation" + putPred.getId() + " = 1");
		commitCodeLnAdd("store B" + putPred.getId() + " = 1;");
		commitCodeLnAdd("end_atomic;");
		
		// now give the put a chance to execute
		readVarList = new ArrayList<String>();
		readVarList.add(putPred.getWriteVar());
		doRemoteOpsAdd(readVarList, new ArrayList<String>(), false);
		
		commitCodeLnAdd("begin_atomic;");
		// now assume that the put completed
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + putPred.getId() + " = 0)");
		commitCodeLnAdd("assume (B" + putPred.getId() + " == 0);");
		commitCodeLnAdd("end_atomic;");
		
		if(pred.getInLoop() && ! pred.getFlushFollowed()){
			// non-deterministically set it to inactive
			int ifAfterLabel = getNewLabel();
			commitCodeLnAdd("if (*) goto " + ifAfterLabel + ";");
			commitNopAdd();
			commitCommentAdd("Statement RdmaOperation" + pred.getId() + " = 0");
			commitCodeLnAdd("store B" + pred.getId() + " = 0;");
			commitLnAdd(ifAfterLabel + ": nop;");
		} else {
			// set it to inactive
			commitNopAdd();
			commitCommentAdd("Statement RdmaOperation" + pred.getId() + " = 0");
			commitCodeLnAdd("store B" + pred.getId() + " = 0;");
		}
		int startLabel = getNewLabel();
		int afterLabel = getNewLabel();
		commitCodeLnAdd("if (true) goto " + afterLabel + ";");
		commitLnAdd(startLabel + ": nop;");
		commitCommentAdd("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
		commitCodeLnAdd("assume (B" + pred.getId() + " == 0);");
		commitLnAdd(afterLabel + ": nop;");
		commitCodeLnAdd("if (*) goto " + startLabel + ";");
		commitNopAdd();
		commitCommentAdd("Statement assume (RdmaOperation" + pred.getId() + " = 1)");
		commitCodeLnAdd("assume (B" + pred.getId() + " == 1);");
		// keep the process in an endless loop
		commitCodeLnAdd("if (true) goto " + processStartLabel + ";");
		return null;
	}

	@Override
	public Object visitPut(PutContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		PutPredicate pred = putPreds.get(ctx);
		List<String> writeVars = new ArrayList<String>();
		List<String> readVars = new ArrayList<String>();
		readVars.add(pred.getReadVar());
		writeVars.add(pred.getWriteVar());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		boolean setAtomic = false;
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
			inAtomic = true;
			setAtomic = true;
		}
		commitCodeLn(unsatString);
		if(accumulate){
			Set<RdmaPredicate> activePreds = new HashSet<RdmaPredicate> (graphSet.get(curProc).getStatementMap().get(ctx));
			Iterator<RdmaPredicate> it = activePreds.iterator();
			while(it.hasNext()){
				RdmaPredicate p = it.next();
				if(p instanceof RgaPredicate || p instanceof CasPredicate){
					// we are not interested in this type of predicate at this point
					it.remove();
					continue;
				}
				// remove the predicates that don't share the same set
				if(!(p.getSrcProcNr() == pred.getSrcProcNr() && p.getReadVar().equals(pred.getReadVar()))){
					it.remove();
				}
			}
			if(activePreds.size() == 0){
				// don't do the join
				commitNop();
				commitComment("Statement setinit " + pred.getSetName() + " = " + pred.getReadVar());
				if(!putSetTransformers.containsKey(pred)){
					if(noExtrapolation){
						Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = computeRdmaAssignmentTransformer(pred.getSetName(), pred.getReadVar() , false, pred.getAddPreds(), pred.getAddNotPreds(), false, true);
						putSetTransformers.put(pred, putTransformer);
					} else {
						// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
						Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
						for(Predicate transPred: pred.getPredMap().keySet()){
							if(!transPred.getMakeNot()){
								// only care about the regular predicates because we would get them 2 times for the negation
								List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
								Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
								Set<Predicate> cubeSet = new HashSet<Predicate>();
								cubeSet.add(transPred);
								trueSet.add(cubeSet);
								transList.add(trueSet);
								boolean error = true;
								// now search for the negated predicate
								for(Predicate negPred: pred.getPredMap().keySet()){
									if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
										// add the negative predicate to the falseSet
										Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
										cubeSet = new HashSet<Predicate>();
										cubeSet.add(negPred);
										falseSet.add(cubeSet);
										transList.add(falseSet);
										error = false;
										break;
									}
								}
								if(error){
									// we should not get here
									System.out.println("error: " + pred.getText());
								}
								newCubes.put(pred.getPredMap().get(transPred), transList);
							}
						}
						putSetTransformers.put(pred, newCubes);
					}
				}
				Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = putSetTransformers.get(pred);
				Set<Integer> loadSet = getContainedPredIds(putTransformer);
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: putTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = putTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, false, loadSet);
				}
				resetLocals(loadSet);
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
			} else {
				// do the join if at least one predicate is active
				int elseLabel = getNewLabel();
				int afterLabel = getNewLabel();
				commitCodeLn("if (*) goto " + elseLabel + ";");
				String assumeString = "";
				String commentAssumeString = "";
				for(RdmaPredicate p: activePreds){
					assumeString = assumeString + "(B" + p.getId() + " != 0)||";
					commentAssumeString = commentAssumeString + "(RdmaOperation" + p.getId() + " != 0)||";
				}
				// remove the trailing ||
				assumeString = assumeString.substring(0, assumeString.length() - 2);
				commentAssumeString = commentAssumeString.substring(0, commentAssumeString.length() - 2);
				commitNop();
				commitComment("Statement assume " + commentAssumeString);
				commitCodeLn("assume(" + assumeString + ");");
				// first set the put predicate to true
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
				// do the join
				commitNop();
				commitComment("Statement addtoset " + pred.getSetName() + " , " + pred.getReadVar());
				
				if(!putSetTransformers.containsKey(pred)){
					if(noExtrapolation){
						Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = computeRdmaAssignmentTransformer(pred.getSetName(), pred.getReadVar() , false, pred.getAddPreds(), pred.getAddNotPreds(), false, true);
						putSetTransformers.put(pred, putTransformer);
					} else {
						// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
						
						Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
						for(Predicate transPred: pred.getPredMap().keySet()){
							if(!transPred.getMakeNot()){
								// only care about the regular predicates because we would get them 2 times for the negation
								List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
								Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
								Set<Predicate> cubeSet = new HashSet<Predicate>();
								cubeSet.add(transPred);
								trueSet.add(cubeSet);
								transList.add(trueSet);
								boolean error = true;
								// now search for the negated predicate
								for(Predicate negPred: pred.getPredMap().keySet()){
									if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
										// add the negative predicate to the falseSet
										Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
										cubeSet = new HashSet<Predicate>();
										cubeSet.add(negPred);
										falseSet.add(cubeSet);
										transList.add(falseSet);
										error = false;
										break;
									}
								}
								if(error){
									// we should not get here
									System.out.println("error: " + pred.getText());
								}
								newCubes.put(pred.getPredMap().get(transPred), transList);
							}
						}
						putSetTransformers.put(pred, newCubes);
					}
				}
				
				Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = putSetTransformers.get(pred);
				
				Set<Integer> loadSet = getContainedPredIds(putTransformer);
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: putTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = putTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, true, loadSet);
				}
				resetLocals(loadSet);
				commitCodeLn("if (true) goto " + afterLabel + ";");
				
				// don't do the join if no predicate is active
				commitLn(elseLabel + ": nop;");
				assumeString = "";
				commentAssumeString = "";
				for(RdmaPredicate p: activePreds){
					assumeString = assumeString + "(B" + p.getId() + " == 0)&&";
					commentAssumeString = commentAssumeString + "(RdmaOperation" + p.getId() + " = 0)&&";
				}
				// remove the trailing &&
				assumeString = assumeString.substring(0, assumeString.length() - 2);
				commentAssumeString = commentAssumeString.substring(0, commentAssumeString.length() - 2);
				
				commitNop();
				commitComment("Statement assume " + commentAssumeString);
				commitCodeLn("assume(" + assumeString + ");");
				// first set the put predicate to true
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
				// don't do the join
				commitNop();
				commitComment("Statement setinit " + pred.getSetName() + " = " + pred.getReadVar());
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: putTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = putTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, false, loadSet);
				}
				resetLocals(loadSet);
				commitLn(afterLabel + ": nop;");
			}
		} else {
			if(graphSet.get(curProc).getStatementMap().get(ctx).contains(pred)){
				int elseLabel = getNewLabel();
				int afterLabel = getNewLabel();
				
				commitCodeLn("if (*) goto " + elseLabel + ";");
				// the predicate is already active -> join
				commitNop();
				commitComment("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
				commitCodeLn("assume (B" + pred.getId() + " != 0);");
				commitNop();
				commitComment("Statement addtoset " + pred.getSetName() + " , " + pred.getReadVar());
				
				if(!putSetTransformers.containsKey(pred)){
					if(noExtrapolation){
						Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = computeRdmaAssignmentTransformer(pred.getSetName(), pred.getReadVar() , false, pred.getAddPreds(), pred.getAddNotPreds(), false, true);
						putSetTransformers.put(pred, putTransformer);
					} else {
						// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
						Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
						for(Predicate transPred: pred.getPredMap().keySet()){
							if(!transPred.getMakeNot()){
								// only care about the regular predicates because we would get them 2 times for the negation
								List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
								Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
								Set<Predicate> cubeSet = new HashSet<Predicate>();
								cubeSet.add(transPred);
								trueSet.add(cubeSet);
								transList.add(trueSet);
								boolean error = true;
								// now search for the negated predicate
								for(Predicate negPred: pred.getPredMap().keySet()){
									if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
										// add the negative predicate to the falseSet
										Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
										cubeSet = new HashSet<Predicate>();
										cubeSet.add(negPred);
										falseSet.add(cubeSet);
										transList.add(falseSet);
										error = false;
										break;
									}
								}
								if(error){
									// we should not get here
									System.out.println("error: " + pred.getText());
								}
								newCubes.put(pred.getPredMap().get(transPred), transList);
							}
						}
						putSetTransformers.put(pred, newCubes);
					}
				}
				
				Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = putSetTransformers.get(pred);
				Set<Integer> loadSet = getContainedPredIds(putTransformer);
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: putTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = putTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, true, loadSet);
				}
				resetLocals(loadSet);
				
				commitCodeLn("if (true) goto " + afterLabel + ";");
				commitLn(elseLabel + ": nop;");
				// the predicate is not active -> no join
				commitNop();
				commitComment("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
				commitCodeLn("assume (B" + pred.getId() + " == 0);");
				
				// first set the put predicate to true
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
				commitNop();
				commitComment("Statement setinit " + pred.getSetName() + " = " + pred.getReadVar());
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: putTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = putTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, false, loadSet);
				}
				resetLocals(loadSet);
				
				commitLn(afterLabel + ": nop;");
			} else {
				if(!putSetTransformers.containsKey(pred)){
					if(noExtrapolation){
						Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = computeRdmaAssignmentTransformer(pred.getSetName(), pred.getReadVar() , false, pred.getAddPreds(), pred.getAddNotPreds(), false, true);
						putSetTransformers.put(pred, putTransformer);
					} else {
						// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
						Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
						for(Predicate transPred: pred.getPredMap().keySet()){
							if(!transPred.getMakeNot()){
								// only care about the regular predicates because we would get them 2 times for the negation
								List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
								Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
								Set<Predicate> cubeSet = new HashSet<Predicate>();
								cubeSet.add(transPred);
								trueSet.add(cubeSet);
								transList.add(trueSet);
								boolean error = true;
								// now search for the negated predicate
								for(Predicate negPred: pred.getPredMap().keySet()){
									if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
										// add the negative predicate to the falseSet
										Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
										cubeSet = new HashSet<Predicate>();
										cubeSet.add(negPred);
										falseSet.add(cubeSet);
										transList.add(falseSet);
										error = false;
										break;
									}
								}
								if(error){
									// we should not get here
									System.out.println("error: " + pred.getText());
								}
								newCubes.put(pred.getPredMap().get(transPred), transList);
							}
						}
						putSetTransformers.put(pred, newCubes);
					}
				}
				Map<Predicate, List<Set<Set<Predicate>>>> putTransformer = putSetTransformers.get(pred);

				commitNop();
				commitComment("Statement setinit " + pred.getSetName() + " = " + pred.getReadVar());
				Set<Integer> loadSet = getContainedPredIds(putTransformer);
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: putTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = putTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, false, loadSet);
				}
				resetLocals(loadSet);
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
			}
		}
		commitCodeLn(unsatString);
		if(setAtomic){
			commitCodeLn("end_atomic;");
			inAtomic = false;
		}
		
		return null;
	}
	
	
	@Override
	public Object visitGet(GetContext ctx) {
		commitLn("");
		commitLn("");
		commitComment(ctx.getText());
		GetPredicate pred = getPreds.get(ctx);
		List<String> writeVars = new ArrayList<String>();
		List<String> readVars = new ArrayList<String>();
		readVars.add(pred.getReadVar());
		writeVars.add(pred.getWriteVar());
		if(! inAtomic){
			doRemoteOps(ctx, readVars, writeVars, false);
		}
		boolean setAtomic = false;
		if(! inAtomic){
			commitCodeLn("begin_atomic;");
			inAtomic = true;
			setAtomic = true;
		}
		commitCodeLn(unsatString);
		if(accumulate){
			Set<RdmaPredicate> activePreds = new HashSet<RdmaPredicate> (graphSet.get(curProc).getStatementMap().get(ctx));
			Iterator<RdmaPredicate> it = activePreds.iterator();
			while(it.hasNext()){
				RdmaPredicate p = it.next();
				if(p instanceof RgaPredicate ||  p instanceof CasPredicate){
					// we are not interested in this type of predicate at this point
					it.remove();
					continue;
				}
				// remove the predicates that don't share the same set
				if(!(p.getSrcProcNr() == pred.getSrcProcNr() && p.getReadVar().equals(pred.getReadVar()))){
					it.remove();
				}
			}
			if(activePreds.size() == 0){
				// don't do the join
				commitNop();
				commitComment("Statement setinit " + pred.getSetName() + " = " + pred.getReadVar());
				if(!getSetTransformers.containsKey(pred)){
					if(noExtrapolation){
						getSetTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getSetName(), pred.getReadVar(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
					} else {
						// for the transformer, we can just copy the values of readvar to the set because they have exactly the same predicates
						Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
						for(Predicate transPred: pred.getPredMap().keySet()){
							if(!transPred.getMakeNot()){
								// only care about the regular predicates because we would get them 2 times for the negation
								List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
								Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
								Set<Predicate> cubeSet = new HashSet<Predicate>();
								cubeSet.add(transPred);
								trueSet.add(cubeSet);
								transList.add(trueSet);
								boolean error = true;
								// now search for the negated predicate
								for(Predicate negPred: pred.getPredMap().keySet()){
									if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
										// add the negative predicate to the falseSet
										Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
										cubeSet = new HashSet<Predicate>();
										cubeSet.add(negPred);
										falseSet.add(cubeSet);
										transList.add(falseSet);
										error = false;
										break;
									}
								}
								if(error){
									// we should not get here
									System.out.println("error: " + pred.getText());
								}
								newCubes.put(pred.getPredMap().get(transPred), transList);
							}
						}
						getSetTransformers.put(pred, newCubes);
					}
				}
				Map<Predicate, List<Set<Set<Predicate>>>> getTransformer = getSetTransformers.get(pred);
				Set<Integer> loadSet = getContainedPredIds(getTransformer);
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: getTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, false, loadSet);
				}
				resetLocals(loadSet);
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
			} else {
				// do the join if at least one predicate is active
				int elseLabel = getNewLabel();
				int afterLabel = getNewLabel();
				commitCodeLn("if (*) goto " + elseLabel + ";");
				String assumeString = "";
				String commentAssumeString = "";
				for(RdmaPredicate p: activePreds){
					assumeString = assumeString + "(B" + p.getId() + " != 0)||";
					commentAssumeString = commentAssumeString + "(RdmaOperation" + p.getId() + " != 0)||";
				}
				// remove the trailing ||
				assumeString = assumeString.substring(0, assumeString.length() - 2);
				commentAssumeString = commentAssumeString.substring(0, commentAssumeString.length() - 2);
				commitNop();
				commitComment("Statement assume " + commentAssumeString);
				commitCodeLn("assume(" + assumeString + ");");
				commitNop();
				commitComment("Statement addtoset " + pred.getSetName() + " , " + pred.getReadVar());
				// first set the put predicate to true
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
				// do the join
				if(!getSetTransformers.containsKey(pred)){
					if(noExtrapolation){
						getSetTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getSetName(), pred.getReadVar(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
					} else {
						Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
						for(Predicate transPred: pred.getPredMap().keySet()){
							if(!transPred.getMakeNot()){
								// only care about the regular predicates because we would get them 2 times for the negation
								List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
								Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
								Set<Predicate> cubeSet = new HashSet<Predicate>();
								cubeSet.add(transPred);
								trueSet.add(cubeSet);
								transList.add(trueSet);
								boolean error = true;
								// now search for the negated predicate
								for(Predicate negPred: pred.getPredMap().keySet()){
									if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
										// add the negative predicate to the falseSet
										Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
										cubeSet = new HashSet<Predicate>();
										cubeSet.add(negPred);
										falseSet.add(cubeSet);
										transList.add(falseSet);
										error = false;
										break;
									}
								}
								if(error){
									// we should not get here
									System.out.println("error: " + pred.getText());
								}
								newCubes.put(pred.getPredMap().get(transPred), transList);
							}
						}
						getSetTransformers.put(pred, newCubes);
					}
				}
				Map<Predicate, List<Set<Set<Predicate>>>> getTransformer = getSetTransformers.get(pred);
				Set<Integer> loadSet = getContainedPredIds(getTransformer);
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: getTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, true, loadSet);
				}
				resetLocals(loadSet);
				commitCodeLn("if (true) goto " + afterLabel + ";");
				
				// don't do the join if no predicate is active
				commitLn(elseLabel + ": nop;");
				assumeString = "";
				commentAssumeString = "";
				for(RdmaPredicate p: activePreds){
					assumeString = assumeString + "(B" + p.getId() + " == 0)&&";
					commentAssumeString = commentAssumeString + "(RdmaOperation" + p.getId() + " = 0)&&";
				}
				// remove the trailing &&
				assumeString = assumeString.substring(0, assumeString.length() - 2);
				commentAssumeString = commentAssumeString.substring(0, commentAssumeString.length() - 2);
				commitNop();
				commitComment("Statement assume " + commentAssumeString);
				commitCodeLn("assume(" + assumeString + ");");
				commitNop();
				commitComment("Statement setinit " + pred.getSetName() + " = " + pred.getReadVar());
				// first set the put predicate to true
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
				// don't do the join
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: getTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, false, loadSet);
				}
				resetLocals(loadSet);
				commitLn(afterLabel + ": nop;");
			}
		} else {
			if(graphSet.get(curProc).getStatementMap().get(ctx).contains(pred)){
				int elseLabel = getNewLabel();
				int afterLabel = getNewLabel();
				commitCodeLn("if (*) goto " + elseLabel + ";");
				// the predicate is already active -> join
				commitNop();
				commitComment("Statement assume (RdmaOperation" + pred.getId() + " != 0)");
				commitCodeLn("assume (B" + pred.getId() + " != 0);");
				commitNop();
				commitComment("Statement addtoset " + pred.getSetName() + " , " + pred.getReadVar());
				if(!getSetTransformers.containsKey(pred)){
					if(noExtrapolation){
						getSetTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getSetName(), pred.getReadVar(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
					} else {
						Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
						for(Predicate transPred: pred.getPredMap().keySet()){
							if(!transPred.getMakeNot()){
								// only care about the regular predicates because we would get them 2 times for the negation
								List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
								Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
								Set<Predicate> cubeSet = new HashSet<Predicate>();
								cubeSet.add(transPred);
								trueSet.add(cubeSet);
								transList.add(trueSet);
								boolean error = true;
								// now search for the negated predicate
								for(Predicate negPred: pred.getPredMap().keySet()){
									if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
										// add the negative predicate to the falseSet
										Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
										cubeSet = new HashSet<Predicate>();
										cubeSet.add(negPred);
										falseSet.add(cubeSet);
										transList.add(falseSet);
										error = false;
										break;
									}
								}
								if(error){
									// we should not get here
									System.out.println("error: " + pred.getText());
								}
								newCubes.put(pred.getPredMap().get(transPred), transList);
							}
						}
						getSetTransformers.put(pred, newCubes);
					}
				}
				Map<Predicate, List<Set<Set<Predicate>>>> getTransformer = getSetTransformers.get(pred);
				
				Set<Integer> loadSet = getContainedPredIds(getTransformer);
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: getTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, true, loadSet);
				}
				resetLocals(loadSet);
				
				commitCodeLn("if (true) goto " + afterLabel + ";");
				commitLn(elseLabel + ": nop;");
				// the predicate is not active -> no join
				commitNop();
				commitComment("Statement assume (RdmaOperation" + pred.getId() + " = 0)");
				commitCodeLn("assume (B" + pred.getId() + " == 0);");
				commitNop();
				commitComment("Statement setinit " + pred.getSetName() + " = " + pred.getReadVar());
				// first set the put predicate to true
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
				
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: getTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, false, loadSet);
				}
				resetLocals(loadSet);
				commitLn(afterLabel + ": nop;");
			} else {

				if(!getSetTransformers.containsKey(pred)){
					if(noExtrapolation){
						getSetTransformers.put(pred, computeRdmaAssignmentTransformer(pred.getSetName(), pred.getReadVar(), false, pred.getAddPreds(), pred.getAddNotPreds(), false, true));
					} else {
						Map<Predicate, List<Set<Set<Predicate>>>> newCubes = new HashMap<Predicate, List<Set<Set<Predicate>>>>();
						for(Predicate transPred: pred.getPredMap().keySet()){
							if(!transPred.getMakeNot()){
								// only care about the regular predicates because we would get them 2 times for the negation
								List<Set<Set<Predicate>>> transList = new ArrayList<Set<Set<Predicate>>>();
								Set<Set<Predicate>> trueSet = new HashSet<Set<Predicate>>();
								Set<Predicate> cubeSet = new HashSet<Predicate>();
								cubeSet.add(transPred);
								trueSet.add(cubeSet);
								transList.add(trueSet);
								boolean error = true;
								// now search for the negated predicate
								for(Predicate negPred: pred.getPredMap().keySet()){
									if(negPred.getMakeNot() && negPred.getId() == transPred.getId()){
										// add the negative predicate to the falseSet
										Set<Set<Predicate>> falseSet = new HashSet<Set<Predicate>>();
										cubeSet = new HashSet<Predicate>();
										cubeSet.add(negPred);
										falseSet.add(cubeSet);
										transList.add(falseSet);
										error = false;
										break;
									}
								}
								if(error){
									// we should not get here
									System.out.println("error: " + pred.getText());
								}
								newCubes.put(pred.getPredMap().get(transPred), transList);
							}
						}
						getSetTransformers.put(pred, newCubes);
					}
				}
				Map<Predicate, List<Set<Set<Predicate>>>> getTransformer = getSetTransformers.get(pred);
				commitNop();
				commitComment("Statement setinit " + pred.getSetName() + " = " + pred.getReadVar());
				Set<Integer> loadSet = getContainedPredIds(getTransformer);
				loadPredsToLocal(loadSet);					
				for(Predicate transPred: getTransformer.keySet()){
					List<Set<Set<Predicate>>> transList = getTransformer.get(transPred);
					commitChoose(transList.get(0), transList.get(1), transPred, false, loadSet);
				}
				resetLocals(loadSet);
				commitNop();
				commitComment("Statement RdmaOperation" + pred.getId() + " = 1");
				commitCodeLn("store B" + pred.getId() + " = 1;");
			}
		}
		commitCodeLn(unsatString);
		if(setAtomic){
			commitCodeLn("end_atomic;");
			inAtomic = false;
		}
		return null;
	}

}
