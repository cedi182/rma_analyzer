package blp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.IntExpr;

import antlr.TLBaseVisitor;
import antlr.TLParser.AddExpressionContext;
import antlr.TLParser.AndExpressionContext;
import antlr.TLParser.BoolExpressionContext;
import antlr.TLParser.DivideExpressionContext;
import antlr.TLParser.EqExpressionContext;
import antlr.TLParser.ExpressionExpressionContext;
import antlr.TLParser.GtEqExpressionContext;
import antlr.TLParser.GtExpressionContext;
import antlr.TLParser.IdentifierExpressionContext;
import antlr.TLParser.LtEqExpressionContext;
import antlr.TLParser.LtExpressionContext;
import antlr.TLParser.ModulusExpressionContext;
import antlr.TLParser.MultiplyExpressionContext;
import antlr.TLParser.NotEqExpressionContext;
import antlr.TLParser.NotExpressionContext;
import antlr.TLParser.NumberExpressionContext;
import antlr.TLParser.OrExpressionContext;
import antlr.TLParser.ParseContext;
import antlr.TLParser.PredicatesContext;
import antlr.TLParser.SubtractExpressionContext;
import antlr.TLParser.UnaryMinusExpressionContext;



/** a visitor that will visit an expression tree and rewrite it to a Z3 expression object */
public class TreeToZ3Visitor extends TLBaseVisitor<Expr>{
	
	Context context;
	
	Map<String, Expr> variables;
	
	String reWriteIdentifier;
	
	Expr reWriteExpression;
	
	boolean reWriteLiteral;
	
	boolean visitingRewriteExpr;
	
	Set<String> containedVars;
	
	Set<String>containedRewriteVars;
	
	// if true, we are rewriting an identifier with an expression
	boolean isReWriting;
	
	public TreeToZ3Visitor(){
		context = new Context();
		variables = new HashMap<String, Expr>();
		isReWriting = false;
		reWriteLiteral = false;
		reWriteIdentifier = null;
		containedVars = new HashSet<String>();
		containedRewriteVars = new HashSet<String>();
		visitingRewriteExpr = false;
	}
	
	public void reset(){
		context = new Context();
		variables = new HashMap<String, Expr>();
		isReWriting = false;
		reWriteLiteral = false;
		reWriteIdentifier = null;
		containedVars = new HashSet<String>();
		containedRewriteVars = new HashSet<String>();
		visitingRewriteExpr = false;
	}
	
	public void resetVars(){
		containedVars = new HashSet<String>();
	}
	
	/** rewrite the instances of identifier with the expression */
	public void reWrite(String identifier, ParseTree expression, boolean reWriteLiteral){
		this.reWriteLiteral = reWriteLiteral;
		reWriteIdentifier = identifier;
		isReWriting = false;
		if(reWriteLiteral){
			try{
				int number = Integer.parseInt(expression.getText());
				reWriteExpression =  context.mkInt(number);
			} catch (NumberFormatException e) {
				// not an integer!
				if(!variables.containsKey(expression)){
					// we add the variable to the context because it was not defined yet
					Expr var = context.mkConst(expression.getText(), context.getIntSort());
					variables.put(expression.getText(), var);
				}
				reWriteExpression = variables.get(expression.getText());
				containedRewriteVars.add(expression.getText());
			}
		} else {
			visitingRewriteExpr = true;
			reWriteExpression = visit(expression);
			visitingRewriteExpr = false;
		}
		isReWriting = true;
	}
	
	/** rewrite the instances of identifier with the expression */
	public void reWrite(String identifier, String expression){
		isReWriting = true;
		this.reWriteLiteral = true;
		reWriteIdentifier = identifier;
		if(!variables.containsKey(expression)){
			// we add the variable to the context because it was not defined yet
			Expr var = context.mkConst(expression, context.getIntSort());
			variables.put(expression, var);
		}
		reWriteExpression = variables.get(expression);
		containedRewriteVars.add(expression);
	}
	
	public void turnOffReWrite(){
		isReWriting = false;
		reWriteIdentifier = null;
		reWriteExpression = null;
		containedVars = new HashSet<String>();
		containedRewriteVars = new HashSet<String>();
		visitingRewriteExpr = false;
	}
	
	public Set<String> getContainedVars(){
		return containedVars;
	}
	
	public Context getContext(){
		return context;
	}
	
	public Map<String, Expr> getVars(){
		return variables;
	}
	
	
	@Override
	public Expr visitParse(ParseContext ctx) {
		return visit(ctx.getChild(0));
	}

	@Override
	public Expr visitPredicates(PredicatesContext ctx) {
		return visit(ctx.getChild(0));
	}


	@Override
	public Expr visitLtExpression(LtExpressionContext ctx) {
		Expr expr = context.mkLt((ArithExpr) visit(ctx.getChild(0)), (ArithExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitGtExpression(GtExpressionContext ctx) {
		Expr expr = context.mkGt((ArithExpr) visit(ctx.getChild(0)), (ArithExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitBoolExpression(BoolExpressionContext ctx) {
		boolean bool = Boolean.parseBoolean(ctx.getText());
		Expr boolExpr = context.mkBool(bool);
		return boolExpr;
		
	}

	@Override
	public Expr visitNotEqExpression(NotEqExpressionContext ctx) {
		BoolExpr notEq = context.mkEq(visit(ctx.getChild(0)), visit(ctx.getChild(2)));
		notEq = context.mkNot(notEq);		
		return notEq;
	}

	@Override
	public Expr visitNumberExpression(NumberExpressionContext ctx) {
		int number = Integer.parseInt(ctx.getText());
		Expr numberExp = context.mkInt(number);	
		
		return numberExp;
	}

	@Override
	public Expr visitIdentifierExpression(IdentifierExpressionContext ctx) {
		Expr result = null;
		String name = ctx.getText();
		
		if(! variables.containsKey(name)){
			// we add the variable to the context because it was not defined yet
			Expr var = context.mkConst(name, context.getIntSort());
			variables.put(name, var);
		}
		if(isReWriting && name.equals(reWriteIdentifier)){
			// we have to rewrite the identifier with the expression, turn off rewriting because we don't want to re write the inserted expression
			result = reWriteExpression;
			containedVars.addAll(containedRewriteVars);
		} else {
			result = variables.get(name);
			if(visitingRewriteExpr){
				containedRewriteVars.add(name);
			} else {
				containedVars.add(name);
			}
		}
		// return the variable
		return result;
	}

	@Override
	public Expr visitModulusExpression(ModulusExpressionContext ctx) {
		Expr expr = context.mkMod((IntExpr) visit(ctx.getChild(0)), (IntExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitNotExpression(NotExpressionContext ctx) {
		Expr expr = context.mkNot((BoolExpr)visit(ctx.getChild(1)));
		return expr;
	}

	@Override
	public Expr visitMultiplyExpression(MultiplyExpressionContext ctx) {
		Expr expr = context.mkMul((IntExpr) visit(ctx.getChild(0)), (IntExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitGtEqExpression(GtEqExpressionContext ctx) {
		Expr expr = context.mkGe((ArithExpr) visit(ctx.getChild(0)), (ArithExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitDivideExpression(DivideExpressionContext ctx) {
		Expr expr = context.mkDiv((IntExpr) visit(ctx.getChild(0)), (IntExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitOrExpression(OrExpressionContext ctx) {
		Expr expr = context.mkOr((BoolExpr) visit(ctx.getChild(0)), (BoolExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitUnaryMinusExpression(UnaryMinusExpressionContext ctx) {
		Expr expr = context.mkSub(context.mkInt(0), (ArithExpr)visit(ctx.getChild(1)));
		return expr;
	}

	@Override
	public Expr visitEqExpression(EqExpressionContext ctx) {
		Expr expr = context.mkEq(visit(ctx.getChild(0)), visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitAndExpression(AndExpressionContext ctx) {
		Expr expr = context.mkAnd((BoolExpr) visit(ctx.getChild(0)), (BoolExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitExpressionExpression(ExpressionExpressionContext ctx) {
		return visit(ctx.getChild(1));
	}

	@Override
	public Expr visitAddExpression(AddExpressionContext ctx) {
		Expr expr = context.mkAdd((ArithExpr) visit(ctx.getChild(0)), (ArithExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitSubtractExpression(SubtractExpressionContext ctx) {
		Expr expr = context.mkSub((ArithExpr) visit(ctx.getChild(0)), (ArithExpr) visit(ctx.getChild(2)));
		return expr;
	}

	@Override
	public Expr visitLtEqExpression(LtEqExpressionContext ctx) {
		Expr expr = context.mkLe((ArithExpr)visit(ctx.getChild(0)), (ArithExpr)visit(ctx.getChild(2)));
		return expr;
	}
}
