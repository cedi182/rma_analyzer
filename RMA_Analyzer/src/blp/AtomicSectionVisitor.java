package blp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import antlr.TLBaseVisitor;
import antlr.TLParser.AssignmentContext;
import antlr.TLParser.AssumptionContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.IfStatementContext;
import antlr.TLParser.LoadContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.StoreContext;
import antlr.TLParser.WhileStatementContext;

/** A class used to collect the read and written variables in an atomic section 
 * in order to execute remote statements of those before the section
 */
public class AtomicSectionVisitor extends TLBaseVisitor<Object>{
	
	Set<String> readVars;
	
	Set<String> writeVars;
	
	Map<PutContext, PutPredicate> putPreds;
	Map<GetContext, GetPredicate> getPreds;
	
	public AtomicSectionVisitor(Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds){
		readVars = new HashSet<String>();
		writeVars = new HashSet<String>();
		this.putPreds = putPreds;
		this.getPreds = getPreds;
	}
	
	public List<String> getReadVars(){
		List<String> result = new ArrayList<String>();
		result.addAll(readVars);
		return result;
	}
	
	public List<String> getWriteVars(){
		List<String> result = new ArrayList<String>();
		result.addAll(writeVars);
		return result;
	}

	@Override
	public Object visitStore(StoreContext ctx) {
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(6));
		readVars.addAll(varVis.getVars());
		writeVars.add(ctx.Identifier().getText());
		return super.visitStore(ctx);
	}

	@Override
	public Object visitLoad(LoadContext ctx) {
		readVars.add(ctx.Identifier(1).getText());
		writeVars.add(ctx.Identifier(0).getText());
		return super.visitLoad(ctx);
	}

	@Override
	public Object visitPut(PutContext ctx) {
		readVars.add(putPreds.get(ctx).getReadVar());
		writeVars.add(putPreds.get(ctx).getWriteVar());
		return super.visitPut(ctx);
	}

	@Override
	public Object visitAssignment(AssignmentContext ctx) {
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(2));
		readVars.addAll(varVis.getVars());
		writeVars.add(ctx.Identifier().getText());
		return super.visitAssignment(ctx);
	}

	@Override
	public Object visitAssumption(AssumptionContext ctx) {
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(2));
		readVars.addAll(varVis.getVars());
		return super.visitAssumption(ctx);
	}

	@Override
	public Object visitIfStatement(IfStatementContext ctx) {
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(1));
		readVars.addAll(varVis.getVars());
		return super.visitIfStatement(ctx);
	}

	@Override
	public Object visitWhileStatement(WhileStatementContext ctx) {
		VarVisitor varVis = new VarVisitor();
		varVis.visit(ctx.getChild(1));
		readVars.addAll(varVis.getVars());
		return super.visitWhileStatement(ctx);
	}

	@Override
	public Object visitGet(GetContext ctx) {
		readVars.add(getPreds.get(ctx).getReadVar());
		writeVars.add(getPreds.get(ctx).getWriteVar());
		return super.visitGet(ctx);
	}

}
