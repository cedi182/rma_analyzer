package blp;

import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLBaseVisitor;
import antlr.TLParser.FlushContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.IfStatementContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.WhileStatementContext;

/** A visitor used to search for flush statements in the same body of a while loop, where a remote statement is located*/
public class FlushLookAheadVisitor  extends TLBaseVisitor<Object>{
	ParseTree rdmaStatement;
	
	RdmaPredicate rdmaPred;
	
	Set<Flush> flushes;
	
	public FlushLookAheadVisitor(ParseTree ctx, RdmaPredicate pred, Set<Flush> flushes){
		this.flushes = flushes;
		this.rdmaStatement = ctx;
		this.rdmaPred = pred;
		this.rdmaPred.setFlushFollowed(false);
	}
	
	@Override
	public Object visitWhileStatement(WhileStatementContext ctx) {
		// don't go into the loop
		return null;
	}
	
	@Override
	public Object visitIfStatement(IfStatementContext ctx) {
		//don't go into the if/else blocks
		return null;
	}
	
	@Override
	public Object visitPut(PutContext ctx) {
		return null;
	}
	
	@Override
	public Object visitGet(GetContext ctx) {
		return null;
	}
	
	@Override
	public Object visitFlush(FlushContext ctx) {
		// first check if the flush is even active
		for(Flush f: flushes){
			if(f.getContext().equals(ctx)){
				// flush is currently not active
				return null;
			}
		}
		// don't have to check for the source process number because we are in the same process
		int dstProc = Integer.parseInt(ctx.Number().getText());
		if(dstProc == rdmaPred.getDstProcNr()){
			// we are past our Rdma statement and there is a flush statement for this statement in the loop body
			rdmaPred.setFlushFollowed(true);
		}
		return null;
	}

}
