package blp;

import antlr.TLParser.FlushContext;

/** a flush object used to represent a flush statement in the source program for flush infer*/
public class Flush {
	
	/** start position in source code */
	int start;
	/** stop position in source code */
	int stop;
	
	/** line in source code */
	int line;
	
	FlushContext ctx;
	
	public Flush(int start, int stop, int line, FlushContext ctx){
		this.start = start;
		this.stop = stop;
		this.line = line;
		this.ctx = ctx;
	}
	
	public int getStart(){
		return start;
	}
	
	public int getStop(){
		return stop;
	}
	
	public int getLine(){
		return line;
	}
	
	public FlushContext getContext(){
		return ctx;
	}
	
	@Override
	public boolean equals(Object other){
		if(other instanceof Flush){
			Flush otherFlush = (Flush) other;
			return (start == otherFlush.getStart()) && (stop == otherFlush.getStop()) && (line == otherFlush.getLine());
		} else {
			return false;
		}
	}
}
