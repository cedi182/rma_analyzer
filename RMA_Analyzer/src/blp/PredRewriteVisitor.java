package blp;

import java.util.ArrayList;
import java.util.List;

import antlr.TLBaseVisitor;
import antlr.TLParser.IdentifierExpressionContext;

/** a visitor that is used to extract the positions of the variable that has to be rewrtitten in the predicate source file */
public class PredRewriteVisitor extends TLBaseVisitor<Object>{
	
	String identifier;
	
	List<Integer> solution;
	
	public PredRewriteVisitor(String identifier){
		solution = new ArrayList<Integer>();
		this.identifier = identifier;
	}
	
	@Override
	public Object visitIdentifierExpression(IdentifierExpressionContext ctx) {
		if(ctx.getText().equals(identifier)){
			// add the position to the solution
			solution.add(ctx.getStart().getStartIndex());
		}
		return null;
	}
	
	public List<Integer> getSolution(){
		return solution;
	}
}
