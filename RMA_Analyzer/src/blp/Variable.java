package blp;

/** class to repesent a variable (not used anymore)*/
public class Variable {
	
	String name;
	
	boolean taken;
	
	public Variable(String name, boolean taken){
		this.name = name;
		this.taken = taken;
	}
	
	
	public String getName(){
		return name;
	}
	
	public boolean getTaken(){
		return taken;
	}
	
	@Override
	public boolean equals(Object obj){
		if(! (obj instanceof Variable)){
			return false;
		}
		Variable other = (Variable) obj;
		return this.name.equals(other.name);
	}
}
