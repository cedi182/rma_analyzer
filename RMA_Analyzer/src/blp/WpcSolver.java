package blp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import com.microsoft.z3.*;

import antlr.TLParser.AssignmentContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.LoadContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.StoreContext;
import antlr.TLParser.VarDeclContext;

/** the class used to compute the weakest pre-condition given a set of predicates, statement, and post-statement predicate*/
public class WpcSolver {
	
	Set<Predicate> addPreds;
	
	Set<Predicate> addNotPreds;
	
	Set<Predicate> predicates;
	
	Set<Predicate> notPredicates;
	
	Set<Set<Predicate>> unsatCubes;
	
	TreeToZ3Visitor z3Visitor;
	
	Set<Predicate> validSet;
	
	Set<Predicate> unsatSet;
	
	Map<CubeCache, List<Set<Set<Predicate>>>> cubeCacheMap;
	
	Map<CubeCache, Set<Set<Predicate>>> cubeCacheMap2;
	
	int maxCubeSize;
	
	int checkedSetTreshold = 500;
	
	public WpcSolver(Set<Predicate> preds, Set<Predicate> notPreds, Set<Predicate> validSet, Set<Predicate> unsatSet, Set<Set<Predicate>> unsatCubes, int cubeSize){
		predicates = preds;
		notPredicates = notPreds;
		z3Visitor = new TreeToZ3Visitor();
		this.validSet = validSet;
		this.unsatSet = unsatSet;
		this.unsatCubes = unsatCubes;
		this.maxCubeSize = cubeSize;
		this.cubeCacheMap = new HashMap<CubeCache, List<Set<Set<Predicate>>>>();
		this.cubeCacheMap2 = new HashMap<CubeCache, Set<Set<Predicate>>>();
	}
	
	List<Set<Set<Predicate>>> computeWpc(ParseTree statement, Predicate predicate){		
		z3Visitor.reset();
		// set the re write rules
		if(statement instanceof AssignmentContext){
			z3Visitor.reWrite(statement.getChild(0).getText(), statement.getChild(2), false);
				
		} else if(statement instanceof LoadContext){
			z3Visitor.reWrite(statement.getChild(4).getText(), statement.getChild(6), true);
			
		}else if(statement instanceof StoreContext){
			z3Visitor.reWrite(statement.getChild(4).getText(), statement.getChild(6), false);
			
		} else if(statement instanceof VarDeclContext){
			// if we get here, we know that there is an assignment in the variable declaration
			z3Visitor.reWrite(statement.getChild(0).getText(), statement.getChild(2), true);
		} else if(statement instanceof PutContext){
			z3Visitor.reWrite(statement.getChild(7).getText(), statement.getChild(11), true);
		} else if(statement instanceof GetContext){
			z3Visitor.reWrite(statement.getChild(0).getText(), statement.getChild(9), true);
		}
			
		return getCubes(predicate.getTree());
	}
	
	List<Set<Set<Predicate>>> computeWpc(String identifier, String replaceIdentifier, Predicate predicate){
		// set the rewrite rules
		z3Visitor.reset();
		z3Visitor.reWrite(identifier,  replaceIdentifier);
		return getCubes(predicate.getTree());
		
	}
	
	List<Set<Set<Predicate>>> computeWpc(String identifier, String replaceIdentifier, Predicate predicate, Set<Predicate> addPreds, Set<Predicate> addNotPreds){
		// set the rewrite rules
		z3Visitor.reset();
		z3Visitor.reWrite(identifier,  replaceIdentifier);
		this.addPreds = addPreds;
		this.addNotPreds = addNotPreds;
		List<Set<Set<Predicate>>> result = getCubes(predicate.getTree());
		this.addPreds = null;
		this.addNotPreds = null;
		
		return result;
		
	}
	
	Set<Set<Predicate>> computeWpc2(ParseTree predicate){
		z3Visitor.reset();
		return getCubes2(predicate);
	}
	
	List<Set<Set<Predicate>>> computeWpc(String identifier, ParseTree replaceExpression, Predicate predicate){
		z3Visitor.reset();
		// set the rewrite rules
		z3Visitor.reWrite(identifier, replaceExpression, false);
		return getCubes(predicate.getTree());
		
	}
	
	List<Set<Set<Predicate>>> computeWpc(String identifier, ParseTree replaceExpression, Predicate predicate, Set<Predicate> addPreds, Set<Predicate> addNotPreds){
		z3Visitor.reset();
		// set the rewrite rules
		z3Visitor.reWrite(identifier, replaceExpression, false);
		this.addPreds = addPreds;
		this.addNotPreds = addNotPreds;
		List<Set<Set<Predicate>>> result = getCubes(predicate.getTree());
		this.addPreds = null;
		this.addNotPreds = null;
		return result;
	}
	
	List<Set<Set<Predicate>>> computeWpc(ParseTree predicate){
		z3Visitor.reset();
		return getCubes(predicate);
	}
	
	
	
	List<Set<Set<Predicate>>> computeWpc(ParseTree statement, Predicate predicate, ParseTree identifier){
		z3Visitor.reset();
		// first we want to compute the weakest precondition as a z3 formula
		if(statement instanceof AssignmentContext){
			z3Visitor.reWrite(identifier.getText(), statement.getChild(2), false);
		} else if(statement instanceof StoreContext){
			z3Visitor.reWrite(identifier.getText(), statement.getChild(6), true);
		} else if(statement instanceof LoadContext){
			z3Visitor.reWrite(statement.getChild(4).getText(), identifier, true);
			
		} else if(statement instanceof VarDeclContext){
			// if we get here, we know that there is an assignment in the variable declaration
			z3Visitor.reWrite(identifier.getText(), statement.getChild(2), true);
		}
			
		return getCubes(predicate.getTree());
	}
	
	List<Set<Set<Predicate>>> getCubes(ParseTree predicate){
		List<Set<Set<Predicate>>> result = new ArrayList<Set<Set<Predicate>>>();
		Set<Set<Predicate>> wpcSet = new HashSet<Set<Predicate>>();
		Set<Set<Predicate>> notWpcSet = new HashSet<Set<Predicate>>();
		Context context = z3Visitor.getContext();
		
		BoolExpr wpc = (BoolExpr) z3Visitor.visit(predicate);
		CubeCache cubeCache;
		if(addPreds == null){
			cubeCache = new CubeCache(wpc.toString(), new HashSet<Predicate>(), new HashSet<Predicate>());
		} else {
			cubeCache = new CubeCache(wpc.toString(), addPreds, addNotPreds);
		}
		if(cubeCacheMap.keySet().contains(cubeCache)){
			return cubeCacheMap.get(cubeCache);
		}
		Solver solver = context.mkSolver();
		Set<String> wpcVars = z3Visitor.getContainedVars();
		z3Visitor.turnOffReWrite();
		// negate the wpc and start searching for cubes
		BoolExpr notWpc = context.mkNot(wpc);
		
		// first we check if the wpc is valid or unsat -> don't do this if we are computing notWpc because it's obsolete if we computed wpc first
		solver.push();
		solver.add(wpc);
		if(solver.check() == Status.UNSATISFIABLE){
			// the weakest precondition is unsatisfiable, no need to look for implications
			wpcSet.add(unsatSet);
			notWpcSet.add(validSet);
			result.add(wpcSet);
			result.add(notWpcSet);
			return result;
		} else {
			solver.pop();
		}
		
		solver.push();		
		//now check for validity
		solver.add(notWpc);
		
		// the weakest precondition is valid, no need to look for implications
		if(solver.check() == Status.UNSATISFIABLE){
			wpcSet.add(validSet);
			notWpcSet.add(unsatSet);
			result.add(wpcSet);
			result.add(notWpcSet);
			return result;
		}	

		List<Predicate> predList = new ArrayList<Predicate>();
		List<Predicate> notPredList = new ArrayList<Predicate>();
		if(addPreds != null){
			predList.addAll(addPreds);
		}
		predList.addAll(predicates);
		if(addNotPreds != null){
			predList.addAll(addNotPreds);
		}
		predList.addAll(notPredicates);
		Set<Predicate> predChoice = computeAffectedPreds(wpcVars, predList, maxCubeSize);
		predList = new ArrayList<Predicate>(predChoice);
		notPredList = new ArrayList<Predicate>(predChoice);
		
		
		Set<Integer> containedSet = new HashSet<Integer>();
		Set<Integer> chosenSet = new HashSet<Integer>();
		boolean searchHigherDimension = true;
		
		// first compute the sized 1 cubes for the wpc
		Iterator<Predicate> it = predList.iterator();
		
		while(it.hasNext()){
			Predicate pred = it.next();
			// first check if the predicate contains a variable that is also in the wpc
			boolean contained = false;
			for(String var: pred.getContainedVars()){
				if(wpcVars.contains(var)){
					contained = true;
					containedSet.add(pred.getId());
					break;
				}
			}
			if(! contained){
				continue;
			}
			if(pred.getMakeNot()){
				// we have a negative predicate
				solver.push();
				solver.add(context.mkNot((BoolExpr)z3Visitor.visit(pred.getTree())));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					wpcSet.add(predSet);
					it.remove();
					chosenSet.add(pred.getId());
					// also remove it from the not set because both can't be valid
					notPredList.remove(pred);
				} 
				solver.pop();
			} else {
				// we have a normal predicate
				solver.push();
				solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					wpcSet.add(predSet);
					it.remove();
					chosenSet.add(pred.getId());
					// also remove it from the not set because both can't be valid
					notPredList.remove(pred);
				}
				solver.pop();
			}
		}
		
		if(chosenSet.equals(containedSet)){
			searchHigherDimension = false;
		}
		
		containedSet = new HashSet<Integer>();
		chosenSet = new HashSet<Integer>();
		boolean notSearchHigherDimension = true;
		
		Solver notSolver = context.mkSolver();
		notSolver.push();		
		notSolver.add(wpc);
		// now compute it for the notWpc
		it = notPredList.iterator();
		
		while(it.hasNext()){
			Predicate pred = it.next();
			// first check if the predicate contains a variable that is also in the wpc
			boolean contained = false;
			for(String var: pred.getContainedVars()){
				if(wpcVars.contains(var)){
					contained = true;
					containedSet.add(pred.getId());
					break;
				}
			}
			if(! contained){
				continue;
			}
			if(pred.getMakeNot()){
				// we have a negative predicate
				notSolver.push();
				notSolver.add(context.mkNot((BoolExpr)z3Visitor.visit(pred.getTree())));
				if(notSolver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					notWpcSet.add(predSet);
					chosenSet.add(pred.getId());
					it.remove();
				}
				notSolver.pop();
			} else {
				// we have a normal predicate
				notSolver.push();
				notSolver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
				if(notSolver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					notWpcSet.add(predSet);
					chosenSet.add(pred.getId());
					it.remove();
					// also remove it from the not set because both can't be valid
					notPredList.remove(pred);
				}
				notSolver.pop();
			}
		}
		
		if(chosenSet.equals(containedSet)){
			notSearchHigherDimension = false;
		}
		
		if(!searchHigherDimension && !notSearchHigherDimension || maxCubeSize == 1){
			result = new ArrayList<Set<Set<Predicate>>>();
			result.add(wpcSet);
			result.add(notWpcSet);
			return result;
		}
		
		int wpcMaxSize = getHighestCubeSize(predList);
		int notWpcMaxSize = getHighestCubeSize(notPredList);
		
		
		for(int i = 2; i <= maxCubeSize; i++){
			result = solveWpcHigherOrder(i, wpcSet, predList, solver, notWpcSet, notPredList, notSolver, searchHigherDimension, notSearchHigherDimension, wpcMaxSize, notWpcMaxSize);
		}
		
		cubeCacheMap.put(cubeCache, result);
		
		return result;
	}
	
	public int getHighestCubeSize(List<Predicate> predList){
		Set<Integer> idSet = new HashSet<Integer>();
		for(Predicate pred: predList){
			idSet.add(pred.getId());
		}
		return idSet.size();
	}
	
	public List<Set<Set<Predicate>>> solveWpcHigherOrder(int cubeOrder, Set<Set<Predicate>> partSol, List<Predicate> preds, Solver solver, Set<Set<Predicate>> notPartSol, List<Predicate> notPreds, Solver notSolver, boolean search, boolean notSearch, int wpcMaxSize, int notWpcMaxSize){
		Context context = z3Visitor.getContext();
		Set<Set<Predicate>> checkedSet = new HashSet<Set<Predicate>>();
		Set<Set<Predicate>> notCheckedSet = new HashSet<Set<Predicate>>();
		int[] counters = new int[cubeOrder];
		for(int i = 0; i < cubeOrder; i++){
			counters[i] = Math.min(i, preds.size() - 1);
		}
		if(search && wpcMaxSize >= cubeOrder){
			while(counters[0] < preds.size()){
				boolean checkSat = true;
				//add the predicates to the set and check if a part of the formula is already present in partSol
				Set<Predicate> predSet = new HashSet<Predicate>();
				Set<Integer> idSet = new HashSet<Integer>();
				for(int i=0; i < cubeOrder; i++){
					// remove the cases where a predicate and its negation is in the cube
					if(idSet.contains(preds.get(counters[i]).getId())){
						checkSat = false;
						break;
					}
					predSet.add(preds.get(counters[i]));
					idSet.add(preds.get(counters[i]).getId());
				}
				if(checkSat && predSet.size() < cubeOrder){
					// remove the cases where a predicate is contained 2 times in the cube
					checkSat = false;
				}
				
				if(checkSat && checkedSet.contains(predSet)){
					checkSat = false;
				}
				
				if(checkSat){
					for(Set<Predicate> unsatSet: unsatCubes){
						if(predSet.containsAll(unsatSet)){
							// our predSet contains a cube that is not satisfiable -> no need to check it 
							checkSat = false;
							break;
						}
					}
				}
				
				if(checkSat){
					for(Set<Predicate> partSolSet: partSol){
						// remove the case where the cube already contains a cube that implies the wpc
						if(predSet.containsAll(partSolSet)){
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					for(Set<Predicate> partSolSet: notPartSol){
						// remove the case where the cube already contains a part that implied the not wpc -> can't imply wpc
						if(predSet.containsAll(partSolSet)){
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					// add the predicates to the solver
					solver.push();
					for(Predicate pred: predSet){
						if(pred.getMakeNot()){
							solver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
						} else {
							solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
						}
					}
					if(solver.check() == Status.UNSATISFIABLE){
						// predSet implies the weakest precondition -> add it to partSol
						partSol.add(predSet);
						// add it to the not checked set, so we won't check if we are looking for the not wpc
						notCheckedSet.add(predSet);
					}
					solver.pop();
					checkedSet.add(predSet);
				}
				
				
				//increment the counters
				counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
				if(counters[cubeOrder - 1] == preds.size()){
					int i = cubeOrder - 1;
					while(i >= 0){
						if(counters[i] == preds.size() && i != 0){
							counters[i] = 0;
							counters[i-1] = counters[i-1] + 1;
							i--;
						} else {
							break;
						}
					}
				}
				
				while(!checkCounters(counters)){
					// keep increasing the counters
					counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
					if(counters[cubeOrder - 1] == preds.size()){
						int i = cubeOrder - 1;
						while(i >= 0){
							if(counters[i] == preds.size() && i != 0){
								counters[i] = 0;
								counters[i-1] = counters[i-1] + 1;
								i--;
							} else {
								break;
							}
						}
					}
				}
				
			}
		}
		
		if(notSearch && notWpcMaxSize >= cubeOrder){
			// now do the same thing for the not wpc
			counters = new int[cubeOrder];
			for(int i = 0; i < cubeOrder; i++){
				counters[i] = Math.min(i, notPreds.size() - 1);
			}
			while(counters[0] < notPreds.size()){
				boolean checkSat = true;
				//add the predicates to the set and check if a part of the formula is already present in partSol
				Set<Predicate> predSet = new HashSet<Predicate>();
				Set<Integer> idSet = new HashSet<Integer>();
				for(int i=0; i < cubeOrder; i++){
					// remove the cases where a predicate and its negation is in the cube
					if(idSet.contains(notPreds.get(counters[i]).getId())){
						checkSat = false;
						break;
					}
					predSet.add(notPreds.get(counters[i]));
					idSet.add(notPreds.get(counters[i]).getId());
				}
				if(checkSat && predSet.size() < cubeOrder){
					// remove the cases where a predicate is contained 2 times in the cube
					checkSat = false;
				}
				if(checkSat &&notCheckedSet.contains(predSet)){
					checkSat = false;
				}
				if(checkSat){
					for(Set<Predicate> unsatSet: unsatCubes){
						if(predSet.containsAll(unsatSet)){
							// our predSet contains a cube that is not satisfiable -> no need to check it 
							checkSat = false;
							break;
						}
					}
				}
				
				if(checkSat){
					for(Set<Predicate> partSolSet: notPartSol){
						// remove the case where the cube already contains a cube that implies the not-wpc
						if(predSet.containsAll(partSolSet)){
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					for(Set<Predicate> partSolSet: partSol){
						// remove the case where the cube already contains a part that implied the wpc -> can't imply not-wpc
						if(predSet.containsAll(partSolSet)){
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					// add the predicates to the solver
					notSolver.push();
					for(Predicate pred: predSet){
						if(pred.getMakeNot()){
							notSolver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
						} else {
							notSolver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
						}
					}
					if(notSolver.check() == Status.UNSATISFIABLE){
						// predSet implies the weakest precondition -> add it to partSol
						notPartSol.add(predSet);
						checkedSet.add(predSet);
					}
					notSolver.pop();
					notCheckedSet.add(predSet);
				}
				
				
				//increment the counters
				counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
				if(counters[cubeOrder - 1] == notPreds.size()){
					int i = cubeOrder - 1;
					while(i >= 0){
						if(counters[i] == notPreds.size() && i != 0){
							counters[i] = 0;
							counters[i-1] = counters[i-1] + 1;
							i--;
						} else {
							break;
						}
					}
				}
				
				while(!checkCounters(counters)){
					// keep increasing the counters
					counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
					if(counters[cubeOrder - 1] == notPreds.size()){
						int i = cubeOrder - 1;
						while(i >= 0){
							if(counters[i] == notPreds.size() && i != 0){
								counters[i] = 0;
								counters[i-1] = counters[i-1] + 1;
								i--;
							} else {
								break;
							}
						}
					}
				}
				
			}
		}
		
		List<Set<Set<Predicate>>> result = new ArrayList<Set<Set<Predicate>>>();
		result.add(partSol);
		result.add(notPartSol);
		return result;
	}
		
	
	Set<Set<Predicate>> getCubes2(ParseTree predicate){
		Set<Set<Predicate>> wpcSet = new HashSet<Set<Predicate>>();
		Context context = z3Visitor.getContext();
		
		
		BoolExpr wpc = (BoolExpr) z3Visitor.visit(predicate);
		
		CubeCache cubeCache;
		if(addPreds == null){
			cubeCache = new CubeCache(wpc.toString(), new HashSet<Predicate>(), new HashSet<Predicate>());
		} else {
			cubeCache = new CubeCache(wpc.toString(), addPreds, addNotPreds);
		}
		if(cubeCacheMap2.keySet().contains(cubeCache)){
			return cubeCacheMap2.get(cubeCache);
		}
		
		Solver solver = context.mkSolver();
		Set<String> wpcVars = z3Visitor.getContainedVars();
		z3Visitor.turnOffReWrite();
		// negate the wpc and start searching for cubes
		BoolExpr notWpc = context.mkNot(wpc);
		
		// first we check if the wpc is valid or unsat -> don't do this if we are computing notWpc because it's obsolete if we computed wpc first
		solver.push();
		
		solver.add(wpc);
		if(solver.check() == Status.UNSATISFIABLE){
			// the weakest precondition is unsatisfiable, no need to look for implications
			wpcSet.add(unsatSet);
			return wpcSet;
		} else {
			solver.pop();
		}
			
		
		solver.push();		
		//now check for validity
		solver.add(notWpc);
		
		// the weakest precondition is valid, no need to look for implications
		if(solver.check() == Status.UNSATISFIABLE){
			wpcSet.add(validSet);
			return wpcSet;
		}
				
		List<Predicate> predList = new ArrayList<Predicate>();
		predList.addAll(predicates);
		predList.addAll(notPredicates);
		Set<Predicate> predChoice = computeAffectedPreds(wpcVars, predList, maxCubeSize);
		predList = new ArrayList<Predicate>(predChoice);
		
		Set<Integer> containedSet = new HashSet<Integer>();
		Set<Integer> chosenSet = new HashSet<Integer>();
		Iterator<Predicate> it = predList.iterator();
		
		while(it.hasNext()){
			Predicate pred = it.next();
			
			// first check if the predicate contains a variable that is also in the wpc
			boolean contained = false;
			for(String var: pred.getContainedVars()){
				if(wpcVars.contains(var)){
					contained = true;
					containedSet.add(pred.getId());
					break;
				}
			}
			if(! contained){
				continue;
			}

			if(pred.getMakeNot()){
				// we have a negative predicate
				solver.push();
				solver.add(context.mkNot((BoolExpr)z3Visitor.visit(pred.getTree())));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					wpcSet.add(predSet);
					chosenSet.add(pred.getId());
					it.remove();
				}
				solver.pop();
			} else {
				// we have a normal predicate
				solver.push();
				solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					wpcSet.add(predSet);
					chosenSet.add(pred.getId());
					it.remove();
				}
				solver.pop();
			}
		}

		if(chosenSet.equals(containedSet)){
			return wpcSet;
		}
		int wpcMaxSize = getHighestCubeSize(predList);
		for(int i = 2; i <= maxCubeSize; i++){
			wpcSet = solveWpcHigherOrder(i, wpcSet, predList, solver, wpcMaxSize);
		}
		
		cubeCacheMap2.put(cubeCache, wpcSet);
		
		return wpcSet;
	}
	
	public Set<Set<Predicate>> solveWpcHigherOrder(int cubeOrder, Set<Set<Predicate>> partSol, List<Predicate> preds, Solver solver, int wpcMaxSize){
		Context context = z3Visitor.getContext();
		Set<Set<Predicate>> checkedSet = new HashSet<Set<Predicate>>();
		int[] counters = new int[cubeOrder];
		if(wpcMaxSize >= cubeOrder){
			while(counters[0] < preds.size()){
				boolean checkSat = true;
				//add the predicates to the set and check if a part of the formula is already present in partSol
				Set<Predicate> predSet = new HashSet<Predicate>();
				Set<Integer> idSet = new HashSet<Integer>();
				for(int i=0; i < cubeOrder; i++){
					// remove the cases where a predicate and its negation is in the cube
					if(idSet.contains(preds.get(counters[i]).getId())){
						checkSat = false;
						break;
					}
					predSet.add(preds.get(counters[i]));
					idSet.add(preds.get(counters[i]).getId());
				}
				if(checkSat && predSet.size() < cubeOrder){
					// remove the cases where a predicate is contained 2 times in the cube
					checkSat = false;
				}
				
				if(checkSat && checkedSet.contains(predSet)){
					// remove the case where we already checked the satisfiability -> case [1][2] results in same cube as [2][1]
					checkSat = false;
				}
				
				if(checkSat){
					for(Set<Predicate> unsatSet: unsatCubes){
						if(predSet.containsAll(unsatSet)){
							// our predSet contains a cube that is not satisfiable -> no need to check it 
							checkSat = false;
							break;
						}
					}
				}
				
				if(checkSat){
					for(Set<Predicate> partSolSet: partSol){
						// remove the case where the cube already contains a cube that implies the wpc
						if(predSet.containsAll(partSolSet)){
							checkSat = false;
							break;
						}
					}
				}
				if(checkSat){
					// add the predicates to the solver
					solver.push();
					for(Predicate pred: predSet){
						if(pred.getMakeNot()){
							solver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
						} else {
							solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
						}
					}
					if(solver.check() == Status.UNSATISFIABLE){
						// predSet implies the weakest precondition -> add it to partSol
						partSol.add(predSet);
					}
					solver.pop();
					checkedSet.add(predSet);
				}
				
				
				//increment the counters
				counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
				if(counters[cubeOrder - 1] == preds.size()){
					int i = cubeOrder - 1;
					while(i >= 0){
						if(counters[i] == preds.size() && i != 0){
							counters[i] = 0;
							counters[i-1] = counters[i-1] + 1;
							i--;
						} else {
							break;
						}
					}
				}
				while(!checkCounters(counters)){
					// keep increasing the counters
					counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
					if(counters[cubeOrder - 1] == preds.size()){
						int i = cubeOrder - 1;
						while(i >= 0){
							if(counters[i] == preds.size() && i != 0){
								counters[i] = 0;
								counters[i-1] = counters[i-1] + 1;
								i--;
							} else {
								break;
							}
						}
					}
				}
				
			}
		}
		return partSol;
	}
	
	/** computes a list of predicates that may change after assigning a new value to identifier */
	public Set<Predicate> computeAffectedPreds(String identifier, Set<Predicate> predicates){
		Set<Predicate> result = new HashSet<Predicate>();
		
		for(Predicate pred: predicates){
				if(pred.getContainedVars().contains(identifier)){
					result.add(pred);
				}
		}
		return result;
	}
	
	/** includes transitive preds */
	public Set<Predicate> computeAffectedPreds(Set<String> vars, List<Predicate> predicates, int cubeSize){
		Set<Predicate> result = new HashSet<Predicate>();
		
		if(cubeSize == 1){
			// only inlcude the predicates that directly contain the variable
			for(Predicate pred: predicates){
				for(String var: vars){
					if(pred.getContainedVars().contains(var)){
						result.add(pred);
						break;
					}
				}
			}
		} else {
			for(int i = 1; i < cubeSize; i++){
				result.addAll(computeAffectedPreds(vars, predicates, i));
			}
			Set<String> identifiers = new HashSet<String>();
			// first add all identifers that we have in predicates so far
			// no need to add the vars argument here as we already added those predicates in cubesize 1
			for(Predicate pred: result){
				identifiers.addAll(pred.getContainedVars());
			}
			// now add all the predicates that contain the identifiers
			for(Predicate pred: predicates){
				for(String var: identifiers){
					if(pred.getContainedVars().contains(var)){
						result.add(pred);
						break;
					}
				}
			}
		}
		return result;
	}
		
	/** returns true if no value in counters is the same, else false */
	public boolean checkCounters(int[] counters){
		Set<Integer> checkSet = new HashSet<Integer>();
		for(int i = 0; i < counters.length; i++){
			if(!checkSet.add(counters[i])){
				return false;
			}
		}
		return true;
	}
}
