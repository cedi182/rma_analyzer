package util;

import java.util.TimerTask;

/** a class used to create a timer interuptable task for a thread (not used anymore)*/
public class InterruptTimerTask extends TimerTask {

    private Thread theTread;

    public InterruptTimerTask(Thread theTread) {
        this.theTread = theTread;
    }

    @Override
    public void run() {
        theTread.interrupt();
    }

}
