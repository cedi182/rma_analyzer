package main;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLLexer;
import antlr.TLParser;
import blp.BlpVisitor;
import blp.CasPredicate;
import blp.Flush;
import blp.Predicate;
import blp.RdmaStatementExtractVisitor;
import blp.RemoteBlpVisitor;
import blp.RgaPredicate;
import blp.VarVisitor;
import flowgraph.FlowGraphAnalyzer;
import org.apache.commons.cli.*;

/** the main class*/
public class RmaAnalyzer {
	
	/** the timeout for fender in seconds */
	static long timeOut = 5;
	
	static long totalStartTime;
	
	static List<Long> fenderTimes;
	
	static List<Long> blpTimes;
	
	static List<Long> flowGraphTimes;
	
	static StringBuilder statsBuilder;
	
	static String sourceFile;
	
	static int numberOfPreds;
	
	static int numberOfRdmaPreds;
	
	static int cubeSize = 3;
	
	static int flushCount;
	
	static boolean flushRemoved = false;
	
	static boolean checkTrace = false;
	
	static boolean noFlushLookahead = false;
	
	static boolean noStaticAnalysis = false;
	
	static String checkMethod;
	
	static String WP = "WP";
	
	static String Interp = "Interp";
	
	static Map<Set<Flush>, Long> solutionTimes;
	
	static boolean noExtrapolation = false;
	
	public static void main(String[] args){
		
		totalStartTime = System.currentTimeMillis();
		fenderTimes = new ArrayList<Long>();
		blpTimes = new ArrayList<Long>();
		flowGraphTimes = new ArrayList<Long>();
		statsBuilder = new StringBuilder();
		
		// parse the arguments and run the analysis
		String mode = parseAndRunArgs(args);

		// save the stats
		if(fenderTimes.size() > 0){
			long acc = 0;
			int i = 0;
			for(long l: fenderTimes){
				acc = acc + l;
				i++;
			}
			statsBuilder.append("Average fender time: " + (acc/i) + " ms \r\n");
		}
		if(blpTimes.size() > 0){
			long acc = 0;
			int i = 0;
			for(long l: blpTimes){
				acc = acc + l;
				i++;
			}
			statsBuilder.append("Average blp time: " + (acc/i) + " ms \r\n");
		}
		if(flowGraphTimes.size() > 0){
			long acc = 0;
			int i = 0;
			for(long l: flowGraphTimes){
				acc = acc + l;
				i++;
			}
			statsBuilder.append("Average flow graph time: " + (acc/i) + " ms \r\n");
		}
		
		long endTime = System.currentTimeMillis() - totalStartTime;
		System.out.println("Run time: "+ endTime + "ms");
		statsBuilder.append("Total Run time: "+ endTime + "ms \r\n");
		// now append the result for the table
		if(solutionTimes != null){
			String tableString = "";
			tableString = tableString + cubeSize + " ";
			tableString = tableString + numberOfPreds + " ";
			tableString = tableString + numberOfRdmaPreds + " ";
			tableString = tableString + blpTimes.get(0) + "ms ";
			int maxFlushesRemoved = 0;
			for(Set<Flush> solSet: solutionTimes.keySet()){
				if(flushRemoved){
					maxFlushesRemoved = Math.max(maxFlushesRemoved, solSet.size());
					tableString = tableString + solutionTimes.get(solSet) + "ms ";
				} else {
					tableString = tableString + solutionTimes.get(solSet) + "ms ";
				}
			}
			tableString = tableString + endTime + "ms ";
			tableString = tableString + maxFlushesRemoved + " ";
			tableString = tableString + flushCount;
			System.out.println(tableString);
			statsBuilder.append(tableString + "\r\n");
		}
		// save the stats file
		try{
        	PrintWriter writer;
    		String writtenFile = sourceFile.substring(0, sourceFile.length() - 3) + "_" + mode + ".stats";
    		writer = new PrintWriter(writtenFile, "UTF-8");
            writer.append(statsBuilder);
            writer.close();
        } catch (IOException e) {
           e.printStackTrace();
        }
	}
	
	public static void runLocalCase(String inputFileName, String predFileName, int cubeSize, int unsatCubeSize){
		// instantiate the BlpVisitor and generate the boolean program
        StringBuilder blpBuilder = new StringBuilder();
        List<Set<Predicate>> predicates = extractPreds(predFileName);
        ParseTree tree = extractTree(inputFileName);
        RdmaStatementExtractVisitor extrVisitor = new RdmaStatementExtractVisitor(predicates.get(0).size(), predicates.get(0), predicates.get(1), false);
        extrVisitor.visit(tree);
        
        // add the predicates for the RGA locals to the normal predicates
        for(RgaPredicate pred: extrVisitor.getRgaPreds().values()){
        	predicates.get(0).addAll(pred.getAddPreds1());
        	predicates.get(0).addAll(pred.getAddPreds2());
        	predicates.get(1).addAll(pred.getAddNotPreds1());
        	predicates.get(1).addAll(pred.getAddNotPreds2());
        }
        //add the predicates for the CAS locals to the normal predicates
        for(CasPredicate pred: extrVisitor.getCasPreds().values()){
        	predicates.get(0).addAll(pred.getAddPreds1());
        	predicates.get(0).addAll(pred.getAddPreds2());
        	predicates.get(0).addAll(pred.getAddPreds3());
        	predicates.get(1).addAll(pred.getAddNotPreds1());
        	predicates.get(1).addAll(pred.getAddNotPreds2());
        	predicates.get(1).addAll(pred.getAddNotPreds3());
        }
        
        long blpTime = System.currentTimeMillis();
        BlpVisitor blpVisitor = new BlpVisitor(blpBuilder, predicates.get(0), predicates.get(1), extrVisitor.getPutPreds(), extrVisitor.getGetPreds(), extrVisitor.getCasPreds(), extrVisitor.getRgaPreds(), cubeSize, unsatCubeSize);
        blpVisitor.visit(tree);
        long endTime = System.currentTimeMillis() - blpTime;
        System.out.println("Blp time: " + endTime + "ms");
    	statsBuilder.append("Blp time: " + endTime + "ms \r\n");
        blpTimes.add(endTime);
        String writtenFile = null;
        
        // save the resulting program to a file
        try{
        	PrintWriter writer;
        	writtenFile = inputFileName.substring(0, inputFileName.length() - 3) + "_local.bl";
        	writer = new PrintWriter(writtenFile, "UTF-8");
            writer.append(blpBuilder);
            writer.close();
        } catch (IOException e) {
           e.printStackTrace();
        }
        
        File writtenF = new File(writtenFile.substring(2, writtenFile.length()));
        File writtenPF = null;
        File writtenSPF = null;
        String[] fenderArgs = null;
        
        if(checkTrace){
        	// save the fender predicate file
        	String writtenPredFile = null;
            try{
            	PrintWriter writer;
        		writtenPredFile = inputFileName.substring(0, inputFileName.length() - 3) + "_local.fpreds";
        		writer = new PrintWriter(writtenPredFile, "UTF-8");
                writer.append(blpVisitor.getFenderPredString());
                writer.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
            writtenPF = new File(writtenPredFile.substring(2, writtenPredFile.length()));
            // create a new empty file for synthesized preds
            String writtenNewPredFile = null;
            try{
            	PrintWriter writer;
        		writtenNewPredFile = inputFileName.substring(0, inputFileName.length() - 3) + "_local_synthesized.fpreds";
        		writer = new PrintWriter(writtenNewPredFile, "UTF-8");
                writer.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
            /* test/peterson_remote.bl sc -predicateFile test/peterson.preds -synthesizePredicatesInto test/peterson_new.preds -predicateSynthesisMethod WP -errorPath succinct -keepTransitionSystem True */
            writtenSPF = new File(writtenNewPredFile.substring(2, writtenNewPredFile.length()));
            fenderArgs = new String[13];
            fenderArgs[0] = writtenF.getAbsolutePath();
            fenderArgs[1] = "sc";
            fenderArgs[2] = "-predicateFile";
            fenderArgs[3] = writtenPF.getAbsolutePath();
            fenderArgs[4] = "-synthesizePredicatesInto";
            fenderArgs[5] = writtenSPF.getAbsolutePath();
            fenderArgs[6] = "-predicateSynthesisMethod";
            if(checkMethod.equals(WP)){
           	 fenderArgs[7] = WP;
            } else {
           	 fenderArgs[7] = Interp;
            }
            fenderArgs[8] = "-errorPath";
            fenderArgs[9] = "succinct";
            fenderArgs[10] = "-keepTransitionSystem";
            fenderArgs[11] = "True";
            fenderArgs[12] = "-lessOutput";
            
        } else {
        	fenderArgs = new String[3];
            fenderArgs[0] = writtenF.getAbsolutePath();
            fenderArgs[1] = "sc";
            fenderArgs[2] = "-lessOutput";
        }
        
        // now call fender
        long fenderTime = System.currentTimeMillis();
        FenderOutput output = callFender(fenderArgs);
        endTime = (System.currentTimeMillis() - fenderTime);
	    System.out.println("Fender runtime: " + endTime + "ms");
	    statsBuilder.append("Fender runtime: " + endTime + "ms \r\n");
	    fenderTimes.add(endTime);
        
        if(output != null && output.isSuccessful()){
        	System.out.println("The program verified");
        	statsBuilder.append("The program verified \r\n");
        	if(checkTrace){
        		// delete the synthesized predicates file as there was no error state
        		writtenSPF.delete();
        	}
        }else {
        	System.out.println("The program didn't verify");
        	statsBuilder.append("The program didn't verify \r\n");
        	if(checkTrace){
	    		if(output.getTraceOk()){
	    			System.out.println("The error trace seems to be ok.");
	    			statsBuilder.append("The error trace seems to be ok.");
	    		} else {
	    			System.out.println("The error trace seems to be spurious. Check the file with synthesized predicates and try adding them to your predicates.");
	    			statsBuilder.append("The error trace seems to be spurious. Check the file with synthesized predicates and try adding them to your predicates.");
	    		}
	    	}
        }

        
	}
	
	static Set<Set<Flush>> triedSet;
	
	public static void runRemoteCase(String inputFileName, String predFileName, boolean accumulate, int cubeSize, int unsatCubeSize){		        
        // instantiate the BlpVisitor and generate the boolean program
		solutionTimes = new HashMap<Set<Flush>, Long>();
		StringBuilder blpBuilder = new StringBuilder();
        List<Set<Predicate>> predicates = extractPreds(predFileName);
        ParseTree tree = extractTree(inputFileName);
        RdmaStatementExtractVisitor extrVisitor = new RdmaStatementExtractVisitor(predicates.get(0).size(), predicates.get(0), predicates.get(1), accumulate);
        extrVisitor.visit(tree);
        
        numberOfRdmaPreds = extrVisitor.getCurId() - numberOfPreds;
        
        // add the predicates for the RGA locals to the normal predicates
        for(RgaPredicate pred: extrVisitor.getRgaPreds().values()){
        	predicates.get(0).addAll(pred.getAddPreds1());
        	predicates.get(0).addAll(pred.getAddPreds2());
        	predicates.get(1).addAll(pred.getAddNotPreds1());
        	predicates.get(1).addAll(pred.getAddNotPreds2());
        }
        
        for(CasPredicate pred: extrVisitor.getCasPreds().values()){
        	predicates.get(0).addAll(pred.getAddPreds1());
        	predicates.get(0).addAll(pred.getAddPreds2());
        	predicates.get(0).addAll(pred.getAddPreds3());
        	predicates.get(1).addAll(pred.getAddNotPreds1());
        	predicates.get(1).addAll(pred.getAddNotPreds2());
        	predicates.get(1).addAll(pred.getAddNotPreds3());
        }
        
        // first run the SC case        
        long blpTime = System.currentTimeMillis();
        BlpVisitor lBlpVisitor = new BlpVisitor(blpBuilder, predicates.get(0), predicates.get(1), extrVisitor.getPutPreds(), extrVisitor.getGetPreds(), extrVisitor.getCasPreds(), extrVisitor.getRgaPreds(), cubeSize, unsatCubeSize);
        lBlpVisitor.visit(tree);
        long endTime = System.currentTimeMillis() - blpTime;
        System.out.println("Blp time: " + endTime + "ms");
    	statsBuilder.append("Blp time: " + endTime + "ms \r\n");
        blpTimes.add(endTime);
        String writtenFile = null;
        
        // save the resulting program to a file
        try{
        	PrintWriter writer;
        	writtenFile = inputFileName.substring(0, inputFileName.length() - 3) + "_local.bl";
        	writer = new PrintWriter(writtenFile, "UTF-8");
            writer.append(blpBuilder);
            writer.close();
        } catch (IOException e) {
           e.printStackTrace();
        }
        
        File writtenF = new File(writtenFile.substring(2, writtenFile.length()));
        File writtenPF = null;
        File writtenSPF = null;
        String[] fenderArgs = null;
        
        if(checkTrace){
        	// save the fender predicate file
        	String writtenPredFile = null;
            try{
            	PrintWriter writer;
        		writtenPredFile = inputFileName.substring(0, inputFileName.length() - 3) + "_local.fpreds";
        		writer = new PrintWriter(writtenPredFile, "UTF-8");
                writer.append(lBlpVisitor.getFenderPredString());
                writer.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
            writtenPF = new File(writtenPredFile.substring(2, writtenPredFile.length()));
            // create a new empty file for synthesized preds
            String writtenNewPredFile = null;
            try{
            	PrintWriter writer;
        		writtenNewPredFile = inputFileName.substring(0, inputFileName.length() - 3) + "_local_synthesized.fpreds";
        		writer = new PrintWriter(writtenNewPredFile, "UTF-8");
                writer.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
            /* test/peterson_remote.bl sc -predicateFile test/peterson.preds -synthesizePredicatesInto test/peterson_new.preds -predicateSynthesisMethod WP -errorPath succinct -keepTransitionSystem True */
            writtenSPF = new File(writtenNewPredFile.substring(2, writtenNewPredFile.length()));
            fenderArgs = new String[13];
            fenderArgs[0] = writtenF.getAbsolutePath();
            fenderArgs[1] = "sc";
            fenderArgs[2] = "-predicateFile";
            fenderArgs[3] = writtenPF.getAbsolutePath();
            fenderArgs[4] = "-synthesizePredicatesInto";
            fenderArgs[5] = writtenSPF.getAbsolutePath();
            fenderArgs[6] = "-predicateSynthesisMethod";
            if(checkMethod.equals(WP)){
           	 fenderArgs[7] = WP;
            } else {
           	 fenderArgs[7] = Interp;
            }
            fenderArgs[8] = "-errorPath";
            fenderArgs[9] = "succinct";
            fenderArgs[10] = "-keepTransitionSystem";
            fenderArgs[11] = "True";
            fenderArgs[12] = "-lessOutput";
            
        } else {
        	fenderArgs = new String[3];
            fenderArgs[0] = writtenF.getAbsolutePath();
            fenderArgs[1] = "sc";
            fenderArgs[2] = "-lessOutput";
        }
        
        // now call fender
        long fenderTime = System.currentTimeMillis();
        FenderOutput output = null;
        output = callFender(fenderArgs);
        endTime = (System.currentTimeMillis() - fenderTime);
	    System.out.println("Fender runtime: " + endTime + "ms");
	    statsBuilder.append("Fender runtime: " + endTime + "ms \r\n");
	    fenderTimes.add(endTime);
        
	    
        if(output != null && output.isSuccessful()){
        	System.out.println("The local program verified");
        	statsBuilder.append("The local program verified \r\n");
        	if(checkTrace){
        		// delete the synthesized predicates file as there was no error state
        		writtenSPF.delete();
        	}
        }else {
        	System.out.println("The local program didn't verify");
        	statsBuilder.append("The local program didn't verify \r\n");
        	if(checkTrace){
	    		if(output.getTraceOk()){
	    			System.out.println("The error trace seems to be ok.");
	    			statsBuilder.append("The error trace seems to be ok.");
	    		} else {
	    			System.out.println("The error trace seems to be spurious. Check the file with synthesized predicates and try adding them to your predicates.");
	    			statsBuilder.append("The error trace seems to be spurious. Check the file with synthesized predicates and try adding them to your predicates.");
	    		}
	    	}
        	// don't go to the remote case if the local case didn't verify
        	return;
        }
        // now go to the remote case
        blpBuilder = new StringBuilder();
        long flowGraphTime = System.currentTimeMillis();
        FlowGraphAnalyzer flowGraphAnalyzer = new FlowGraphAnalyzer(tree, extrVisitor.getPutPreds(), extrVisitor.getGetPreds(), extrVisitor.getCasPreds(), extrVisitor.getRgaPreds(), new HashSet<Flush>(), noStaticAnalysis);
        flowGraphAnalyzer.analyzeGraphs();
        endTime = System.currentTimeMillis() - flowGraphTime;
        System.out.println("Flow graph time: " + endTime + "ms");
        statsBuilder.append("Flow graph time: " + endTime + "ms \r\n");
        flowGraphTimes.add(endTime);
        blpTime = System.currentTimeMillis();
        RemoteBlpVisitor blpVisitor = new RemoteBlpVisitor(blpBuilder, predicates.get(0), predicates.get(1), extrVisitor.getPutPreds(), extrVisitor.getGetPreds(), extrVisitor.getCasPreds(), extrVisitor.getRgaPreds(), flowGraphAnalyzer.getGraphs(), accumulate, cubeSize, unsatCubeSize, extrVisitor.getAssertionVars(), lBlpVisitor.getWpcSolver(), lBlpVisitor.getSpcSolver(), lBlpVisitor.getValidSet(), lBlpVisitor.getUnsatSet(), lBlpVisitor.getUnsatSolver(), noExtrapolation, extrVisitor.getMaxProcess(), noFlushLookahead);
        // build the boolean program
    	blpVisitor.visit(tree);
    	endTime = System.currentTimeMillis() - blpTime;
    	System.out.println("Blp time: " + endTime + "ms");
    	statsBuilder.append("Blp time: " + endTime + "ms \r\n");
    	blpTimes.add(endTime);
    	
    	// now call fender to verify the program
    	writtenFile = null;
    	// save the resulting program to a file
        try{
        	PrintWriter writer;
    		writtenFile = inputFileName.substring(0, inputFileName.length() - 3) + "_remote_all_flushes.bl";
    		writer = new PrintWriter(writtenFile, "UTF-8");
            writer.append(blpBuilder);
            writer.close();
        } catch (IOException e) {
           e.printStackTrace();
        }
        writtenF = new File(writtenFile.substring(2, writtenFile.length()));
        writtenPF = null;
        writtenSPF = null;
        fenderArgs = null;
        if(checkTrace){
        	// save the fender predicate file
        	String writtenPredFile = null;
            try{
            	PrintWriter writer;
        		writtenPredFile = inputFileName.substring(0, inputFileName.length() - 3) + "_remote.fpreds";
        		writer = new PrintWriter(writtenPredFile, "UTF-8");
                writer.append(blpVisitor.getFenderPredString());
                writer.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
            writtenPF = new File(writtenPredFile.substring(2, writtenPredFile.length()));
            // create a new empty file for synthesized preds
            String writtenNewPredFile = null;
            try{
            	PrintWriter writer;
        		writtenNewPredFile = inputFileName.substring(0, inputFileName.length() - 3) + "_remote_allFlushes_synthesized.fpreds";
        		writer = new PrintWriter(writtenNewPredFile, "UTF-8");
                writer.close();
            } catch (IOException e) {
               e.printStackTrace();
            }
           
            writtenSPF = new File(writtenNewPredFile.substring(2, writtenNewPredFile.length()));
            fenderArgs = new String[13];
            fenderArgs[0] = writtenF.getAbsolutePath();
            fenderArgs[1] = "sc";
            fenderArgs[2] = "-predicateFile";
            fenderArgs[3] = writtenPF.getAbsolutePath();
            fenderArgs[4] = "-synthesizePredicatesInto";
            fenderArgs[5] = writtenSPF.getAbsolutePath();
            fenderArgs[6] = "-predicateSynthesisMethod";
            if(checkMethod.equals(WP)){
           	 fenderArgs[7] = WP;
            } else {
           	 fenderArgs[7] = Interp;
            }
            fenderArgs[8] = "-errorPath";
            fenderArgs[9] = "succinct";
            fenderArgs[10] = "-keepTransitionSystem";
            fenderArgs[11] = "True";
            fenderArgs[12] = "-lessOutput";
        } else {
        	fenderArgs = new String[3];
            fenderArgs[0] = writtenF.getAbsolutePath();
            fenderArgs[1] = "sc";
            fenderArgs[2] = "-lessOutput";
        }
        
        // now call fender
        fenderTime = System.currentTimeMillis();
        output = callFender(fenderArgs);
        endTime = (System.currentTimeMillis() - fenderTime);
	    System.out.println("Fender runtime: " + endTime + "ms");
	    statsBuilder.append("Fender runtime: " + endTime + "ms \r\n");
	    fenderTimes.add(endTime);
	    flushCount = extrVisitor.getFlushes().size();
        
	    if(output != null && output.isSuccessful()){
	        System.out.println("Program with all flushes verified");
	        statsBuilder.append("Program with all flushes verified \r\n");
	        if(checkTrace){
            	// delete the synthesized predicates file as there was no error state
	        	writtenSPF.delete();
	        }
	        
	        Set<Set<Flush>> triedSet = new HashSet<Set<Flush>>();
	        Set<Set<Flush>> solutionSet = new HashSet<Set<Flush>>();
	        Set<Set<Flush>> notVerifiedSet = new HashSet<Set<Flush>>();
	        Set<Flush> flushes = new HashSet<Flush>(extrVisitor.getFlushes());
	        Set<Set<Flush>> workQueue = new HashSet<Set<Flush>>();
	        for(Flush flush: flushes){
	        	Set<Flush> s = new HashSet<Flush>();
	        	s.add(flush);
	        	workQueue.add(s);
	        }
	        workLoop: while(!workQueue.isEmpty()){
	        	Set<Flush> solutionFlushes = null;
	        	for(Set<Flush> workQueueElement: workQueue){
	        		if(workQueueElement.size() == 1){
	        			// we first want to verify single removed flushes
	        			solutionFlushes = workQueueElement;
	        			break;
	        		}
	        		if(solutionFlushes == null){
	        			solutionFlushes = workQueueElement;
	        		}
	        		// solutionFlushes can't be null now, add a bigger set if there is one
	        		if(workQueueElement.size() > solutionFlushes.size()){
	        			solutionFlushes = workQueueElement;
	        		}
	        	}
	        	workQueue.remove(solutionFlushes);
	        	// go over if we already had this set
	        	if(triedSet.contains(solutionFlushes)){
	        		continue;
	        	}
	        	// check if we already have a verified solution that contains all the flushes that we want to verify now
	        	for(Set<Flush> solution: solutionSet){
	        		if(solution.containsAll(solutionFlushes)){
	        			continue workLoop;
	        		}
	        	}
	        	triedSet.add(solutionFlushes);
	        	// continue if the set contains flushes that didn't verify
	        	for(Set<Flush> notVerified: notVerifiedSet){
	        		if(solutionFlushes.containsAll(notVerified)){
	        			continue workLoop;
	        		}
	        	}
	        	
	        	String lines = "";
	    		for(Flush f: solutionFlushes){
	    			lines = lines + f.getLine() + "_";
	    		}
	    		if(solutionFlushes.size() > 0){
	    			lines = lines.substring(0, lines.length() - 1);
	    		}
	    		System.out.println("Starting with Run" + lines);
	        	
	        	// reset the visitors, build new flowgraph and boolean program
	        	flowGraphTime = System.currentTimeMillis();
	        	flowGraphAnalyzer = new FlowGraphAnalyzer(tree, extrVisitor.getPutPreds(), extrVisitor.getGetPreds(), extrVisitor.getCasPreds(), extrVisitor.getRgaPreds(), solutionFlushes, noStaticAnalysis);
	            flowGraphAnalyzer.analyzeGraphs();
	            endTime = System.currentTimeMillis() - flowGraphTime;
	            System.out.println("Flow graph time: " + endTime + "ms");
	            statsBuilder.append("Flow graph time: " + endTime + "ms \r\n");
	            flowGraphTimes.add(endTime);
	            blpBuilder = new StringBuilder();
	            blpTime = System.currentTimeMillis();
	    		blpVisitor.reset(blpBuilder, flowGraphAnalyzer.getGraphs(), solutionFlushes);
	    			        		
	        	// build the boolean program
	        	blpVisitor.visit(tree);
	        	endTime = System.currentTimeMillis() - blpTime;
	    		System.out.println("Blp time: " + endTime + "ms");
	    		statsBuilder.append("Blp time: " + endTime + "ms \r\n");
	    		blpTimes.add(endTime);
	        	writtenFile = null;
	        	// save the resulting program to a file
	            try{
	            	PrintWriter writer;
	        		writtenFile = inputFileName.substring(0, inputFileName.length() - 3) + "_remote_" + lines + "_.bl";
	        		writer = new PrintWriter(writtenFile, "UTF-8");
	                writer.append(blpBuilder);
	                writer.close();
	            } catch (IOException e) {
	               e.printStackTrace();
	            }
	            writtenF = new File(writtenFile.substring(2, writtenFile.length()));
	            if(checkTrace){
	            	 String writtenNewPredFile = null;
	                 try{
	                 	PrintWriter writer;
	             		writtenNewPredFile = inputFileName.substring(0, inputFileName.length() - 3) + "_remote_" + lines +"_synthesized.fpreds";
	             		writer = new PrintWriter(writtenNewPredFile, "UTF-8");
	                     writer.close();
	                 } catch (IOException e) {
	                    e.printStackTrace();
	                 }
	                
	                 writtenSPF = new File(writtenNewPredFile.substring(2, writtenNewPredFile.length()));
	            	
	            	 fenderArgs = new String[13];
	                 fenderArgs[0] = writtenF.getAbsolutePath();
	                 fenderArgs[1] = "sc";
	                 fenderArgs[2] = "-predicateFile";
	                 fenderArgs[3] = writtenPF.getAbsolutePath();
	                 fenderArgs[4] = "-synthesizePredicatesInto";
	                 fenderArgs[5] = writtenSPF.getAbsolutePath();
	                 fenderArgs[6] = "-predicateSynthesisMethod";
	                 if(checkMethod.equals(WP)){
	                	 fenderArgs[7] = WP;
	                 } else {
	                	 fenderArgs[7] = Interp;
	                 }
	                 fenderArgs[8] = "-errorPath";
	                 fenderArgs[9] = "succinct";
	                 fenderArgs[10] = "-keepTransitionSystem";
	                 fenderArgs[11] = "True";
	                 fenderArgs[12] = "-lessOutput";
	            } else {
	            	fenderArgs = new String[3];
		            fenderArgs[0] = writtenF.getAbsolutePath();
		            fenderArgs[1] = "sc";
		            fenderArgs[2] = "-lessOutput";
	            }
	            
	            // now call fender
	            fenderTime = System.currentTimeMillis();
	            output = callFender(fenderArgs);
	            endTime = (System.currentTimeMillis() - fenderTime);
	    	    System.out.println("Fender runtime: " + endTime + "ms");
	    	    statsBuilder.append("Fender runtime: " + endTime + "ms \r\n");
	    	    fenderTimes.add(endTime);
	    	    solutionTimes.put(solutionFlushes, endTime);
	            
	            triedSet.add(new HashSet<Flush>(solutionFlushes));
	            // remove the flush if it didn't verify
	            if(output == null || ! output.isSuccessful()){
	            	System.out.println("Run " + lines + " did not verify");
	            	statsBuilder.append("Run " + lines + " did not verify \r\n");
	            	if(checkTrace){
	    	    		if(output.getTraceOk()){
	    	    			System.out.println("The error trace seems to be ok.");
	    	    			statsBuilder.append("The error trace seems to be ok.");
	    	    		} else {
	    	    			System.out.println("The error trace seems to be spurious. Check the file with synthesized predicates andding try add them to your predicates.");
	    	    			statsBuilder.append("The error trace seems to be spurious. Check the file with synthesized predicates andding try add them to your predicates.");
	    	    		}
	    	    	}
	            	notVerifiedSet.add(solutionFlushes);
	            	if(solutionFlushes.size() == 1){
	            		flushes.removeAll(solutionFlushes);
	            	}
	            } else {
	            	flushRemoved = true;
	            	System.out.println("Run " + lines + " verified");
	            	statsBuilder.append("Run " + lines + " verified \r\n");
	            	if(checkTrace){
		            	// delete the synthesized predicates file as there was no error state
		            	writtenSPF.delete();
	            	}
	            	solutionSet.add(solutionFlushes);
	            	for(Flush f: flushes){
	            		Set<Flush> newSet = new HashSet<Flush>(solutionFlushes);
	            		// only add it if it didn't already contain the flush
	            		if(newSet.add(f)){
	            			workQueue.add(newSet);
	            		}
	            	}
	            	
	            }
	        }
	        
	        HashSet<Set<Flush>> solutionCopy = new HashSet<Set<Flush>>(solutionSet);
	        Iterator<Set<Flush>> it = solutionSet.iterator();
	        while(it.hasNext()){
	        	Set<Flush> flushSet = it.next();
	        	for(Set<Flush> s: solutionCopy){
	        		Set<Flush>copyFlushSet = new HashSet<Flush>(s);
	        		if(!flushSet.containsAll(copyFlushSet) && copyFlushSet.containsAll(flushSet)){
	        			it.remove();
	        			break;
	        		}
	        	}
	        }
	        if(solutionSet.isEmpty()){
	        	System.out.println("No flushes could be removed.");
	        	statsBuilder.append("No flushes could be removed.\r\n");
	        } else {
	        	Map<Set<Flush>, Long> newSol = new HashMap<Set<Flush>, Long>();
		        for(Set<Flush> solSet: solutionSet){
		        	newSol.put(solSet, solutionTimes.get(solSet));
		        	String lines = "";
		    		for(Flush f: solSet){
		    			lines = lines + f.getLine() + ", ";
		    		}
		    		if(solSet.size() > 0){
		    			lines = lines.substring(0, lines.length() - 2);
		    		}
		        	System.out.println("Solution: remove flushes at line " + lines + ".");
		        	statsBuilder.append("Solution: remove flushes at line " + lines + ". \r\n");
		        	saveSource(inputFileName, solSet);
		        }
		        solutionTimes = newSol;
	        }
	    } else{
	    	System.out.println("Input program with all flushes didn't verify!");
	    	statsBuilder.append("Input program with all flushes didn't verify! \r\n");
	    	if(checkTrace){
	    		if(output.getTraceOk()){
	    			System.out.println("The error trace seems to be ok.");
	    			statsBuilder.append("The error trace seems to be ok.");
	    		} else {
	    			System.out.println("The error trace seems to be spurious. Check the file with synthesized predicates and try adding them to your predicates.");
	    			statsBuilder.append("The error trace seems to be spurious. Check the file with synthesized predicates and try adding them to your predicates.");
	    		}
	    	}
	    }

	}

	
	/** parses the program contained in the file specified in programFile */
	public static ParseTree extractTree(String programFile){
		TLLexer lexer = null;
		try {
			lexer = new TLLexer(new ANTLRFileStream(programFile));
		} catch (IOException e) {
			System.out.println("Error with provided input file name");
			e.printStackTrace();
		}

        TLParser parser = new TLParser(new CommonTokenStream(lexer));

        parser.setBuildParseTree(true);
        return parser.parse();
        
	}
	
	/** parses the program contained in the file specified in programFile and removes the specified flushes */
	public static void saveSource(String programFile, Set<Flush> removeFlushes){
		Charset charset = Charset.forName("US-ASCII");
		int i = programFile.indexOf(".ir");
		if(i < 0){
			throw new Error("invalid program file");
		}
		String lines = "";
		for(Flush f: removeFlushes){
			lines = lines + f.getLine() + "_";
		}
		if(removeFlushes.size() > 0){
			lines = lines.substring(0, lines.length() - 1);
		}
		String newFile = programFile.substring(0, i) + "_removedFlushes_" + lines + programFile.substring(i, programFile.length());
		File newF=new File(newFile);
		if(newF.exists()){
			newF.delete();
			newF=new File(newFile);
		}
		// create a writer
		BufferedWriter writer = null;
		try {
			writer = Files.newBufferedWriter(newF.toPath(), charset);
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
		// now read the old file, remove the flushes and write to the new one
		try (BufferedReader reader = Files.newBufferedReader(FileSystems.getDefault().getPath(programFile), charset)) {
		    String line = null;
		    int lineCount = 1;
		    while ((line = reader.readLine()) != null) {
		        for(Flush flush: removeFlushes){
		        	// remove the flush if we have to by comment
		        	if(lineCount == flush.getLine()){
		        		line = line.substring(0, line.indexOf("flush(")) + "//" + line.substring(line.indexOf("flush("), line.length());
		        		break;
		        	}
		        }
		        // write the line to the output
		        writer.append(line, 0, line.length());
		        writer.append('\r');
		        writer.append('\n');
		    	lineCount++;
		    }
		    reader.close();
		    writer.flush();
		    writer.close();
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
        
	}
	
	
	/** reads the predicates contained in the predsFile line for line and returns them as a list of Strings */
	public static List<Set<Predicate>> extractPreds(String predsFile){
		Set<Predicate> preds = new HashSet<Predicate>();
		Set<Predicate> notPreds = new HashSet<Predicate>();
		List<String> predList = new ArrayList<String>();
		VarVisitor varVis = new VarVisitor();
		
		// first we extract the predicates from the file
		try (BufferedReader br = new BufferedReader(new FileReader(predsFile))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       predList.add(line);
		    }
		} catch (FileNotFoundException e) {
			System.out.println("Error with provided predicates file name");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// now we parse the predicate as an expression tree and save it in a predicate object
		int id = 0;
		for(String pred: predList){
			TLLexer lexer = new TLLexer(new ANTLRInputStream(pred));
			TLParser parser = new TLParser(new CommonTokenStream(lexer));
			parser.setBuildParseTree(true);
			ParseTree tree = parser.parse();
			varVis.visit(tree);
			Predicate predObj = new Predicate(pred, tree, varVis.getVars(), true, id);
			notPreds.add(predObj);
			predObj = new Predicate(pred, tree, varVis.getVars(), false, id);
			preds.add(predObj);
			varVis.reset();
			id++;
		}
		numberOfPreds = id;
		List<Set<Predicate>> result = new ArrayList<Set<Predicate>>();
		result.add(preds);
		result.add(notPreds);
		return result;
	}

	public static FenderOutput callFender(String[] args){
		boolean error = false;
		// Create a stream to hold the output
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    PrintStream ps = new PrintStream(baos);
	    PrintStream old = System.out;
	    // Tell Java to use your special stream
	    System.setOut(ps);
	    
	    // call fender
	    fsb.parser.Main.main(args);
        
	    System.out.flush();
	    // reset System.out to the old stream
	    System.setOut(old);
	    System.gc();
	    if(error){
	    	return null;
	    }
	    // parse the output
	    String output = baos.toString();
	    return new FenderOutput(output, checkMethod, checkTrace);
	}
	
	
	private static void help(Options options) {
	        HelpFormatter formater = new HelpFormatter();
	        formater.printHelp("Main", options);
	        System.exit(0);
	}
	
	private static void reportError(String error) {
        System.err.println(error);
        System.exit(0);
    }
	
	private static String  parseAndRunArgs(String[] args) {
			int unsatCubeSize = 3;
			int cubeSize = 3;
	        Options options = new Options();
	        options.addOption("h", "help", false, "Show help.");        
	        options.addOption("ir", "sourcefile", true, "The input program that will be verified.");
	        options.addOption("pr", "predicatefile", true, "The file containing the predicates that will be used for verification.");
	        options.addOption("l", "local", false, "If set, we are treating rdma statements as local statements.");
	        options.addOption("r", "remote", false, "If set, we are treating rdma statements as remote statements (Default).");
	        options.addOption("acc", "accumulate", false, "Accumulate all the sets of the same read variable to one set per process (results in less predicates but lower precision, only for remote case). Default: false");
	        options.addOption("ucs", "unsatcubesize", true, "The maximum cube size for the search of predicates that can't hold at the same time (lower size might result in lower precision)");
	        options.addOption("cs", "cubesize", true, "The maximum cube size for the cube search of Weakest Pre-Condition and Strongest Post-Conditions for the boolean program");
	        options.addOption("ct", "checktrace", false, "If set, the model checker checks, whether a found error state is spurious.");
	        options.addOption("WP", "WeakestPrecondition", false, "Weakest Precondition method to check the trace (default).");
	        options.addOption("Interp", "Interpolate", false, "Path interpolation method to check the trace.");
	        options.addOption("NoExtrap", "NoExtrapolation", false, "If set, extrapolation is used for the creation of the Rdma boolean program.");
	        options.addOption("NoFL", "NoFlushLoopSearch", false, "If set, the analyzer won't look for flushes in loops  to optimize the analysis for remote statements in loops.");
	        options.addOption("NoStAn", "NoStaticAnalysis", false, "If set, we won't do any static analysis do teremine what statements might be active at certain points in the boolean program.");

	        CommandLineParser parser = new DefaultParser();
	        try {
	        	
	            CommandLine cmd = parser.parse(options, args);

	            if(cmd.hasOption("h")) {
	                help(options);
	            }

	            if(!cmd.hasOption("ir")) {
	                reportError("No input program source file specified.");
	            }else if(!cmd.hasOption("pr")){
	            	reportError("No predicate file specified.");
	            }
	            sourceFile = cmd.getOptionValue("ir");
	            if(cmd.hasOption("l") && cmd.hasOption("r")){
	            	reportError("Either specify to run remote or local case.");
	            }
	            if(cmd.hasOption("ucs")){
	            	unsatCubeSize = Integer.parseInt(cmd.getOptionValue("ucs"));
	            }
	            if(cmd.hasOption("cs")){
	            	cubeSize = Integer.parseInt(cmd.getOptionValue("cs"));
	            	RmaAnalyzer.cubeSize = cubeSize;
	            }
	            
	            if(cmd.hasOption("ct")){
	            	checkTrace = true;
	            	if(cmd.hasOption("Interp")){
	            		checkMethod = Interp;
	            	} else {
	            		checkMethod = WP;
	            	}
	            }
	            
	            if(cmd.hasOption("NoExtrap")){
	            	noExtrapolation = true;
	            }
	            
	            if(cmd.hasOption("NoFL")){
	            	noFlushLookahead = true;
	            }
	            
	            if(cmd.hasOption("NoStAn")){
	            	noStaticAnalysis = true;
	            }
	            
	            if(cmd.hasOption("l")){
	            	runLocalCase(cmd.getOptionValue("ir"), cmd.getOptionValue("pr"), cubeSize, unsatCubeSize);
	            	return "local";
	            } else {
	            	boolean acc;
	            	if(cmd.hasOption("acc")){
	            		acc = true;
	            	} else {
	            		acc = false;
	            	}
	            	runRemoteCase(cmd.getOptionValue("ir"), cmd.getOptionValue("pr"), acc, cubeSize, unsatCubeSize);
	            	return "remote";
	            }
	            
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }
	        return null;
	}
}
