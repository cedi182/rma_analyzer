package main;

/** a class used to represent the output of fender */
public class FenderOutput {
	
	boolean successful;
	
	String memory;
	
	String states;
	
	String edges;
	
	String runTime;
	
	boolean traceOk = true;
	
	public FenderOutput(String fenderOutput, String checkMethod, boolean checkTrace){
		boolean finished = false;
		if(checkTrace && checkMethod.equals(RmaAnalyzer.WP)){
			traceOk = true;
		} else {
			traceOk = false;
		}
		for(String s: fenderOutput.split(System.getProperty("line.separator"))){
			if(! finished && s.startsWith("---FINISHED---")){
				finished = true;
			} else {
				if(finished){
					if(s.startsWith("States: ")){
						states = s.substring(s.indexOf(":") + 2, s.indexOf(","));
						edges = s.substring(s.indexOf(",") + 10, s.length());
					} else if(s.startsWith("Error states: ")){
						String numErrors = s.substring(s.indexOf(":") + 2);
						if(numErrors.equals("0")){
							successful = true;
						} else {
							successful = false;
						}
					} else if(s.startsWith("Running Time: ")){
						runTime = s.substring(s.indexOf(":") + 2);
					} else if(s.startsWith("Memory usage: ")){
						memory = s.substring(s.indexOf(":") + 2);
					}
				}
			}
			if(checkTrace && checkMethod.equals(RmaAnalyzer.WP) && s.startsWith("--- UNSAT Core ---")){
				traceOk = false;
				break;
			} else if(checkTrace && checkMethod.equals(RmaAnalyzer.Interp) && s.contains("the error trace translated to z3 is satisfable")){
				traceOk = true;
				break;
			}
		}
		if(finished == false){
			successful = false;
		}
	}
	
	public boolean getTraceOk(){
		return traceOk;
	}
	
	public boolean isSuccessful(){
		return successful;
	}
}
