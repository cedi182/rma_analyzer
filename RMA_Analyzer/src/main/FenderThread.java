package main;

/** a class used to run fender in a separate thread (not used anymore)*/
public class FenderThread implements Runnable{

	String[] args;
	
	long timeOut;
	
	public FenderThread(String[] args, long timeOut){
		this.args = args;
		this.timeOut = timeOut;
	}
	
	@Override
	public void run() {
		fsb.parser.Main.main(args);
		
	}
	
}
