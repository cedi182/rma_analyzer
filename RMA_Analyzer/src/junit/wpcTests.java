package junit;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import blp.BlpVisitor;
import blp.Predicate;
import blp.RdmaStatementExtractVisitor;
import main.RmaAnalyzer;


public class wpcTests {

	@Test
	public void wpcTest0(){
		//String predFileName = "./test/peterson/peterson.preds";
		//String inputFileName = "./test/peterson/peterson.ir";
		//test(predFileName, inputFileName);
	}
	
	@Test
	public void wpcTest1(){
		String predFileName = "./test/cas-equal.preds";
		String inputFileName = "./test/cas-equal.ir";
		test(predFileName, inputFileName);
	}
	
	@Test
	public void wpcTest2(){
		String predFileName = "./test/cas_server-equal.preds";
		String inputFileName = "./test/cas_server-equal.ir";
		test(predFileName, inputFileName);
	}
	
	
	@Test
	public void wpcTest3(){
		String predFileName = "./test/cas_server-unequal.preds";
		String inputFileName = "./test/cas_server-unequal.ir";
		test(predFileName, inputFileName);
	}
	
	@Test
	public void wpcTest4(){
		String predFileName = "./test/test1.preds";
		String inputFileName = "./test/test1.ir";
		test(predFileName, inputFileName);
	}
	
	
	
	public void test(String predFileName, String inputFileName){
		// instantiate the BlpVisitor and generate the boolean program
        StringBuilder blpBuilder = new StringBuilder();
        StringBuilder refBlpBuilder = new StringBuilder();
        List<Set<Predicate>> predicates = RmaAnalyzer.extractPreds(predFileName);
        ParseTree tree = RmaAnalyzer.extractTree(inputFileName);
        
        Predicate validPred = new Predicate("true", null, null, false, -1);
        Predicate unsatPred = new Predicate("false", null, null, false, -1);
        
        RdmaStatementExtractVisitor extrVisitor = new RdmaStatementExtractVisitor(predicates.get(0).size(), predicates.get(0), predicates.get(1), false);
        extrVisitor.visit(tree);
        BlpVisitor blpVisitor = new BlpVisitor(blpBuilder, predicates.get(0), predicates.get(1), extrVisitor.getPutPreds(), extrVisitor.getGetPreds(), extrVisitor.getCasPreds(), extrVisitor.getRgaPreds(), validPred, unsatPred);
        blpVisitor.visit(tree);
        ReferenceBlpVisitor refBlpVisitor = new ReferenceBlpVisitor(refBlpBuilder, predicates.get(0), predicates.get(1),extrVisitor.getPutPreds(), extrVisitor.getGetPreds(), extrVisitor.getCasPreds(), extrVisitor.getRgaPreds(), validPred, unsatPred);
        refBlpVisitor.visit(tree);
        
        List<Set<Set<Predicate>>> wpcTestList = blpVisitor.getWpcTestList();
        List<Set<Set<Predicate>>> refWpcTestList = refBlpVisitor.getWpcTestList();
        
        List<Set<Set<Predicate>>> spcTestList = blpVisitor.getSpcTestList();
        List<Set<Set<Predicate>>> refSpcTestList = refBlpVisitor.getSpcTestList();
        
        if(wpcTestList.size() != refWpcTestList.size() || spcTestList.size() != refSpcTestList.size()){
        	assertEquals(true,false);
        }
        
        // now we compare the lists of the solver and reference solver
        for(int i = 0; i < wpcTestList.size(); i++){
        	System.out.println("Wpc");
        	Set<Set<Predicate>> wpcTest = wpcTestList.get(i);
        	for(Set<Predicate> cube: wpcTest){
        		String cubeStr = "";
        		for(Predicate pred: cube){
        			if(pred.getMakeNot()){
        				cubeStr = cubeStr + "!" + pred.getPredString() + "   ";
        			} else {
        				cubeStr = cubeStr + pred.getPredString() + "   ";
        			}
        			
        		}
        		System.out.println(cubeStr);
        	}
        	Set<Set<Predicate>> refWpcTest = refWpcTestList.get(i);
        	System.out.println("refWpc");
        	for(Set<Predicate> cube: refWpcTest){
        		String cubeStr = "";
        		for(Predicate pred: cube){
        			if(pred.getMakeNot()){
        				cubeStr = cubeStr + "!" + pred.getPredString() + "   ";
        			} else {
        				cubeStr = cubeStr + pred.getPredString() + "   ";
        			}
        		}
        		System.out.println(cubeStr);
        	}
        	
        	//assertEquals(wpcTest, refWpcTest);
        }
        for(int i = 0; i < spcTestList.size(); i++){
        	Set<Set<Predicate>> spcTest = spcTestList.get(i);
        	Set<Set<Predicate>> refSpcTest = refSpcTestList.get(i);
        	assertEquals(spcTest, refSpcTest);
        }
	}
	
}
