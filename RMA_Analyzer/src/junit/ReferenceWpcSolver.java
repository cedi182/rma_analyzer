package junit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.tree.ParseTree;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import antlr.TLParser.AssignmentContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.LoadContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.StoreContext;
import antlr.TLParser.VarDeclContext;
import blp.Predicate;
import blp.TreeToZ3Visitor;

public class ReferenceWpcSolver {
Set<Predicate> predicates;
	
	Set<Predicate> notPredicates;
	
	TreeToZ3Visitor z3Visitor;
	
	Set<Predicate> validSet;
	
	Set<Predicate> unsatSet;
	
	static int maxCubeSize = 3;
	
	public ReferenceWpcSolver(Set<Predicate> preds, Set<Predicate> notPreds, Set<Predicate> validSet, Set<Predicate> unsatSet){
		predicates = preds;
		notPredicates = notPreds;
		z3Visitor = new TreeToZ3Visitor();
		this.validSet = validSet;
		this.unsatSet = unsatSet; 
	}
	
	Set<Set<Predicate>> computeWpc(ParseTree statement, Predicate predicate, boolean compNotWpc){		
		
		// set the re write rules
		if(statement instanceof AssignmentContext){
			z3Visitor.reWrite(statement.getChild(0).getText(), statement.getChild(2), false);
				
		} else if(statement instanceof StoreContext || statement instanceof LoadContext){
			z3Visitor.reWrite(statement.getChild(4).getText(), statement.getChild(6), true);
			
		} else if(statement instanceof VarDeclContext){
			// if we get here, we know that there is an assignment in the variable declaration
			z3Visitor.reWrite(statement.getChild(0).getText(), statement.getChild(2), true);
		} else if(statement instanceof PutContext){
			z3Visitor.reWrite(statement.getChild(7).getText(), statement.getChild(11), true);
		} else if(statement instanceof GetContext){
			z3Visitor.reWrite(statement.getChild(0).getText(), statement.getChild(9), true);
		}
			
		return getCubes(predicate.getTree(), compNotWpc);
	}
	
	Set<Set<Predicate>> computeWpc(String identifier, String replaceIdentifier, Predicate predicate, boolean compNotWpc){
		// set the rewrite rules
		z3Visitor.reWrite(identifier,  replaceIdentifier);
		return getCubes(predicate.getTree(), compNotWpc);
		
	}
	
	Set<Set<Predicate>> computeWpc(String identifier, ParseTree replaceExpression, Predicate predicate, boolean compNotWpc){
		// set the rewrite rules
		z3Visitor.reWrite(identifier, replaceExpression, false);
		return getCubes(predicate.getTree(), compNotWpc);
		
	}
	
	Set<Set<Predicate>> computeWpc(ParseTree predicate, boolean compNotWpc){
		return getCubes(predicate, compNotWpc);
	}
	
	
	Set<Set<Predicate>> computeWpc(ParseTree statement, Predicate predicate, boolean compNotWpc, ParseTree identifier){
		// first we want to compute the weakest precondition as a z3 formula
		if(statement instanceof AssignmentContext){
			z3Visitor.reWrite(identifier.getText(), statement.getChild(2), false);
		} else if(statement instanceof StoreContext){
			z3Visitor.reWrite(identifier.getText(), statement.getChild(6), true);
		} else if(statement instanceof LoadContext){
			z3Visitor.reWrite(statement.getChild(4).getText(), identifier, true);
			
		} else if(statement instanceof VarDeclContext){
			// if we get here, we know that there is an assignment in the variable declaration
			z3Visitor.reWrite(identifier.getText(), statement.getChild(2), true);
		}
			
		return getCubes(predicate.getTree(), compNotWpc);
	}
	
	
	Set<Set<Predicate>> getCubes(ParseTree predicate, boolean compNotWpc){
		Set<Set<Predicate>> wpcSet = new HashSet<Set<Predicate>>();
		Context context = z3Visitor.getContext();
		Solver solver = context.mkSolver();
		
		BoolExpr wpc = (BoolExpr) z3Visitor.visit(predicate);
		z3Visitor.turnOffReWrite();
		// negate the wpc and start searching for cubes
		BoolExpr notWpc = context.mkNot(wpc);
		
		// first we check if the wpc is valid or unsat -> don't do this if we are computing notWpc because it's obsolete if we computed wpc first
		solver.push();
		if(!compNotWpc){
			solver.add(wpc);
			if(solver.check() == Status.UNSATISFIABLE){
				// the weakest precondition is unsatisfiable, no need to look for implications
				wpcSet.add(unsatSet);
				return wpcSet;
			} else {
				solver.pop();
			}
			
		}
		
				
		//now check for validity
		if(compNotWpc){
			solver.add(wpc);
		} else {
			solver.add(notWpc);
		}
		
		// the weakest precondition is valid, no need to look for implications
		if(!compNotWpc && solver.check() == Status.UNSATISFIABLE){
			wpcSet.add(validSet);
			return wpcSet;
		}
				

		List<Predicate> predList = new ArrayList<Predicate>();
		predList.addAll(predicates);
		predList.addAll(notPredicates);
		
		Iterator<Predicate> it = predList.iterator();
		
		while(it.hasNext()){
			Predicate pred = it.next();
			if(pred.getMakeNot()){
				// we have a negative predicate
				solver.push();
				solver.add(context.mkNot((BoolExpr)z3Visitor.visit(pred.getTree())));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					wpcSet.add(predSet);
					it.remove();
				}
				solver.pop();
			} else {
				// we have a normal predicate
				solver.push();
				solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					wpcSet.add(predSet);
					it.remove();
				}
				solver.pop();
			}
		}
		
		for(int i = 2; i <= maxCubeSize; i++){
			wpcSet = solveWpcHigherOrder(i, wpcSet, predList, solver);
		}
		
		return wpcSet;
	}
	
	
	
	/*TODO: make this hole thing recursive instead of counter
	 * first search for the cubes of the given size
	 * add unsuccessful cubes to a list
	 * for the list with the unsuccessful cubes -> call function of next order to find all the cubes containing this cube
	 * -> only have to pop the last predicate
	 */
	
	public Set<Set<Predicate>> solveWpcHigherOrder(int cubeOrder, Set<Set<Predicate>> partSol, List<Predicate> preds, Solver solver){
		Context context = z3Visitor.getContext();
		int[] counters = new int[cubeOrder];
		while(counters[0] < preds.size()){
			boolean checkSat = true;
			//add the predicates to the set and check if a part of the formula is already present in partSol
			Set<Predicate> predSet = new HashSet<Predicate>();
			Set<Integer> idSet = new HashSet<Integer>();
			for(int i=0; i < cubeOrder; i++){
				// remove the cases where a predicate and its negation is in the cube
				if(idSet.contains(preds.get(counters[i]).getId())){
					checkSat = false;
					break;
				}
				predSet.add(preds.get(counters[i]));
				idSet.add(preds.get(counters[i]).getId());
			}
			if(predSet.size() < cubeOrder){
				// remove the cases where a predicate is contained 2 times in the cube
				checkSat = false;
			}
			for(Set<Predicate> partSolSet: partSol){
				// remove the case where the cube already contains a cube that implies the wpc
				if(predSet.containsAll(partSolSet)){
					checkSat = false;
					break;
				}
			}
			if(checkSat){
				// add the predicates to the solver
				solver.push();
				for(Predicate pred: predSet){
					if(pred.getMakeNot()){
						solver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
					} else {
						solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
					}
				}
				if(solver.check() == Status.UNSATISFIABLE){
					// predSet implies the weakest precondition -> add it to partSol
					partSol.add(predSet);
				}
				solver.pop();
			}
			
			
			//increment the counters
			counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
			if(counters[cubeOrder - 1] == preds.size()){
				int i = cubeOrder - 1;
				while(i >= 0){
					if(counters[i] == preds.size() && i != 0){
						counters[i] = 0;
						counters[i-1] = counters[i-1] + 1;
						i--;
					} else {
						break;
					}
				}
			}
			
		}
		return partSol;
	}
	/** computes a list of predicates that may change after assigning a new value to identifier */
	public Set<Predicate> computeAffectedPreds(String identifier, Set<Predicate> predicates){
		Set<String> identifiers = new HashSet<String>();
		identifiers.add(identifier);
		Set<Predicate> result = new HashSet<Predicate>();
		boolean changed = true;
		
		changeloop: while(changed){
			changed = false;
			for(Predicate pred: predicates){
				for(String var: identifiers){
					if(pred.getContainedVars().contains(var) && !result.contains(pred)){
						changed = true;
						identifiers.addAll(pred.getContainedVars());
						result.add(pred);
						continue changeloop;
					}
				}
			}
		}
		return result;
	}
}
