package junit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import antlr.TLBaseVisitor;
import antlr.TLLexer;
import antlr.TLParser;
import antlr.TLParser.AssertAlwaysContext;
import antlr.TLParser.AssertFinalContext;
import antlr.TLParser.AssignmentContext;
import antlr.TLParser.AssumptionContext;
import antlr.TLParser.CasContext;
import antlr.TLParser.FlushContext;
import antlr.TLParser.GetContext;
import antlr.TLParser.IfStatementContext;
import antlr.TLParser.LoadContext;
import antlr.TLParser.ProcessContext;
import antlr.TLParser.ProgramfileContext;
import antlr.TLParser.PutContext;
import antlr.TLParser.RgaContext;
import antlr.TLParser.StatementContext;
import antlr.TLParser.StoreContext;
import antlr.TLParser.VarDeclContext;
import antlr.TLParser.WhileStatementContext;
import blp.CasPredicate;
import blp.GetPredicate;
import blp.Predicate;
import blp.PutPredicate;
import blp.RgaPredicate;


/** A visitor that visits the parsed tree and generates the boolean program*/
public class ReferenceBlpVisitor extends TLBaseVisitor<Object>{
	
	StringBuilder result;
	Set<Predicate> predicates;
	Set<Predicate> notPredicates;
	ReferenceWpcSolver wpcSolver;
	ReferenceSpcSolver spcSolver;
	int label = 0;
	int curProc = 0;
	Predicate validPred;
	Predicate unsatPred;
	Set<Predicate> validSet;
	Set<Predicate> unsatSet;
	/** used for the junit test to compare the produced cubes by reference solver and solver */
	List<Set<Set<Predicate>>> wpcTestList;
	List<Set<Set<Predicate>>> spcTestList;
	Map<PutContext, PutPredicate> putPreds;
	Map<GetContext, GetPredicate> getPreds;
	Map<CasContext, CasPredicate> casPreds;
	Map<RgaContext, RgaPredicate> rgaPreds;
	
	public ReferenceBlpVisitor(StringBuilder result, Set<Predicate> predicates, Set<Predicate> notPredicates, Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds, Map<CasContext, CasPredicate> casPreds, Map<RgaContext, RgaPredicate>rgaPreds){
		// create a representation for valid and unsat conditions
		validPred = new Predicate("true", null, null, false, -1);
		unsatPred = new Predicate("false", null, null, false, -1);
		
		validSet = new HashSet<Predicate>();
		unsatSet = new HashSet<Predicate>();
		validSet.add(validPred);
		unsatSet.add(unsatPred);
		wpcTestList = new ArrayList<Set<Set<Predicate>>>();
		spcTestList = new ArrayList<Set<Set<Predicate>>>();
		this.result = result;
		this.predicates = predicates;
		this.notPredicates = notPredicates;
		this.wpcSolver = new ReferenceWpcSolver(predicates, notPredicates, validSet, unsatSet);
		this.spcSolver = new ReferenceSpcSolver(predicates, notPredicates, unsatSet, validSet);
		this.putPreds = putPreds;
		this.getPreds = getPreds;
		this.casPreds = casPreds;
		this.rgaPreds = rgaPreds;
		
		addAndInitVars();
		
	}

	/** constructor used for the junit tests */
	public ReferenceBlpVisitor(StringBuilder result, Set<Predicate> predicates, Set<Predicate> notPredicates, Map<PutContext, PutPredicate> putPreds, Map<GetContext, GetPredicate> getPreds, Map<CasContext, CasPredicate> casPreds, Map<RgaContext, RgaPredicate>rgaPreds, Predicate validPred, Predicate unsatPred){
		// create a representation for valid and unsat conditions
		this.validPred = validPred;
		this.unsatPred = unsatPred;
		validSet = new HashSet<Predicate>();
		unsatSet = new HashSet<Predicate>();
		validSet.add(validPred);
		unsatSet.add(unsatPred);
		wpcTestList = new ArrayList<Set<Set<Predicate>>>();
		spcTestList = new ArrayList<Set<Set<Predicate>>>();
		this.result = result;
		this.predicates = predicates;
		this.notPredicates = notPredicates;
		this.wpcSolver = new ReferenceWpcSolver(predicates, notPredicates, validSet, unsatSet);
		this.spcSolver = new ReferenceSpcSolver(predicates, notPredicates, unsatSet, validSet);
		this.putPreds = putPreds;
		this.getPreds = getPreds;
		this.casPreds = casPreds;
		this.rgaPreds = rgaPreds;
		
		addAndInitVars();
	}
	
	@Override
	public Object visitIfStatement(IfStatementContext ctx) {
		commitComment("Statement: " + ctx.getText());		
		// compute the spc for the if and else block
		Set<Set<Predicate>> spc = spcSolver.computeSpc(ctx.getChild(1), false);
		String spcString = formPredString(spc);
		Set<Set<Predicate>> elseSpc = spcSolver.computeSpc(ctx.getChild(1), true);
		String elseSpcString = formPredString(elseSpc);
		
		int elseLabel = getNewLabel();
		int afterLabel = getNewLabel();
		commitCodeLn("if (*) goto " + elseLabel + ";");
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		commitNop();
		
		// visit the then block
		visit(ctx.getChild(3));
		commitCodeLn("if(true) goto " + afterLabel + ";");
		
		
		
		commitLn(elseLabel + ": nop;");
		if(!elseSpcString.equals("")){
			commitCodeLn("assume(" + elseSpcString + ");");
		}
		commitNop();
		if(ctx.children.size() == 7){
			// there is a else block
			visit(ctx.getChild(5));
		}
		commitLn(afterLabel + ": nop;");
		
		spcTestList.add(spc);
		spcTestList.add(elseSpc);
		
		return null;
	}

	@Override
	public Object visitWhileStatement(WhileStatementContext ctx) {
		commitComment("Statement: " + ctx.getText());		
		// compute the spc for the if and else block
		Set<Set<Predicate>> spc = spcSolver.computeSpc(ctx.getChild(1), false);
		String spcString = formPredString(spc);
		Set<Set<Predicate>> elseSpc = spcSolver.computeSpc(ctx.getChild(1), true);
		String elseSpcString = formPredString(elseSpc);
		
		int startLabel = getNewLabel();
		int condCheckLabel = getNewLabel();
		commitCodeLn("if(true) goto " + condCheckLabel + ";");
		commitLn(startLabel + ": nop;");
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		
		// now we visit the while block
		visit(ctx.getChild(3));
		
		if(!spcString.equals("")){
			// TODO: is the spc enough here or do we need the loop condition as a predicate to be sound?
			// TODO: or can't we just go with non determinism because the condition is checked in the assume statement
			//commitLn(condCheckLabel + ": if (" + spcString + ") goto " + startLabel + ";");
			commitLn(condCheckLabel + ": if (*) goto " + startLabel + ";");
		} else {
			// we can't express the loop condition with a predicate -> use non determinism
			commitLn(condCheckLabel + ": if (*) goto " + startLabel + ";");
		}
		if(!elseSpcString.equals("")){
			commitCodeLn("assume(" + elseSpcString + ");");
		}
		
		spcTestList.add(spc);
		spcTestList.add(elseSpc);
		
		return null;
	}

	@Override
	public Object visitAssumption(AssumptionContext ctx) {
		commitComment("Statement: " + ctx.getText());
		// TODO: is spc the right thing here?
		Set<Set<Predicate>> spc = spcSolver.computeSpc(ctx.getChild(2), false);
		String spcString = formPredString(spc);
		if(!spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		
		spcTestList.add(spc);
		
		return null;
	}

	@Override
	public Object visitVarDecl(VarDeclContext ctx) {
		commitComment("Statement: " + ctx.getText());
		if(ctx.children.size() == 3){
			// the variable declaration involves an assignment -> code copied from assignment abstract transformer computation
			commitNop();
			commitCodeLn("begin_atomic;");
			Set<Predicate> preds = wpcSolver.computeAffectedPreds(ctx.getChild(0).getText(), predicates);
			List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
			List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
			List<Predicate> predList = new ArrayList<Predicate>();
			for(Predicate pred: preds){
				Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx, pred, false);
				Set<Set<Predicate>> notWpc;
				if(wpc.contains(validSet)){
					notWpc = new HashSet<Set<Predicate>>();
					notWpc.add(unsatSet);
				} else if(wpc.contains(unsatSet)){
					notWpc = new HashSet<Set<Predicate>>();
					notWpc.add(validSet);
				}else {
					notWpc = wpcSolver.computeWpc(ctx, pred, true);
				}
				wpcList.add(wpc);
				notWpcList.add(notWpc);
				predList.add(pred);
				
			}
			
			Set<Integer> loadSet = getContainedPredIds(wpcList);
			loadSet.addAll(getContainedPredIds(notWpcList));
			loadPredsToLocal(loadSet);
			
			for(int i = 0; i < wpcList.size(); i++){
				commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
			}
			
			
			resetLocals(loadSet);
			commitCodeLn("end_atomic;");
			
			for(Set<Set<Predicate>> wpc: wpcList){
				wpcTestList.add(wpc);
			}
			for(Set<Set<Predicate>> notWpc: notWpcList){
				wpcTestList.add(notWpc);
			}
		}
		return null;
	}

	@Override
	public Object visitAssertAlways(AssertAlwaysContext ctx) {
		commitComment("Statement: " + ctx.getText());
		Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx.getChild(3), false);
		String wpcString = formPredString(wpc);
		if(!wpcString.equals("")){
			if(wpcString.startsWith("!")){
				// remove the leading ! because this is placed for the weakest postcondition strings
				//TODO: have own function for assertion strings?
				wpcString = wpcString.substring(1, wpcString.length());
			}
			commitLn("assert always(" + wpcString + ");");
		} else {
			commitLn("assert always(false);");
		}
		
		spcTestList.add(wpc);
		
		return null;
	}

	@Override
	public Object visitAssertFinal(AssertFinalContext ctx) {
		commitComment("Statement: " + ctx.getText());
		Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx.getChild(3), false);
		String wpcString = formPredString(wpc);
		if(!wpcString.equals("")){
			if(wpcString.startsWith("!")){
				// remove the leading ! because this is placed for the weakest postcondition strings
				//TODO: have own function for assertion strings?
				wpcString = wpcString.substring(1, wpcString.length());
			}
			commitLn("assert final(" + wpcString + ");");
		} else {
			commitLn("assert final(false);");
		}
		
		spcTestList.add(wpc);
		
		return null;
	}

	@Override
	public Object visitLoad(LoadContext ctx) {
		commitComment("Statement: " + ctx.getText());
		commitCodeLn("begin_atomic;");
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(ctx.getChild(4).getText(), predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		for(Predicate pred: preds){
			Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx, pred, false);
			Set<Set<Predicate>> notWpc;
			if(wpc.contains(validSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(unsatSet);
			} else if(wpc.contains(unsatSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(validSet);
			}else {
				notWpc = wpcSolver.computeWpc(ctx, pred, true);
			}
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
			
		}
		
		
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		
		/*
		// now we go through the put and get predicates and check whether there are additional transformations
		for(PutPredicate putPred: putPreds.values()){
			if(ctx.getChild(4).getText().equals(putPred.getReadVar())){
				// we have to compute the transformer for the assignment to writeVar
				preds = wpcSolver.computeAffectedPreds(putPred.getWriteVar(), predicates);
				
				List<Set<Set<Predicate>>> putWpcList = new ArrayList<Set<Set<Predicate>>>();
				List<Set<Set<Predicate>>> putNotWpcList = new ArrayList<Set<Set<Predicate>>>();
				List<Predicate> putPredList = new ArrayList<Predicate>();
				for(Predicate pred: preds){
					Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx, pred, false, putPred.getWriteTree());
					Set<Set<Predicate>> notWpc;
					if(wpc.contains(validSet)){
						notWpc = new HashSet<Set<Predicate>>();
						notWpc.add(unsatSet);
					} else if(wpc.contains(unsatSet)){
						notWpc = new HashSet<Set<Predicate>>();
						notWpc.add(validSet);
					}else {
						notWpc = wpcSolver.computeWpc(ctx, pred, true);
					}
					putWpcList.add(wpc);
					putNotWpcList.add(notWpc);
					putPredList.add(pred);
					
				}
				
				
				// now we commit the additional transformers
				int afterLabel = getNewLabel();
				commitCodeLn("if (!B" + putPred.getId() + ") goto " + afterLabel + ";");

				for(int i = 0; i < putWpcList.size(); i++){
					commitPutGetChoose(putWpcList.get(i), putNotWpcList.get(i), putPredList.get(i));
				}
				
				commitLn(afterLabel + ": nop;");
			}
		}
		for(GetPredicate getPred: getPreds.values()){
			if(ctx.getChild(4).getText().equals(getPred.getReadVar())){
				// we have to compute the transformer for the assignment to writeVar
				preds = wpcSolver.computeAffectedPreds(getPred.getWriteVar(), predicates);
				
				List<Set<Set<Predicate>>> getWpcList = new ArrayList<Set<Set<Predicate>>>();
				List<Set<Set<Predicate>>> getNotWpcList = new ArrayList<Set<Set<Predicate>>>();
				List<Predicate> getPredList = new ArrayList<Predicate>();
				for(Predicate pred: preds){
					Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx, pred, false, getPred.getWriteTree());
					Set<Set<Predicate>> notWpc;
					if(wpc.contains(validSet)){
						notWpc = new HashSet<Set<Predicate>>();
						notWpc.add(unsatSet);
					} else if(wpc.contains(unsatSet)){
						notWpc = new HashSet<Set<Predicate>>();
						notWpc.add(validSet);
					}else {
						notWpc = wpcSolver.computeWpc(ctx, pred, true);
					}
					getWpcList.add(wpc);
					getNotWpcList.add(notWpc);
					getPredList.add(pred);
					
				}
				
				
				// now we commit the additional transformers
				int afterLabel = getNewLabel();
				commitCodeLn("if (!B" + getPred.getId() + ") goto " + afterLabel + ";");

				for(int i = 0; i < getWpcList.size(); i++){
					commitPutGetChoose(getWpcList.get(i), getNotWpcList.get(i), getPredList.get(i));
				}
				
				commitLn(afterLabel + ": nop;");

			}
		}*/
		
		resetLocals(loadSet);
		commitCodeLn("end_atomic;");
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
		
		return null;
	}

	@Override
	public Object visitStore(StoreContext ctx) {
		commitComment("Statement: " + ctx.getText());
		commitCodeLn("begin_atomic;");
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(ctx.getChild(4).getText(), predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		for(Predicate pred: preds){
			Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx, pred, false);
			Set<Set<Predicate>> notWpc;
			if(wpc.contains(validSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(unsatSet);
			} else if(wpc.contains(unsatSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(validSet);
			}else {
				notWpc = wpcSolver.computeWpc(ctx, pred, true);
			}
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
			
		}
		
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		
		/*
		//TODO: put this in own function
		// now we go through the put and get predicates and check whether there are additional transformations
		for(PutPredicate putPred: putPreds.values()){
			if(ctx.getChild(4).getText().equals(putPred.getReadVar())){
				// we have to compute the transformer for the assignment to writeVar
				preds = wpcSolver.computeAffectedPreds(putPred.getWriteVar(), predicates);
				
				List<Set<Set<Predicate>>> putWpcList = new ArrayList<Set<Set<Predicate>>>();
				List<Set<Set<Predicate>>> putNotWpcList = new ArrayList<Set<Set<Predicate>>>();
				List<Predicate> putPredList = new ArrayList<Predicate>();
				for(Predicate pred: preds){
					Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx, pred, false, putPred.getWriteTree());
					Set<Set<Predicate>> notWpc;
					if(wpc.contains(validSet)){
						notWpc = new HashSet<Set<Predicate>>();
						notWpc.add(unsatSet);
					} else if(wpc.contains(unsatSet)){
						notWpc = new HashSet<Set<Predicate>>();
						notWpc.add(validSet);
					}else {
						notWpc = wpcSolver.computeWpc(ctx, pred, true);
					}
					putWpcList.add(wpc);
					putNotWpcList.add(notWpc);
					putPredList.add(pred);
					
				}
				
				
				// now we commit the additional transformers
				int afterLabel = getNewLabel();
				commitCodeLn("if (!B" + putPred.getId() + ") goto " + afterLabel + ";");

				for(int i = 0; i < putWpcList.size(); i++){
					commitPutGetChoose(putWpcList.get(i), putNotWpcList.get(i), putPredList.get(i));
				}
				
				commitLn(afterLabel + ": nop;");
			}
		}
		for(GetPredicate getPred: getPreds.values()){
			if(ctx.getChild(4).getText().equals(getPred.getReadVar())){
				// we have to compute the transformer for the assignment to writeVar
				preds = wpcSolver.computeAffectedPreds(getPred.getWriteVar(), predicates);
				
				List<Set<Set<Predicate>>> getWpcList = new ArrayList<Set<Set<Predicate>>>();
				List<Set<Set<Predicate>>> getNotWpcList = new ArrayList<Set<Set<Predicate>>>();
				List<Predicate> getPredList = new ArrayList<Predicate>();
				for(Predicate pred: preds){
					Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx, pred, false, getPred.getWriteTree());
					Set<Set<Predicate>> notWpc;
					if(wpc.contains(validSet)){
						notWpc = new HashSet<Set<Predicate>>();
						notWpc.add(unsatSet);
					} else if(wpc.contains(unsatSet)){
						notWpc = new HashSet<Set<Predicate>>();
						notWpc.add(validSet);
					}else {
						notWpc = wpcSolver.computeWpc(ctx, pred, true);
					}
					getWpcList.add(wpc);
					getNotWpcList.add(notWpc);
					getPredList.add(pred);
					
				}
				
				
				// now we commit the additional transformers
				int afterLabel = getNewLabel();
				commitCodeLn("if (!B" + getPred.getId() + ") goto " + afterLabel + ";");

				for(int i = 0; i < getWpcList.size(); i++){
					commitPutGetChoose(getWpcList.get(i), getNotWpcList.get(i), getPredList.get(i));
				}
				
				commitLn(afterLabel + ": nop;");

			}
		}*/
		
		
		
		resetLocals(loadSet);
		commitCodeLn("end_atomic;");
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
		
		return null;
	}

	@Override
	public Object visitAssignment(AssignmentContext ctx) {
		commitComment("Statement: " + ctx.getText());
		computeAssignmentTransformer(ctx, ctx.getChild(0).getText());
		
		
		return null;
	}
	
	@Override
	public Object visitProcess(ProcessContext ctx) {
		int procNumber = Integer.parseInt(ctx.Number().getText());
		curProc = procNumber;
		// commit the process label to the result
		commitLn("process " + procNumber);
		for(StatementContext statement: ctx.statement()){
			visit(statement);
		}
		return null;
	}
	
	@Override
	public Object visitProgramfile(ProgramfileContext ctx) {
		for(ProcessContext proc: ctx.process()){
			visit(proc.decl());
		}
		return super.visitProgramfile(ctx);
	}

	public Set<Integer> getContainedPredIds(List<Set<Set<Predicate>>> setList){
		Set<Integer> preds = new HashSet<Integer>();
		for(Set<Set<Predicate>> predSets: setList){
			if(predSets != null){
				for(Set<Predicate> predSet: predSets){
					for(Predicate pred: predSet){
						preds.add(new Integer(pred.getId()));
					}
				}
			}
		}
		
		return preds;
	}
	
	public void loadPredsToLocal(Set<Integer> preds){
		for(Integer id: preds){
			if(id != -1){
				// id == -1 represents true or false
				commitCodeLn("load t" + id.toString() + " = B" + id.toString() + ";");
			}
		}
	}
	
	public void resetLocals(Set<Integer> preds){
		for(Integer id: preds){
			if(id != -1){
				commitCodeLn("t" + id.toString() + " = 0;");
			}
		}
	}
	/** used for the conditions of if, assert and while statements */
	public String formPredString(Set<Set<Predicate>> predSets){
		String result;
		if(predSets.size()==1 && predSets.contains(unsatSet)){
			// the condition is not satisfiable -> return false to mark unreachable code
			result = "false";
		} else if(predSets.size()==1 && predSets.contains(validSet)){
			// the condition is valid
			result = "true";
		}
		else if(predSets.size() > 0){
			// we have to put a ! in front because we calculated the cubes for the inverted case
			result = "!(";
			for(Set<Predicate> set: predSets){
				result = result + "(";
				for(Predicate pred: set){
					if(pred.getMakeNot()){
						result = result + "(B" + pred.getId() + " == 0)&&";
					} else {
						result = result + "(B" + pred.getId() + " != 0)&&";
					}
				}
				result = result.substring(0, result.length() - 2);
				result = result + ")||";
			}
			// remove the last "||"
			result = result.substring(0, result.length() - 2);
			result = result + ")";
		} else {
			// the condition implies no predicate -> no assume statement will be needed
			result = "";
		}
		return result;
	}
	
	public List<String> getCommitString(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred){
		commitComment("Transform B" + storePred.getId() + " that corresponds to predicate: " + storePred.getPredString());
		String trueString = "";
		String falseString = "";
		if(trueSet.size() == 1 && trueSet.contains(validSet)){
			trueString = "true";
			falseString = "false";
		} else if(trueSet.size() == 1 && trueSet.contains(unsatSet)) {
			trueString = "false";
			falseString = "true";
		} else {
			if(trueSet.size() > 0){
				for(Set<Predicate> set: trueSet){
					trueString = trueString + "(";
					for(Predicate pred: set){
						if(pred.getMakeNot()){
							trueString = trueString + "(t" + pred.getId() + " == 0)&&";
						} else {
							trueString = trueString + "(t" + pred.getId() + " != 0)&&";
						}
					}
					trueString = trueString.substring(0, trueString.length() - 2);
					trueString = trueString + ")||";
				}
				// remove the last "||"
				trueString = trueString.substring(0, trueString.length() - 2);
			} else {
				trueString = "false";
			}
		}
		
		if(falseString.equals("")){
			if(falseSet.size() > 0){
				for(Set<Predicate> set: falseSet){
					falseString = falseString + "(";
					for(Predicate pred: set){
						if(pred.getMakeNot()){
							falseString = falseString + "(t" + pred.getId() + " == 0)&&";
						} else {
							falseString = falseString + "(t" + pred.getId() + " != 0)&&";
						}
					}
					falseString = falseString.substring(0, falseString.length() - 2);
					falseString = falseString + ")||";
				}
				// remove the last "||"
				falseString = falseString.substring(0, falseString.length() - 2);
			} else {
				falseString = "false";
			}
		}
		List<String> result = new ArrayList<String>();
		result.add(trueString);
		result.add(falseString);
		return result;
		}
	
	public void commitChoose(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred){
		List<String> strings = getCommitString(trueSet, falseSet, storePred);
		String trueString = strings.get(0);
		String falseString = strings.get(1);
		
		commitCodeLn("store B" + storePred.getId() + "= choose(" + trueString + ", " + falseString + ");");
	}
	
	public void commitPutGetChoose(Set<Set<Predicate>> trueSet, Set<Set<Predicate>> falseSet, Predicate storePred){
		List<String> strings = getCommitString(trueSet, falseSet, storePred);
		String trueString = strings.get(0);
		String falseString = strings.get(1);
		
		commitCodeLn("store B" + storePred.getId() + "= choose(B" + storePred.getId() + "&&"  + trueString + ", !B"+ storePred.getId() + "&&" + falseString + ");");
	}
	
	


	/** returns a unique number at each call used for the program labels */
	int getNewLabel(){
		return label++;
	}
	
	void commitCodeLn(String text){
		commitLn(getNewLabel() + ": " + text);
	}
	
	/** adds text to the string builder and adds a new line character at the end */
	void commitLn(String text){
		result.append(text + "\r\n");
	}
	
	void commitNop(){
		commitLn(getNewLabel() + ": nop;");
	}
	
	void commitComment(String text){
		commitLn("/* " + text + " */");
	}
	
	@Override
	public Object visitFlush(FlushContext ctx) {
		commitComment("Statement: " + ctx.getText());
		// TODO this is useless in the local case
		return super.visitFlush(ctx);
	}

	@Override
	public Object visitRga(RgaContext ctx) {
		commitComment("Statement: " + ctx.getText());
		RgaPredicate  rgaPred = rgaPreds.get(ctx);
		computeRdmaAssignmentTransformer("rga" + rgaPred.getId(), rgaPred.getAccumulateVar());
		commitCodeLn("begin_atomic;");
		computeRdmaAssignmentTransformer("rga" + (rgaPred.getId() + 1), rgaPred.getWriteVar());
		computeRdmaAssignmentTransformerExpr("rga" + (rgaPred.getId() + 2), "rga" + rgaPred.getId() + " + " + "rga" + (rgaPred.getId() + 1));
		computeRdmaAssignmentTransformer(rgaPred.getWriteVar(), "rga" + (rgaPred.getId() + 2));
		commitCodeLn("end_atomic;");
		computeRdmaAssignmentTransformer(rgaPred.getFinalWriteVar(), "rga" + (rgaPred.getId() + 1));
		return null;
	}

	@Override
	public Object visitCas(CasContext ctx) {/*
		commitComment("Statement: " + ctx.getText());
		CasPredicate casPred = casPreds.get(ctx);
		int elseLabel = getNewLabel();
		int endLabel = getNewLabel();
		commitCodeLn("if (*) goto " + elseLabel + ";");
		computeRdmaAssignmentTransformer("cas" + casPred.getId(), casPred.getExchangeVar().getText());
		computeRdmaAssignmentTransformer("cas" + (casPred.getId() + 1), casPred.getEqualityVar().getText());
		commitCodeLn("if (true) goto " + endLabel + ";");
		commitLn(elseLabel + ": nop;");
		computeRdmaAssignmentTransformer("cas" + (casPred.getId() + 1), casPred.getEqualityVar().getText());
		computeRdmaAssignmentTransformer("cas" + casPred.getId(), casPred.getExchangeVar().getText());
		commitLn(endLabel + ": nop;");
		commitCodeLn("begin_atomic;");
		computeRdmaAssignmentTransformer("cas" + (casPred.getId() + 2), casPred.getWriteVar());
		elseLabel = getNewLabel();
		endLabel = getNewLabel();
		commitCodeLn("if (*) goto " + elseLabel + ";");
		Set<Set<Predicate>> spc = spcSolver.computeSpc("(cas" + (casPred.getId() + 1) + " == cas" + (casPred.getId() + 2) + ")", false);
		String spcString = formPredString(spc);
		if(! spcString.equals("")){
			commitCodeLn("assume(" + spcString + ");");
		}
		computeRdmaAssignmentTransformer(casPred.getWriteVar(), "cas" + casPred.getId());
		commitCodeLn("if (true) goto " + endLabel + ";");
		commitLn(elseLabel + ": nop;");
		Set<Set<Predicate>> notSpc = spcSolver.computeSpc("(cas" + (casPred.getId() + 1) + " == cas" + (casPred.getId() + 2) + ")", true);
		String notSpcString = formPredString(notSpc);
		if(! notSpcString.equals("")){
			commitCodeLn("assume(" + notSpcString + ");");
		}
		commitLn(endLabel + ": nop;");
		commitCodeLn("end_atomic");
		computeRdmaAssignmentTransformer(casPred.getFinalWriteVar().getText(), "cas" + casPred.getId() + 2);
		*/
		return null;
	}
	
	public void computeRdmaAssignmentTransformer(String identifier, String identifier2){
		commitCodeLn("begin_atomic;");
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		for(Predicate pred: preds){
			Set<Set<Predicate>> wpc = wpcSolver.computeWpc(identifier, identifier2, pred, false);
			Set<Set<Predicate>> notWpc;
			if(wpc.contains(validSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(unsatSet);
			} else if(wpc.contains(unsatSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(validSet);
			}else {
				notWpc = wpcSolver.computeWpc(identifier, identifier2, pred, true);
			}
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
			
		}
		
		
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		
		resetLocals(loadSet);
		commitCodeLn("end_atomic;");
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
	}
	
	public void computeRdmaAssignmentTransformerExpr(String identifier, String rhsExpr){
		commitCodeLn("begin_atomic;");
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		
		TLLexer lexer = new TLLexer(new ANTLRInputStream(rhsExpr));
		TLParser parser = new TLParser(new CommonTokenStream(lexer));
		parser.setBuildParseTree(true);
		ParseTree tree = parser.parse();
		
		for(Predicate pred: preds){
			Set<Set<Predicate>> wpc = wpcSolver.computeWpc(identifier, tree, pred, false);
			Set<Set<Predicate>> notWpc;
			if(wpc.contains(validSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(unsatSet);
			} else if(wpc.contains(unsatSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(validSet);
			}else {
				notWpc = wpcSolver.computeWpc(identifier, tree, pred, true);
			}
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
			
		}
		
		
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		
		resetLocals(loadSet);
		commitCodeLn("end_atomic;");
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
	}
	
	public void computeAssignmentTransformer(ParseTree ctx, String identifier){
		commitCodeLn("begin_atomic;");
		Set<Predicate> preds = wpcSolver.computeAffectedPreds(identifier, predicates);
		List<Set<Set<Predicate>>> wpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Set<Set<Predicate>>> notWpcList = new ArrayList<Set<Set<Predicate>>>();
		List<Predicate> predList = new ArrayList<Predicate>();
		for(Predicate pred: preds){
			Set<Set<Predicate>> wpc = wpcSolver.computeWpc(ctx, pred, false);
			Set<Set<Predicate>> notWpc;
			if(wpc.contains(validSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(unsatSet);
			} else if(wpc.contains(unsatSet)){
				notWpc = new HashSet<Set<Predicate>>();
				notWpc.add(validSet);
			}else {
				notWpc = wpcSolver.computeWpc(ctx, pred, true);
			}
			wpcList.add(wpc);
			notWpcList.add(notWpc);
			predList.add(pred);
			
		}
		
		
		Set<Integer> loadSet = getContainedPredIds(wpcList);
		loadSet.addAll(getContainedPredIds(notWpcList));
		loadPredsToLocal(loadSet);
		
		for(int i = 0; i < wpcList.size(); i++){
			commitChoose(wpcList.get(i), notWpcList.get(i), predList.get(i));
		}
		
		resetLocals(loadSet);
		commitCodeLn("end_atomic;");
		
		for(Set<Set<Predicate>> wpc: wpcList){
			wpcTestList.add(wpc);
		}
		for(Set<Set<Predicate>> notWpc: notWpcList){
			wpcTestList.add(notWpc);
		}
	}

	public List<Set<Set<Predicate>>> getWpcTestList(){
		return wpcTestList;
	}
	
	public List<Set<Set<Predicate>>> getSpcTestList(){
		return spcTestList;
	}
	
	public void addAndInitVars(){
		// first we add a global variable for each predicate and the Put & Get Predicates*/
		String vars = "shared ";
		
		for(Predicate pred: predicates){
			//commitComment("Predicate: " + pred.getPredString() + " corresponds to B" + pred.getId());
			vars = vars + "B" + pred.getId() + ", ";
		}
		/*
		for(PutPredicate pred: putPreds.values()){
			vars = vars + "B" + pred.getId() + ", ";
		}
		for(GetPredicate pred: getPreds.values()){
			vars = vars + "B" + pred.getId() + ", ";
		}
		
		for(CasPredicate pred: casPreds.values()){
			for(int i = 0; i < 6; i++){
				vars = vars + "B" + (pred.getId() + i) + ", ";
			}
		}*/
		
		vars = vars.substring(0, vars.length() - 2) + ";";
		commitLn(vars);
		// now we add a local variable for each normal predicate*/
		vars = "local ";
		for(Predicate pred: predicates){
			vars = vars + "t" + pred.getId() + ", ";
		}
		vars = vars.substring(0, vars.length() - 2) + ";";
		
		commitLn(vars);
		
		commitLn("init");
		for(Predicate pred: predicates){
			commitComment("Predicate: " + pred.getPredString() + " corresponds to B" + pred.getId());
		}
		//initialize all Predicates to *
		for(Predicate pred: predicates){
			commitCodeLn("store B" + pred.getId() + " = *;");
		}
		
		//initialize all Put & Get Predicates to false
		/*for(PutPredicate pred: putPreds.values()){
			commitCodeLn("store B" + pred.getId() + " = 0;");
		}
		for(GetPredicate pred: getPreds.values()){
			commitCodeLn("store B" + pred.getId() + " = 0;");
		}*/
		
		
		
	}

	@Override
	public Object visitPut(PutContext ctx) {
		commitComment("Statement: " + ctx.getText());
		
		//TODO: already compute join here
		// set the according put predicate to true
		/**PutPredicate pred = putPreds.get(ctx);
		commitCodeLn("store B" + pred.getId() + " = 1;");
		return super.visitPut(ctx);*/
		computeAssignmentTransformer(ctx, ctx.getChild(7).getText());
		
		return null;
	}

	@Override
	public Object visitGet(GetContext ctx) {
		commitComment("Statement: " + ctx.getText());
		// TODO: already compute join here
		// set the according get predicate to true
		/**GetPredicate pred = getPreds.get(ctx);
		commitCodeLn("store B" + pred.getId() + " = 1;");
		return super.visitGet(ctx);*/
		computeAssignmentTransformer(ctx, ctx.getChild(0).getText());
		
		return null;
	}


}