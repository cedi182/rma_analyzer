package junit;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import antlr.TLLexer;
import antlr.TLParser;
import blp.Predicate;
import blp.TreeToZ3Visitor;

public class ReferenceSpcSolver {
Set<Predicate> predicates;
	
	Set<Predicate> notPredicates;
	
	TreeToZ3Visitor z3Visitor;
	
	Set<Predicate> unsatSet;
	
	Set<Predicate> validSet;
	
	
	// TODO: parameterize this
	int maxCubeSize = 3;
	
	public ReferenceSpcSolver(Set<Predicate> preds, Set<Predicate> notPreds, Set<Predicate> unsatSet, Set<Predicate> validSet){
		predicates = preds;
		notPredicates = notPreds;
		z3Visitor = new TreeToZ3Visitor();
		this.unsatSet = unsatSet;
		this.validSet = validSet;
	}
	
	
	Set<Set<Predicate>> computeSpc(String condition, boolean compNotSpc){
		TLLexer lexer = new TLLexer(new ANTLRInputStream(condition));
		TLParser parser = new TLParser(new CommonTokenStream(lexer));
		parser.setBuildParseTree(true);
		ParseTree tree = parser.parse();
		return computeSpc(tree, compNotSpc);
	}
	
	
	Set<Set<Predicate>> computeSpc(ParseTree condition, boolean compNotSpc){
		Set<Set<Predicate>> spcSet = new HashSet<Set<Predicate>>();
		Context context = z3Visitor.getContext();
		Solver solver = context.mkSolver();
		
		
		// first we want to compute the condition as a z3 formula
		BoolExpr cond = (BoolExpr) z3Visitor.visit(condition);
		BoolExpr notCond = context.mkNot(cond);
		
		//first we want to check whether the condition is even satisfiable
		solver.push();
		if(compNotSpc){
			solver.add(cond);
		} else {
			solver.add(notCond);	
		}
		
		
		
		if(solver.check() == Status.UNSATISFIABLE){
			// the condition is always false -> not reachable
			spcSet.add(validSet);
			return spcSet;
		}
		solver.pop();
		if(compNotSpc){
			solver.add(notCond);
		} else {
			solver.add(cond);	
		}

		if(solver.check() == Status.UNSATISFIABLE){
			// the condition is always false -> not reachable
			spcSet.add(unsatSet);
			return spcSet;
		}
				
		List<Predicate> predList = new ArrayList<Predicate>();
		predList.addAll(predicates);
		predList.addAll(notPredicates);
		
		Iterator<Predicate> it = predList.iterator();
		
		while(it.hasNext()){
			Predicate pred = it.next();
			if(pred.getMakeNot()){
				// we have a negative predicate
				solver.push();
				solver.add(context.mkNot((BoolExpr)z3Visitor.visit(pred.getTree())));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					spcSet.add(predSet);
					it.remove();
				}
				solver.pop();
			} else {
				// we have a normal predicate
				solver.push();
				solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
				if(solver.check() == Status.UNSATISFIABLE){
					Set<Predicate> predSet = new HashSet<Predicate>();
					predSet.add(pred);
					spcSet.add(predSet);
					it.remove();
				}
				solver.pop();
			}
		}
		
		for(int i = 2; i <= maxCubeSize; i++){
			spcSet = solveWpcHigherOrder(i, spcSet, predList, solver);
		}
		
		
		return spcSet;
	}
	
	
	public Set<Set<Predicate>> solveWpcHigherOrder(int cubeOrder, Set<Set<Predicate>> partSol, List<Predicate> preds, Solver solver){
		Context context = z3Visitor.getContext();
		int[] counters = new int[cubeOrder];
		while(counters[0] < preds.size()){
			boolean checkSat = true;
			//add the predicates to the set and check if a part of the formula is already present in partSol
			Set<Predicate> predSet = new HashSet<Predicate>();
			Set<Integer> idSet = new HashSet<Integer>();
			for(int i=0; i < cubeOrder; i++){
				// remove the cases where a predicate and its negation is in the cube
				if(idSet.contains(preds.get(counters[i]).getId())){
					checkSat = false;
					break;
				}
				predSet.add(preds.get(counters[i]));
				idSet.add(preds.get(counters[i]).getId());
			}
			if(predSet.size() < cubeOrder){
				// remove the cases where a predicate is contained 2 times in the cube
				checkSat = false;
			}
			for(Set<Predicate> partSolSet: partSol){
				// remove the case where the cube already contains a cube that implies the wpc
				if(predSet.containsAll(partSolSet)){
					checkSat = false;
					break;
				}
			}
			if(checkSat){
				// add the predicates to the solver
				solver.push();
				for(Predicate pred: predSet){
					if(pred.getMakeNot()){
						solver.add(context.mkNot((BoolExpr) z3Visitor.visit(pred.getTree())));
					} else {
						solver.add((BoolExpr)z3Visitor.visit(pred.getTree()));
					}
				}
				if(solver.check() == Status.UNSATISFIABLE){
					// predSet implies the weakest precondition -> add it to partSol
					partSol.add(predSet);
				}
				solver.pop();
			}
			
			
			//increment the counters
			counters[cubeOrder -1] = counters[cubeOrder - 1] + 1;
			if(counters[cubeOrder - 1] == preds.size()){
				int i = cubeOrder - 1;
				while(i >= 0){
					if(counters[i] == preds.size() && i != 0){
						counters[i] = 0;
						counters[i-1] = counters[i-1] + 1;
						i--;
					} else {
						break;
					}
				}
			}
			
		}
		return partSol;
	}
}
