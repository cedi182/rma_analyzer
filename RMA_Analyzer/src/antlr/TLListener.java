package antlr;

// Generated from TL.g4 by ANTLR 4.5.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TLParser}.
 */
public interface TLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TLParser#parse}.
	 * @param ctx the parse tree
	 */
	void enterParse(TLParser.ParseContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#parse}.
	 * @param ctx the parse tree
	 */
	void exitParse(TLParser.ParseContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(TLParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(TLParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code programfile}
	 * labeled alternative in {@link TLParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgramfile(TLParser.ProgramfileContext ctx);
	/**
	 * Exit a parse tree produced by the {@code programfile}
	 * labeled alternative in {@link TLParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgramfile(TLParser.ProgramfileContext ctx);
	/**
	 * Enter a parse tree produced by the {@code predicates}
	 * labeled alternative in {@link TLParser#program}.
	 * @param ctx the parse tree
	 */
	void enterPredicates(TLParser.PredicatesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code predicates}
	 * labeled alternative in {@link TLParser#program}.
	 * @param ctx the parse tree
	 */
	void exitPredicates(TLParser.PredicatesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#process}.
	 * @param ctx the parse tree
	 */
	void enterProcess(TLParser.ProcessContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#process}.
	 * @param ctx the parse tree
	 */
	void exitProcess(TLParser.ProcessContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(TLParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(TLParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#atomicSection}.
	 * @param ctx the parse tree
	 */
	void enterAtomicSection(TLParser.AtomicSectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#atomicSection}.
	 * @param ctx the parse tree
	 */
	void exitAtomicSection(TLParser.AtomicSectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#load}.
	 * @param ctx the parse tree
	 */
	void enterLoad(TLParser.LoadContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#load}.
	 * @param ctx the parse tree
	 */
	void exitLoad(TLParser.LoadContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#store}.
	 * @param ctx the parse tree
	 */
	void enterStore(TLParser.StoreContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#store}.
	 * @param ctx the parse tree
	 */
	void exitStore(TLParser.StoreContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#flush}.
	 * @param ctx the parse tree
	 */
	void enterFlush(TLParser.FlushContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#flush}.
	 * @param ctx the parse tree
	 */
	void exitFlush(TLParser.FlushContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#put}.
	 * @param ctx the parse tree
	 */
	void enterPut(TLParser.PutContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#put}.
	 * @param ctx the parse tree
	 */
	void exitPut(TLParser.PutContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#get}.
	 * @param ctx the parse tree
	 */
	void enterGet(TLParser.GetContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#get}.
	 * @param ctx the parse tree
	 */
	void exitGet(TLParser.GetContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#rga}.
	 * @param ctx the parse tree
	 */
	void enterRga(TLParser.RgaContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#rga}.
	 * @param ctx the parse tree
	 */
	void exitRga(TLParser.RgaContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#cas}.
	 * @param ctx the parse tree
	 */
	void enterCas(TLParser.CasContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#cas}.
	 * @param ctx the parse tree
	 */
	void exitCas(TLParser.CasContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#atomicity}.
	 * @param ctx the parse tree
	 */
	void enterAtomicity(TLParser.AtomicityContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#atomicity}.
	 * @param ctx the parse tree
	 */
	void exitAtomicity(TLParser.AtomicityContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterDecl(TLParser.DeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitDecl(TLParser.DeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#localDecl}.
	 * @param ctx the parse tree
	 */
	void enterLocalDecl(TLParser.LocalDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#localDecl}.
	 * @param ctx the parse tree
	 */
	void exitLocalDecl(TLParser.LocalDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#sharedDecl}.
	 * @param ctx the parse tree
	 */
	void enterSharedDecl(TLParser.SharedDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#sharedDecl}.
	 * @param ctx the parse tree
	 */
	void exitSharedDecl(TLParser.SharedDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#varDeclList}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclList(TLParser.VarDeclListContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#varDeclList}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclList(TLParser.VarDeclListContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#varDecl}.
	 * @param ctx the parse tree
	 */
	void enterVarDecl(TLParser.VarDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#varDecl}.
	 * @param ctx the parse tree
	 */
	void exitVarDecl(TLParser.VarDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(TLParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(TLParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assertAlways}
	 * labeled alternative in {@link TLParser#assertion}.
	 * @param ctx the parse tree
	 */
	void enterAssertAlways(TLParser.AssertAlwaysContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assertAlways}
	 * labeled alternative in {@link TLParser#assertion}.
	 * @param ctx the parse tree
	 */
	void exitAssertAlways(TLParser.AssertAlwaysContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assertFinal}
	 * labeled alternative in {@link TLParser#assertion}.
	 * @param ctx the parse tree
	 */
	void enterAssertFinal(TLParser.AssertFinalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assertFinal}
	 * labeled alternative in {@link TLParser#assertion}.
	 * @param ctx the parse tree
	 */
	void exitAssertFinal(TLParser.AssertFinalContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#assumption}.
	 * @param ctx the parse tree
	 */
	void enterAssumption(TLParser.AssumptionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#assumption}.
	 * @param ctx the parse tree
	 */
	void exitAssumption(TLParser.AssumptionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(TLParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(TLParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(TLParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(TLParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#idList}.
	 * @param ctx the parse tree
	 */
	void enterIdList(TLParser.IdListContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#idList}.
	 * @param ctx the parse tree
	 */
	void exitIdList(TLParser.IdListContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#exprList}.
	 * @param ctx the parse tree
	 */
	void enterExprList(TLParser.ExprListContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#exprList}.
	 * @param ctx the parse tree
	 */
	void exitExprList(TLParser.ExprListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtExpression(TLParser.LtExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtExpression(TLParser.LtExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGtExpression(TLParser.GtExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGtExpression(TLParser.GtExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpression(TLParser.BoolExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpression(TLParser.BoolExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotEqExpression(TLParser.NotEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotEqExpression(TLParser.NotEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNumberExpression(TLParser.NumberExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNumberExpression(TLParser.NumberExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierExpression(TLParser.IdentifierExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierExpression(TLParser.IdentifierExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterModulusExpression(TLParser.ModulusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitModulusExpression(TLParser.ModulusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNotExpression(TLParser.NotExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNotExpression(TLParser.NotExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplyExpression(TLParser.MultiplyExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplyExpression(TLParser.MultiplyExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGtEqExpression(TLParser.GtEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGtEqExpression(TLParser.GtEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterDivideExpression(TLParser.DivideExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitDivideExpression(TLParser.DivideExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpression(TLParser.OrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpression(TLParser.OrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryMinusExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryMinusExpression(TLParser.UnaryMinusExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryMinusExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryMinusExpression(TLParser.UnaryMinusExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqExpression(TLParser.EqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqExpression(TLParser.EqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(TLParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(TLParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code expressionExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpressionExpression(TLParser.ExpressionExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code expressionExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpressionExpression(TLParser.ExpressionExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddExpression(TLParser.AddExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddExpression(TLParser.AddExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSubtractExpression(TLParser.SubtractExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSubtractExpression(TLParser.SubtractExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtEqExpression(TLParser.LtEqExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtEqExpression(TLParser.LtEqExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TLParser#list}.
	 * @param ctx the parse tree
	 */
	void enterList(TLParser.ListContext ctx);
	/**
	 * Exit a parse tree produced by {@link TLParser#list}.
	 * @param ctx the parse tree
	 */
	void exitList(TLParser.ListContext ctx);
}