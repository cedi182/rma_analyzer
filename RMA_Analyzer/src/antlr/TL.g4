grammar TL;

parse
 : program EOF
 ;

block
 : statement*
 ;

program
 : process+ 	#programfile
 | expression		#predicates
 ;

process
 : Process Number decl? statement*
 ;

statement
 : assignment ';'
 | ifStatement
 | whileStatement
 | put ';'
 | get ';'
 | rga ';'
 | cas ';'
 | flush ';'
 | load ';'
 | store ';'
 | assumption ';'
 | assertion ';'
 | atomicSection
 ;

atomicSection
 : BeginAtomic ';' statement* EndAtomic ';'
 ;

load
 : Load '(' atomicity ')' Identifier '=' Identifier
 ;

store
 : Store '(' atomicity ')' Identifier '=' expression
 ;

flush
 : Flush '(' Number ')'
 ;

put
 : Put '(' atomicity ',' atomicity ')' '(' Identifier ',' Number ',' Identifier ')'
 ;

get
 : Identifier '=' Get '(' atomicity ',' atomicity ')' '(' Identifier ',' Number ')'
 ;

rga
 : Identifier '=' Rga '(' atomicity ',' atomicity ',' atomicity ')' '(' Identifier ',' Number ',' Identifier ')'
 ;

cas
 : Identifier '=' Cas '(' atomicity ',' atomicity ',' atomicity ',' atomicity ')' '(' Identifier ',' Number ',' Identifier ',' Identifier ')'
 ;


atomicity
 : Atomic | NAtomic
 ;

decl
 : sharedDecl? localDecl?
 ;

localDecl
 : Local varDeclList ';'
 ;

sharedDecl
 : Shared varDeclList ';'
 ;

varDeclList
 : varDecl (',' varDecl)*
 ;

varDecl
 : Identifier ('=' Number)?
 ;

assignment
 : Identifier '=' expression
 ;

assertion
 : Assert Always '(' expression ')'	#assertAlways
 | Assert Final '(' expression ')'	#assertFinal
 ;

assumption
 : Assume '(' expression ')'
 ;

ifStatement
 : If expression Then block End
 | If expression Then block Else block End
 ;

whileStatement
 : While expression Do block End
 ;

idList
 : Identifier (Comma Identifier)*
 ;

exprList
 : expression (Comma expression)*
 ;


expression
 : Subtract expression                      #unaryMinusExpression
 | Excl expression                          #notExpression
 | expression Multiply  expression          #multiplyExpression
 | expression Divide    expression          #divideExpression
 | expression Modulus   expression          #modulusExpression
 | expression Add       expression          #addExpression
 | expression Subtract  expression          #subtractExpression
 | expression GTEquals  expression          #gtEqExpression
 | expression LTEquals  expression          #ltEqExpression
 | expression GT        expression          #gtExpression
 | expression LT        expression          #ltExpression
 | expression Equals    expression          #eqExpression
 | expression NEquals   expression          #notEqExpression
 | expression And       expression          #andExpression
 | expression Or        expression          #orExpression
 | Number                                   #numberExpression
 | Bool                                     #boolExpression

 | Identifier                               #identifierExpression
 | '(' expression ')'                       #expressionExpression
 ;

list
 : '[' exprList? ']'
 ;

BeginAtomic : 'begin_atomic';
EndAtomic : 'end_atomic';
Load     : 'load';
Store    : 'store';
Put      : 'put' ;
Get      : 'get' ;
Rga      : 'rga' ;
Cas      : 'cas' ;
Flush    : 'flush' ;
Shared   : 'shared' ;
Local    : 'local' ;
Process  : 'process';
Assert   : 'assert';
Always	 : 'always';
Final	 : 'final';
Assume   : 'assume';
Atomic   : 'atm';
NAtomic  : 'natm';

Input    : 'input';
Def      : 'def';
If       : 'if';
Else     : 'else';
While    : 'while';
To       : 'to';
Then     : 'then';
Do       : 'do';
End      : 'end';
In       : 'in';
Null     : 'null';

Or       : '||';
And      : '&&';
Equals   : '==';
NEquals  : '!=';
GTEquals : '>=';
LTEquals : '<=';
Excl     : '!';
GT       : '>';
LT       : '<';
Add      : '+';
Subtract : '-';
Multiply : '*';
Divide   : '/';
Modulus  : '%';
OBrace   : '{';
CBrace   : '}';
OBracket : '[';
CBracket : ']';
OParen   : '(';
CParen   : ')';
SColon   : ';';
Assign   : '=';
Comma    : ',';
QMark    : '?';
Colon    : ':';

Bool
 : 'true' 
 | 'false'
 ;

Number
 : Int ('.' Digit*)?
 ;

Identifier
 : [a-zA-Z_] [a-zA-Z_0-9]*
 ;

String
 : ["] (~["\r\n] | '\\\\' | '\\"')* ["]
 | ['] (~['\r\n] | '\\\\' | '\\\'')* [']
 ;

Comment
 : ('//' ~[\r\n]* | '/*' .*? '*/') -> skip
 ;

Space
 : [ \t\r\n\u000C] -> skip
 ;

fragment Int
 : [1-9] Digit*
 | '0'
 ;
  
fragment Digit 
 : [0-9]
 ;

