package antlr;

// Generated from TL.g4 by ANTLR 4.5.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link TLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface TLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link TLParser#parse}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParse(TLParser.ParseContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(TLParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code programfile}
	 * labeled alternative in {@link TLParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgramfile(TLParser.ProgramfileContext ctx);
	/**
	 * Visit a parse tree produced by the {@code predicates}
	 * labeled alternative in {@link TLParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPredicates(TLParser.PredicatesContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#process}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProcess(TLParser.ProcessContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(TLParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#atomicSection}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomicSection(TLParser.AtomicSectionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#load}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoad(TLParser.LoadContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#store}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStore(TLParser.StoreContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#flush}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFlush(TLParser.FlushContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#put}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPut(TLParser.PutContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#get}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGet(TLParser.GetContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#rga}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRga(TLParser.RgaContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#cas}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCas(TLParser.CasContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#atomicity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAtomicity(TLParser.AtomicityContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDecl(TLParser.DeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#localDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLocalDecl(TLParser.LocalDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#sharedDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSharedDecl(TLParser.SharedDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#varDeclList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDeclList(TLParser.VarDeclListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#varDecl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVarDecl(TLParser.VarDeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#assignment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment(TLParser.AssignmentContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assertAlways}
	 * labeled alternative in {@link TLParser#assertion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssertAlways(TLParser.AssertAlwaysContext ctx);
	/**
	 * Visit a parse tree produced by the {@code assertFinal}
	 * labeled alternative in {@link TLParser#assertion}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssertFinal(TLParser.AssertFinalContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#assumption}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssumption(TLParser.AssumptionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#ifStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfStatement(TLParser.IfStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#whileStatement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileStatement(TLParser.WhileStatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#idList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdList(TLParser.IdListContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#exprList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExprList(TLParser.ExprListContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ltExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLtExpression(TLParser.LtExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code gtExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGtExpression(TLParser.GtExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpression(TLParser.BoolExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotEqExpression(TLParser.NotEqExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numberExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumberExpression(TLParser.NumberExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code identifierExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifierExpression(TLParser.IdentifierExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code modulusExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModulusExpression(TLParser.ModulusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotExpression(TLParser.NotExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplyExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplyExpression(TLParser.MultiplyExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code gtEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGtEqExpression(TLParser.GtEqExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code divideExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivideExpression(TLParser.DivideExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code orExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrExpression(TLParser.OrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryMinusExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryMinusExpression(TLParser.UnaryMinusExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code eqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqExpression(TLParser.EqExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpression(TLParser.AndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code expressionExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpressionExpression(TLParser.ExpressionExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code addExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddExpression(TLParser.AddExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code subtractExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSubtractExpression(TLParser.SubtractExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ltEqExpression}
	 * labeled alternative in {@link TLParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLtEqExpression(TLParser.LtEqExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link TLParser#list}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitList(TLParser.ListContext ctx);
}